const request = require('request-promise');


async function post(requestOptions) {

    return await request(requestOptions);
}

module.exports.post = post;