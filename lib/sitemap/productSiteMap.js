const con = require('../db.js');
const fs = require('fs');
const path = require('path');
const {createDirIfNotExists} = require('./../utility.js');
const xml2js = require('xml2js');


const generateXML = require('./generateXml.js');

async function generate(req){
    let db = await con.db();
    let products = await db.collection('product').aggregate([
        {$project:{
            _id:1,
            url:1,
            added:1,
            updated:1,
            serial:1
        }},
        {$sort:{serial:1, primaryCategory:1, category:1, _id:1}}

    ]).toArray();

    generateXML(req, products, 'product');

}

module.exports.generate = generate;