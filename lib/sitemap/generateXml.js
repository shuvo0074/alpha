const con = require('../db.js');
const fs = require('fs');
const path = require('path');
const {createDirIfNotExists} = require('./../utility.js');
const xml2js = require('xml2js');


function generateXML(req, list, type){
    

    let xmlList = {}; //list of xml files

    //multiple xml files in case product >50,000
    let numberOfFiles = Math.ceil(list.length / 50000);
    for(let i = 0; i < numberOfFiles; i++){

        xmlList["siteMap-" + type + "-"+ (i + 1)] = {
            "urlset":{
                "$":{
                    "xmlns": "http://www.sitemaps.org/schemas/sitemap/0.9"
                },
                
                "url":[]
            }
        }
    }
        

    let domain = req.hostOptions.domain;

    //remove trailing slash, product url has leading slash
    domain = domain.split('')
    if(domain[domain.length-1] == '/'){
        domain.length = domain.length-1;
    }
    domain = domain.join('');

    list.forEach((item, index)=>{
        let fileNumber = Math.ceil(index / 50000);

        fileNumber = fileNumber == 0 ? 1: fileNumber; // must be at least 1,

        let url = domain+item.url;
        let lastMod = item.updated? item.updated : item.added;
        
        let node = {
            "loc": url,
            "lastmod": lastMod
        }

        xmlList["siteMap-" + type + "-"+ fileNumber ]["urlset"]["url"].push(node);


    });
    let xmlDir = path.join( req.publicDir, 'xml');
    createDirIfNotExists(xmlDir);

    let builder = new xml2js.Builder;

    let xmlInfo = [];

    let fileNames = Object.keys(xmlList); // 

    for(let file in xmlList){

        let xml = builder.buildObject(xmlList[file]);

        let filePath = path.join(xmlDir, file + '.xml');

        //write in file
        fs.writeFile(filePath, xml);

        let info = {
            name: file + '.xml',
            path: filePath,

        }
        if(fileNames.indexOf(file) == fileNames.length) info.active = true;

    }
    return ;
}


module.exports = generateXML;