const con = require('../db.js');
const fs = require('fs');
const xml2js = require('xml2js');

const path = require('path');
const {createDirIfNotExists} = require('./../utility.js');


async function generate(req){

    let db = await con.db();
    let brands = await db.collection('brand').aggregate([

        {$project:{ _id:1, url:1, added:1, updated:1, serial:1}},
        {$sort:{serial:1, name:1, _id:1}}

    ]).toArray();

    let xmlList = {}; //list of xml files

    //multiple xml files in case brand >50,000
    let numberOfFiles = Math.ceil(brands.length / 50000);
    for(let i = 0; i < numberOfFiles; i++){
        
        xmlList["siteMap-brand-"+ (i + 1)] = {
            "urlset":{
                "$":{
                    "xmlns": "http://www.sitemaps.org/schemas/sitemap/0.9"
                },
                
                "url":[]
            }
        }
    }
        

    let domain = req.hostOptions.domain;

    //remove trailing slash, brand url has leading slash
    domain = domain.split('')
    if(domain[domain.length-1] == '/'){
        domain.length = domain.length-1;
    }
    domain = domain.join('');

    brands.forEach((item, index)=>{
        let fileNumber = Math.ceil(index / 50000);

        fileNumber = fileNumber == 0 ? 1: fileNumber; // must be at least 1,

        let url = domain+item.url;
        let lastMod = item.updated? item.updated : item.added;
        
        let node = {
            "loc": url,
            "lastmod": lastMod
        }

        xmlList["siteMap-brand-"+ fileNumber ]["urlset"]["url"].push(node);

    });


    let xmlDir = path.join( req.publicDir, 'xml');
    createDirIfNotExists(xmlDir);

    let siteMapInformation = [];

    let builder = new xml2js.Builder;


    for(let file in xmlList){

        let xml = builder.buildObject(xmlList[file]);

        //write in file
        fs.writeFile(path.join(xmlDir, file + '.xml'), xml);

        //store information in database
        
    }

    return ;
}

module.exports.generate = generate;