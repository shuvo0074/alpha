const domainMap = require('./../config/domainMap.json');
const con = require('./db.js');
const path = require('path');
const customStatic = require('./customStatic.js');

/**
 * set db name, req.publicDir, themePath depending on domain and hostOptions on req.hostOptions
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */

function setHostOptions(req, res, next) {
  let hostName = req.headers.host;
  req.hostName = hostName = hostName.replace('www.', ''); //domainMaps are set in non-www format
  
  let options = domainMap[hostName];
  
  if(!options) options = domainMap["*"];

  if (!options) {
    //TODO: replace with parking page
    console.log('missing : ' + hostName);
    return res.end(`
        <h1 align='center'>404 : Host Not Found</h1> 
        `);
  }

  //save mapping based options for later use in another middleware
  req.userDir = path.join(__dirname, './../', 'user', options.publicDir);

  req.publicDir = path.join(req.userDir, 'public');

  req.hostOptions = options;

  //set dbName in mongoDb connection instance
  con.dbName = req.hostOptions.db;
  req.themePath = req.hostOptions.theme;

  next();
}

/**
 *set  assetDirectory  directory differently for each domain
 *and  assetDirectory, publicDir in req
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
function setHostDirectories(req, res, next) {
  req.assetDirectory = path.join(req.userDir, req.hostOptions.theme, 'assets');
  customStatic.root = req.publicDir;
  customStatic.asset.root = req.assetDirectory;

  next();
}

module.exports.setHostDirectories = setHostDirectories;
module.exports.setHostOptions = setHostOptions;
