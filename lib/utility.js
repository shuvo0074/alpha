const fs = require('fs');


function removeTrailingSlashes(str) {
    console.log()
    return removeTrailingSlash(removeDuplicateSlashes(str));
}
module.exports.removeTrailingSlashes = removeTrailingSlashes;


function removeTrailingSlash(str) {

    str = str.split('');

    if (str[str.length - 1] == '/') str.length = str.length - 1

    return str.join('');
}
module.exports.removeTrailingSlash = removeTrailingSlash;

function removeDuplicateSlashes(str) {
    return str.replace(/\/+/g, '/');
}
module.exports.removeDuplicateSlashes = removeDuplicateSlashes;

function createDirIfNotExists(dir) {
    if (!fs.existsSync(dir)) fs.mkdir(dir, { recursive: true });
}

module.exports.createDirIfNotExists = createDirIfNotExists;

function getFormattedDate(date) {
    date = date ? new Date(date) : new Date();

    let month = date.getMonth() + 1;
    month = month < 10 ? '0' + month : month; //mm

    let day = date.getDate();
    day = day < 10 ? '0' + day : day; //dd

    date = date.getFullYear() + '-' + month + '-' + day; //yyyy-mm-dd
    return date;
}
module.exports.getFormattedDate = getFormattedDate;

function convertParams(param) {
    if (param === 'true' || param === 1 || param === '1' || param === true) {
        return true;
    } else if (param === 'false' || param === -1 || param === '0' || param === false || param === undefined) {
        return false
    }
}

module.exports.convertParams = convertParams;


/**
 * generates random string of given length
 * @param {Number} length of random string, default 6
 * @param {String} mode Binary representation of mode numeric, alphaCapital, alphaSmall, default 111 (all) 
 */
function generateRandomString(length = 6, mode = "111") {


    // let numericStart = 48;
    // let numericEnd = 57;

    // let alphaCapitalStart = 65;
    // let alphaCapitalEnd = 90;

    // let alphaSmallStart = 97;
    // let alphaSmallEnd = 122;


    mode = "" + mode;

    let alphaCapital = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let alphaSmall = "abcdefghijklmnopqrstuvwxyz";
    let numeric = "0123456789"

    let selectString = "";
    if (+mode[0]) selectString += numeric; //add numbers
    if (+mode[1]) selectString += alphaCapital; //add cap letters
    if (+mode[2]) selectString += alphaSmall;


    let returnString = "";
    for (let i = 0; i < length; i++) {

        let random = randomIntInRange(0, selectString.length-1);

        returnString += selectString[random];
    }


    return returnString;

}
module.exports.generateRandomString = generateRandomString;


/**
 * Generate random int within range
 * @param {Number} min minimum range 
 * @param {Number} max maximum range
 */

function randomIntInRange(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
module.exports.randomIntInRange = randomIntInRange;