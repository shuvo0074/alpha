const session = require('./session.js');
const adminAuth = require('./auth/adminAuth.js');
const { addListener } = require('../app.js');
const uuid = require('uuid');


const socketList = {};

function socketHandler(io) {
    io.use(function (socket, next) {
        session(socket.request, socket.request.res || {}, next);
    });

    io.on('connection', (socket) => {
        //check connection type
        let connectionType = socket.handshake.query.client || 'user';
        
        if (connectionType == 'admin') { //authenticate
            // io.use(function (socket, next) {
            //     adminAuth.checkSocketAuth(socket, next);
            // });
        }

        socket.broadcast.emit('userConnect', `${socket.id} connected`);

        addListeners(socket)
    });
}

function addListeners(socket) {
    //text message
    socket.on('text', message => handleText(message, socket));

}

function handleText(message, socket) {
    console.log(socket.id + " : " + message);
}


module.exports.socketHandler = socketHandler; 