const passport = require("passport");

// function checkAuth(req, res, next){
//     if(req.session.customer && req.session.customer._id && req.session.customer.domain == req.hostOptions.domain){
//         next()
//     }else{
//         res.status(403).json({'403':'access forbidden'});
//     }

// }




function checkoutAuth(req, res, next) {
    if (passport.authenticate("jwt", { session: false })) {
        next();
    } else {
        return null;
    }
}
module.exports = checkoutAuth;