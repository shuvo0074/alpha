
// let roleMap = { 
//     'getCatalogue': [], 'postCatalogue', 
// 'getDelivery', 'postDelivery', 
// 'getOrder', 'postOrder', 'orderAnalytics',
// 'getCustomer', 'postCustomer', 
// 'getPost', 'postPost', 
// 'getPage', 'postPage',
// 'analytics', 'superAdmin'
// }

let roleMap = {
    
    get: {
        'admin/api/image': 'allAdmin',
        'admin/api/credentials': 'allAdmin',

        'admin/api/search': 'getCatalogue',
        'admin/api/product': 'getCatalogue',
        'admin/api/brand': 'getCatalogue',
        'admin/api/tags': 'getCatalogue',
        'admin/api/category': 'getCatalogue',
        'admin/api/attribute': 'getCatalogue',
        'admin/api/coupon': 'getCatalogue',
        'admin/api/bundle': 'getCatalogue',
        'admin/api/offer': 'getCatalogue',

        'admin/api/component': 'superAdmin',
        'admin/api/settings': 'superAdmin',
        'admin/api/theme': 'superAdmin',
        'admin/api/email': 'superAdmin',
        'admin/api/sms': 'superAdmin',
        'admin/api/adminRoles': 'superAdmin',

        'admin/api/dealer': 'getDealer',

        'admin/api/analytics': 'analytics',

        'admin/api/page': 'getBlog',
        'admin/api/post': 'getBlog',

        'admin/api/delivery': 'getDelivery',

        'admin/api/order': 'getOrder',
        'admin/api/notification': 'getOrder',
        'admin/api/customer': 'getOrder',

        'admin/api/staff': 'account',
        'admin/api/expense': 'account',
    },

    post: {
        'admin/api/image': 'allAdmin',
        'admin/api/credentials': 'allAdmin',


        'admin/api/search': 'postCatalogue',
        'admin/api/product': 'postCatalogue',
        'admin/api/brand': 'postCatalogue',
        'admin/api/tags': 'postCatalogue',
        'admin/api/category': 'postCatalogue',
        'admin/api/attribute': 'postCatalogue',
        'admin/api/coupon': 'postCatalogue',
        'admin/api/bundle': 'postCatalogue',
        'admin/api/offer': 'postCatalogue',

        'admin/api/component': 'superAdmin',
        'admin/api/settings': 'superAdmin',
        'admin/api/theme': 'superAdmin',
        'admin/api/email': 'superAdmin',
        'admin/api/sms': 'superAdmin',
        'admin/api/adminRoles': 'superAdmin',

        'admin/api/dealer': 'postDealer',

        'admin/api/analytics': 'analytics',

        'admin/api/page': 'postBlog',
        'admin/api/post': 'postBlog',

        'admin/api/delivery': 'postDelivery',

        'admin/api/order': 'postOrder',
        'admin/api/notification': 'postOrder',
        'admin/api/customer': 'postOrder',

        'admin/api/staff': 'account',
        'admin/api/expense': 'account',
    },

    socket: {
        '/': 'customerSupport'
    }

}


/**
 * check if admin has access to the requested route
 * @param {Array} role roles of admin from session
 * @returns {Boolean}
 */
function checkAccess(url, method, role) {

    if (url == '/admin/' || url == '/admin') return true; //leave auth page

    if (!role) return false;

    // all true for superAdmin
    if (role.includes('superAdmin')) return true;

    // sub admins
    let roleUrl = url.split('/').filter(i => i.length); //['admin', 'api', 'category', 'add']
    roleUrl = roleUrl.splice(0, 3).join('/'); // ['admin/api/category]

    let requiredRole = roleMap[method.toLowerCase()][roleUrl];


    if (requiredRole == 'allAdmin') return true;
    if (role.includes(requiredRole)) return true;
    return false;

}

function checkAuth(req, res, next) {
    if (req.session.admin && req.session.admin._id && req.session.admin.host == req.hostOptions.domain) {
        // console.log('auth in')
        if (!checkAccess(req.originalUrl, req.method, req.session.admin.role)){
            return res.status(403).json({ "Authentication": "You are not authorized to access this information" });

        }else next()

    } else
        res.status(403).redirect('/admin/auth/login');
}


module.exports = checkAuth;

function disconenctSocket(socket, message){
    return socket.disconnect({ status: 403, message });
}

function checkSocketAuth(socket, next) {

    //check authentication
    if (!socket.request.session.admin || !socket.request.session.admin._id) return disconenctSocket(socket, "Unauthorized");

    //check access
    if (checkAccess('/', 'socket', socket.request.session.admin.role)) return next();
    else return disconenctSocket(socket, "Not enough privillege");

}

module.exports.checkSocketAuth = checkSocketAuth;