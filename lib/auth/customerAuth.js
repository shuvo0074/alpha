// const passport = require("passport");

// function checkAuth(req, res, next){
//     if(req.session.customer && req.session.customer._id && req.session.customer.domain == req.hostOptions.domain){
//         next()
//     }else{
//         res.status(403).json({'403':'access forbidden'});
//     }

// }

function isCustomer (req, res, next) {
  if(req.user.userType === 'customer') {
    next();
  } else {
    return res.status(403).json({ "403": "access forbidden" });
  }
}

module.exports = isCustomer;

// function checkAuth(req, res, next) {
//   console.log("inside customer auth middleware");
//   if (passport.authenticate("jwt", { session: false })) {
//     next();
//   } else {
//     res.status(403).json({ "403": "access forbidden" });
//   }
// }
// module.exports = checkAuth;
