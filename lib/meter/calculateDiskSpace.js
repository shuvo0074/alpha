const fs = require('fs');
const path = require('path');
const du = require('du');


async function calculateDiskSpace(req) {
  try {
    if(typeof req != 'object') throw new TypeError('req must be an object, received type '+ typeof req);

    let limit = req.hostOptions.package.disk;

    if (fs.existsSync(req.userDir)) {
      let fileSizeInBytes = await du(req.userDir);
      let fileSizeInKB = fileSizeInBytes / 1024;
      let fileSizeInMegabytes = parseFloat((fileSizeInKB / 1024).toFixed(2));

      const diskUsage = {
        available: parseInt((limit - fileSizeInMegabytes).toFixed(2)),
        used: fileSizeInMegabytes,
        total: limit
      };
      return diskUsage;
    } else {
      return false;
    }
  } catch (err) {
    console.log(err);
    return false;
  }
}

// getDiskinfo =>  {req} => {abSpace,usedSpace,totalSpace} => for each package feild.
// check resourc avab

/* 

      "package": {
      "name": "basic",
      "diskLimit": "100MB",
      "dbLimit": "100MB",
      "productLimit": "100"
    }




*/

function getDiskInfo(req) {
  return {
    disk: {
      availableSpace: 0,
      usedSpace: 0,
      totalSpace: '100MB'
    },
    db: {
      availableSpace: 0,
      usedSpace: 0,
      totalSpace: 0
    },
    product: {
      availableSpace: 0,
      usedSpace: 0,
      totalSpace: 0
    }
  };
}

module.exports = calculateDiskSpace;
