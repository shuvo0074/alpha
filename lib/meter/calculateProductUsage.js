const con = require('../db.js');


async function calculateProductUsage(req) {
  try {
    if(typeof req != 'object') throw new TypeError('req must be an object, received type '+ typeof req);
    
    let limit = req.hostOptions.package.disk;

    const db = await con.db();
    const productCount = await db
      .collection('product')
      .find({})
      .count();

    const productUsage = {
      available: limit - productCount,
      used: productCount,
      total: limit
    };
    return productUsage;
  } catch (err) {
    console.log(err);
  }
}

module.exports = calculateProductUsage;
