const con = require('../db.js');
const domainMap = require('./../../config/domainMap.json');


async function calculatePageUsage(req) {
  try {
    if(typeof req != 'object') throw new TypeError('req must be an object, received type '+ typeof req);
    
    let limit = req.hostOptions.package.disk;

    const db = await con.db();
    const pageCount = await db
      .collection('page')
      .find({})
      .count();

    const pageUsage = {
      available: limit - pageCount,
      used: pageCount,
      total: limit
    };
    return pageUsage;
  } catch (err) {
    console.log(err);
  }
}

module.exports = calculatePageUsage;
