const con = require('../db.js');
const domainMap = require('./../../config/domainMap.json');


async function calculateDbUsage(req) {
  try {
    if(typeof req != 'object') throw new TypeError('req must be an object, received type '+ typeof req);
    let limit = req.hostOptions.package.disk;

    const db = await con.db();
    const dbStats = await db.stats();
    const storageSizeInBytes = dbStats.storageSize;
    let storageSizeInKB = storageSizeInBytes / 1024;
    let storageSizeInMB = parseFloat((storageSizeInKB / 1024).toFixed(2));

    const dbUsage = {
      available: parseFloat(parseInt((limit - storageSizeInMB).toFixed(2))),
      used: storageSizeInMB,
      total: limit
    };
    return dbUsage;
  } catch (err) {
    console.log(err);
  }
}

module.exports = calculateDbUsage;
