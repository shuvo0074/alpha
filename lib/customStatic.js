/*!
 * customStatic
 * this is a modified version of serve-static 
 * for dynamic public routing
 */

'use strict';

/**
 * Module dependencies.
 * @private
 */

const encodeUrl = require ('encodeurl');
const escapeHtml = require ('escape-html');
const parseUrl = require ('parseurl');
const resolve = require ('path').resolve;
const send = require ('send');
const url = require ('url');

/**
 * Module exports.
 * @public
 */



/**
 * @param {object} [options]
 * @return {function}
 * @public
 */

class ServeStatic {
  constructor (options) {
   
    // copy options object
    this.opts = Object.create (options || null);

    // fall-though
    this.fallthrough = this.opts.fallthrough !== false;

    // default redirect
    this.redirect = this.opts.redirect !== false;

    // headers listener
    this.setHeaders = this.opts.setHeaders;

    if (this.setHeaders && typeof this.setHeaders !== 'function') {
      throw new TypeError ('option setHeaders must be function');
    }

    // setup options for send
    this.opts.maxage = this.opts.maxage || this.opts.maxAge || 0;
    
    // construct directory listener
    this.onDirectory = this.redirect
    ? this.createRedirectDirectoryListener ()
    : this.createNotFoundDirectoryListener ();
    
    this.serveStatic = this.serveStatic.bind(this);
    this.root;
  }

  serveStatic (req, res, next) {

    if (!this.root) {
      throw new TypeError ('set root path first');
    }

    if (typeof this.root !== 'string') {
      throw new TypeError ('root path must be a string');
    }

    this.opts.root = resolve(this.root);

    if (req.method !== 'GET' && req.method !== 'HEAD') {
      if (this.fallthrough) {
        return next ();
      }

      // method not allowed
      res.statusCode = 405;
      res.setHeader ('Allow', 'GET, HEAD');
      res.setHeader ('Content-Length', '0');
      res.end ();
      return;
    }

    var forwardError = !this.fallthrough;
    var originalUrl = parseUrl.original (req);
    var path = parseUrl (req).pathname;

    // make sure redirect occurs at mount
    if (path === '/' && originalUrl.pathname.substr (-1) !== '/') {
      path = '';
    }

    // create send stream
    var stream = send (req, path, this.opts);

    // add directory handler
    stream.on ('directory', this.onDirectory);

    // add headers listener
    if (this.setHeaders) {
      stream.on ('headers', this.setHeaders);
    }

    // add file listener for fallthrough
    if (this.fallthrough) {
      stream.on ('file', function onFile () {
        // once file is determined, always forward error
        forwardError = true;
      });
    }

    // forward errors
    stream.on ('error', function error (err) {
      if (forwardError || !(err.statusCode < 500)) {
        next (err);
        return;
      }

      next ();
    });

    // pipe
    stream.pipe (res);
  }

  /**
   * Collapse all leading slashes into a single slash
   * @private
   */
  collapseLeadingSlashes (str) {
    for (var i = 0; i < str.length; i++) {
      if (str.charCodeAt (i) !== 0x2f /* / */) {
        break;
      }
    }

    return i > 1 ? '/' + str.substr (i) : str;
  }

  /**
   * Create a minimal HTML document.
   *
   * @param {string} title
   * @param {string} body
   * @private
   */

  createHtmlDocument (title, body) {
    return (
      '<!DOCTYPE html>\n' +
      '<html lang="en">\n' +
      '<head>\n' +
      '<meta charset="utf-8">\n' +
      '<title>' +
      title +
      '</title>\n' +
      '</head>\n' +
      '<body>\n' +
      '<pre>' +
      body +
      '</pre>\n' +
      '</body>\n' +
      '</html>\n'
    );
  }

  /**
   * Create a directory listener that just 404s.
   * @private
   */

  createNotFoundDirectoryListener () {
    return function notFound () {
      this.error (404);
    };
  }

  /**
 * Create a directory listener that performs a redirect.
 * @private
 */

  createRedirectDirectoryListener () {
    return function redirect (res) {
      if (this.hasTrailingSlash ()) {
        this.error (404);
        return;
      }

      // get original URL
      var originalUrl = parseUrl.original (this.req);

      // append trailing slash
      originalUrl.path = null;
      originalUrl.pathname = this.collapseLeadingSlashes (
        originalUrl.pathname + '/'
      );

      // reformat the URL
      var loc = encodeUrl (url.format (originalUrl));
      var doc = this.createHtmlDocument (
        'Redirecting',
        'Redirecting to <a href="' +
          escapeHtml (loc) +
          '">' +
          escapeHtml (loc) +
          '</a>'
      );

      // send redirect response
      res.statusCode = 301;
      res.setHeader ('Content-Type', 'text/html; charset=UTF-8');
      res.setHeader ('Content-Length', Buffer.byteLength (doc));
      res.setHeader ('Content-Security-Policy', "default-src 'self'");
      res.setHeader ('X-Content-Type-Options', 'nosniff');
      res.setHeader ('Location', loc);
      res.end (doc);
    };
  }
}

module.exports = new ServeStatic();
module.exports.asset = new ServeStatic();
module.exports.mime = send.mime;