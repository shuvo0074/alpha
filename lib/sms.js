const con = require('./db.js');
const request = require('request-promise');
const { promisify } = require('util');
const queryString = require('querystring');

async function sendSms(options, cb) {

	try {
		let { recipient, text, event } = options;
		let db = await con.db();

		let settings = await db.collection('settings').findOne({ name: 'sms' })
		if (!settings) {
			throw new Error('Sms Settings not found, configure sms settings first');
		}

		// format recipient list
		if (!Array.isArray(recipient)) recipient = recipient.split(',');
		if (!settings.parameters.recipient.arraySupport) {
			recipient = recipient.join(settings.parameters.recipient.separator);
		}

		let sms = {
			[settings.parameters.recipient.key]: recipient,
			[settings.parameters.text.key]: text,
			[settings.parameters.mask.key]: settings.parameters.mask.value
		}

		if (settings.authentication) {
			sms[settings.parameters.authentication.username.key] = settings.parameters.authentication.username.value
			sms[settings.parameters.authentication.password.key] = settings.parameters.authentication.password.value
		}

		for (let key in settings.parameters.additional) {
			sms[key] = settings.parameters.additional[key]
		}

		let method = settings.method;

		let requestOptions = {
			uri: settings.url,
		}

		if (method == "GET") {
			requestOptions.method = "GET";
			requestOptions.qs = sms

			if (settings.authHeader) {
				requestOptions.headers = {}
				requestOptions.headers.Authorization = settings.authHeader
			}

		} else {
			requestOptions.method = "POST";

			if (settings.contentType && settings.contentType.toLowerCase() == 'json') {
				requestOptions.body = sms
			} else {
				requestOptions.form = sms
			}

		}


		//save history
		db.collection('sentSMS').insertOne({
			recipient,
			text,
			time: new Date(),
			event
		});

		
		return await request(requestOptions)

		
	} catch (err) {
		console.log(err);
	}

}

module.exports = {
	sendSms: sendSms
}