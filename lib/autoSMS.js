const nodeMailer = require('nodemailer');
const con = require('./db.js');
const smsSender = require('./sms.js');
const ejs = require('ejs');
const he = require('he'); //encode/ decode html entity


async function eventSMS(event, recipient, opts) {
    const db = await con.db();

    //check if autoSMS for this event is enabled
    let autoConfig = await db.collection('settings').findOne({ name: 'autoSMS' });
    if (!autoConfig) {
        console.log("missing settings")
        return false;
    }

    //get SMS template
    let template = await db.collection('SMSTemplates').findOne({ event: event });


    let ejsOpts = { ...opts, delimiter: '?' }

    if (autoConfig[event].user &&  template.user) {

        template.user.body = ejs.render(template.user.body || "", ejsOpts);


        //user SMS
        let SMS = {
            recipient,
            text: template.user.body,
            event: event,
        }

        await smsSender.sendSms(SMS);
        console.log(`[SMS sent] :  event => ${SMS.event}, recipient => ${SMS.recipient}`)

    }

    if (autoConfig[event].admin && template.admin) {


        //get admin SMS
        let siteInfo = await db.collection('settings').findOne({ name: 'site' });
        if (!siteInfo) return false;

        
        //check for valid number
        if(!isFinite(siteInfo.adminPhone.replace("+", ""))) return false;

        template.admin.body = ejs.render(template.admin.body || "", ejsOpts);


        //admin SMS
        let SMS = {
            recipient: siteInfo.adminPhone,
            text: template.admin.body,
            event: event,
        }

        await smsSender.sendSms(SMS);
        console.log(`[SMS sent] :  event => ${SMS.event}, recipient => ${SMS.recipient}`)

    }

}

module.exports = {
    eventSMS
}