const categoryFunctions = require('./public/categoryFunctions');
const productFunctions = require('./public/productFunctions');
const brandFunctions = require('./public/brandFunctions');
const attributeFunctions = require('./public/attributeFunctions');
const componentFunctions = require('./public/componentFunctions');
const searchFunctions = require('./public/searchFunctions');
const tagFunctions = require('./public/tagFunctions');
const geo = require('./public/geoFunctions');
const page = require('./public/pageFunctions');

module.exports = {
    ...categoryFunctions,
    ...productFunctions,   
    ... brandFunctions,
    ...attributeFunctions,
    ...componentFunctions,
    ...searchFunctions,
    ...tagFunctions,
    ...geo,
    ...page,
}
