const con = require('../../db.js');
const paginate = require('../../paginate.js');
const moment = require('moment');

async function getOrderList(customerId, opts){
    if(!customerId || !con.ObjectID.isValid(customerId)) return false;
    let preFacetedStage = [
        {$match: {customer: new con.ObjectID(customerId)}},
    ]
    let aggregationPipeLine= [];
    let orders = await paginate('order', preFacetedStage, aggregationPipeLine, opts);
    return orders;
}
module.exports.getOrderList = getOrderList;

async function getOrder(orderId, opts){
    if(!orderId || !con.ObjectID.isValid(orderId)) return false;
    let db = await con.db();

    let aggregationPipeLine = [
        {$match: {_id: new con.ObjectID(orderId)}},
        {$lookup:{
            from: "product",
            localField: "products._id",
            foreignField: "_id",
            as: "product",
        }},
        {$unwind:{
            path:'$product',
            preserveNullAndEmptyArrays:true,
        }},
        {$lookup:{
            from: "images",
            localField: "product.cover",
            foreignField: "_id",
            as: "product.cover",
        }},
        {$unwind:{
            path:'$product.cover',
            preserveNullAndEmptyArrays:true,
        }},
        {
        $group: {
            _id: '$_id',
            product: { $push: '$product' },
            doc: { $first: '$$ROOT' }
        }
        },
        { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
        { $project: { doc: 0 } },
        {$lookup:{
            from: "customer",
            localField: "customer",
            foreignField: "_id",
            as: "customer",
        }},
        {$unwind:{
            path:'$customer',
            preserveNullAndEmptyArrays:true,
        }},
    ]

    let order =await db.collection('order').aggregate(aggregationPipeLine).toArray();
    order = order[0];
    if(!order) return {};

    //combine product & products
    order.products.forEach((i, index, array)=>{
        let productInfo = order.product.find(item=>item._id.toHexString() == i._id.toHexString())
        i = { ...productInfo, ...i},
        array[index] = i;
    })

    delete order.product;
    delete order.customer.password;
    return order;
}

module.exports.getOrder = getOrder;