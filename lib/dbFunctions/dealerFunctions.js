const con = require('../db.js');
const paginate = require('../paginate.js');


async function getDealerList(opts) {
	let db = await con.db();

	let aggregatePipeLine = [];

	aggregatePipeLine.push({
		$lookup: {
			from: "dealerArea",
			localField: "area",
			foreignField: "_id",
			as: "area"
		}
	})

	let dealers = await paginate("dealer", [], aggregatePipeLine, opts);
	dealers.data.forEach((i) => {
		delete i.password;
	}); //delete password hash

	return dealers
}

module.exports.getDealerList = getDealerList;


async function getDealer(id, opts) {

	let db = await con.db();
	// let dealer = await db.collection("dealer").findOne({ _id: new con.ObjectID(id) });

	let aggregatePipeLine = [];

	aggregatePipeLine.push({
		$match: {
			_id: new con.ObjectID(id)
		}
	});

	aggregatePipeLine.push({
		$lookup: {
			from: "dealerArea",
			localField: "area",
			foreignField: "_id",
			as: "area"
		}
	})

	let dealer = await db.collection("dealer").aggregate(aggregatePipeLine).toArray();
	dealer = dealer[0]

	if (!dealer) return res.status(400).json({ id: "no dealer with given id" });

	delete dealer.password; //delete password hash

	return dealer;
}

module.exports.getDealer = getDealer;



async function getDealerCustomer(dealer, opts) {

	let db = await con.db();
	// let dealer = await db.collection("dealer").findOne({ _id: new con.ObjectID(id) });

	let { startDate, endDate, date } = opts

	let preFacetStage = [];



	preFacetStage.push({
		$match: {
			_id: new con.ObjectID(dealer)
		}
	});

	preFacetStage.push({
		$lookup: {
			from: "customer",
			localField: "_id",
			foreignField: "dealer",
			as: "customer"
		}
	})

	preFacetStage.push({
		$unwind: {
			path: "$customer",
			// preserveNullAndEmptyArrays: true **enabling will give error trying to replace root if order is empty 
		}
	})

	//filters
	if (startDate) {
		preFacetStage.push({
			$match: {
				"customer.added": { $gte: new Date(startDate) }
			}
		})
	}
	if (endDate) {
		preFacetStage.push({
			$match: {
				"customer.added": { $lte: new Date(endDate) }
			}
		})
	}

	preFacetStage.push({
		$project: {
			"customer.password": 0
		}
	})


	preFacetStage.push({
		$replaceRoot: { newRoot: "$customer" }
	})



	let customers = await paginate("dealer", preFacetStage, [], opts);
	// if (!dealer) return res.status(400).json({ id: "no dealer with given id" });

	// let orders = dealer[0] && dealer[0].order;



	return customers;
}

module.exports.getDealerCustomer = getDealerCustomer;



async function getDealerOrders(dealer, opts) {

	let db = await con.db();
	// let dealer = await db.collection("dealer").findOne({ _id: new con.ObjectID(id) });

	let { startDate, endDate, date, status } = opts

	let preFacetStage = [];



	preFacetStage.push({
		$match: {
			dealer: new con.ObjectID(dealer)
		}
	});



	preFacetStage.push({
		$lookup: {
			from: "order",
			localField: "_id",
			foreignField: "customer",
			as: "order"
		}
	})

	preFacetStage.push({
		$unwind: "$order"
	})

	preFacetStage.push({
		$project: {
			orderDetail: "$order",
			customer: "$$ROOT",
			_id: "$orderDetail._id",

		}
	})
	preFacetStage.push({
		$project: {
			"customer.order": 0,
		}
	})
	preFacetStage.push({
		$replaceRoot: { newRoot: { $mergeObjects: ["$orderDetail", "$$ROOT"] } }
	})
	preFacetStage.push({
		$project: {
			"orderDetail": 0
		}
	})

	//filters
	if (startDate) {
		preFacetStage.push({
			$match: {
				"added": { $gte: new Date(startDate) }
			}
		})
	}
	if (endDate) {
		preFacetStage.push({
			$match: {
				"added": { $lte: new Date(endDate) }
			}
		})
	}

	if (status) {
		preFacetStage.push({
			$match: { "status.name": status }
		})
	}



	let orders = await paginate("customer", preFacetStage, [], opts);
	// if (!dealer) return res.status(400).json({ id: "no dealer with given id" });

	// let orders = dealer[0] && dealer[0].order;


	return orders;
}

module.exports.getDealerOrders = getDealerOrders;


async function getDealerOrderListWithCommission(dealerId, opts) {
	// * get orders by dealer
	let orders = await getDealerOrders(dealerId, opts);

	//calculate commission
	if (orders && orders.data && orders.data[0]) {
		let dealerDetail = await getDealer(dealerId, opts);
		orders.data.forEach(order => {
			if(order.status.name == "complete") //only complete orders
				order.commission = order.totalPrice * dealerDetail.commission / 100
		})
	}

	return orders;
}

module.exports.getDealerOrderListWithCommission = getDealerOrderListWithCommission;



async function getDealerOrderDetail(dealerId, orderId, opts) {

	dealerId = new con.ObjectID(dealerId);
	orderId = new con.ObjectID(orderId);

	let db = await con.db();
	let order = await db.collection("order").aggregate([
		{
			$match: {
				_id: new con.ObjectID(orderId)
			}
		},
		{
			$lookup: {
				from: "customer",
				localField: "customer",
				foreignField: "_id",
				as: "customer"
			}
		},
		{
			$match: {
				"customer.dealer": new con.ObjectID(dealerId)
			}
		}

	]).toArray();

	order = order && order[0];
	return order

}

module.exports.getDealerOrderDetail = getDealerOrderDetail;



async function getDealerOrderDetailWithCommission(dealerId, orderId, opts) {
	// * get orders by dealer
	let order = await getDealerOrderDetail(dealerId, orderId, opts);

	//calculate commission
	let dealerDetail = await getDealer(dealerId, opts);

	if(order.status.name == "complete") //only complete orders
		order.commission = order.totalPrice * dealerDetail.commission / 100

	return order;
}

module.exports.getDealerOrderDetailWithCommission = getDealerOrderDetailWithCommission;



async function getAreaDealerList(areaId, opts) {

	areaId = new con.ObjectID(areaId);

	let db = await con.db();
	let dealers = await db.collection("dealer").aggregate([
		{
			$match: {
				area: areaId
			}
		}
	]).toArray();

	return dealers

}

module.exports.getAreaDealerList = getAreaDealerList;






