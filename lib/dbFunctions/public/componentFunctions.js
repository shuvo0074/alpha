const con = require('../../db.js');

/**
 *get component List
 *
 * @param {*} opts
 * @returns {Array} component list 
 * 
 * default opts
 * 
 * ```
 * image:true
 * 
 * ```
 */
async function componentList(opts={}) {
  let {image} = opts;

  let db = await con.db();
  let aggregationPipeLine = [];
  if(image){
    aggregationPipeLine.push(...[
      {$lookup:{
        from: 'images',
        localField: 'image',
        foreignField:'_id',
        as:'image'
      }},
      {$unwind:{
        path:'$items',
        preserveNullAndEmptyArrays:true
      }},
      {$lookup:{
        from: "images",
        localField: "items.image",
        foreignField: "_id",
        as: "items.image",
      }},
      {
        $group: {
          _id: '$_id',
          items: { $push: '$items' },
          doc: { $first: '$$ROOT' }
        }
      },
      { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
      { $project: { doc: 0 } },
    ])
  }
  
  let components = await db.collection('component').aggregate(aggregationPipeLine).toArray();
  return components;
}

module.exports.getComponentList = componentList;

/**
 *get component detail
 *
 * @param {*} id id or name of component
 * @param {*} [opts={}]
 * @returns component 
 */
async function component(id, opts={}) {

  if(!id) return false;

  let {image} = opts;

  let db = await con.db();
  let aggregationPipeLine = [];
  
  if(con.ObjectID.isValid(id)){
    aggregationPipeLine.push(    {$match:{_id: new con.ObjectID(id)}});
  }else{ //search with name
    aggregationPipeLine.push(    {$match:{name:id}});
  }
  
  
  if(image !='false' && image !=false && +image != -1){
    aggregationPipeLine.push(...[
      {$lookup:{
        from: 'images',
        localField: 'image',
        foreignField:'_id',
        as:'image'
      }},
      {$unwind:{
        path:'$items',
        preserveNullAndEmptyArrays:true
      }},
      {$lookup:{
        from: "images",
        localField: "items.image",
        foreignField: "_id",
        as: "items.image",
      }},
      {
        $group: {
          _id: '$_id',
          items: { $push: '$items' },
          doc: { $first: '$$ROOT' }
        }
      },
      { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
      { $project: { doc: 0 } },
    ])
  }
  
  let components = await db.collection('component').aggregate(aggregationPipeLine).toArray();
  return components[0];
}

module.exports.getComponent = component;
