const con = require('../../db.js');
const paginate = require('./../../paginate.js');
const { removeTrailingSlashes } = require('./../../utility.js');
const { post } = require('../../../routes/dealer.js');

/**
 *get category list from db
 *
 * @param {Object} [opts={}]
 * @returns {Object} result with data and pagination info
 *
 *
 * define subCategory, image, limit, page, sort, sortOrder, cover inside opts
 * default values:
 *
 * ```
 * cover = true;
 * limit = 50; //per page
 * page = 1;
 * sort = 'serial' //sort by serial
 * sortOrder = 1; //ASC
 * subCategory = false; //excluded unless specified '1' or 'true'
 * image = false; //excluded unless specified '1' or 'true'
 * type = 'top';  // lists top level category only;
 * parent = false;//excluded unless specified '1' or 'true'
 * productCount =  false;
 * subCategoryCount = false;
 * 
 * ```
 *
 * Example:
 *
 * ```
 * let opts = {
 *  limit:20, //20 per page
 *  page:3, //page 3, (41-60)
 *  sort:'added' //by added date
 *  sortOrder:1; //newest first;
 *  subCategory: true; //include subCategory
 *  image: true //include images
 * }
 * await categoryList(opts);
 *
 * ```
 *
 */
async function categoryList(opts = {}) {

  if(!opts.sort) {
    opts.sort = 'displayOrder';
    if(!opts.sortOrder) opts.sortOrder = 1;
  }


  let db = await con.db();
  let { subCategory, image, productCount, subCategoryCount, parent, cover = 1, type } = opts;

  let aggregatePipeLine = [], preFacetStage = [];

  //match
  if (type == 'sub') {
    preFacetStage.push({ $match: { type: 'sub' } })
  }
  if (!type || type == 'top') {
    preFacetStage.push({ $match: { type: 'top' } })
  }

  //cover 
  if (cover != 'false' && +cover != '-1' && cover != false) {
    aggregatePipeLine.push({
      $lookup: {
        from: "images",
        localField: "cover",
        foreignField: "_id",
        as: "cover",
      }
    });
    aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
  }

  //image
  if (image) {

    //lookup images
    aggregatePipeLine.push({
      $lookup: {
        from: "images",
        localField: "image",
        foreignField: "_id",
        as: "image",
      }
    })
  }

  // subCategory
  if (subCategory) {
    //lookup subcategories
    aggregatePipeLine.push({
      $lookup: {
        from: "category",
        localField: "_id",
        foreignField: "parent",
        as: "subCategory",
      }
    });
    aggregatePipeLine.push({ $unwind: { path: '$subCategory', preserveNullAndEmptyArrays: true } });

    //lookup subcategory cover images
    aggregatePipeLine.push({
      $lookup: {
        from: "images",
        localField: "subCategory.cover",
        foreignField: "_id",
        as: "subCategory.cover",
      }
    });

    aggregatePipeLine.push({ $unwind: { path: '$subCategory.cover', preserveNullAndEmptyArrays: true } });

    //group
    aggregatePipeLine.push({
      $group: {
        _id: '$_id', 
        subCategory:{ $push: "$subCategory" },
        doc: { $first: '$$ROOT' }
      }
    })

    aggregatePipeLine.push({
      $replaceRoot: {
        newRoot: { $mergeObjects: ['$doc', '$$ROOT'] }
      }
    });


    aggregatePipeLine.push({
      $project: { doc: 0 }
    });
  }

  //parent
  if (parent) {
    aggregatePipeLine.push({
      $lookup: {
        from: 'category',
        localField: 'parent',
        foreignField: '_id',
        as: 'parent'
      }
    });
    aggregatePipeLine.push({
      $unwind: { path: '$parent', preserveNullAndEmptyArrays: true }
    });
    aggregatePipeLine.push({
      $lookup: {
        from: 'images',
        localField: 'parent.cover',
        foreignField: '_id',
        as: 'parent.cover'
      }
    });
    aggregatePipeLine.push({
      $unwind: { path: '$parent.cover', preserveNullAndEmptyArrays: true }
    });
  }

  if (productCount) {

    //lookup images
    aggregatePipeLine.push(...[{
      $lookup: {
        from: "product",
        localField: "_id",
        foreignField: "category",
        as: "productCount",
      }
    },
    {
      $project: {
        doc: "$$ROOT",
        productCount: { $size: "$productCount" },
      }
    },

    { $replaceRoot: { newRoot: { $mergeObjects: ["$doc", "$$ROOT"] } } },
    ])
  }
  if (subCategoryCount) {

    //lookup images
    aggregatePipeLine.push(...[{
      $lookup: {
        from: "category",
        localField: "_id",
        foreignField: "parent",
        as: "subCategoryCount",
      }
    },
    {
      $project: {
        doc: "$$ROOT",
        subCategoryCount: { $size: "$subCategoryCount" },
      }
    },

    { $replaceRoot: { newRoot: { $mergeObjects: ["$doc", "$$ROOT"] } } },
    ])
  }
  let categories = await paginate('category', preFacetStage, aggregatePipeLine, opts)
  return categories;

}
module.exports.getCategoryList = categoryList;

/**
 *get specific category detail
 *
 * @param {String} id id or url of category
 * @param {Object} [opts={}] options to include/exclude
 * @returns {Object} category object
 *
 *
 * Default values:
 *
 * ```
 * cover = true; //included unless specified '-1' or 'false'
 * subCategory = true; //included unless specified '-1' or 'false'
 * image = true; //included unless specified '-1' or 'false'
 * ```
 *
 *
 */
async function category(id, opts = {}) {

  //id validation
  if (!id) {
    return false;
  }
  let db = await con.db();
  let { subCategory, image, cover, productCount, subCategoryCount } = opts;

  let aggregatePipeLine = [];

  if (con.ObjectID.isValid(id) && !id.includes('/')) {
    // console.log(id, 1, id.includes('/'))
    aggregatePipeLine.push({ $match: { _id: new con.ObjectID(id) } });
  } else { //id not valid, try as url
    // console.log(id, 2);
    aggregatePipeLine.push({
      $match: {
        url: decodeURI(removeTrailingSlashes(id))
      }
    });

  }

  //cover
  if (cover != 'false' && +cover != '-1' && cover != false) {
    aggregatePipeLine.push({
      $lookup: {
        from: "images",
        localField: "cover",
        foreignField: "_id",
        as: "cover",
      }
    });
    aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
  }
  if (image != 'false' && +image !== -1 && image != false) {
    //lookup images
    aggregatePipeLine.push({
      $lookup: {
        from: "images",
        localField: "image",
        foreignField: "_id",
        as: "image",
      }
    })
  }

  if (productCount) {

    //lookup images
    aggregatePipeLine.push(...[{
      $lookup: {
        from: "product",
        localField: "_id",
        foreignField: "category",
        as: "productCount",
      }
    },
    {
      $project: {
        doc: "$$ROOT",
        productCount: { $size: "$productCount" },
      }
    },

    { $replaceRoot: { newRoot: { $mergeObjects: ["$doc", "$$ROOT"] } } },
    ])
  }
  if (subCategoryCount) {

    //lookup images
    aggregatePipeLine.push(...[{
      $lookup: {
        from: "category",
        localField: "_id",
        foreignField: "parent",
        as: "subCategoryCount",
      }
    },
    {
      $project: {
        doc: "$$ROOT",
        subCategoryCount: { $size: "$subCategoryCount" },
      }
    },

    { $replaceRoot: { newRoot: { $mergeObjects: ["$doc", "$$ROOT"] } } },
    ])
  }
  if (subCategory != 'false' && +subCategory != -1 && subCategory != false) {
    //lookup subcategories
    aggregatePipeLine.push({
      $lookup: {
        from: "category",
        localField: "_id",
        foreignField: "parent",
        as: "subCategory",
      }
    });
    aggregatePipeLine.push({ $unwind: { path: '$subCategory', preserveNullAndEmptyArrays: true } });

    //lookup subcategory cover images
    aggregatePipeLine.push({
      $lookup: {
        from: "images",
        localField: "subCategory.cover",
        foreignField: "_id",
        as: "subCategory.cover",
      }
    });
    aggregatePipeLine.push({ $unwind: { path: '$subCategory.cover', preserveNullAndEmptyArrays: true } });

    //group
    aggregatePipeLine.push({
      $group: {
        _id: '$_id',
        subCategory:{ $push: "$subCategory" },
        doc: { $first: '$$ROOT' }
      }
    })
    aggregatePipeLine.push({
      $replaceRoot: {
        newRoot: { $mergeObjects: ['$doc', '$$ROOT'] }
      }
    });
    aggregatePipeLine.push({
      $project: { doc: 0 }
    });
  }
  let category = await db.collection('category').aggregate(aggregatePipeLine).toArray();
  return category[0];
}
module.exports.getCategory = category;
/**
 *get category by url
 *
 * @param {String} url url of category
 * @param {Object} opts options
 * @returns {Object} the category from db
 *
 *
 * Default values:
 *
 * ```
 * cover = true; //included unless specified '-1' or 'false'
 * subCategory = true; //included unless specified '-1' or 'false'
 * image = true; //included unless specified '-1' or 'false'
 * ```
 *
 * example:
 * ```
 * await getCategoryByUrl('/category/t-shirt', {subCategory:-1, image:-1})
 *
 * ```
 */
async function categoryByUrl(url, opts = {}) {
  //id validation
  if (!url) {
    return false;
  }
  let db = await con.db();
  let { cover, subCategory, image } = opts;

  let aggregatePipeLine = [
    {
      $match: {
        url: decodeURI(removeTrailingSlashes(url))
      }
    },
  ]

  //cover
  if (cover != 'false' && +cover != '-1' && cover != false) {
    aggregatePipeLine.push({
      $lookup: {
        from: "images",
        localField: "cover",
        foreignField: "_id",
        as: "cover",
      }
    });
    aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
  }

  if (image != 'false' && +image != -1 && image != false) {

    //lookup images
    aggregatePipeLine.push({
      $lookup: {
        from: "images",
        localField: "image",
        foreignField: "_id",
        as: "image",
      }
    })
  }

  //lookup subcategory cover images
  aggregatePipeLine.push({
    $lookup: {
      from: 'images',
      localField: 'subCategory.cover',
      foreignField: '_id',
      as: 'subCategory.cover'
    }
  });
  aggregatePipeLine.push({
    $unwind: { path: '$subCategory.cover', preserveNullAndEmptyArrays: true }
  });

  //group
  aggregatePipeLine.push({
    $group: {
      _id: '$_id',
      doc: { $first: '$$ROOT' }
    }
  })
  aggregatePipeLine.push({
    $replaceRoot: {
      newRoot: { $mergeObjects: ['$doc', '$$ROOT'] }
    }
  });
  aggregatePipeLine.push({
    $project: { doc: 0 }
  });

  let category = await db
    .collection('category')
    .aggregate(aggregatePipeLine)
    .toArray();
  return category;
}
module.exports.getCategoryByUrl = categoryByUrl;

/**
 *get category images
 *
 * @param {String} id category id
 * @returns {Array} array of images
 */

async function categoryImage(id) {
  //id validation
  if (!id) {
    return false;
  }

  let db = await con.db();
  let aggregatePipeLine = [];

  if (con.ObjectID.isValid(id) && !id.includes('/')) {
    aggregatePipeLine.push({ $match: { _id: new con.ObjectID(id) } });
  } else {
    //id not valid, try as url
    aggregatePipeLine.push({
      $match: {
        url: decodeURI(removeTrailingSlashes(id))
      }
    });
  }
  aggregatePipeLine.push({
    $lookup: {
      from: 'images',
      localField: 'image',
      foreignField: '_id',
      as: 'image'
    }
  });
  let category = await db
    .collection('category')
    .aggregate(aggregatePipeLine)
    .toArray();
  if (!category || !category[0]) return false;

  return category[0].image;
}
module.exports.getCategoryImage = categoryImage;

/**
 *get subcategories of a category
 *
 * @param {String} id id or url of category
 * @returns {Array} array of subcategories
 */
async function subCategory(id) {
  //id validation
  if (!id) {
    return false;
  }
  let db = await con.db();
  let aggregatePipeLine = [];

  aggregatePipeLine.push({
    $unwind: { path: '$subCategory', preserveNullAndEmptyArrays: true }
  });

  //lookup subcategory cover images
  aggregatePipeLine.push({
    $lookup: {
      from: 'images',
      localField: 'subCategory.cover',
      foreignField: '_id',
      as: 'subCategory.cover'
    }
  });
  aggregatePipeLine.push({
    $unwind: { path: '$subCategory.cover', preserveNullAndEmptyArrays: true }
  });

  let childCategories = await db
    .collection('category')
    .aggregate(aggregatePipeLine)
    .toArray();
  if (!childCategories || childCategories.length == 0) return false;
  return childCategories;
}
module.exports.getSubCategory = subCategory;

/**
 *get parent category of category
 *
 * @param {String} id id or url of category
 * @returns {Object} parent category
 */
async function categoryParent(id) {
  //id validation
  if (!id) {
    return false;
  }

  let db = await con.db();
  let aggregatePipeLine = [];

  if (con.ObjectID.isValid(id) && !id.includes('/')) {
    aggregatePipeLine.push({ $match: { _id: new con.ObjectID(id) } });
  } else {
    //id not valid, try as url
    aggregatePipeLine.push({
      $match: {
        url: decodeURI(removeTrailingSlashes(id))
      }
    });
  }

  aggregatePipeLine.push({
    $lookup: {
      from: 'category',
      localField: 'parent',
      foreignField: '_id',
      as: 'parent'
    }
  });
  aggregatePipeLine.push({
    $unwind: { path: '$parent', preserveNullAndEmptyArrays: true }
  });
  aggregatePipeLine.push({
    $lookup: {
      from: 'images',
      localField: 'parent.cover',
      foreignField: '_id',
      as: 'parent.cover'
    }
  });
  aggregatePipeLine.push({
    $unwind: { path: '$parent.cover', preserveNullAndEmptyArrays: true }
  });

  let category = await db
    .collection('category')
    .aggregate(aggregatePipeLine)
    .toArray();
  if (!category || !category.parent) return false;

  return category.parent;
}
module.exports.getCategoryParent = categoryParent;

/**
 * get products of category
 *
 * @param {Array || String} id  id/url of category
 * @returns {Array}  product 
 * 
 */
async function categoryProduct(id, opts = {}) {

  if (!id) return false;
  let db = await con.db();
  let { image, cover = 1, brand, category, primaryCategory, recursive } = opts;
  let aggregatePipeLine = [], preFacetStage = [];


  if (con.ObjectID.isValid(id) && !id.includes('/')) { // valid id
    // console.log(id, 1)
    preFacetStage.push({ $match: { _id: new con.ObjectID(id) } });
  } else { //not id, try with url
    preFacetStage.push({ $match: { url: decodeURI(id) } });
  }

  if (recursive) {
    preFacetStage.push(...[
      {
        $graphLookup: {
          from: "category",
          startWith: "$_id",
          connectFromField: "_id",
          connectToField: "parent",
          as: "sub"
        }
      },
      {
        $unwind: {
          path: '$sub',
          preserveNullAndEmptyArrays: true,
        }
      },
      {
        $group: {
          _id: '$_id',
          idArr: { $push: '$sub._id' },
          doc: { $first: '$$ROOT' }
        }
      },
      { $addFields: { lookArray: { $concatArrays: ['$idArr', ['$_id']] } } },

      { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
      { $project: { doc: 0 } },
      {
        $lookup: {
          from: 'product',
          localField: 'lookArray',
          foreignField: 'category',
          as: 'product'
        }
      },
      {
        $unwind: {
          path: '$product',
        }
      },
      { $replaceRoot: { newRoot: { $ifNull: ["$product", {}] } } },

    ])

  } else {
    preFacetStage.push(...[
      { $project: { _id: 1 } },
      {
        $lookup: {
          from: 'product',
          localField: '_id',
          foreignField: 'category',
          as: 'product'
        }
      },
      {
        $unwind: {
          path: '$product',
        }
      },
      { $replaceRoot: { newRoot: { $ifNull: ["$product", {}] } } },
    ])
  }

  //cover
  if (cover != 'false' && +cover != -1 && cover != false) {
    aggregatePipeLine.push({
      $lookup: {
        from: "images",
        localField: "cover",
        foreignField: "_id",
        as: "cover",
      }
    });
    aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
  }

  //image
  if (image) {

    //lookup images
    aggregatePipeLine.push({
      $lookup: {
        from: "images",
        localField: "image",
        foreignField: "_id",
        as: "image",
      }
    })
  }

  //brand
  if (brand) {
    aggregatePipeLine.push({
      $lookup: {
        from: "brand",
        localField: "brand",
        foreignField: "_id",
        as: "brand",
      }
    })
    aggregatePipeLine.push({ $unwind: { path: '$brand', preserveNullAndEmptyArrays: true } });
    aggregatePipeLine.push({
      $lookup: {
        from: "images",
        localField: "brand.cover",
        foreignField: "_id",
        as: "brand.cover",
      }
    })
    aggregatePipeLine.push({ $unwind: { path: '$brand.cover', preserveNullAndEmptyArrays: true } });

  }

  //primaryCategory
  if (primaryCategory) {
    aggregatePipeLine.push({
      $lookup: {
        from: "category",
        localField: "primaryCategory",
        foreignField: "_id",
        as: "primaryCategory",
      }
    })
    aggregatePipeLine.push({ $unwind: { path: '$primaryCategory', preserveNullAndEmptyArrays: true } });
    aggregatePipeLine.push({
      $lookup: {
        from: "images",
        localField: "primaryCategory.cover",
        foreignField: "_id",
        as: "primaryCategory.cover",
      }
    })
    aggregatePipeLine.push({ $unwind: { path: '$primaryCategory.cover', preserveNullAndEmptyArrays: true } });

  }

  if (category) {
    aggregatePipeLine.push(...[
      {
        $lookup: {
          from: 'category',
          localField: 'category',
          foreignField: '_id',
          as: 'category'
        }
      },
      {
        $unwind: {
          path: '$category',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: 'images',
          localField: 'category.cover',
          foreignField: '_id',
          as: 'category.cover'
        }
      },
      {
        $unwind: {
          path: '$category.cover',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $group: {
          _id: '$_id',
          category: { $push: '$category' },
          doc: { $first: '$$ROOT' }
        }
      },
      { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
      { $project: { doc: 0 } },
    ])
  }
  let products = await paginate('category', preFacetStage, aggregatePipeLine, opts);

  return products;


}

module.exports.getCategoryProduct = categoryProduct;
