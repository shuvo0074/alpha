const con = require('../../db.js');

/**
 * retrieves productList from database
 *
 *
 * @param {Object} options
 * @returns {Array}
 *
 *
 */

async function attributeList(options = {}) {
  let db = await con.db();
  let limit = options.limit || 50; //load last 50 by default
  let pageNumber = options.page || 1; //first page default
  let attributes = await db
    .collection('attributes')
    .find({})
    .toArray();

  return attributes;
}

module.exports.getAttributeList = attributeList;

/**
 * retrieves product details
 *
 * @param {String} id
 * @returns {Object}
 * @public
 */

async function attribute(id) {
  //id validation
  if (!id || !con.ObjectID.isValid(id)) {
    throw new Error('invalid attribute Id');
  }

  let db = await con.db();
  let attribute = await db
    .collection('attributes')
    .find({ _id: new con.ObjectID(id) })
    .toArray();

  attribute = attribute[0];
  if (!attribute) return false;
  return attribute;
}
module.exports.getAttribute = attribute;

async function attributeProducts(id) {
  //id validation
  if (!id || !con.ObjectID.isValid(id)) {
    return false;
  }

  let db = await con.db();

  let attribute = await db
    .collection('attributes')
    .findOne({ _id: new con.ObjectID(id) });
  if (!attribute) return false;

  let products = await db
    .collection('product')
    .find({ attributes: new con.ObjectID(id) })
    .project({
      category: 0,
      attributes: 0,
      attributes: 0,
      image: 0,
      brand: 0
    })
    .toArray();

  //convert cover image ids to image objects
  for (let product of products) {
    if (!product.cover || !con.ObjectID.isValid(product.cover)) continue;

    let coverImage = await db
      .collection('images')
      .findOne({ _id: product.cover });
    product.cover = coverImage || {};
  }

  return products;
}
module.exports.getAttributeProducts = attributeProducts;
