const _ = require('lodash');
const con = require('../../db.js');
const paginate = require('./../../paginate.js');
const { removeTrailingSlashes, convertParams } = require('./../../utility.js');


/**
 * get productList from database
 *
 * @param {Object} opts options to include or exclude fields
 * @returns {Array} products
 * 
 * **Default Values:**
 * 
 * ```
 * images = false; // excluded unless specified true
 * limit = 50;  // 50 products per page
 * page = 1; //first page
 * sort = 'serial' 
 * sortOrder = 1; //ASC
 * cover = 1; //included unless specified -1 or false
 * brand = false; // excluded unless specified true
 * category = false; //excluded unless specified true
 * primaryCategory = false; //excluded unless specified true
 * ```
 * 
 * Example:
 * 
 * ```
 * await dbFunctions.getProductList({
 *       limit:2,
 *       page:2,
 *       image:'true',
 *       cover:true,
 *       brand:true,
 *       category:true,
 *       primaryCategory:true,
 *   });
 * 
 * ```
 * 
 */


function convertToOID(id){

    if(Array.isArray(id)){
        id.forEach((i, index)=>{
            id[index] = new con.ObjectID(i);
        })
        
    }else{
        id = new con.ObjectID(id);
    }
    return id;
}



function addFilters(filter){
    let filterPipeLine = [];
    let {category, brand, tags, lowestPrice, highestPrice, type} = filter;


    if(category && Array.isArray(category)){
        
        filterPipeLine.push({
            $match: {"category": {$in: convertToOID(category)}}
        })
    }

    if(brand && Array.isArray(brand)){
        
        filterPipeLine.push({
            $match: {"brand": {$in: convertToOID(brand)}}
        })
    }

    if(tags && Array.isArray(tags)){
        
        filterPipeLine.push({
            $match: {"tags": {$in: convertToOID(tags)}}
        })
    }

    if(lowestPrice && isFinite(+lowestPrice)){
        filterPipeLine.push({
            $match: {"price.regular": {$gte: lowestPrice}}
        })
    }
    if(highestPrice && isFinite(+highestPrice)){
        filterPipeLine.push({
            $match: {"price.regular": {$lte: highestPrice}}
        })
    }
    console.log(JSON.stringify(filterPipeLine));
    return filterPipeLine;
}

function addResolver(resolve){
    let resolvePipeLine = [];
    let {category, brand, tags, image, categoryCover} = resolve;


    if(category ){
        resolvePipeLine.push({
            $lookup:{
                from: 'category',
                foreignField: '_id',
                localField: 'category',
                as: 'category'
            }
        })
    }

    if(category && categoryCover){ 
        resolvePipeLine.push(...[
            {
                $unwind:{
                    path: "$category",
                    preserveNullAndEmptyArrays: true,
                }
                
            },
            {
                $lookup:{
                    from: 'images',
                    foreignField: '_id',
                    localField: 'category.cover',
                    as: 'category.cover'
                }
            },
            {
                $unwind:{
                    path: "$category.cover",
                    preserveNullAndEmptyArrays: true,
                }
                
            },
            {
                $group: {
                    _id: '$_id',
                    category: { $push: '$category' },
                    doc: { $first: '$$ROOT' }
                }
            },
            { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
            { $project: { doc: 0 } },
        ])
    }
    return resolvePipeLine;
}

async function list(filter = {}, resolve = {}, options = {}) {
   

    let aggregatePipeLine = [], preFacetStage=[];

    let filterStages = addFilters(filter);
    let resolveStages = addResolver(resolve);
    preFacetStage = [...filterStages];
    aggregatePipeLine = [...resolveStages];


    // //type
    // if (type) {
    //     //lookup images
    //     preFacetStage.push({
    //         $match: {
    //             type
    //         }
    //     })
    // }

    // //cover
    // if (cover != 'false' && +cover != -1 && cover != false) {
    //     aggregatePipeLine.push({
    //         $lookup: {
    //             from: "images",
    //             localField: "cover",
    //             foreignField: "_id",
    //             as: "cover",
    //         }
    //     });
    //     aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
    // }

    // //image
    // if (image) {

    //     //lookup images
    //     aggregatePipeLine.push({
    //         $lookup: {
    //             from: "images",
    //             localField: "image",
    //             foreignField: "_id",
    //             as: "image",
    //         }
    //     })
    // }

    

    // //brand
    // if (brand) {
    //     aggregatePipeLine.push({
    //         $lookup: {
    //             from: "brand",
    //             localField: "brand",
    //             foreignField: "_id",
    //             as: "brand",
    //         }
    //     })
    //     aggregatePipeLine.push({ $unwind: { path: '$brand', preserveNullAndEmptyArrays: true } });
    //     aggregatePipeLine.push({
    //         $lookup: {
    //             from: "images",
    //             localField: "brand.cover",
    //             foreignField: "_id",
    //             as: "brand.cover",
    //         }
    //     })
    //     aggregatePipeLine.push({ $unwind: { path: '$brand.cover', preserveNullAndEmptyArrays: true } });

    // }
    // //tags   
    // if (tags) {
    //     aggregatePipeLine.push({
    //         $lookup: {
    //             from: "tag",
    //             localField: "tags",
    //             foreignField: "_id",
    //             as: "tags",
    //         }
    //     })
    // }

    // //primaryCategory
    // if (primaryCategory) {
    //     aggregatePipeLine.push({
    //         $lookup: {
    //             from: "category",
    //             localField: "primaryCategory",
    //             foreignField: "_id",
    //             as: "primaryCategory",
    //         }
    //     })
    //     aggregatePipeLine.push({ $unwind: { path: '$primaryCategory', preserveNullAndEmptyArrays: true } });
    //     aggregatePipeLine.push({
    //         $lookup: {
    //             from: "images",
    //             localField: "primaryCategory.cover",
    //             foreignField: "_id",
    //             as: "primaryCategory.cover",
    //         }
    //     })
    //     aggregatePipeLine.push({ $unwind: { path: '$primaryCategory.cover', preserveNullAndEmptyArrays: true } });

    // }

    // if (category) {
    //     aggregatePipeLine.push(...[
    //         {
    //             $lookup: {
    //                 from: 'category',
    //                 localField: 'category',
    //                 foreignField: '_id',
    //                 as: 'category'
    //             }
    //         },
    //         {
    //             $unwind: {
    //                 path: '$category',
    //                 preserveNullAndEmptyArrays: true
    //             }
    //         },
    //         {
    //             $group: {
    //                 _id: '$_id',
    //                 category: { $push: '$category' },
    //                 doc: { $first: '$$ROOT' }
    //             }
    //         },
    //         { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
    //         { $project: { doc: 0 } },
    //     ])
    // }

    let products = await paginate('product', preFacetStage, aggregatePipeLine, options);
    return products;
}

module.exports = list;
