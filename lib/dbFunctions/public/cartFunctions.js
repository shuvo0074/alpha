const con = require('../../lib/db.js');

async function getCart(customerId) {
  if (!customerId || !con.ObjectID.isValid(customerId)) {
    return false;
  }

  let db = await con.db();
  let cart = await db
    .collection('cart')
    .findOne({ customer: new con.ObjectID(customerId) });
  if (!cart) return false;

  return cart;
}

module.exports.getCart = getCart;
