const con = require('../../db.js');
const paginate = require('../../paginate.js');

async function createFeedback(info = {}) {

    let { title, message, product, customer } = info;
    let feedback = {
        title,
        message,
        customer: new con.ObjectID(customer),
        product: new con.ObjectID(product),
        added: new Date(),
        updated: new Date(),
        status: 'pending'
    }

    let db = await con.db();
    let result = await db.collection('feedback').insertOne(feedback);
    return result.ops;

}
module.exports.createFeedback = createFeedback;

async function updateFeedback(info = {}) {

    let { _id, title, message, product } = info;
    let feedback = {
        title,
        message,
        product: new con.ObjectID(product),
        updated: new Date(),
    }

    let db = await con.db();
    let updateCondition = {
        _id: new con.ObjectID(_id)
    }

    if(info.customer){
        updateCondition.customer = new con.ObjectID(info.customer);
    }

    let result = await db.collection('feedback').updateOne(updateCondition, { $set: feedback });
    return feedback;
}
module.exports.updateFeedback = updateFeedback;

async function getFeedback(filter, resolve, opts = {}) {

    let preFacetStage = [], aggregatePipeLine = [];

    //filters
    if (filter._id) {
        preFacetStage.push({
            $match: { _id: new con.ObjectID(filter._id) }
        })
    }
    if (filter.customer) {
        preFacetStage.push({
            $match: { customer: new con.ObjectID(filter.customer) }
        })
    }
    if (filter.product) {
        preFacetStage.push({
            $match: { product: new con.ObjectID(filter.product) }
        })
    }
    if (filter.status) {
        preFacetStage.push({
            $match: { status: filter.status }
        })
    }

    //resolvers
    if (resolve.product) {
        aggregatePipeLine.push(...[
            {
                $lookup: {
                    from: "product",
                    localField: "product",
                    foreignField: "_id",
                    as: "product"
                }
            },
            {
                $unwind: {
                    path: "$product",
                    preserveNullAndEmptyArrays: true,
                }
            },
            {
                $lookup: {
                    from: "images",
                    localField: "product.cover",
                    foreignField: "_id",
                    as: "product.cover"
                }
            },
            {
                $unwind: {
                    path: "$product.cover",
                    preserveNullAndEmptyArrays: true,
                }
            },
        ])
    }

    if (resolve.customer) {
        aggregatePipeLine.push(...[
            {
                $lookup: {
                    from: "customer",
                    localField: "customer",
                    foreignField: "_id",
                    as: "customer"
                }
            },
            {
                $unwind: {
                    path: "$customer",
                    preserveNullAndEmptyArrays: true,
                }
            }
        ])
    }

    let feedbackList = await paginate('feedback', preFacetStage, aggregatePipeLine, opts);

    return feedbackList;

}

module.exports.getFeedback = getFeedback;



async function deleteFeedback(info = {}) {

    let { _id, customer } = info;
   
    let db = await con.db();
    let deleteCondition = {
        _id: new con.ObjectID(_id)
    }

    if(customer){
        deleteCondition.customer = new con.ObjectID(customer);
    }

    let result = await db.collection('feedback').deleteOne(deleteCondition);

    return result;

}
module.exports.deleteFeedback = deleteFeedback;



async function updateFeedbackStatus(info = {}) {

    let { _id, status} = info;
    let feedback = {
        status
    }

    let updateCondition = {
        _id: new con.ObjectID(_id)
    }
    
    let db = await con.db();
    let result = await db.collection('feedback').updateOne(updateCondition, { $set: feedback });
    return feedback;
}
module.exports.updateFeedbackStatus = updateFeedbackStatus;
