const con = require('../../db.js');
const paginate = require('./../../paginate.js');
const { removeTrailingSlashes } = require('./../../utility.js');



/**
 *get brand list from db
 * 
 * @param {Object} [opts={}]
 * @returns {Object} result with data and pagination info
 * 
 * 
 * define image, limit, page, sort, sortOrder, cover inside opts
 * default values:
 * 
 * ```
 * cover = true;
 * limit = 50; //per page
 * page = 1;
 * sort = 'serial' //sort by serial
 * sortOrder = 1; //ASC
 * image = false; //excluded unless specified '1' or 'true'
 * productCount:false; 
 * ```
 * 
 * Example:
 * 
 * ```
 * let opts = {
 *  limit:20, //20 per page
 *  page:3, //page 3, (41-60)
 *  sort:'added' //by added date
 *  sortOrder:1; //newest first;
 *  image: true //include images
 *  productCount:true
 * }
 * await brandList(opts);
 * 
 * ```
 * 
 */
async function brandList(opts = {}) {
    let db = await con.db();
    let { image, cover = 1, productCount } = opts;

    let aggregatePipeLine = [], preFacetStage = [];

    //cover
    if (cover != 'false' && +cover != '-1' && cover != false) {
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "cover",
                foreignField: "_id",
                as: "cover",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
    }

    //image
    if (image) {

        //lookup images
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "image",
                foreignField: "_id",
                as: "image",
            }
        })
    }

    if (productCount) {

        //lookup images
        aggregatePipeLine.push(...[{
            $lookup: {
                from: "product",
                localField: "_id",
                foreignField: "brand",
                as: "productCount",
            }
        },
        {
            $project: {
                doc: "$$ROOT",
                productCount: { $size: "$productCount" },
            }
        },

        { $replaceRoot: { newRoot: { $mergeObjects: ["$doc", "$$ROOT"] } } },
        ])
    }


    let brands = await paginate('brand', preFacetStage, aggregatePipeLine, opts)
    return brands;
}
module.exports.getBrandList = brandList;

/**
 *get specific brand detail
 *
 * @param {String} id id or url of brand
 * @param {Object} [opts={}] options to include/exclude
 * @returns {Object} brand object
 * 
 * 
 * Default values:
 * 
 * ```
 * cover = true; //included unless specified '-1' or 'false'
 * image = true; //included unless specified '-1' or 'false'
 * ```
 * 
 * 
 */
async function brand(id, opts = {}) {

    //id validation
    if (!id) {
        return false;
    }
    let db = await con.db();
    let { image, cover } = opts;

    let aggregatePipeLine = [];

    if (con.ObjectID.isValid(id) && !id.includes('/')) {
        aggregatePipeLine.push({ $match: { _id: new con.ObjectID(id) } });
    } else { //id not valid, try as url
        aggregatePipeLine.push({
            $match: {
                url: decodeURI(removeTrailingSlashes(id))
            }
        });

    }

    //cover
    if (cover != 'false' && +cover != '-1' && cover != false) {
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "cover",
                foreignField: "_id",
                as: "cover",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
    }

    if (image != 'false' && +image !== -1 && image != false) {
        //lookup images
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "image",
                foreignField: "_id",
                as: "image",
            }
        })
    }

    let brand = await db.collection('brand').aggregate(aggregatePipeLine).toArray();
    return brand[0];
}
module.exports.getBrand = brand;

/**
 *get brand by url
 *
 * @param {String} url url of brand
 * @param {Object} opts options
 * @returns {Object} the brand from db
 * 
 * 
 * Default values:
 * 
 * ```
 * cover = true; //included unless specified '-1' or 'false'
 * subCategory = true; //included unless specified '-1' or 'false' 
 * image = true; //included unless specified '-1' or 'false'
 * ```
 * 
 * example:
 * ```
 * await getCategoryByUrl('/brand/t-shirt', {subCategory:-1, image:-1})
 * 
 * ```
 */
async function brandByUrl(url, opts = {}) {

    //id validation
    if (!url) {
        return false;
    }
    let db = await con.db();
    let { cover, subCategory, image } = opts;

    let aggregatePipeLine = [
        {
            $match: {
                url: decodeURI(removeTrailingSlashes(url))
            }
        },
    ]

    //cover
    if (cover != 'false' && +cover != '-1' && cover != false) {
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "cover",
                foreignField: "_id",
                as: "cover",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
    }

    if (image != 'false' && +image != -1 && image != false) {

        //lookup images
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "image",
                foreignField: "_id",
                as: "image",
            }
        })
    }

    if (subCategory != 'false' && +subCategory != -1 && subCategory != false) {
        //lookup subcategories
        aggregatePipeLine.push({
            $lookup: {
                from: "brand",
                localField: "_id",
                foreignField: "parent",
                as: "subCategory",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$subCategory', preserveNullAndEmptyArrays: true } });

        //lookup subbrand cover images
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "subCategory.cover",
                foreignField: "_id",
                as: "subCategory.cover",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$subCategory.cover', preserveNullAndEmptyArrays: true } });

        //group
        aggregatePipeLine.push({
            $group: {
                _id: '$_id',
                doc: { $first: '$$ROOT' }
            }
        })
        aggregatePipeLine.push({
            $replaceRoot: {
                newRoot: { $mergeObjects: ['$doc', '$$ROOT'] }
            }
        });
        aggregatePipeLine.push({
            $project: { doc: 0 }
        });
    }

    let brand = await db.collection('brand').aggregate(aggregatePipeLine).toArray();
    return brand;
}
module.exports.getCategoryByUrl = brandByUrl;


/**
 *get brand images
 *
 * @param {String} id brand id
 * @returns {Array} array of images
 */
async function brandImage(id) {
    //id validation
    if (!id) {
        return false
    }

    let db = await con.db();
    let aggregatePipeLine = [];

    if (con.ObjectID.isValid(id) && !id.includes('/')) {
        aggregatePipeLine.push({ $match: { _id: new con.ObjectID(id) } });
    } else { //id not valid, try as url
        aggregatePipeLine.push({
            $match: {
                url: decodeURI(removeTrailingSlashes(id))
            }
        });

    }
    aggregatePipeLine.push(
        {
            $lookup: {
                from: "images",
                localField: "image",
                foreignField: "_id",
                as: "image",
            }
        }
    )
    let brand = await db.collection('brand').aggregate(aggregatePipeLine).toArray();
    if (!brand || !brand[0]) return false;

    return brand[0].image;
}
module.exports.getBrandImage = brandImage;

/**
 * get products of brand
 *
 * @param {Array || String} id id/url of brand
 * @returns {Array}  product
 * 
 *  
 */
async function brandProduct(id, opts = {}) {

    if (!id) return false;
    let db = await con.db();
    let { image, cover = 1, brand, primaryCategory } = opts;
    let aggregatePipeLine = [], preFacetStage = [];


    if (con.ObjectID.isValid(id) && !id.includes('/')) { // valid id
        preFacetStage.push({ $match: { _id: new con.ObjectID(id) } });
    } else { //not id, try with url
        preFacetStage.push({
            $match: {
                url: decodeURI(removeTrailingSlashes(id))
            }
        });
    }
    preFacetStage.push(...[
        { $project: { _id: 1 } },
        {
            $lookup: {
                from: 'product',
                localField: '_id',
                foreignField: 'brand',
                as: 'product'
            }
        },
        {
            $unwind: {
                path: '$product',
            }
        },
        { $replaceRoot: { newRoot: { $ifNull: ["$product", {}] } } },


    ])

    //cover
    if (cover != 'false' && +cover != -1 && cover != false) {
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "cover",
                foreignField: "_id",
                as: "cover",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
    }

    //image
    if (image) {

        //lookup images
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "image",
                foreignField: "_id",
                as: "image",
            }
        })
    }

    //primaryCategory
    if (primaryCategory) {
        aggregatePipeLine.push({
            $lookup: {
                from: "brand",
                localField: "primaryCategory",
                foreignField: "_id",
                as: "primaryCategory",
            }
        })
        aggregatePipeLine.push({ $unwind: { path: '$primaryCategory', preserveNullAndEmptyArrays: true } });
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "primaryCategory.cover",
                foreignField: "_id",
                as: "primaryCategory.cover",
            }
        })
        aggregatePipeLine.push({ $unwind: { path: '$primaryCategory.cover', preserveNullAndEmptyArrays: true } });

    }

    if (brand) {
        aggregatePipeLine.push(...[
            {
                $lookup: {
                    from: 'brand',
                    localField: 'brand',
                    foreignField: '_id',
                    as: 'brand'
                }
            },
            {
                $unwind: {
                    path: '$brand',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    from: 'images',
                    localField: 'brand.cover',
                    foreignField: '_id',
                    as: 'brand.cover'
                }
            },
            {
                $unwind: {
                    path: '$brand.cover',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $group: {
                    _id: '$_id',
                    brand: { $push: '$brand' },
                    doc: { $first: '$$ROOT' }
                }
            },
            { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
            { $project: { doc: 0 } },
        ])
    }
    let products = await paginate('brand', preFacetStage, aggregatePipeLine, opts);

    return products;


}
module.exports.getBrandProduct = brandProduct;
