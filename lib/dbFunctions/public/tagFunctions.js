const con = require('../../db.js');
const paginate = require('./../../paginate.js')
/**
 *retrieves productList from database
 *
 * @param {Object} options 
 * @returns {Array}
 */

async function tagList(opts = {}){
    let db = await con.db();
    let preFacetStage = [];
    let aggregatePipeLine = [];
    let tags = paginate('tag', preFacetStage, aggregatePipeLine, opts);
    return tags;
}

module.exports.getTagList = tagList;

/**
 * retrieves product details 
 *
 * @param {String} id
 * @returns {Object} 
 * @public
 */

async function tag(id){

    //id validation
    if(!id){
        return false;
    }
    let db = await con.db();

    let aggregatePipeLine = [];

    if(con.ObjectID.isValid(id)  && !id.includes('/')){
        aggregatePipeLine.push({$match:{_id: new con.ObjectID(id)}});
    }else{ //id not valid, try as url
        aggregatePipeLine.push({$match:{url: decodeURI(id) }});
    }

    let tag = await db.collection('tag').aggregate(aggregatePipeLine).toArray();
    // let tagItem = await db.collection('tag').findOne({_id: new con.ObjectID(id)});

    tag = tag[0];
    if(!tag) return false;
    return tag;
}
module.exports.getTag = tag;

async function tagProduct(id, opts){
    if(!id) return false;
    let {  image, cover =  1, brand, category, primaryCategory,} = opts;
    let aggregatePipeLine = [], preFacetStage = [];


    if(con.ObjectID.isValid(id)  && !id.includes('/')){ // valid id
        preFacetStage.push({$match:{_id:new con.ObjectID(id)}});
    }else{ //not id, try with url
        preFacetStage.push({$match:{url: decodeURI(id) }});
    }
    preFacetStage.push(...[
        {$project: {_id:1}},
        {$lookup:{
            from:'product',
            localField:'_id',
            foreignField:'tags',
            as:'product'
        }},
        {$unwind:{
            path:'$product',
        }},
        { $replaceRoot: { newRoot: {$ifNull: ["$product", {}]} }},
        ])

    //cover
    if(cover != 'false' && +cover != -1 && cover !=false){
        aggregatePipeLine.push({$lookup:{
            from: "images",
            localField: "cover",
            foreignField: "_id",
            as: "cover",
        }});
        aggregatePipeLine.push({$unwind:{path:'$cover', preserveNullAndEmptyArrays:true}})
    }
    

    //image
    if(image){

        //lookup images
        aggregatePipeLine.push({$lookup:{
            from: "images",
            localField: "image",
            foreignField: "_id",
            as: "image",
        }})
    }

    //brand
    if(brand){
        aggregatePipeLine.push({$lookup:{
            from: "brand",
            localField: "brand",
            foreignField: "_id",
            as: "brand",
        }})
        aggregatePipeLine.push({$unwind:{path:'$brand', preserveNullAndEmptyArrays:true}});
        aggregatePipeLine.push({$lookup:{
            from: "images",
            localField: "brand.cover",
            foreignField: "_id",
            as: "brand.cover",
        }})
        aggregatePipeLine.push({$unwind:{path:'$brand.cover', preserveNullAndEmptyArrays:true}});
        
    }

    //primaryCategory
    if(primaryCategory ){
        aggregatePipeLine.push({$lookup:{
            from: "category",
            localField: "primaryCategory",
            foreignField: "_id",
            as: "primaryCategory",
        }})
        aggregatePipeLine.push({$unwind:{path:'$primaryCategory', preserveNullAndEmptyArrays:true}});
        aggregatePipeLine.push({$lookup:{
            from: "images",
            localField: "primaryCategory.cover",
            foreignField: "_id",
            as: "primaryCategory.cover",
        }})
        aggregatePipeLine.push({$unwind:{path:'$primaryCategory.cover', preserveNullAndEmptyArrays:true}});
    }

    if(category){
        aggregatePipeLine.push(...[
            {
                $lookup: {
                  from: 'category',
                  localField: 'category',
                  foreignField: '_id',
                  as: 'category'
                }
              },
              {
                $unwind: {
                  path: '$category',
                  preserveNullAndEmptyArrays: true
                }
              },
            {
                $lookup: {
                  from: 'images',
                  localField: 'category.cover',
                  foreignField: '_id',
                  as: 'category.cover'
                }
              },
              {
                $unwind: {
                  path: '$category.cover',
                  preserveNullAndEmptyArrays: true
                }
              },
            {
                $group: {
                  _id: '$_id',
                  category: { $push: '$category' },
                  doc: { $first: '$$ROOT' }
                }
              },
              { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
              { $project: { doc: 0 } },
        ])
    }

    
    let products = await paginate('tag', preFacetStage, aggregatePipeLine, opts);
    return products;

}
module.exports.getTagProduct = tagProduct;






    