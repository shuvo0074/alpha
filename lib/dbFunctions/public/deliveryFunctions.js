const con = require('../../db.js');
const paginate = require('./../../paginate.js');

async function getAllRegion(){
    let db = await con.db();
    let regions = await db.collection('deliveryRegion').find({}).toArray();
    return regions;
}

module.exports.getAllRegion = getAllRegion;

async function getRegion(regionId){
    if(!regionId || !con.ObjectID.isValid(regionId)) return false;

    let db = await con.db();
    let region = await db.collection('deliveryRegion').findOne({_id : new con.ObjectID(regionId)});
    return region;
}

module.exports.getRegion = getRegion;

async function getRegionByCountry(country){
    if(!country) return false;

    let aggregationPipeLine = [];

    if(con.ObjectID.isValid(country)){
        aggregationPipeLine.push({$match:{country: new con.ObjectID(country)}})
    }else{
        aggregationPipeLine.push({$match:{$or:[{countryCode:country}, {countryName:country}]}});
    }

    let db = await con.db();
    let region = await db.collection('deliveryRegion').aggregate(aggregationPipeLine).toArray();
    return region;
}
module.exports.getRegionByCountry = getRegionByCountry;

async function getRegionByCity(city){
    if(!city) return false;

    let db = await con.db();

    let aggregationPipeLine = [];

    aggregationPipeLine.push({$match:{city: {'$regex': city ,$options:'i'}}})
    aggregationPipeLine.push({$sort: {name:1}})
    
    let regionByCity = await db.collection('deliveryRegion').aggregate(aggregationPipeLine).toArray();
    
    if(!regionByCity) return false; //no region added by admin

    return regionByCity;
}
module.exports.getRegionByCity = getRegionByCity;

async function getRegionByName(name){
    if(!name) return false;

    let db = await con.db();

    let aggregationPipeLine = [];

    aggregationPipeLine.push({$match:{name:{'$regex': name ,$options:'i'}}})
    
    let regionByName = await db.collection('deliveryRegion').aggregate(aggregationPipeLine).toArray();
    
    if(!regionByName) return false; //no region added by admin

    return regionByName;
}
module.exports.getRegionByName = getRegionByName;

async function getCustomerRegion(customerId){
    if(!customerId || !con.ObjectID.isValid(customerId)) return false;

    let db = await con.db();

    let aggregationPipeLine = [
        {$match:{_id: new con.ObjectID(customerId)}},
        {$lookup:{
            from: "deliveryRegion",
            localField: "city",
            foreignField: "city",
            as: "deliveryRegion",
        }},
        {$unwind:'$deliveryRegion'},
        {$project:{deliveryRegion:1, country:1, city:1}}
    ];

    let customerRegion = await db.collection('customer').aggregate(aggregationPipeLine).toArray();
    
    let regionList = [];

    for(let region of customerRegion){
        regionList.push(region);
    }

    return regionList;

}
module.exports.getCustomerRegion = getCustomerRegion;






