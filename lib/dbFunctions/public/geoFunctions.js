const con = require('../../db.js');
const paginate = require('./../../paginate.js')
/**
 *retrieves productList from database
 *
 * @param {Object} options 
 * @returns {Array}
 */

async function getContinentList(opts = {}){
    let db = await con.db('alpha');
    let continentList = await db.collection('continentList').find({}).toArray();
    return continentList;
}
module.exports.getContinentList = getContinentList;

async function getContinent(id, opts={}){
    if(!id) return false;

    let db = await con.db('alpha');
    let aggregationPipeLine = [];

    if(con.ObjectID.isValid(id)){
        aggregationPipeLine.push({$match:{_id: new con.ObjectID(id)}})
    }else{
        aggregationPipeLine.push({$match:{$or:[{code:id}, {name:id}]}});
    }
    
    let country = await db.collection('continentList').aggregate(aggregationPipeLine).toArray();
    return country[0];
}

module.exports.getContinent = getContinent;

async function getCountryList(opts={}){
    let db = await con.db('alpha');
    
    let aggregationPipeLine = [];
    if(opts.continent){
        aggregationPipeLine.push(...[
            {$lookup:{
                from: "continentList",
                localField: "continent",
                foreignField: "_id",
                as: "continent",
            }},
            {$unwind:{
                path:'$continent',
                preserveNullAndEmptyArrays:true
            }}
        ])
    }
    let countryList = await db.collection('countryList').aggregate(aggregationPipeLine).toArray();
    countryList.sort((a,b)=>{
        return a.name<b.name?-1:1;
    });

    return countryList;
}
module.exports.getCountryList = getCountryList;

async function getCountry(id, opts={}){
    if(!id) return false;

    let db = await con.db('alpha');
    let aggregationPipeLine = [];

    if(con.ObjectID.isValid(id)){
        aggregationPipeLine.push({$match:{_id: new con.ObjectID(id)}})
    }else{
        aggregationPipeLine.push({$match:{$or:[{code:id}, {name:id}]}});
    }
    if(opts.continent){
        aggregationPipeLine.push(...[
            {$lookup:{
                from: "continentList",
                localField: "continent",
                foreignField: "_id",
                as: "continent",
            }},
            {$unwind:{
                path:'$continent',
                preserveNullAndEmptyArrays:true
            }}
        ])
    }
    let country = await db.collection('countryList').aggregate(aggregationPipeLine).toArray();
    return country[0];
}
module.exports.getCountry = getCountry;

async function getCountryByName(id, opts={}){
    if(!id) return false;

    let db = await con.db('alpha');
    let aggregationPipeLine = [];

    
    aggregationPipeLine.push({$match: {name:id}});
    
    if(opts.continent){
        aggregationPipeLine.push(...[
            {$lookup:{
                from: "continentList",
                localField: "continent",
                foreignField: "_id",
                as: "continent",
            }},
            {$unwind:{
                path:'$continent',
                preserveNullAndEmptyArrays:true
            }}
        ])
    }
    let country = await db.collection('countryList').aggregate(aggregationPipeLine).toArray();
    return country[0];
}
module.exports.getCountryByName = getCountryByName;

async function getCityList(country, opts={}){
    if(!country) return false;
    let db = await con.db('alpha');
    
    let aggregationPipeLine = [];

    if(con.ObjectID.isValid(country)){
        aggregationPipeLine.push({$match:{country: new con.ObjectID(country)}})
    }else{
        aggregationPipeLine.push({$match:{$or:[{countryCode:country}, {countryName:country}]}});
    }

    if(opts.country){
        aggregationPipeLine.push(...[
            {$lookup:{
                from: "countryList",
                localField: "country",
                foreignField: "_id",
                as: "country",
            }},
            {$unwind:{
                path:'$country',
                preserveNullAndEmptyArrays:true
            }}
        ])
    }
    let cityList = await db.collection('cityList').aggregate(aggregationPipeLine).toArray();
    cityList.sort((a,b)=>{
        return a.name<b.name?-1:1;
    });
    return cityList; 
}
module.exports.getCityList = getCityList;

    
async function getCity(id, opts={}){
    if(!id) return false;
    let db = await con.db('alpha');
    
    let aggregationPipeLine = [];

    if(con.ObjectID.isValid(id)){
        aggregationPipeLine.push({$match:{city: new con.ObjectID(id)}})
    }else{
        aggregationPipeLine.push({$match: {name:id}});
    }

    if(opts.country){
        aggregationPipeLine.push(...[
            {$lookup:{
                from: "countryList",
                localField: "country",
                foreignField: "_id",
                as: "country",
            }},
            {$unwind:{
                path:'$country',
                preserveNullAndEmptyArrays:true
            }}
        ])
    }
    let city= await db.collection('cityList').aggregate(aggregationPipeLine).toArray();
    
    return city[0]; 
}
module.exports.getCity = getCity;
    
async function getCityByName(id, opts={}){
    if(!id) return false;
    let db = await con.db('alpha');
    
    let aggregationPipeLine = [];

    aggregationPipeLine.push({$match: {name:id}});


    if(opts.country){
        aggregationPipeLine.push(...[
            {$lookup:{
                from: "countryList",
                localField: "country",
                foreignField: "_id",
                as: "country",
            }},
            {$unwind:{
                path:'$country',
                preserveNullAndEmptyArrays:true
            }}
        ])
    }
    let city= await db.collection('cityList').aggregate(aggregationPipeLine).toArray();
    
    return city[0]; 
}
module.exports.getCityByName = getCityByName;

    