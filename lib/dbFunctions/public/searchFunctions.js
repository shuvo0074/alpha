const con = require('../../db.js');
const paginate = require('../../paginate.js');

/**
 *search product from db
 *
 * @param {String} query search string
 * @param {*} [criteria={}] criteria to search, e.g:searchCategory, searchBrand, searchTag
 * @param {*} [opts={}] pagination and resolve opts
 * @returns pagination object with search result
 *
 * *valid criteria:
 * 1. searchCategory //search for product in category with query string
 * 2. searchBrand // ..... in brand with ....
 * 3. searchTag // ..... in tag with ....
 *
 *
 * ```
 * await searchProduct('black shoe',
 *      {inCategory: '98d0asd9809dsa9dsd}//category id
 *      {limit:5} //5 per page
 * )
 *
 * ```
 */
async function searchProduct(query, criteria = {}, opts = {}) {
  if (!query) query = '';
  let { image, cover = 1, brand, category, primaryCategory } = opts;

  let preFacetStage = [
    // { $match: { $text: { $search: query } } },
    // {$sort:{ score: { $meta: "textScore" } }},
    {
      $match: {
        $or: [
          { "name": { $regex: `.*${query}.*`, '$options': 'i' } },
          { "description": { $regex: `.*${query}.*`, '$options': 'i' } },
          { "bn.name": { $regex: `.*${query}.*`, '$options': 'i' } },
          { "bn.description": { $regex: `.*${query}.*`, '$options': 'i' } },
        ]
      }
    }
  ];

  if(criteria.type) {
    preFacetStage.push({ $match: {type: criteria.type }})
  }

  if(criteria.venue) {
    preFacetStage.push({ $match: { venue: criteria.venue }});
  }

  // if (criteria.date) {
  //   preFacetStage.push({ $match: { date: { $and: [ {$gte: []}]} } });
  // }

  //set criteria
  if (
    criteria.searchCategory &&
    con.ObjectID.isValid(criteria.searchCategory)
  ) {
    preFacetStage.push({
      $match: { category: new con.ObjectID(criteria.searchCategory) },
    });
  }
  if (criteria.searchBrand && con.ObjectID.isValid(criteria.searchBrand)) {
    preFacetStage.push({
      $match: { brand: new con.ObjectID(criteria.searchBrand) },
    });
  }
  if (criteria.searchTag && con.ObjectID.isValid(criteria.searchTag)) {
    preFacetStage.push({
      $match: { tags: new con.ObjectID(criteria.searchTag) },
    });
  }

  let aggregatePipeLine = [];

  //cover
  if (cover != 'false' && +cover != -1 && cover != false) {
    aggregatePipeLine.push({
      $lookup: {
        from: 'images',
        localField: 'cover',
        foreignField: '_id',
        as: 'cover',
      },
    });
    aggregatePipeLine.push({
      $unwind: { path: '$cover', preserveNullAndEmptyArrays: true },
    });
  }

  //image
  if (image) {
    //lookup images
    aggregatePipeLine.push({
      $lookup: {
        from: 'images',
        localField: 'image',
        foreignField: '_id',
        as: 'image',
      },
    });
  }

  //brand
  if (brand) {
    aggregatePipeLine.push({
      $lookup: {
        from: 'brand',
        localField: 'brand',
        foreignField: '_id',
        as: 'brand',
      },
    });
    aggregatePipeLine.push({
      $unwind: { path: '$brand', preserveNullAndEmptyArrays: true },
    });
    aggregatePipeLine.push({
      $lookup: {
        from: 'images',
        localField: 'brand.cover',
        foreignField: '_id',
        as: 'brand.cover',
      },
    });
    aggregatePipeLine.push({
      $unwind: { path: '$brand.cover', preserveNullAndEmptyArrays: true },
    });
  }

  //primaryCategory
  if (primaryCategory) {
    aggregatePipeLine.push({
      $lookup: {
        from: 'category',
        localField: 'primaryCategory',
        foreignField: '_id',
        as: 'primaryCategory',
      },
    });
    aggregatePipeLine.push({
      $unwind: { path: '$primaryCategory', preserveNullAndEmptyArrays: true },
    });
    aggregatePipeLine.push({
      $lookup: {
        from: 'images',
        localField: 'primaryCategory.cover',
        foreignField: '_id',
        as: 'primaryCategory.cover',
      },
    });
    aggregatePipeLine.push({
      $unwind: {
        path: '$primaryCategory.cover',
        preserveNullAndEmptyArrays: true,
      },
    });
  }

  if (category) {
    aggregatePipeLine.push(
      ...[
        {
          $lookup: {
            from: 'category',
            localField: 'category',
            foreignField: '_id',
            as: 'category',
          },
        },
        {
          $unwind: {
            path: '$category',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $group: {
            _id: '$_id',
            category: { $push: '$category' },
            doc: { $first: '$$ROOT' },
          },
        },
        { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
        { $project: { doc: 0 } },
      ]
    );
  }

  let products = await paginate(
    'product',
    preFacetStage,
    aggregatePipeLine,
    opts
  );


  if (!opts.sort) {

    function findOccurene(str = "") {
      str = str || "";
      return (str.toLowerCase().split(query.toLowerCase()).length) - 1;
    }


    for (let item of products.data) {
      let ocInTitle = findOccurene(item.name);
      let ocInDesc = findOccurene(item.description);;

      let metaScore = (ocInTitle * 2) + (ocInDesc * 0.75);
      item.metaScore = metaScore;
    }

    products.data.sort((a, b) => b.metaScore - a.metaScore);

  }


  return products;
}
module.exports.searchProduct = searchProduct;


async function searchProductSuggestion(query, criteria = {}, opts = {}) {
  if (!query) return [];

  let preFacetStage = [
    // { $match: { $text: { $search: query } } },
    // {$sort:{ score: { $meta: "textScore" } }},
    {
      $match: {
        $or: [
          { "name": { $regex: `.*${query}.*`, '$options': 'i' } },
          { "bn.name": { $regex: `.*${query}.*`, '$options': 'i' } },
        ]
      }
    }
  ];

  //set criteria
  if (
    criteria.searchCategory &&
    con.ObjectID.isValid(criteria.searchCategory)
  ) {
    preFacetStage.push({
      $match: { category: new con.ObjectID(criteria.searchCategory) },
    });
  }
  if (criteria.searchBrand && con.ObjectID.isValid(criteria.searchBrand)) {
    preFacetStage.push({
      $match: { brand: new con.ObjectID(criteria.searchBrand) },
    });
  }
  if (criteria.searchTag && con.ObjectID.isValid(criteria.searchTag)) {
    preFacetStage.push({
      $match: { tags: new con.ObjectID(criteria.searchTag) },
    });
  }


  preFacetStage.push({
    $project: { 
      name:1,
      "bn.name":1
     },
  });

  let db = await con.db();
  let products = await db.collection('product').aggregate(preFacetStage).toArray();


  function findOccurene(str = "") {
    str = str || "";
    return (str.toLowerCase().split(query.toLowerCase()).length) - 1;
  }


  let suggestion = [];

  for (let item of products) {
    let ocInTitle = findOccurene(item.name);
    let ocInBnTitle = findOccurene(item.bn.name);

    if (ocInTitle) suggestion.push({
      keyword: item.name,
      metaScore: ocInTitle
    })
    if (ocInBnTitle) suggestion.push({
      keyword: item.bn.name,
      metaScore: ocInBnTitle
    })
    
  }

  suggestion.sort((a, b) => b.metaScore - a.metaScore);

  //limit 
  opts.limit = parseInt(opts.limit);
  opts.limit = opts.limit || 10;

  suggestion.length = Math.min(suggestion.length, opts.limit);

  return suggestion;
}

module.exports.searchProductSuggestion = searchProductSuggestion;
