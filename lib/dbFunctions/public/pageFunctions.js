const con = require('../../db.js');
const paginate = require('./../../paginate.js');
const {removeTrailingSlashes} = require('./../../utility.js');



/**
 *get page list from db
 * 
 * @param {Object} [opts={}]
 * @returns {Object} result with data and pagination info
 * 
 * 
 * define  limit, page, sort, sortOrder, cover inside opts
 * default values:
 * 
 * ```
 * cover = true;
 * limit = 50; //per page
 * page = 1;
 * sort = 'serial' //sort by serial
 * sortOrder = 1; //ASC
 * ```
 * 
 * Example:
 * 
 * ```
 * let opts = {
 *  limit:20, //20 per page
 *  page:3, //page 3, (41-60)
 *  sort:'added' //by added date
 *  sortOrder:1; //newest first;
 *  productCount:true
 * }
 * await pageList(opts);
 * 
 * ```
 * 
 */
async function pageList(opts={}){
    let db = await con.db();
    let {cover =  1} = opts;

    let aggregatePipeLine = [], preFacetStage = [];

    //cover
    if(cover != 'false' && +cover != '-1' && cover !=false){
        aggregatePipeLine.push({$lookup:{
            from: "images",
            localField: "cover",
            foreignField: "_id",
            as: "cover",
        }});
        aggregatePipeLine.push({$unwind:{path:'$cover', preserveNullAndEmptyArrays:true}})
    }

      
    let pages = await paginate('page', preFacetStage, aggregatePipeLine, opts)
    return pages;
}
module.exports.getPageList = pageList;

/**
 *get specific page detail
 *
 * @param {String} id id or url of page
 * @param {Object} [opts={}] options to include/exclude
 * @returns {Object} page object
 * 
 * 
 * Default values:
 * 
 * ```
 * cover = true; //included unless specified '-1' or 'false'
 * ```
 * 
 * 
 */
async function page(id, opts={}){

    //id validation
    if(!id){
        return false;
    }
    let db = await con.db();
    let {cover} = opts;

    let aggregatePipeLine = [];

    if(con.ObjectID.isValid(id) && !id.includes('/')){
        aggregatePipeLine.push({$match:{_id: new con.ObjectID(id)}});
    }else{ //id not valid, try as url
        aggregatePipeLine.push({$match:{
            url:decodeURI( removeTrailingSlashes(id))
        }});

    }

    //cover
    if(cover != 'false' && +cover != '-1' && cover !=false){
        aggregatePipeLine.push({$lookup:{
            from: "images",
            localField: "cover",
            foreignField: "_id",
            as: "cover",
        }});
        aggregatePipeLine.push({$unwind:{path:'$cover', preserveNullAndEmptyArrays:true}})
    }

    let page = await db.collection('page').aggregate(aggregatePipeLine).toArray();
    return page[0];
}
module.exports.getPage = page;
