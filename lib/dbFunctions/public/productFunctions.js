const _ = require('lodash');
const con = require('../../db.js');
const paginate = require('./../../paginate.js');
const { removeTrailingSlashes, convertParams } = require('./../../utility.js');


/*
 * get productList by filter from database
 *
 * @param {Object} opts options to include or exclude fields
 * @returns {Array} products
 * 
 * **Default Values:**
 * 
 * ```
 * images = true; // excluded unless specified true
 * limit = 50;  // 50 products per page
 * page = 1; //first page
 * sort = 'serial' 
 * sortOrder = 1; //ASC
 * cover = 1 || true; //included unless specified -1 or false
 * brand = 1 || true; // not resolved for now
 * category = 1 || true; // not resolved for now
 * primaryCategory = 1 || true; //not resolved for now
 * tags = 1 || true
 * ```
 * 
 * Example:
 * 
 * ```
 * await dbFunctions.getProductsByFilter({
 *       limit:2,
 *       page:2,
 *       image:'true',
 *       cover:true,
 *       brand:true,
 *       category:true,
 *       primaryCategory:true,
 *   });
 * 
 * ```
 * 
 */

async function productByFilter(searchObj, opts = {}) {
    let { brand, category, primaryCategory, tag, query, sort, sortOrder, limit } = opts;
    
    // brand = convertParams(brand);
    // category = convertParams(category);

    let aggregatePipeLine = [];

    // cover and image resolved by default
    aggregatePipeLine.push({
        $lookup: {
            from: "images",
            localField: "cover",
            foreignField: "_id",
            as: "cover",
        }
    });
    aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })

    aggregatePipeLine.push({
        $lookup: {
            from: "images",
            localField: "image",
            foreignField: "_id",
            as: "image",
        }
    });

    if(!_.isEmpty(searchObj)) {
        aggregatePipeLine.push({ $match: { $or: searchObj }})
    }
    
    if(!_.isEmpty(query)) {
        if(query) {
            aggregatePipeLine.push({
                $match: {
                    $or: [
                        { "name": { $regex: `.*${query}.*`, '$options': 'i' } },
                        { "description": { $regex: `.*${query}.*`, '$options': 'i' } },
                    ]
                }
            })
        }
      
    }
    
    let products = await paginate('product', [], aggregatePipeLine, {sort, sortOrder, limit})
    return products;
};

module.exports.getProductsByFilter = productByFilter

/**
 * get productList from database
 *
 * @param {Object} opts options to include or exclude fields
 * @returns {Array} products
 * 
 * **Default Values:**
 * 
 * ```
 * images = false; // excluded unless specified true
 * limit = 50;  // 50 products per page
 * page = 1; //first page
 * sort = 'serial' 
 * sortOrder = 1; //ASC
 * cover = 1; //included unless specified -1 or false
 * brand = false; // excluded unless specified true
 * category = false; //excluded unless specified true
 * primaryCategory = false; //excluded unless specified true
 * ```
 * 
 * Example:
 * 
 * ```
 * await dbFunctions.getProductList({
 *       limit:2,
 *       page:2,
 *       image:'true',
 *       cover:true,
 *       brand:true,
 *       category:true,
 *       primaryCategory:true,
 *   });
 * 
 * ```
 * 
 */

async function productList(opts = {}) {
    let { image, limit, page, sort, sortOrder, type, cover = 1, brand, category, primaryCategory, tags } = opts;

    let aggregatePipeLine = [], preFacetStage=[];

    //type
    if (type) {
        //lookup images
        preFacetStage.push({
            $match: {
                type
            }
        })
    }

    //cover
    if (cover != 'false' && +cover != -1 && cover != false) {
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "cover",
                foreignField: "_id",
                as: "cover",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
    }

    //image
    if (image) {

        //lookup images
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "image",
                foreignField: "_id",
                as: "image",
            }
        })
    }

    

    //brand
    if (brand) {
        aggregatePipeLine.push({
            $lookup: {
                from: "brand",
                localField: "brand",
                foreignField: "_id",
                as: "brand",
            }
        })
        aggregatePipeLine.push({ $unwind: { path: '$brand', preserveNullAndEmptyArrays: true } });
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "brand.cover",
                foreignField: "_id",
                as: "brand.cover",
            }
        })
        aggregatePipeLine.push({ $unwind: { path: '$brand.cover', preserveNullAndEmptyArrays: true } });

    }
    //tags   
    if (tags) {
        aggregatePipeLine.push({
            $lookup: {
                from: "tag",
                localField: "tags",
                foreignField: "_id",
                as: "tags",
            }
        })
    }

    //primaryCategory
    if (primaryCategory) {
        aggregatePipeLine.push({
            $lookup: {
                from: "category",
                localField: "primaryCategory",
                foreignField: "_id",
                as: "primaryCategory",
            }
        })
        aggregatePipeLine.push({ $unwind: { path: '$primaryCategory', preserveNullAndEmptyArrays: true } });
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "primaryCategory.cover",
                foreignField: "_id",
                as: "primaryCategory.cover",
            }
        })
        aggregatePipeLine.push({ $unwind: { path: '$primaryCategory.cover', preserveNullAndEmptyArrays: true } });

    }

    if (category) {
        aggregatePipeLine.push(...[
            {
                $lookup: {
                    from: 'category',
                    localField: 'category',
                    foreignField: '_id',
                    as: 'category'
                }
            },
            {
                $unwind: {
                    path: '$category',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $group: {
                    _id: '$_id',
                    category: { $push: '$category' },
                    doc: { $first: '$$ROOT' }
                }
            },
            { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
            { $project: { doc: 0 } },
        ])
    }

    let products = await paginate('product', preFacetStage, aggregatePipeLine, opts);
    return products;
}

module.exports.getProductList = productList;

/**
 * get single product detail from database
 *
 * @param {String} id product id or url
 * @param {Object} opts options to include or exclude fields
 * @returns {Array} products
 * 
 * **Default Values:**
 * 
 * ```
 * images = 1; // included unless specified -1 or false
 * cover = 1; //included unless specified -1 or false
 * brand = 1; // included unless specified -1 or false
 * category = 1; //included unless specified -1 or false
 * primaryCategory = 1; //included unless specified -1 or false
 * ```
 * 
 * Example:
 * 
 * ```
 * await dbFunctions.getProductList({
 *       image:'true',
 *       cover:true,
 *       brand:true,
 *       category:true,
 *       primaryCategory:true,
 *   });
 * 
 * ```
 * 
 */

async function product(id, opts = {}) {

    //id validation
    if (!id) {
        return false;
    }
    let { image, cover = 1, brand, category, primaryCategory, tags, product } = opts;

    let aggregatePipeLine = [];

    //match
    if (con.ObjectID.isValid(id) && !id.includes('/')) { // valid id
        aggregatePipeLine.push({ $match: { _id: new con.ObjectID(id) } });
    } else { //not id, try with url
        aggregatePipeLine.push({
            $match: {
                url: decodeURI(removeTrailingSlashes(id))
            }
        });
    }



    //cover
    if (cover != 'false' && +cover != -1 && cover != false) {
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "cover",
                foreignField: "_id",
                as: "cover",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
    }


    //brand
    if (brand != 'false' && +brand != -1 && brand != false) {
        aggregatePipeLine.push({
            $lookup: {
                from: "brand",
                localField: "brand",
                foreignField: "_id",
                as: "brand",
            }
        })
        aggregatePipeLine.push({ $unwind: { path: '$brand', preserveNullAndEmptyArrays: true } });
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "brand.cover",
                foreignField: "_id",
                as: "brand.cover",
            }
        })
        aggregatePipeLine.push({ $unwind: { path: '$brand.cover', preserveNullAndEmptyArrays: true } });

    }
 
    if (product) {
        aggregatePipeLine.push({
            $lookup: {
                from: "product",
                localField: "product._id",
                foreignField: "_id",
                as: "product",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$product', preserveNullAndEmptyArrays: true } });

        //lookup category cover images
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "product.cover",
                foreignField: "_id",
                as: "product.cover",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$product.cover', preserveNullAndEmptyArrays: true } });
    }

    //tags   
    if (tags != 'false' && +tags != -1 && tags != false) {
        aggregatePipeLine.push({
            $lookup: {
                from: "tag",
                localField: "tags",
                foreignField: "_id",
                as: "tags",
            }
        })
    }
    //primaryCategory
    if (primaryCategory != 'false' && +primaryCategory != -1 && primaryCategory != false) {
        aggregatePipeLine.push({
            $lookup: {
                from: "category",
                localField: "primaryCategory",
                foreignField: "_id",
                as: "primaryCategory",
            }
        })
        aggregatePipeLine.push({ $unwind: { path: '$primaryCategory', preserveNullAndEmptyArrays: true } });
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "primaryCategory.cover",
                foreignField: "_id",
                as: "primaryCategory.cover",
            }
        })
        aggregatePipeLine.push({ $unwind: { path: '$primaryCategory.cover', preserveNullAndEmptyArrays: true } });

    }

    if (category != 'false' && +category != -1 && category != false) {
        aggregatePipeLine.push(...[
            {
                $lookup: {
                    from: 'category',
                    localField: 'category',
                    foreignField: '_id',
                    as: 'category'
                }
            },
            {
                $unwind: {
                    path: '$category',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $group: {
                    _id: '$_id',
                    category: { $push: '$category' },
                    doc: { $first: '$$ROOT' }
                }
            },
            { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
            { $project: { doc: 0 } },
        ])
    }
    //image
    if (image != 'false' && +image != -1 && image != false) {

        //lookup images
        aggregatePipeLine.push(...[
            {
                $lookup: {
                    from: 'images',
                    localField: 'image',
                    foreignField: '_id',
                    as: 'image'
                }
            },
            {
                $unwind: {
                    path: '$image',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $group: {
                    _id: '$_id',
                    image: { $push: '$image' },
                    doc: { $first: '$$ROOT' }
                }
            },
            { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
            { $project: { doc: 0 } },
        ])
    }

    let db = await con.db();
    let products = await db.collection('product').aggregate(aggregatePipeLine).toArray();
    products = products[0];
    return products;
}
module.exports.getProduct = product;

async function productImage(id) {
    //id validation
    if (!id || !con.ObjectID.isValid(id)) {
        throw new Error('invalid product Id');
    }

    let db = await con.db();
    let product = await db.collection('product').findOne({ _id: new con.ObjectID(id) });
    if (!product) return false;

    let images = [];
    //convert image ids to image objects
    for (let imageId of product.image) {
        let image = await db.collection('images').findOne({ _id: imageId }) || {};
        images.push(image);
    }

    return images;

}
module.exports.getProductImage = productImage;

async function productCategory(id) {
    //id validation
    if (!id || !con.ObjectID.isValid(id)) {
        return false;
    }

    let db = await con.db();
    let product = await db.collection('product').findOne({ _id: new con.ObjectID(id) });
    if (!product) return false;

    let categories = [];
    //convert image ids to image objects
    for (let categoryId of product.category) {
        let category = await db.collection('category').findOne({ _id: categoryId }) || {};
        categories.push(category);
    }

    return categories;

}
module.exports.getProductCategory = productCategory;

async function productPrimaryCategory(id) {
    //id validation
    if (!id || !con.ObjectID.isValid(id)) {
        return false;
    }

    let db = await con.db();
    let product = await db.collection('product').findOne({ _id: new con.ObjectID(id) });
    if (!product) return false;

    let category = await db.collection('category').findOne({ _id: product.primaryCategory }) || false;
    return category;

}
module.exports.getProductPrimaryCategory = productPrimaryCategory;


async function productBrand(id) {
    //id validation
    if (!id || !con.ObjectID.isValid(id)) {
        return false;
    }

    let db = await con.db();
    let product = await db.collection('product').findOne({ _id: new con.ObjectID(id) });
    if (!product) return false;
    //convert image ids to image objects
    let brand = await db.collection('brand').findOne({ _id: product.brand }) || false;

    return [brand];

}
module.exports.getProductBrand = productBrand;

async function productTag(id) {
    //id validation
    if (!id || !con.ObjectID.isValid(id)) {
        return false;
    }

    let db = await con.db();
    let aggregatePipeLine = [
        { $match: { _id: new con.ObjectID(id) } },
        { $project: { tags: 1, _id: 0 } },
        {
            $unwind: {
                path: '$tags',
                preserveNullAndEmptyArrays: true,
            }
        },
        {
            $lookup: {
                from: "tag",
                localField: "tags",
                foreignField: "_id",
                as: "tags",
            }
        }
    ]
    let tags = await db.collection('product').aggregate(aggregatePipeLine).toArray();
    return tags[0].tags;
}
module.exports.getProductTag = productTag;


async function productAttr(id) {
    //id validation
    if (!id || !con.ObjectID.isValid(id)) {
        return false;
    }

    let db = await con.db();
    let product = await db.collection('product').findOne({ _id: new con.ObjectID(id) });
    if (!product) return false;
    let attributes = [];
    //convert image ids to image objects
    for (let attr of product.attributes) {
        if (!attr._id || !con.ObjectID.isValid(attr._id)) continue;
        console.log(attr._id);
        let attribute = await db.collection('attributes').findOne({ _id: attr._id });
        if (!attribute) continue;
        attribute.value = attr.value;
        attributes.push(attribute);
    }

    return attributes;
}
module.exports.getProductAttr = productAttr;


async function getRelatedProducts(product, opts){

    //sorting
    opts.sort = "count";
    opts.sortOrder = -1;


    if(!con.ObjectID.isValid(product)) return false; 
    let db = await con.db();
    let aggregatePipeLine = [];

    //get product info
    aggregatePipeLine.push({
        $match:{_id: new con.ObjectID(product)}
    })

    //get product from same category
    aggregatePipeLine.push({
        $unwind: "$category"
    })
    aggregatePipeLine.push({
        $lookup:{
            from: 'product', 
            localField: "category",
            foreignField: "category",
            as: "productsOfSameCategory"
        }
    })
    aggregatePipeLine.push({
        $lookup:{
            from: 'product',
            localField: "brand",
            foreignField: "brand",
            as: "productsOfSameBrand"
        }
    })

     aggregatePipeLine.push({
        $lookup:{
            from: 'product',
            localField: "brand",
            foreignField: "brand",
            as: "productsOfSameTag"
        }
    })
    aggregatePipeLine.push({
        $project:{
            relatedProducts: {
                $concatArrays: ["$productsOfSameCategory", "$productsOfSameBrand", "$productsOfSameTag"]
            }
        }
    })
    aggregatePipeLine.push({
        $unwind: "$relatedProducts"
    })
    aggregatePipeLine.push({
        $match:{"relatedProducts._id":{$ne: new con.ObjectID(product)}}
    })
    aggregatePipeLine.push({
        $group: {
            _id: "$relatedProducts._id",
            count: {$sum:1},
            relatedProducts: {$first: "$relatedProducts"}
        }
    })
    aggregatePipeLine.push({
         $replaceRoot: { newRoot: { $mergeObjects: ['$relatedProducts', '$$ROOT'] } } 
    })
    aggregatePipeLine.push({
        $lookup:{
            from:"images",
            localField:"cover",
            foreignField:"_id",
            as:"cover"
        }
    })
    aggregatePipeLine.push({
        $unwind: "$cover"
    })
    
   
    // //get product from same tags
    // aggregatePipeLine.push({
    //     $unwind: "$tags"
    // })
    // aggregatePipeLine.push({
    //     $lookup:{
    //         from: 'product',
    //         localField: "tags",
    //         foreignField: "tags",
    //         as: "productsOfSameTags"
    //     }
    // })
    // aggregatePipeLine.push({
    //     $unwind: "$productsOfSameTags"
    // })
    // aggregatePipeLine.push({
    //     $group: {
    //         _id: "$_id",
    //         related: {$push: "$productsOfSameTags"},
    //         brand: {$first: "$brand"}
    //     }
    // })

    
    // //get product from same brand
    // aggregatePipeLine.push({
    //     $unwind: "$tags"
    // })
    // aggregatePipeLine.push({
    //     $lookup:{
    //         from: 'product',
    //         localField: "brand",
    //         foreignField: "brand",
    //         as: "productsOfSameBrand"
    //     }
    // })
    // aggregatePipeLine.push({
    //     $unwind: "$productsOfSameBrand"
    // })
    // aggregatePipeLine.push({
    //     $group: {
    //         _id: "$_id",
    //         related: {$push: "$productsOfSameBrand"},
    //     }
    // })


    let relatedProducts = await paginate('product', aggregatePipeLine, [], opts);
    return relatedProducts;
}

module.exports.getRelatedProducts = getRelatedProducts;




async function getOfferProducts(opts){
    let { image, type, cover = 1, brand, category, primaryCategory, tags, } = opts;

    let preFacetStage = [];

    //see if has offer price
    preFacetStage.push({
        $match:{
                "price.offer" : {$exists: true, $nin: [null, 0, ""]},
                // {"price.offer" : {$ne: ''}}
        }
    })

    //cover
    if (cover != 'false' && +cover != -1 && cover != false) {
        preFacetStage.push({
            $lookup: {
                from: "images",
                localField: "cover",
                foreignField: "_id",
                as: "cover",
            }
        });
        preFacetStage.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
    }

    //image
    if (image) {

        //lookup images
        preFacetStage.push({
            $lookup: {
                from: "images",
                localField: "image",
                foreignField: "_id",
                as: "image",
            }
        })
    }

    //type
    if (type) {
        preFacetStage.push({
            $match: {
                type
            }
        })
    }

    //brand
    if (brand) {
        preFacetStage.push({
            $lookup: {
                from: "brand",
                localField: "brand",
                foreignField: "_id",
                as: "brand",
            }
        })
        preFacetStage.push({ $unwind: { path: '$brand', preserveNullAndEmptyArrays: true } });
        preFacetStage.push({
            $lookup: {
                from: "images",
                localField: "brand.cover",
                foreignField: "_id",
                as: "brand.cover",
            }
        })
        preFacetStage.push({ $unwind: { path: '$brand.cover', preserveNullAndEmptyArrays: true } });

    }
    //tags   
    if (tags) {
        preFacetStage.push({
            $lookup: {
                from: "tag",
                localField: "tags",
                foreignField: "_id",
                as: "tags",
            }
        })
    }

    //primaryCategory
    if (primaryCategory) {
        preFacetStage.push({
            $lookup: {
                from: "category",
                localField: "primaryCategory",
                foreignField: "_id",
                as: "primaryCategory",
            }
        })
        preFacetStage.push({ $unwind: { path: '$primaryCategory', preserveNullAndEmptyArrays: true } });
        preFacetStage.push({
            $lookup: {
                from: "images",
                localField: "primaryCategory.cover",
                foreignField: "_id",
                as: "primaryCategory.cover",
            }
        })
        preFacetStage.push({ $unwind: { path: '$primaryCategory.cover', preserveNullAndEmptyArrays: true } });

    }

    if (category) {
        preFacetStage.push(...[
            {
                $lookup: {
                    from: 'category',
                    localField: 'category',
                    foreignField: '_id',
                    as: 'category'
                }
            },
            {
                $unwind: {
                    path: '$category',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $group: {
                    _id: '$_id',
                    category: { $push: '$category' },
                    doc: { $first: '$$ROOT' }
                }
            },
            { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
            { $project: { doc: 0 } },
        ])
    }

    let products = await paginate('product', preFacetStage, [], opts);

    if(opts.countOnly) return {count:products.data.length};

    return products;
}

module.exports.getOfferProducts = getOfferProducts;