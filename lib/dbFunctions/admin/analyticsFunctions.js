const con = require('../../db.js');
const paginate = require('../../paginate.js');
const moment = require('moment');

async function getItemAnalytics( opts={}){

    let {type, item, metric, verbose} = opts;
    if(!opts.sort) opts.sort = "count";
    if(!opts.sortOrder) opts.sortOrder = -1;

    if(item && !con.ObjectID.isValid(item)) return false;

    let validTypes =  ['page', 'category', 'product', 'brand', 'tag', 'page', 'post'];
    if(type && !validTypes.includes(type)) return false;

    let timeStampCurrent = new Date();
    let startingDate = opts.startDate? new Date(opts.startDate): new Date(timeStampCurrent-(7*24*60*60*1000));
    let endingDate = opts.endDate ? new Date(opts.endDate) : timeStampCurrent ; // default 7 days backward
 

    let match= [
        {date:{$gte:startingDate}},
        {date:{$lte:endingDate}},
    ]
    
    if(item) match.push({item: new con.ObjectID(item)});
    if(type) match.push({type});

    let aggregationPipeLine = []
         
    aggregationPipeLine.push({$match: {$and:match}})

    if(item){
        aggregationPipeLine.push({$lookup:{
            from: type,
            localField: "item",
            foreignField: "_id",
            as: "item",
        }})
        aggregationPipeLine.push({$unwind:{
            path:'$item',
            preserveNullAndEmptyArrays:true,
        }})
    }
    
    
    //metric group
    let timeMetrics = ["year", "month", "dayOfMonth",  "dayOfYear", "dayOfWeek", "hour", "date"];
    if (timeMetrics.includes(metric)) {
        if (metric == "date") {
            aggregationPipeLine.push(
                {
                    $addFields:
                    {
                        date: { $dateToString: { format: "%Y-%m-%d", date: "$date" , timezone: "Asia/Dhaka"} },

                    }
                }
            )
        }
        else {
            aggregationPipeLine.push(
                {
                    $addFields:
                    {
                        [metric]: { ["$" + metric]:{date: "$date", timezone: "Asia/Dhaka" }},
                        

                    }
                }
            )
        }

    }

    if (metric) {
        aggregationPipeLine.push({
            $group: {
                _id: "$" + metric,
                verbose: verbose? { "$push": "$$ROOT" }: {"$first": false},
                count: { $sum: 1 },
                [metric]: { $first: '$' + metric },
            }
        })

    }

    if(metric == "item" && type){
        aggregationPipeLine.push({
            $lookup:{
                from: type,
                localField: "item",
                foreignField: "_id",
                as : "detail"   
            }
        })
    }
    aggregationPipeLine.push({$project:{
        _id:0
    }})
    
    let db = await con.db();
    // let analytics = await db.collection(`analytics`).aggregate(aggregationPipeLine).toArray();
    let analytics = await paginate('analytics', aggregationPipeLine, [], opts);
    // console.log(analytics);
    return analytics;

}
module.exports.getItemAnalytics = getItemAnalytics;



async function getUserAnalytics( opts={}){


    let {type, item, metric, verbose} = opts;
    // if(!opts.sort) opts.sort = "count";
    // if(!opts.sortOrder) opts.sortOrder = -1;

    if(item && !con.ObjectID.isValid(item)) return false;

    let validTypes =  ['page', 'category', 'product', 'brand', 'tag', 'page', 'post'];
    if(type && !validTypes.includes(type)) return false;

    let timeStampCurrent = new Date();
    let startingDate = opts.startDate? new Date(opts.startDate): new Date(timeStampCurrent-(7*24*60*60*1000));
    let endingDate = opts.endDate ? new Date(opts.endDate) : timeStampCurrent ; // default 7 days backward
 

    let match= [
        {date:{$gte:startingDate}},
        {date:{$lte:endingDate}},
    ]
    
    if(item) match.push({item: new con.ObjectID(item)});
    if(type) match.push({type});

    let aggregationPipeLine = []
         
    aggregationPipeLine.push({$match: {$and:match}})

    if(item){
        aggregationPipeLine.push({$lookup:{
            from: type,
            localField: "item",
            foreignField: "_id",
            as: "item",
        }})
        aggregationPipeLine.push({$unwind:{
            path:'$item',
            preserveNullAndEmptyArrays:true,
        }})
    }
    
    
    //metric group
    let timeMetrics = ["year", "month", "dayOfMonth",  "dayOfYear", "dayOfWeek", "hour", "date"];
    if (timeMetrics.includes(metric)) {
        if (metric == "date") {
            aggregationPipeLine.push(
                {
                    $addFields:
                    {
                        date: { $dateToString: { format: "%Y-%m-%d", date: "$date" , timezone: "Asia/Dhaka"} },

                    }
                }
            )
        }
        else {
            aggregationPipeLine.push(
                {
                    $addFields:
                    {
                        [metric]: { ["$" + metric]:{date: "$date", timezone: "Asia/Dhaka" }},
                        

                    }
                }
            )
        }

    }

    
    if(metric){
        aggregationPipeLine.push({
            $group:{
                _id: "$token",
                [metric]: {$first: "$"+metric},
            }
        })
        // aggregationPipeLine.push({
        //     $unwind:{
        //         path: "$"+metric,
        //         preserveNullAndEmptyArrays: true
        //     }
        // })
    }
     


    if (metric) {
        aggregationPipeLine.push({
            $group: {
                _id: '$'+metric,
                verbose: verbose? { "$push": "$$ROOT" }: {"$first": false},
                count: { $sum: 1 },
                [metric]: { $first: '$' + metric },
            }
        })

    }

    if(metric == "item" && type){
        aggregationPipeLine.push({
            $lookup:{
                from: type,
                localField: "item",
                foreignField: "_id",
                as : "detail"   
            }
        })
    }
    aggregationPipeLine.push({$project:{
        _id:0
    }})
    
    let db = await con.db();
    // let analytics = await db.collection(`analytics`).aggregate(aggregationPipeLine).toArray();
    let analytics = await paginate('analytics', aggregationPipeLine, [], opts);
    // console.log(analytics);
    return analytics;

}
module.exports.getUserAnalytics = getUserAnalytics;


// async function numberOfUser(metric='date',opts){
    
//     let startingDate = opts.startDate? new Date(opts.startDate): new Date() - 30 * 24 * 60 * 60 * 1000; //30 days ago
//     let endingDate = opts.endDate ? new Date(opts.endDate) : new Date();

//     opts.sort = opts.sort || 'date'; //
//     opts.sortOrder = opts.sortOrder || -1; //

//     let aggregationPipeLine = [
//         {$match:{$and:[
//             {date:{$gte:startingDate}},
//             {date:{$lte:endingDate}},
//         ]}},  
//         {$group:{
//             _id: {
//                 token:'$token',
//                 [metric] : '$'+metric,
//             },
//             date: {$first:'$date'},
//             [metric] : {$first:'$'+metric},
//         }},
//         {$group:{
//             _id: '$'+metric,
//             count:{$sum:1},
//             date: {$first:'$date'},
//             [metric] : {$first:'$'+metric},

//         }},
//         {$sort: {
//             [opts.sort] : opts.sortOrder
//         }},

//     ];

    
//     let db = await con.db();

//     let analytics = await db.collection('analytics').aggregate(aggregationPipeLine).toArray()
//     console.log(analytics)
//     return analytics;
// }
// module.exports.numberOfUser = numberOfUser;

// async function totalUser(metric='date',opts){
    
//     let startingDate = opts.startDate? new Date(opts.startDate): new Date() - 30 * 24 * 60 * 60 * 1000; //30 days ago
//     let endingDate = opts.endDate ? new Date(opts.endDate) : new Date();

//     opts.sort = opts.sort || 'date'; //
//     opts.sortOrder = opts.sortOrder || -1; // 

//     let aggregationPipeLine = [
//         {$match:{$and:[
//             {date:{$gte:startingDate}},
//             {date:{$lte:endingDate}},
//         ]}},  
//         {$group:{
//             _id: {
//                 token:'$token',
//                 date:'$date'
//             },
//             date: {$first:'$date'},
//             [metric] : {$first:'$'+metric},
//         }},
//         {$group:{
//             _id: '$'+metric,
//             date: {$first:'$date'},
//             count:{$sum:1},
//             [metric] : {$first:'$'+metric},
//         }},
        
//         {$sort: {
//             [opts.sort] : opts.sortOrder
//         }},

//     ];

    
//     let db = await con.db();

//     let analytics = await db.collection('analytics').aggregate(aggregationPipeLine).toArray()
//     console.log(analytics)
//     return analytics;
// }
// module.exports.totalUser = totalUser;

// async function newUser(metric='date',opts){
    
//     let startingDate = opts.startDate? new Date(opts.startDate): new Date() - 30 * 24 * 60 * 60 * 1000; //30 days ago
//     let endingDate = opts.endDate ? new Date(opts.endDate) : new Date();

//     //format to yyyy-mm-dd
//     startingDate =  getFormattedDate(startingDate);
//     endingDate =  getFormattedDate(endingDate);


//     opts.sort = opts.sort || 'date'; //
//     opts.sortOrder = opts.sortOrder || -1; // 

//     let aggregationPipeLine = [
//         {$match:{$and:[
//             {date:{$gte:startingDate}},
//             {date:{$lte:endingDate}},
//             {returning:false}
//         ]}},  
//         {$group:{
//             _id: {
//                 remoteAddress:'$remoteAddress',
//                 date:'$date'
//             },
//             date: {$first:'$date'},
//             [metric] : {$first:'$'+metric},

//         }},
//         {$group:{
//             _id: '$'+metric,
//             date: {$first:'$date'},
//             count:{$sum:1},
//             [metric] : {$first:'$'+metric},
//         }},
        
//         {$sort: {
//             [opts.sort] : opts.sortOrder
//         }},

//     ];

    
//     let db = await con.db();

//     let analytics = await db.collection('analytics').aggregate(aggregationPipeLine).toArray()
//     console.log(analytics)
//     return analytics;
// }
// module.exports.newUser = newUser;


// /**
//  * 
//  * @param {String} type type of data product | page | category ...
//  * @param {Object} opts
//  * @param {Number} opts.startDate number of days ago from today  
//  * @param {Number} opts.endDate number of days ago from today  
//  */
// async function pageVisits(type, opts={}){
//     let validTypes =  ['page', 'category', 'product', 'brand', 'tag', 'page', 'post'];
//     if(type && !validTypes.includes(type)) return false;

//     let timestapCurrent = new Date();
//     let startingDate = opts.startDate? new Date(opts.startDate): new Date(timestapCurrent-(7*24*60*60*1000));
//     let endingDate = opts.endDate ? new Date(opts.endDate) : timestapCurrent ; // dafault 7 days backward
 
//     // //format to yyyy-mm-dd
//     // startingDate =  getFormattedDate(startingDate);
//     // endingDate =  getFormattedDate(endingDate);

//     opts.limit = opts.limit || 10; //10 item per request
//     opts.sort = opts.sort || 'visit'; //sort by times visited
//     opts.sortOrder = opts.sortOrder || -1; //show highest visited first 
   
//     let match = {$and:[
//         {type: type},
//         {date:{$gte:startingDate}},
//         {date:{$lte:endingDate}},
//     ]}

//     let preFacetStage = [
//         {$match:match},
//         {$lookup:{
//             from: type,
//             localField: "item",
//             foreignField: "_id",
//             as: "item",
//         }},
//         {$unwind:{
//             path:'$item',
//             preserveNullAndEmptyArrays:true,
//         }},
//         {$project:{
//             id:1,
//             date:1,
//             'item._id':1,
//             'item.name':1,
//             'item.url':1,

//         }},
//         {$group:{
//             _id: '$item._id',
//             'item':{$first:'$item'},
//             count:{$sum:1},
//         }},
//         {$project:{
//             _id:0,
//         }},
        
//     ];


//     let aggregationPipeLine = [
       
//     ];


//     let analytics = await paginate(`analytics`, preFacetStage, aggregationPipeLine, opts);
//     // console.log(analytics)
//     return analytics;
// }
// module.exports.pageVisits = pageVisits;

