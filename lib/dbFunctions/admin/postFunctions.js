const con = require('../../db.js');
const paginate = require('./../../paginate.js');
const { removeTrailingSlashes } = require('../../../lib/utility.js')


async function category(id, opts = {}) {
    if (!id) return false;

    let aggregatePipeLine = [];

    let { parentCategory, image, cover = 1 } = opts;
    //match
    if (con.ObjectID.isValid(id) && !id.includes('/')) { // valid id
        aggregatePipeLine.push({ $match: { _id: new con.ObjectID(id) } });
    } else { //not id, try with url
        aggregatePipeLine.push({
            $match: {
                url: decodeURI(removeTrailingSlashes(id))
            }
        });
    }


    if (cover != '-1') {
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "cover",
                foreignField: "_id",
                as: "cover",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
    }

    if (image != -1) {
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "image",
                foreignField: "_id",
                as: "image",
            }
        });
        // aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
    }

    if (parentCategory) {
        aggregatePipeLine.push({
            $lookup: {
                from: "postCategory",
                localField: "parentCategory",
                foreignField: "_id",
                as: "parentCategory",
            }
        });
    }

    let db = await con.db();
    let category = await db.collection('postCategory').aggregate(aggregatePipeLine).toArray();
    return category[0];
}

module.exports.getCategory = category;

async function tag(id, opts = {}) {
    if (!id) return false;

    let { image, cover = 1 } = opts;

    let aggregatePipeLine = []

    //match
    if (con.ObjectID.isValid(id) && !id.includes('/')) { // valid id
        aggregatePipeLine.push({ $match: { _id: new con.ObjectID(id) } });
    } else { //not id, try with url
        aggregatePipeLine.push({
            $match: {
                url: decodeURI(removeTrailingSlashes(id))
            }
        });
    }

    if (cover) {
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "cover",
                foreignField: "_id",
                as: "cover",
            }
        });
        // aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
    }

    if (image) {
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "image",
                foreignField: "_id",
                as: "image",
            }
        });
        // aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
    }

    let db = await con.db();
    let tag = await db.collection('postTag').aggregate(aggregatePipeLine).toArray();
    return tag[0];
}
module.exports.getTag = tag;


async function post(id, opts = {}) {

    //id validation
    if (!id) {
        return false;
    }
    let { image, cover = 1, category, primaryCategory, tag, requiredProducts = 1 } = opts;

    let aggregatePipeLine = [];

    //match
    if (con.ObjectID.isValid(id) && !id.includes('/')) { // valid id
        aggregatePipeLine.push({ $match: { _id: new con.ObjectID(id) } });
    } else { //not id, try with url
        aggregatePipeLine.push({
            $match: {
                url: decodeURI(removeTrailingSlashes(id))
            }
        });
    }

    //cover
    if (cover) {
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "cover",
                foreignField: "_id",
                as: "cover",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
    }



    //tags   
    if (tag) {
        aggregatePipeLine.push({
            $lookup: {
                from: "postTag",
                localField: "tags",
                foreignField: "_id",
                as: "tags",
            }
        })
    }


    //primaryCategory
    if (primaryCategory) {
        aggregatePipeLine.push({
            $lookup: {
                from: "postCategory",
                localField: "primaryCategory",
                foreignField: "_id",
                as: "primaryCategory",
            }
        })
        aggregatePipeLine.push({ $unwind: { path: '$primaryCategory', preserveNullAndEmptyArrays: true } });
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "primaryCategory.cover",
                foreignField: "_id",
                as: "primaryCategory.cover",
            }
        })
        aggregatePipeLine.push({ $unwind: { path: '$primaryCategory.cover', preserveNullAndEmptyArrays: true } });

    }

    if (category) {
        aggregatePipeLine.push(...[
            {
                $lookup: {
                    from: 'postCategory',
                    localField: 'category',
                    foreignField: '_id',
                    as: 'category'
                }
            },
            {
                $unwind: {
                    path: '$category',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $group: {
                    _id: '$_id',
                    category: { $push: '$category' },
                    doc: { $first: '$$ROOT' }
                }
            },
            { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
            { $project: { doc: 0 } },
        ])
    }


    //image
    if (image != -1) {

        //lookup images
        aggregatePipeLine.push(...[
            {
                $lookup: {
                    from: 'images',
                    localField: 'image',
                    foreignField: '_id',
                    as: 'image'
                }
            },
            {
                $unwind: {
                    path: '$image',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $group: {
                    _id: '$_id',
                    image: { $push: '$image' },
                    doc: { $first: '$$ROOT' }
                }
            },
            { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
            { $project: { doc: 0 } },
        ])
    }

    
  
    if (requiredProducts != -1) {
        aggregatePipeLine.push({ $unwind: { path: '$requiredProducts', preserveNullAndEmptyArrays: true } });

        aggregatePipeLine.push({
            $lookup: {
                from: "product",
                localField: "requiredProducts._id",
                foreignField: "_id",
                as: "requiredProducts.detail",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$requiredProducts.detail', preserveNullAndEmptyArrays: true } });
 
        //lookup category cover images
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "requiredProducts.detail.cover",
                foreignField: "_id",
                as: "requiredProducts.detail.cover",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$requiredProducts.detail.cover', preserveNullAndEmptyArrays: true } });
        
        aggregatePipeLine.push({
            $group:{
                _id: "$_id",
                requiredProducts: {$push: "$requiredProducts"},
                doc: {$first: "$$ROOT"}
            }
        })

        aggregatePipeLine.push({
            $replaceRoot: {newRoot: {$mergeObjects: ["$doc", "$$ROOT"]}}
        })

        aggregatePipeLine.push({ $project: { doc: 0 } });

        
    }

    let db = await con.db();
    let post = await db.collection('post').aggregate(aggregatePipeLine).toArray();
    post = post[0];
    return post;
}

module.exports.getPost = post;

async function postsList(opts = {}, criteria = {}) {
    let db = await con.db();
    let {
        category, image,
        primaryCategory, tag, postCount, page, sort, order, cover = 1, requiredProducts } = opts;

    let aggregatePipeLine = [], preFacetStage = [];

    if (criteria.category && con.ObjectID.isValid(criteria.category)) {
        aggregatePipeLine.push({
            $match: { category: new con.ObjectID(criteria.category) }
        })
    }

    if (criteria.tag && con.ObjectID.isValid(criteria.tag)) {
        aggregatePipeLine.push({
            $match: { tag: new con.ObjectID(criteria.tag) }
        })
    }


    // lookup cover image from images
    if (cover) {
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "cover",
                foreignField: "_id",
                as: "cover",
            }
        });

        aggregatePipeLine.push({ $unwind: { path: "$cover", preserveNullAndEmptyArrays: true } })
    }

    // lookup image from images
    if (image) {
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "image",
                foreignField: "_id",
                as: "image",
            }
        })
        aggregatePipeLine.push({ $unwind: { path: '$image', preserveNullAndEmptyArrays: true } });
    }

    // lookup category from postCategory collection
    if (category) {
        aggregatePipeLine.push({
            $lookup: {
                from: "postCategory",
                localField: "category",
                foreignField: "_id",
                as: "category",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$category', preserveNullAndEmptyArrays: true } });

        // lookup parentCategory (not working)
        aggregatePipeLine.push({
            $lookup: {
                from: "postCategory",
                localField: "category.parentCategory",
                foreignField: "_id",
                as: "category.parentCategory",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$category.parentCategory', preserveNullAndEmptyArrays: true } });

        //lookup category cover images
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "category.cover",
                foreignField: "_id",
                as: "category.cover",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$category.cover', preserveNullAndEmptyArrays: true } });

        // lookup category images
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "category.image",
                foreignField: "_id",
                as: "category.image",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$category.image', preserveNullAndEmptyArrays: true } });
    }


    // lookup category from postCategory collection
    if (requiredProducts) {
        aggregatePipeLine.push({
            $lookup: {
                from: "product",
                localField: "requiredProducts._id",
                foreignField: "_id",
                as: "requiredProducts",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$requiredProducts', preserveNullAndEmptyArrays: true } });

        //lookup category cover images
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "requiredProducts.cover",
                foreignField: "_id",
                as: "requiredProducts.cover",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$requiredProducts.cover', preserveNullAndEmptyArrays: true } });

        aggregatePipeLine.push({
            $group:{
                _id: "$_id",
                requiredProducts: {$push: "$requiredProducts"},
                doc: {$first: "$$ROOT"}
            }
        })

        aggregatePipeLine.push({
            $replaceRoot: {newRoot: {$mergeObjects: ["$doc", "$$ROOT"]}}
        })

        // lookup requiredProducts images
        // aggregatePipeLine.push({
        //     $lookup: {
        //         from: "images",
        //         localField: "requiredProducts.image",
        //         foreignField: "_id",
        //         as: "requiredProducts.image",
        //     }
        // });
        // aggregatePipeLine.push({ $unwind: { path: '$requiredProducts.image', preserveNullAndEmptyArrays: true } });
    }

    // lookup tags
    if (tag) {
        aggregatePipeLine.push({
            $lookup: {
                from: "postTag",
                localField: "tags",
                foreignField: "_id",
                as: "tags",
            }
        });
        // aggregatePipeLine.push({ $unwind: { path: '$tags', preserveNullAndEmptyArrays: true } });  
    }

    // lookup parentCategory
    if (primaryCategory) {
        aggregatePipeLine.push({
            $lookup: {
                from: "postCategory",
                localField: "primaryCategory",
                foreignField: "_id",
                as: "primaryCategory",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$primaryCategory', preserveNullAndEmptyArrays: true } });
    }

    // group
    aggregatePipeLine.push({
        $group: {
            _id: '$_id',
            name: { $first: '$name' },
            doc: { $first: '$$ROOT' }
        }
    })

    aggregatePipeLine.push(
        { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
    )
    aggregatePipeLine.push({
        $project:{doc:0}
    })

    let posts = await paginate('post', preFacetStage, aggregatePipeLine, opts)
    return posts;
}
module.exports.getPostsList = postsList;

async function postTags(opts = {}) {
    let aggregatePipeLine = [], preFacetStage = [];

    // lookup image from images
    aggregatePipeLine.push({
        $lookup: {
            from: "images",
            localField: "image",
            foreignField: "_id",
            as: "image",
        }
    })
    aggregatePipeLine.push({ $unwind: { path: '$image', preserveNullAndEmptyArrays: true } });

    // lookup cover from image
    aggregatePipeLine.push({
        $lookup: {
            from: "images",
            localField: "cover",
            foreignField: "_id",
            as: "cover",
        }
    })
    aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } });


    let tags = await paginate('postTag', preFacetStage, aggregatePipeLine, opts)
    return tags
}
module.exports.getPostTags = postTags;

async function postCategory(opts = {}) {
    let { parentCategory, image } = opts;

    let aggregatePipeLine = [], preFacetStage = [];

    if (image) {
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "image",
                foreignField: "_id",
                as: "image",
            }
        });
        // aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } })
    }

    // lookup cover from image
    aggregatePipeLine.push({
        $lookup: {
            from: "images",
            localField: "cover",
            foreignField: "_id",
            as: "cover",
        }
    })
    aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } });

    //lookup parentCategory
    if (parentCategory) {
        aggregatePipeLine.push({
            $lookup: {
                from: "postCategory",
                localField: "parentCategory",
                foreignField: "_id",
                as: "parentCategory"
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$parentCategory', preserveNullAndEmptyArrays: true } });
    }

    let categories = await paginate('postCategory', preFacetStage, aggregatePipeLine, opts)
    return categories;

}

module.exports.getPostCategory = postCategory;