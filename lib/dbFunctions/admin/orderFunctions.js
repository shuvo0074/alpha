const con = require('../../db.js');
const paginate = require('../../paginate.js');

async function getOrderList(opts) {

	let preFacetStage = []
	let { status, startDate, endDate, customer, deliveryRegion, deliveryRegionName,
		minimumPrice, maximumPrice, item, metric, verbose } = opts


	if (status) {
		preFacetStage.push({
			$match: { "status.name" : status}
		})	
	}

	if (item) {
		preFacetStage.push({
			$match: { "products._id": item }
		})
	}

	if (startDate) {
		preFacetStage.push(
			{
				$match: { added: { $gte: new Date(startDate) } }
			}
		)
	}
	if (endDate) {
		preFacetStage.push(
			{
				$match: { added: { $lte: new Date(endDate) } }
			}
		)
	}

	if (customer) {
		if (!con.ObjectID.isValid(customer)) return false;
		preFacetStage.push({
			$match: { customer: new con.ObjectID(customer) }
		})
	}

	if (deliveryRegion) {
		preFacetStage.push({
			$match: { "deliveryRegion._id": new con.ObjectID(deliveryRegion) }
		})
	}
	if (deliveryRegionName) {
		preFacetStage.push({
			$match: { "deliveryRegion.name": deliveryRegionName }
		})
	}
	
	if (minimumPrice) {
		preFacetStage.push(
			{
				$match: { totalPrice: { $gte: +minimumPrice } }
			}
		)
	}

	if (maximumPrice) {
		preFacetStage.push(
			{
				$match: { totalPrice: { $lte: +maximumPrice } }
			}
		)
	}

	//metric group
	let timeMetrics = ["year", "month", "dayOfMonth", "dayOfYear", "dayOfWeek", "hour", "date"];
	if (timeMetrics.includes(metric)) {
		if (metric == "date") {
			preFacetStage.push(
				{
					$addFields:
					{
						date: { $dateToString: { format: "%Y-%m-%d", date: "$added", timezone: "Asia/Dhaka" } },

					}
				}
			)
		}

		else {
			preFacetStage.push(
				{
					$addFields:
					{
						[metric]: { ["$" + metric]: { date: "$added", timezone: "Asia/Dhaka" } },


					}
				}
			)
		}

	}
	if (metric == "delivery") {
		preFacetStage.push({
			$lookup: {
				from: "deliveryRegion",
				localField: "deliveryRegion._id",
				foreignField: "_id",
				as: "delivery"
			}
		})
		// preFacetStage.push(
		//     {
		//         $addFields:
		//         {
		//             delivery:"$deliveryDetail.name",

		//         }
		//     }
		// )
		preFacetStage.push(
			{
				$unwind:
				{
					path: "$delivery",

				}
			}
		)
	}
	if (metric) {
		preFacetStage.push({
			$group: {
				_id: "$" + metric,
				verbose: verbose ? { "$push": "$$ROOT" } : { "$first": false },
				count: { $sum: 1 },
				[metric]: { $first: '$' + metric },
			}
		})

	}

	let aggregationPipeLine = [
		{
			$lookup: {
				from: "customer",
				localField: "customer",
				foreignField: "_id",
				as: "customer",
			}
		},
		{
			$unwind: {
				path: '$customer',
				preserveNullAndEmptyArrays: true,
			}
		},
		
	]

	let orders = await paginate('order', preFacetStage, aggregationPipeLine, opts);
	return orders;
}

module.exports.getOrderList = getOrderList;


async function searchOrder(query, criteria = {}, opts = {}) {
	if (!query) query = '';

	let preFacetStage = [
		{
			$match: {
				$or: [
					{ "id": { $regex: `.*${query}.*`, '$options': 'i' } },
					{ "shortCode": { $regex: `.*${query}.*`, '$options': 'i' } },
					{ "customer": { $regex: `.*${query}.*`, '$options': 'i' } },
				]
			}
		}
	];


	let orders = await paginate('order',preFacetStage,aggregatePipeLine,opts);

	return orders;
}

module.exports.searchOrder = searchOrder;
