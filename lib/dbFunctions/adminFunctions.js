const analyticsFunctions = require('./admin/analyticsFunctions.js');
const postFunctions = require('./admin/postFunctions.js');

module.exports = {
    ...analyticsFunctions,
    ...postFunctions
};
