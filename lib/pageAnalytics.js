const con = require('./db.js');
const path = require('path');


async function countVisit(req, res, next){
    let url = req.url;
    let basePath = path.basename(url);
    if(url.startsWith('/admin')) next(); //skip all admin req
    if(basePath.includes('.')) next(); //skip all static req
    
    let newVisit = {
        url,
        time: new Date(),
        deviceIp: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
    }
    con.db()
    .then(db=>{
        db.collection('pageVisits').insertOne(newVisit);
    })
    next();
}

module.exports.countVisit = countVisit;
