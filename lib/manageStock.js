const con = require('./db.js');
const {notify} = require('./adminNotification.js');

function manageStock(products){

    for(let product of products){
        //decrease available stock
        if(product.availableStock){ //empty means stock wont be managed
            product.availableStock = +product.availableStock - +product.quantity;
            
            //set available stock in db;
            con.db().then(db=>{
                db.collection('product').updateOne({_id: product._id}, {$set:{
                    availableStock: product.availableStock,
                    updated: new Date()
                }});
            });

            if(product.minimumStock){ //empty means notification not wanted
                if(+product.availableStock <= +product.minimumStock){
                    let notification = {
                        type: 'stock',
                        heading: 'Product low on stock',
                        text: `Product ${product.name} is low on stock`,
                        link: `/admin/product/view/${product._id}`
                    };
                    notify(notification)
                }
            }
        }


    }
}

module.exports = manageStock;