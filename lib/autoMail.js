const nodeMailer = require('nodemailer');
const con = require('./db.js');
const mail = require('./mail.js');
const ejs = require('ejs');
const he = require('he'); //encode/ decode html entity


async function eventMail(event , recipient, opts = {}) {
    if(!event || !recipient) return false;

    const db = await con.db();

    //check if autoEmail for this event is enabled
    let autoConfig = await db.collection('settings').findOne({ name: 'autoEmail' });
    if (!autoConfig) {
        console.log("missing settings")
        return false;
    }

    //get email template
    let template = await db.collection('emailTemplates').findOne({ event: event });

    if(template.admin) template.admin.body = he.decode(template.admin && template.admin.body || '')
    if(template.user) template.user.body = he.decode(template.user && template.user.body || '')

    let ejsOpts = { ...opts, delimiter: '?' }

    if (autoConfig[event].user && template.user) {


        template.user.subject = ejs.render(template.user.subject || "", ejsOpts);
        template.user.body = ejs.render(template.user.body || "", ejsOpts);


        //user email
        let email = {
            recipient,
            subject: template.user.subject,
            html: template.user.body,
            event: event,
        }

        await mail.sendMail(email);
        console.log(`[email sent] :  event => ${email.event}, recipient => ${email.recipient}`)

    }

    if (autoConfig[event].admin && template.admin) {
        //get admin email
        let siteInfo = await db.collection('settings').findOne({ name: 'site' });
        if (!siteInfo) return false;


        template.admin.subject = ejs.render(template.admin.subject || "", ejsOpts);
        template.admin.body = ejs.render(template.admin.body || "", ejsOpts);


        //admin email
        let email = {
            recipient: siteInfo.adminEmail,
            subject: template.admin.subject,
            html: template.admin.body,
            event: event,
        }

        await mail.sendMail(email);
        console.log(`[email sent] :  event => ${email.event}, recipient => ${email.recipient}`)

    }

}

module.exports = {
    eventMail
}