const nodeMailer = require('nodemailer');
const con = require('./db.js');


/**
 * sends email
 * configures transporter using information from db
 * 
 * @param {Object} options mail fields
 * @param {Array} options.recipient emails recipients address
 * @param {Array} options.cc emails cc recipients address
 * @param {Array} options.bcc emails bcc recipients address
 * @param {String} options.subject email subject
 * @param {String} options.html email html
 * @param {String} options.event email event 
 */
async function sendMail(options){
    let {recipient, subject, html, text, event, cc, bcc} = options;

    const db  = await con.db();
    let settings = await db.collection('settings').findOne({name:'email'});
    if(!settings){
        throw new Error('Email Settings not found, configure email settings first');
    }

    let email = {
        from: `"${settings.fromName}" <${settings.fromEmail}>`,
        to: recipient,
        cc, bcc,
        subject: subject, 
        html,
        text
    }

    const transporter = createTransporter(settings);
    await transporter.sendMail(email);

    //save history
    await db.collection('sentEmail').insertOne({
        recipient,
        cc,
        bcc,
        subject,
        html,
        text,
        time: new Date(),
        event
    });


} 

/**
 * create mail transporter 
 * 
 * @param {Object} opts 
 * @param {String} opts.host 
 * @param {Number} opts.port
 * @param {String} opts.encryption
 * @param {Boolean} opts.authentication
 * @param {String} opts.smtpUsername
 * @param {String} opts.smtpPassword
 * 
 * @returns {Object} nodemailer transporter object
 */
function createTransporter(opts){
    let transportOptions = {
        host:opts.host,
        port: opts.port,
    }

    if(opts.encryption == 'SSL'){
        transportOptions.secure = true;
    }

    if(opts.authentication){
        transportOptions.auth = {
            user: opts.smtpUsername, 
            pass:   opts.smtpPassword
        };

    }

    return nodeMailer.createTransport(transportOptions);
}

/**
 * verify smtp configuration 
 * 
 * @param {Object} config 
 * @param {String} config.host 
 * @param {Number} config.port
 * @param {String} config.encryption
 * @param {Boolean} config.authentication
 * @param {String} config.smtpUsername
 * @param {String} config.smtpPassword
 * 
 * @returns {Boolean} validity of smtp config
 */
async function verifySMTP(config){
    let transporter = createTransporter(config);
    return transporter.verify();

}

function validateEmail(email) {
    const re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return re.test(String(email).toLowerCase());
}

module.exports = {
    verifySMTP,
    sendMail,
    validateEmail
}