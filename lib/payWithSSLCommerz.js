
const router = require('express').Router();
const Validator = require('../validator/customer.js');
const _ = require('lodash');
const con = require('../lib/db.js');
const httpRequest = require('./httpRequest.js');



async function initiate(order, payment, customerInfo, paymentConfig, productsInfo) {

    if(!customerInfo.email){
        throw new SSLPaymentError(400, 'email', 'Email required for online payment')
    }

    //prepare params
    let params = {
        store_id: paymentConfig.sslcommerz.storeId,
        store_passwd: paymentConfig.sslcommerz.secret,

        total_amount: payment.totalCharge,
        currency: paymentConfig.sslcommerz.currency,
        tran_id: payment.paymentId.toHexString(),


        success_url: new URL('/order/success', paymentConfig.sslcommerz.callBackUrl).href,
        fail_url: new URL('/order/fail', paymentConfig.sslcommerz.callBackUrl).href,
        cancel_url: new URL('/order/cancel', paymentConfig.sslcommerz.callBackUrl).href,
        ipn_url: new URL('/api/payment/ipn/sslcommerz', paymentConfig.sslcommerz.callBackUrl).href,

        cus_name: customerInfo.firstName + " " + customerInfo.lastName,
        cus_email: customerInfo.email,
        cus_add1: customerInfo.address1,
        cus_add2: customerInfo.address2,
        cus_city: customerInfo.city,
        cus_country: "Bangladesh",
        cus_phone: customerInfo.phone,

        // bullshit section 😑😑😑
        // collecting analytics data through payment api... motherfuckers 😑😑😑
        shipping_method: "NO",
        product_name: productsInfo.map(i => i.name).join(','),
        product_category: productsInfo.map(i => i.primaryCategory[0].name).join(','),
        product_profile: 'general'
    }

    //request options
    let requestOptions = {
        uri: paymentConfig.sslcommerz.url,
        method: "POST",
        form: params, //? sslcommerz api can accept only form data, hail php 😊
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    }

    //send api request
    try {

        let paymentResponse = await httpRequest.post(requestOptions);
        paymentResponse = JSON.parse(paymentResponse);

        // check failure 
        //? they send 200 for failed request too, could not be any better example of response code
        //? have to check for failed status manually
        if (!paymentResponse.status || paymentResponse.status.toLowerCase() !== 'success') {
            console.log(paymentResponse.failedreason); // for debugging in production; keep in place
            throw new Error('sslCommerz api failed');
        }

        //? only useful things for now, will add rest of the params later
        payment.redirectTo = paymentResponse.GatewayPageURL;
        payment.sessionKey = paymentResponse.sessionkey;
        payment.paid = false;


        return payment;

    } catch (err) {
        // catch and throw custom error 
        console.log(err);
        throw new SSLPaymentError(500, 'payment', "Error initiating payment")
    }



}
module.exports.initiate = initiate;



async function verify(info, paymentConfig) {

    // console.log(info)
    const {
        status,
        tran_id: paymentId,
        amount,
        currency_type: currencyType,
        currency_amount: currencyAmount,
        val_id: validationId,
        tran_date: transactionDate
    } = info;

    // validate paymentId from db
    let db = await con.db();

    let order = await db.collection('order').findOne({ "payment.paymentId": new con.ObjectID(paymentId) });
    if (!order) throw new SSLPaymentError('400', 'paymentId', 'No order with tran_id');

    if (+currencyAmount !== +order.totalCharge) {
        console.log(currencyAmount, +currencyAmount, +order.totalCharge)
        throw new SSLPaymentError('400', 'currencyAmount', 'currencyAmount does not match');
    }

    //prepare params
    let params = {
        store_id: paymentConfig.sslcommerz.storeId,
        store_passwd: paymentConfig.sslcommerz.secret,
        val_id: validationId,
        format: 'json'
    }

    //request options
    // let requestOptions = {
    //     uri: paymentConfig.sslcommerz.validationUrl,
    //     method: "POST",
    //     form: params, //? sslcommerz api can accept only form data
    //     headers: {
    //         'content-type': 'application/x-www-form-urlencoded'
    //     }
    // }

    //!this api does not work on their end
    //send api request
    // try {

    //     let validationResponse = await httpRequest.post(requestOptions);
    //     validationResponse = JSON.parse(validationResponse);

    //     if(!validationResponse.status || !validationResponse.status.toLowerCase() == 'valid'){
    //         throw new SSLPaymentError(400, 'status', 'failed transaction: '+ validationResponse.status);
    //     }

    //     if( +validationResponse.amount !== +order.totalCharge){
    //         throw new SSLPaymentError('400', 'amount', 'amount does not match');
    //     }

    //     if( +validationResponse.currencyAmount !== +order.totalCharge){
    //         throw new SSLPaymentError('400', 'currencyAmount', 'currencyAmount does not match');
    //     }


    //     let payment = order.payment;
    //     payment.paid = true;
    //     payment.transactionDate = validationResponse.tran_date;
    //     payment.validationId = validationId; //! not to be exposed

    //     return {
    //         orderId: order._id,
    //         payment,
    //     };

    // } catch (err) {
    //     // catch and throw custom error 
    //     console.log(err);
    //     throw new SSLPaymentError(500, 'payment', "Error confirming payment")
    // }

    // ?temporary solution ... less secure
    let payment = order.payment;
    payment.paid = true;
    payment.transactionDate = transactionDate;

    return {
        orderId: order._id,
        payment,
    };

}

module.exports.verify = verify;


class SSLPaymentError extends Error {
    constructor(code, errorField, errorMessage) {
        super()
        this.statusCode = code;
        this.errorField = errorField;
        this.message = errorMessage;
    }

    get response() {
        if (this.errorList) return this.errorList;
        else return { [this.errorField]: this.message }
    }

}