const router = require('express').Router();
const Validator = require('../validator/customer.js');
const _ = require('lodash');
const con = require('../lib/db.js');
const bcrypt = require('bcrypt');
const autoMail = require('../lib/autoMail.js');
const autoSMS = require('./../lib/autoSMS.js');
const { notify } = require('../lib/adminNotification.js');
const deliveryFunction = require('../lib/dbFunctions/public/deliveryFunctions.js');
const { generateRandomString } = require("../lib/utility.js");
const defaultOrderStatus = require("./../config/defaultOrderStatus.json");
const jwt = require("jsonwebtoken");
const httpRequest = require('./httpRequest.js');
const payWithSSLCommerz = require('./payWithSSLCommerz.js');

async function parseCustomerId(req) {
    let authToken = req.headers.authorization;
    if (authToken) {
        try {
            const customerData = await jwt.verify(authToken.split(' ')[1], process.env.JWT_SECRET_KEY);
            return customerData._id;
        } catch (error) {
            return null
        }
    } else return null
}
module.exports.parseCustomerId = parseCustomerId;



async function getProductIds(items) {
    let productIds = [];

    // validate and add id to list
    for (let item of items) {
        let { product, variation } = item;

        if (!con.ObjectID.isValid(product)) {
            throw new CustomCheckoutError(400, 'product', 'Invalid product id');
        }
        if (!con.ObjectID.isValid(variation)) {
            throw new CustomCheckoutError(400, 'variation', 'Invalid variation id');
        }

        productIds.push(new con.ObjectID(product));
    }

    return productIds;
}
module.exports.getProductIds = getProductIds;



async function getProductsInfo(productIds) {
    const db = await con.db();

    //get products from db
    return await db.collection('product').aggregate([
        { $match: { _id: { $in: productIds } } },
        {
            $lookup: {
                from: "category",
                foreignField: "_id",
                localField: "primaryCategory",
                as: "primaryCategory"
            }
        }
    ]).toArray();

}
module.exports.getProductsInfo = getProductsInfo;




async function productWithVariationAndPrice(productsInfo, items) {
    let products = [];


    for (let product of productsInfo) {


        let orderedItem = items.find(i => product._id.toHexString() == i.product);
        let variation = product.pricing.find(i => {

            if (!i._id) {
                throw new CustomCheckoutError(400, 'variation', 'Invalid variation for product');
            }

            return i._id.toHexString ?
                (i._id.toHexString() == orderedItem.variation) : (i._id == orderedItem.variation)

        });

        if (!variation) {
            throw new CustomCheckoutError(400, 'variation', `Invalid variation for product ${product.name}`);
        }


        // maximum purchase limit check
        if ( Number(variation.maximumPurchaseLimit) && isFinite(variation.maximumPurchaseLimit) &&
            orderedItem.quantity > variation.maximumPurchaseLimit) {
                console.log(variation.maximumPurchaseLimit)
            throw new CustomCheckoutError(400, 'quantity', 
            `Maximum purchase limit for selected variation of ${product.name} is ${variation.maximumPurchaseLimit}`)
        }

        let unitPrice = + (variation.price.offer || variation.price.regular);

        orderedItem.quantity = orderedItem.quantity < 1 ? 1 : orderedItem.quantity;
        let price = unitPrice * +orderedItem.quantity;

        products.push({
            _id: product._id,
            variation: variation._id,
            unitPrice,
            price,
            quantity: +orderedItem.quantity,
        })

    }

    return products;
}
module.exports.productWithVariationAndPrice = productWithVariationAndPrice;



async function calculateTotalPrice(products) {
    let totalPrice = 0;

    for (let product of products) {
        totalPrice += +product.price;
    }
    return totalPrice;

}
module.exports.calculateTotalPrice = calculateTotalPrice;



async function getProducts(productsInfo, items) {
    let products = await productWithVariationAndPrice(productsInfo, items);
    return products;
}
module.exports.getProducts = getProducts;



function getShippingAddress(shippingAddress) {

    //geolocation
    let { longitude, latitude } = shippingAddress.coords || {};
    let location = {
        type: "Point",
        coordinates: [longitude, latitude]
    }

    return {
        firstName: shippingAddress.firstName,
        lastName: shippingAddress.lastName,
        country: shippingAddress.country,
        city: shippingAddress.city,
        address1: shippingAddress.address1,
        address2: shippingAddress.address2,
        zipCode: shippingAddress.zipCode,
        phone: shippingAddress.phone,
        email: shippingAddress.email,
        additionalInfo: shippingAddress.additionalInfo,
        location
    };

}
module.exports.getShippingAddress = getShippingAddress;


async function getDeliveryRegion(delivery) {
    //verify from db
    return await deliveryFunction.getRegion(delivery);

}
module.exports.getDeliveryRegion = getDeliveryRegion;



function getDeliveryCharge(delivery, totalPrice) {
    let deliveryCharge;

    let chargeBreakPoints = Object.keys(delivery.charge); //array of total price only
    chargeBreakPoints.sort((a, b) => a - b);

    if (totalPrice < chargeBreakPoints[0]) {
        //lower than minimum order
        throw new CustomCheckoutError(400, "delivery", 'Minimum order amount is ' + chargeBreakPoints[0])

    } else if (totalPrice >= chargeBreakPoints[chargeBreakPoints.length - 1]) {
        //higher than defined amount
        deliveryCharge = delivery.charge[chargeBreakPoints[chargeBreakPoints.length - 1]];
    } else {

        //get delivery charge according to totalPrice
        for (let index in chargeBreakPoints) {
            if (totalPrice >= chargeBreakPoints[index] && totalPrice < chargeBreakPoints[+index + 1]) {
                deliveryCharge = delivery.charge[chargeBreakPoints[index]];
                break;
            }
        }
    }
    return deliveryCharge;
}
module.exports.getDeliveryCharge = getDeliveryCharge;



async function createAccount(shippingAddress, flds) {
    let registerError = {};
    let { password, password2 } = flds;

    //validation 
    if (!password) registerError.password = 'Enter password';
    if (!password2) registerError.password2 = 'Repeat password';
    if (password !== password2) registerError.password2 = 'Passwords not matching';

    if (!_.isEmpty(registerError)) {
        let error = new CustomCheckoutError(400);
        error.errorList = { error: 'Could not register new customer', registerError };
        throw error;
    }

    //checking if account with phone exists
    const db = await con.db();
    let exists = await db.collection("customer").findOne({ phone: shippingAddress.phone });
    if (exists) {
        throw new CustomCheckoutError(400, "phone", "There is already an account with this phone number")
    }

    //checking if account with email exists
    if (shippingAddress.email && shippingAddress.email != "" && shippingAddress.email != " ") { // 😕 != "" ???
        exists = await db.collection("customer").findOne({ email: { $regex: new RegExp("^" + shippingAddress.email, "i") } });
        if (exists) {
            throw new CustomCheckoutError(400, "email", "There is already an account with this email")

        }
    }


    //hash password
    let salt = await bcrypt.genSalt(10);
    let hash = await bcrypt.hash(password, salt);

    let result = await db.collection('customer').insertOne({
        ...shippingAddress,
        password: hash,
        added: new Date(),
        updated: new Date()
    });

    let customer = result && result.ops && result.ops[0] && result.ops[0];
    delete customer.password;
    return customer;
}
module.exports.createAccount = createAccount;



async function applyCoupon(coupon, customerId, order) {

    //verify coupon from db 
    const db = await con.db();
    coupon = await db.collection('coupon').findOne({
        $and: [
            { code: coupon.toUpperCase() },
            { startDate: { $lte: new Date() } },
            { endDate: { $gte: new Date() } }
        ]
    });

    //validate coupon
    if (!coupon) {
        throw new CustomCheckoutError(409, "coupon", "Invalid coupon");
    }


    //usage limit total
    let couponUsage = await db.collection('couponUsage').find({ coupon: coupon._id }).toArray();
    if (coupon.maxUseTotal && couponUsage && couponUsage.length > coupon.maxUseTotal) {
        throw new CustomCheckoutError(409, "coupon", "Coupon is not available anymore");
    }

    //usage limit by user
    let couponUsageByUser = couponUsage && couponUsage.filter(i => i.customer.toHexString() == customerId);
    if (coupon.maxUsePerUser && couponUsage && couponUsageByUser.length > coupon.maxUsePerUser) {
        throw new CustomCheckoutError(409, "coupon", "Coupon max use reached");
    }

    //order matching
    if (coupon.minimumOrder && order.totalPrice < coupon.minimumOrder) {
        throw new CustomCheckoutError(
            409, "coupon", `minimum order for coupon ${coupon.name} is ${coupon.minimumOrder}`
        );


    }
    if (coupon.maximumOrder && order.totalPrice > coupon.maximumOrder) {
        throw new CustomCheckoutError(
            409, "coupon", `maximum order for coupon ${coupon.name} is ${coupon.maximumOrder}`
        );
    }

    if (coupon.orderedProducts) {
        for (let requiredProduct of coupon.orderedProducts) {
            if (!requiredProduct || !requiredProduct._id) continue; //temporary fix for empty objects inside

            let productInOrder = order.products.find(i =>
                i._id.toHexString() == requiredProduct._id.toHexString()
            )
            if (!productInOrder) {
                throw new CustomCheckoutError(
                    409, "coupon", `you did not buy all required product to apply coupon ${coupon.name}`
                );

            }
        }
    }


    let discountAmount;
    if (coupon.amount) { //price discount

        if (coupon.amountType == "fixed") {
            discountAmount = coupon.amount;
            order.discountPrice = order.totalPrice - coupon.amount
        } else if (coupon.amountType == "percentage") {
            order.discountPrice = order.totalPrice - (order.totalPrice / 100 * coupon.amount);
            discountAmount = order.totalPrice - order.discountPrice
        }

    }
    if (coupon.freeDelivery) {
        order.freeDelivery = true;
    }

    order.coupon = coupon.code;

    await db.collection('couponUsage').insertOne({
        coupon: coupon._id,
        customer: new con.ObjectID(customerId),
        discount: discountAmount,
        freeDelivery: order.freeDelivery,
        freeProducts: order.freeProducts,
        date: new Date(),
    })

    return order;
}
module.exports.applyCoupon = applyCoupon;



function calculateTotalCharge(order) {
    //calculate total charge (price, discount, delivery)
    let totalCharge = 0;

    //delivery charge
    if (!order.freeDelivery) {
        totalCharge += +order.deliveryCharge
    }

    if (order.discountPrice) { //charge discounted amount if exists
        totalCharge += +order.discountPrice
    } else {
        totalCharge += +order.totalPrice
    }

    return totalCharge;
}
module.exports.calculateTotalCharge = calculateTotalCharge;



async function applyPayment(order, customerInfo, paymentMethod, paymentConfig, productsInfo) {

    if (!paymentMethod) paymentMethod = 'cod';

    if (!Object.keys(paymentConfig).includes(paymentMethod)) {
        throw new CustomCheckoutError(400, "paymentMethod", "Invalid Payment Method");
    };

    let payment = {
        paymentId: new con.ObjectID(), //!secret
    }

    //total amount
    payment.totalCharge = await calculateTotalCharge(order);
    order.totalCharge = payment.totalCharge;

    // cash on delivery
    if (paymentMethod.toLowerCase() == "cod") {
        payment.method = "cod";
    }

    //sslCommerz
    else if (paymentMethod.toLowerCase() == "sslcommerz") {
        payment = await payWithSSLCommerz.initiate(order, payment, customerInfo, paymentConfig, productsInfo);
        payment.method = 'sslzommerz'
    }

    return payment;
}
module.exports.applyPayment = applyPayment;





async function notifyOrder(shippingAddress, customerDetails, order) {
    //notify admin
    notify({
        type: 'order',
        heading: 'New order received',
        text: `New order from ${shippingAddress.firstName} ${shippingAddress.lastName} , total ${order.totalPrice}`,
        eventId: order._id
    });

    //send confirmation mail & sms to customer and admin
    autoMail.eventMail(
        'order',
        customerDetails && customerDetails.email || shippingAddress.email,
        { user: customerDetails || shippingAddress, order }
    );
    autoSMS.eventSMS(
        'order',
        customerDetails && customerDetails.phone || shippingAddress.phone,
        { user: customerDetails || shippingAddress, order }
    )
}

module.exports.notifyOrder = notifyOrder;


class CustomCheckoutError extends Error {
    constructor(code, errorField, errorMessage) {
        super()
        this.statusCode = code;
        this.errorField = errorField;
        this.message = errorMessage;
    }

    get response() {
        if (this.errorList) return this.errorList;
        else return { [this.errorField]: this.message }
    }

}