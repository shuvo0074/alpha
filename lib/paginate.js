const con = require('./db.js');

/**
 *paginate results from db
 *
 * @param {String} collection name of collection
 * @param {Array} preFacetStage operations to run before sort/skip/limit
 * @param {Array} agPipe aggregation pipeLine after sort/skip/limit
 * @param {Number} opts.limit limit of item per page
 * @param {Number} opts.page current page number
 * @returns {Object} object with current, previous, next page info as page, and actual result as data
 */
module.exports = async function paginate( collection, preFacetStage, agPipe, opts){
  let db = await con.db();
 
  let { sort, page, limit, sortOrder } = opts;

  if (sort == 'price') sort = 'price.regular';
  if (sort == 'offerPrice') sort = 'price.offer';

  // console.log(sort, sortOrder)
  //defaults
  if (sort) {
    sort = sort;
  } else sort = 'serial'; //default sort by serial;

  if (sortOrder == '-1') {
    sortOrder = +sortOrder;
  } else sortOrder = 1; //default 1
  
  limit = (isFinite(+limit) && +limit > 0) ? +limit : 50; //default limit is 50
  page = isFinite(+page) ? +page : 1; //default first page

  let aggregatePipeLine = [];

  aggregatePipeLine.push(...preFacetStage);

  //sort, skip, limit

  let sortStage = [
    { $sort: { [sort]: sortOrder, _id: 1 } },
    { $skip: limit * (page - 1) },
    { $limit: limit },
  ]

  //check for any $group stage in agPipe pipeline
  // sort before $group does not work, has to be inserted after $group

  let groupIndex = 0;
  
  //get the last index of $group
  agPipe.forEach((item, index)=>{ 
    if(item['$group'] || item['$replaceRoot']) groupIndex = index;
  })
  agPipe.splice(groupIndex+1, 0, ...sortStage);

  aggregatePipeLine.push({
    $facet: {
      page: [
        {
          $group: {
            _id: null,
            totalIndex: { $sum: 1 },
          },
        },
        {
          $project: {
            _id: 0,
            totalIndex: 1,
          },
        },
      ],
      data: agPipe,
    },
  });

  

  let result = await db.collection(collection).aggregate(aggregatePipeLine).toArray();

  result = result[0];
  if (!result || !result.page || !result.page[0]) return { page: {}, data: [] };

  let pageInfo = result.page[0];
  pageInfo.startingIndex = limit * (page - 1) + 1;
  pageInfo.endingIndex = pageInfo.startingIndex + result.data.length - 1;
  pageInfo.total = Math.ceil(pageInfo.totalIndex / limit);
  pageInfo.current = page;
  pageInfo.next = pageInfo.total > page ? page + 1 : null;
  pageInfo.previous = page > 1 ? page - 1 : null;
  pageInfo.limit = limit;
  pageInfo.items = result.data.length;
  result.page = pageInfo;
  return result;
};
 