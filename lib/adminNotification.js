const con = require('./db.js');

/**
 *save a notification to db
 *
 * @param {Object} notification
 * @returns
 *
 *
 * Example:
 *
 *      let notification = {
 *          type: 'order',
 *          heading: 'new order',
 *          text: 'new order of BDT 2000'
 *          
 *      }
 *      notify(notification);
 */

async function notify(notification) {
  notification = {
    ...notification,
    added: new Date(),
    read: false
  };
  let db = await con.db();
  await db.collection('adminNotification').insertOne(notification);
  return;
}
module.exports.notify = notify;


/**
 * notification mark as read
 *
 * @param {Array} id _id of notification
 * @returns
 */

async function markRead(id) {
  let db = await con.db();

  let result = await db.collection('adminNotification').updateOne(
    { _id: {$in: id}}, 
    { $set: { read: true} }
    );
  
  if(result.matchedCount == 0 ) return false;

  return true;
}
module.exports.markRead = markRead;


/**
 * all notification mark as read
 *
 * @returns
 */

async function markReadAll() {
  let db = await con.db();

  let result = await db.collection('adminNotification').updateMany(
    {}, 
    { $set: { read: true} }
  );
  return true;
}
module.exports.markReadAll = markReadAll;

/**
 * notification mark as unread
 *
 * @param {Array} id _id of notification
 * @returns
 */

async function markUnread(id) {
  let db = await con.db();

  let result = await db.collection('adminNotification').updateOne(
    { _id: {$in: id}}, 
    { $set: { read: false} }
    );
  
  if(result.matchedCount == 0 ) return false;

  return true;
}
module.exports.markUnread = markUnread;


/**
 * all notification mark as unread
 *
 * @returns
 */

async function markUnreadAll() {
  let db = await con.db();

  let result = await db.collection('adminNotification').updateMany(
    {}, 
    { $set: { read: false} }
  );
  return true;
}
module.exports.markUnreadAll = markUnreadAll;
