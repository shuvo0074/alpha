const fileType = require("file-type");


/**
 * 
 * @param {Array} images images from request files
 */
async function image(images){
    // *validate images mime types
		let validMimeTypes = [
			"image/bmp",
			"image/gif",
			"image/jpeg",
			"image/png",
			"image/webp",
			"image/x-icon",
		];

		let mimeErrors = [];
		let mimeCheck = images.map(image => fileType.fromFile(image.path));
		await Promise.all(mimeCheck).then(types => {
			types.forEach(type => {
				if (!validMimeTypes.includes(type.mime)) {
					mimeErrors.push({ type: `invalid image type ${type.mime || ''}` });
				}
			})

		})
        return mimeErrors;
}

module.exports.image = image;