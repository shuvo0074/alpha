const con = require('./db.js');
const path = require('path');

function removeNonUrlCharacters(str){
  //replace non-url friendly characters with - 
  return str.replace(/[<>#%$+,{}|\\^~\[`\];\/?:@=&]/g, '')
  .replace(/\s+/g, '-')
  .toLowerCase();
}
module.exports.removeNonUrlCharacters = removeNonUrlCharacters;

async function findUnusedUrl(type, url, currentId, count = '',) {

  let db = await con.db();

  let existingWithUrl = await db.collection(type).findOne({ url: url + count });
  if (!existingWithUrl) return url + count;
  if (existingWithUrl._id.toHexString() == currentId) return url+count
  return await findUnusedUrl(type, url, currentId, +count + 1);

}

async function createUrl(slug, parentSlug, currentId, collection){

  slug = removeNonUrlCharacters(slug);
  parentSlug = parentSlug ? removeNonUrlCharacters(parentSlug) : '';

  let url = path.posix.join('/'+collection, parentSlug, slug);
  url = await findUnusedUrl(collection, url, currentId);
  
  return url;
}

// export with differentNames for easy use
module.exports.categoryUrl = (slug, parentSlug, currentId)=>createUrl(slug, parentSlug, currentId, 'category');
module.exports.productUrl = (slug, parentSlug, currentId)=>createUrl(slug, parentSlug, currentId, 'product');
module.exports.postUrl = (slug, parentSlug, currentId) => createUrl(slug, parentSlug, currentId, 'post')

module.exports.postUrl = (slug, parentSlug, currentId)=>createUrl(slug, parentSlug, currentId, 'post');
module.exports.postCategoryUrl = (slug, parentSlug, currentId)=>createUrl(slug, parentSlug, currentId, 'postCategory');

// no parentSlug for brand, tag, page 
module.exports.brandUrl = (slug, currentId)=>createUrl(slug, null, currentId, 'brand');
module.exports.tagUrl = (slug, currentId)=>createUrl(slug, null, currentId, 'tag');
module.exports.pageUrl = (slug, currentId)=>createUrl(slug, null, currentId, 'page');