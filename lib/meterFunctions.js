const getDBUsage = require('./meter/calculateDbUsage.js');
const getDiskUsage = require('./meter/calculateDiskSpace.js');
const getProductUsage = require('./meter/calculateProductUsage.js');
const getPageUsage = require('./meter/calculatePageUsage.js');

/**
 * check whether a resource is available or not :
 *
 * @param {Object} req - the request object
 * @param {String} propertyName - a perticular resource name
 * @returns {Boolean}
 *
 * Example
 *
 * ```
 * await isResourceAvailable({..reqObject},'product');
 * // returns true
 *
 * ```
 */

async function isResourceAvailable(req, propertyName) {
  switch (propertyName) {
    case 'disk':
      let diskInfo = await getDiskUsage(req);
      if (diskInfo.used >= diskInfo.total) return false;
      else return true;
      break;

    case 'db':
      let dbInfo = await getDBUsage(req);
      if (dbInfo.used >= dbInfo.total) return false;
      else return true;
      break;

    case 'product':
      let productInfo = await getProductUsage(req);
      if (productInfo.used >= productInfo.total) return false;
      else return true;
      break;

    case 'page':
      let pageInfo = await getPageUsage(req);
      if (pageInfo.used >= pageInfo.total) return false;
      else return true;
      break;

    default:
      break;
  }
}

/**
 *get resources information e.g: availability, remaining and total space
 *
 * @param {Object} req - the request object
 * @returns {Object} resources usage info
 *
 * Example:
 *
 * ```
 * await getAllResourceInfo(req)
 *
 * //Returns:
 * {disk:{available:100, used: 50, total:150}}
 * {db:{available:100, used: 50, total:150}}
 * {product:{available:100, used: 50, total:150}}
 * {page:{available:100, used: 50, total:150} }
 *
 * ```
 */

async function getAllResourceInfo(req) {
  if (!req || typeof req != 'object') {
    throw new TypeError('req must be an Object, received type ' + typeof req);
  }

  const diskUsage = await getDiskUsage(req);
  const productUsage = await getProductUsage(req);
  const pageUsage = await getPageUsage(req);
  const dbUsage = await getDBUsage(req);

  return {
    disk: diskUsage,
    product: productUsage,
    db: dbUsage,
    page: pageUsage
  };
}

/**
 *check whether resources is available or not e.g : {}
 * @param {Object} req - request object
 * @returns {Boolean}
 *
 * Example:
 *
 * ```
 * await isAllResourceAvailable(req);
 *
 * // returns true
 * ```
 *
 */
async function isAllResourceAvailable(req) {
  if (!req || typeof req != 'object') {
    throw new TypeError('req must be an Object, received type ' + typeof req);
  }

  if (
    (await isResourceAvailable(req, 'db')) &&
    (await isResourceAvailable(req, 'disk')) &&
    (await isResourceAvailable(req, 'product'))
  )
    return true;
  else return false;
}

/**
 * Check the resources and see if there is any free space left
 *
 * @param {Object} req - the request object
 * @param {Array} props - propertyNames ['product','disk']
 * @returns {Object | Boolean}
 *
 * Example:
 *
 * ```
 * await usedResource(req,['disk','product'])
 *
 * // returns false
 *
 * ```
 */
async function usedResource(req, ...props) {
  if (!req || typeof req != 'object') {
    throw new TypeError('req must be an Object, received type ' + typeof req);
  }

  let missingResource = [];
  let validResource = ['db', 'disk', 'product', 'page'];

  for (let item of props) {
    if (!validResource.includes(item)) {
      throw new Error(item + ' is not a valid resource label');
    }
    let availability = await isResourceAvailable(req, item);

    if (!availability) {
      missingResource.push(item);
    }
  }

  if (missingResource.length <= 0) {
    return false;
  } else {
    return missingResource;
  }
}

module.exports = {
  getDBUsage,
  getDiskUsage,
  getProductUsage,
  isResourceAvailable,
  isAllResourceAvailable,
  getAllResourceInfo,
  usedResource
};
