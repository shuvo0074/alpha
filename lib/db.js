"use strict";

//cloud srv
//mongodb+srv://imtiaz:555666@cluster0-kczzy.gcp.mongodb.net/test?retryWrites=true&w=majority
const fs = require("fs");

//for dev purpose
//TODO: remove while publishing
let localMongo = "mongodb://127.0.0.1:27017";
let mongoSrv = "mongodb+srv://imtiaz:555666@cluster0-kczzy.gcp.mongodb.net/test?retryWrites=true&w=majority";

const MongoClient = require("mongodb").MongoClient;

const url = localMongo;
// const url = mongoSrv;

let { ObjectID } = require("mongodb");

class Client {
  constructor() {
    this.client = null;
    this.ObjectID = ObjectID;
    this.dbName;

    this.db = this.db.bind(this);
    this.connect = this.connect.bind(this);
  }

  async connect() {
    try {
      if (!this.client) {
        try {
          this.client = await MongoClient.connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
          });
          //as of mongodb 4.0.13 MongoClient.connect return client object
          return this.client;
        } catch (err) {
          /**
           * logging errors to file as cpanel has no console log view
           */
          fs.appendFile("./.logs", JSON.stringify(err), (errr) => {
            if (errr) {
              // :| what to do ? lol Just kidding
            }
          });
          console.log(err);
          throw err;
        }
      }
      return this.client;
    } catch (err) {
      throw err;
    }
  }

  async get() {
    try {
      //return client if already connected
      if (this.client) return Promise.resolve(this.client);
      return await this.connect(); //or create new client and return
    } catch (err) {
      return Promise.reject(err);
    }
  }

  async db(dbName) {
    try {
      if (!this.client) await this.connect(); //create client if not created already
      dbName = dbName ? dbName : this.dbName;
      let _db = this.client.db(dbName);
      return _db;
    } catch (err) {
      throw err;
    }
  }

  async collection(collectionName, dbName) {
    try {
      if (!this.client) await this.connect(); //create client if not created
      dbName = dbName ? dbName : this.dbName;
      let _collection = this.client.db(dbName).collection(collectionName);
      return Promise.resolve(_collection);
    } catch (err) {
      return Promise.reject(err);
    }
  }

  async close() {
    try {
      if (this.client) {
        //close client only if created beforehand
        await this.client.close();
        this.client = null; //set to initial fo re-creation
      }
    } catch (err) {
      return Promise.reject(err);
    }
  }
}

let client = new Client();
module.exports = client;

// db.admin.insert({ "name": "Admin", "email": "admin", "phone": "123", "password": "$2b$10$6Lo1C9zzmt1xcAbvRqt5yucnmCLCBA8g8J5PoBgQBqJWFmSDGnXxu", "role": ["superAdmin"]});
// db.admin.insert({ "name": "Development Admin", "email": "devAdmin", "phone": "devAdmin", "password": "$2b$10$GbPvPFcll5z9e/zMwWu1JOrf4NKUMeuDbDiFzHf499w6VAhvX3KSK", "role": ["superAdmin"]});
