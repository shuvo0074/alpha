const session = require("express-session");
const MongoStore = require("connect-mongo")(session);
const con = require("./db.js");



let sessionStore = new MongoStore({
    clientPromise: con.get(),
    dbName: 'alpha' // TODO: dynamically set db name according to domainMap
});

const sessionOptions = {
    key: 'sesTkn',
    secret: 'BKJER$%u%RERBKjbkjrer&%76rbek%&#7fj',
    store: sessionStore,
    resave: true,
    saveUninitialized: false,
    cookie: { httpOnly: true }
};

module.exports = session(sessionOptions);