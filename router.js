const express = require('express');
const router = express.Router();
const ejs = require('ejs');
const path = require('path');
const con = require('./lib/db.js');

const adminRouter = require('./routes/admin.js');
const customerRouter = require('./routes/customer.js');
const dealerRouter = require('./routes/dealer.js');
const publicRouter = require('./routes/public.js');
 
router.get('/socket', async(req, res)=>{
    res.render('socketTest.html');
})

router.use('/admin', adminRouter);
router.use('/customer', customerRouter);
router.use('/dealer', dealerRouter);
router.use('/', publicRouter);

module.exports = router;
