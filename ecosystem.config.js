module.exports = {
  apps : [{
    script: './lib/www',
    watch: '.'
  }],

  deploy : {
    production : {
      user : 'piprastore',
      host : '139.59.3.127',
      ref  : 'origin/master',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production',
    }
  }
};
