//initiate imageLibrary on image input
let defImg = new ImageLibrary(document.getElementById(`imageInput`));

let itemCounter = 0, component={items:[]}, elementCounter=0;
 
function setComponentName(ev){
    let name = ev.target.value;
    component.name = name;
}


function addItem(itemName='', item = {}){
    let items = document.getElementById('items');
    itemCounter++;
    let itemId = itemName? itemName.replace('item', ''):itemCounter; //char after 'item' is the id (e.g: item1, item2, item11)

    let container = document.createElement('div');//create vdom element, appending string html resets the form
    container.className = 'mt-3 mb-3 p-2 border border-primary ' //bootstrap classes
    container.setAttribute('id', 'item'+itemId);
    let html = `
    <div class="input-group mt-1">
        <div class="input-group-prepend">
            <label for="" class="input-group-text">Item Name</label>
        </div>
        <input type="text" name="item${itemId}name" id="" class="form-control" value="${item.name || ''}">
        <div class="input-group-prepend">
            <label for="" class="input-group-text">Target Url</label>
        </div>
        <input type="url" name="item${itemId}target" id="" placeholder='Target url' value="${item.target || ''}" class="form-control">
    </div>
    <div class="input-group mt-1">
        <div class="input-group-prepend">
            <label for="" class="input-group-text">Iframe</label>
        </div>
        <input type="url" name="item${itemId}iframe"  placeholder='Iframe url' value="${item.iframe || ''}" class="form-control">
        <div class="input-group-prepend">
            <label for="" class="input-group-text">Icon</label>
        </div>
        <input type="text"  name="item${itemId}icon"  value="${item.icon || ''}" placeholder='Press (windows + .) for icon' class="form-control">
    </div>
    <div class="input-group mt-1">
        <div class="input-group-prepend">
            <label for="" class="input-group-text">Image</label>
        </div>
        <div class='p-0 pl-3 form-control' style='height:auto !important'>
            <input type="text" name="item${itemId}image" id="imageInput${itemId}" class="form-control">
        </div>
        <div class="input-group-prepend">
            <label for="" class="input-group-text">Heading</label>
        </div>
        <input type="text" name="item${itemId}heading" id="headingInput${itemId}" class="form-control">
        
    </div>
    <div class="input-group mt-1">
        <div class="input-group-prepend">
            <label for="" class="input-group-text">Text</label>
        </div>
        <textarea name="item${itemId}text" id="" rows="3" class='form-control'>${item.text || ''}</textarea>
    </div>
    <button class=' mt-1 btn btn-danger fal fa-trash-alt' type='button' onclick='removeItem("${itemId}")'> Delete Item</button>
    `
    container.innerHTML = html; //add html inside vdom div
    items.appendChild(container); //append vdom div to items

    //initiate imageLibrary on image input
    new ImageLibrary(document.getElementById(`imageInput${itemId}`), item.image);
}
document.componentForm.reset();

async function removeItem(id){
    let item = document.getElementById('item'+id);
    if(item){
        item.parentElement.removeChild(item);
    }
}
async function editComponent(id){
    //clear 
    let items = document.getElementById('items');
    items.innerHTML = '';

    let _component = components.find(item=> item._id == id);
    document.componentForm.id.value = _component._id;
    document.componentForm.componentName.value = _component.name;
    document.componentForm.componentName.setAttribute('readonly', 'readonly')

    document.componentForm.iframe.value = _component.iframe || '';
    document.componentForm.icon.value = _component.icon || '';
    document.componentForm.heading.value  = _component.heading || '' ;
    document.componentForm.text.value  = _component.text || '';

    defImg.addPreselectImage(_component.image);

    for(let item in _component.items){
        addItem(item, _component.items[item])
    }
    component = _component;
}

async function delComponent(id){
    let ans = confirm('Developer Only. \n Are you sure you want to delete this component? \n note: this can not be undone');
    if(!ans) return false;
    let res = await fetch(`${baseUrl}/admin/component/update/component/delete/${id}`, {
        credentials: 'include'
    });
    if(!res.ok) return showErrors(await res.json());
    window.location.reload();
}

async function submitComponent(ev){
    ev.preventDefault();
    let id = document.getElementById('componentId').value;
    let spinner = showSpinner();//show spinner and store return 

    let url = '';
    if(id){ //update component
        url = '/admin/component/update'
    }else{
        url = '/admin/component/add'
    }
    let res = await fetch(`${baseUrl}${url}`, {
        method:'post',
        body:new FormData(ev.target),
        credentials: 'include',
    })
    let obj = await res.json();

    removeSpinner(spinner);//remove spinner
    
    if(res.ok){
        showMsg(obj.msg);
        console.log(obj)
        location.reload();
    }else{
        showErrors(obj);
    }

}
function resetForm(){
    document.componentForm.reset();
    document.componentForm.componentName.removeAttribute('readonly');
    document.componentForm.id.value='';
    
    //clear 
    let items = document.getElementById('items');
    items.innerHTML = '';
 
}