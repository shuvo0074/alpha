
async function submitEmailSettings(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);
    let spinner = showSpinner();//show spinner

    let res = await fetch(`${baseUrl}/admin/email/update`, {
        method:'post',
        body:formData,
        credentials: 'include'
    })
    removeSpinner(spinner); //remove spinner

    let obj = await res.json();
    console.log(obj)
    if(res.ok){
        showMsg(obj.msg);
    }else{
        showErrors(obj);
    }

}


async function sendEmail(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);

    //get editor html data
    let editorData = editor.getData();
    formData.append('html', editorData);

    let spinner = showSpinner();//show spinner

    let res = await fetch(`${baseUrl}/admin/email/send`, {
        method:'post',
        body:formData,
        credentials: 'include'
    })
    removeSpinner(spinner); //remove spinner

    let obj = await res.json();
    console.log(obj)
    if(res.ok){
        showMsg(obj.msg);
    }else{
        showErrors(obj);
    }

}
async function deleteEmail(id){
    
    
    let confirmation = confirm('Delete Email?' );
    if(!confirmation) return false
    
    let spinner = showSpinner();//show spinner
    let res = await fetch(`${baseUrl}/admin/email/update/delete/sent/${id}`, {
        method:'get',
    })
    removeSpinner(spinner); //remove spinner
    
    let obj = await res.json();
    if(res.ok){
        window.location = '/admin/email/sent'
    }else{
        showErrors(obj);
    }
    
}



async function submitAutoConfig(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);

    let spinner = showSpinner();//show spinner

    let res = await fetch(`${baseUrl}/admin/email/update/auto`, {
        method:'post',
        body:formData,
        credentials: 'include'
    })
    removeSpinner(spinner); //remove spinner

    let obj = await res.json();
    if(res.ok){
        showMsg(obj.msg);
    }else{
        showErrors(obj);
    }

}
async function submitTemplate(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);
    formData.append('customerBody', customerEditor.getData());
    formData.append('adminBody', adminEditor.getData());

    let spinner = showSpinner();//show spinner

    let res = await fetch(`${baseUrl}/admin/email/update/auto/template`, {
        method:'post',
        body:formData,
        credentials: 'include'
    })
    removeSpinner(spinner); //remove spinner

    let obj = await res.json();
    if(res.ok){
        showMsg(obj.msg);
    }else{
        showErrors(obj);
    }

}