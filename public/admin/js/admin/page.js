
async function submitPage(ev) {
  ev.preventDefault();
  
  let formData = new FormData(document.getElementById('pageForm'));
  let htmlData = editor.getData();
  formData.append('content', htmlData);
  let pageId = document.getElementById('pageId')
  pageId = pageId? pageId.value : false;


  let spinner = showSpinner(); //show spinner

  let url = '/admin/page/add';
  if (pageId) url = '/admin/page/update/' + pageId;

  let res = await fetch(url, {
    method: 'post',
    body: formData,
    credentials: 'include'
  });

  removeSpinner(spinner); //remove spinner

  let obj = await res.json();
  if (res.ok) {
    showMsg(obj.msg);
    document.getElementById('pageId').value = obj.insertedId;
  } else {
    showErrors(obj);
  }
}



async function delPage(ev){
  let ans = confirm('Are you sure you want to delete this page? \n note: this can not be undone');
  if(!ans) return false;
  let spinner = showSpinner();//show spinner
  
  let res = await fetch(`${baseUrl}/admin/page/update/page/delete/${page._id}`, {
      credentials: 'include'
  });
  removeSpinner(spinner); //remove spinner
  
  if(!res.ok) return showErrors(await res.json());
  window.location = '/admin/page/list'
}