/**
 * reusable images library
 * 
 *
 * @class ImageLibrary
 */
class ImageLibrary{
    /**
     *Creates an instance of ImageLibrary.
     * @param {Object} elem input element with type text, it will be replaced by 'select from library' button
     * @param {Array} preSelect Array of preselected images
     * @param {*} limit no use xD
     * @memberof ImageLibrary
     * 
     * Example
     *  ```js
     *     new ImageLibrary(document.getElementById('image'), [{_id:5ed489cd4g89fe, ...}])
     *  ```
     */
    constructor(elem, preSelect, limit){

        this.selectedImages = preSelect || [];

        //bind this
        this.closeImageLibrary = this.closeImageLibrary.bind(this);
        this.showImageLibrary = this.showImageLibrary.bind(this);
        this.showUploadForm = this.showUploadForm.bind(this);
        this.hideUploadForm = this.hideUploadForm.bind(this);
        this.submitUpdateImage= this.submitUpdateImage.bind(this);
        this.toggleSelectImage= this.toggleSelectImage.bind(this);
        this.uploadImage= this.uploadImage.bind(this);
        this.viewImageDetail= this.viewImageDetail.bind(this);
        this.viewImages= this.viewImages.bind(this);
        this.doneSelecting= this.doneSelecting.bind(this);
        this.target = elem;

        //hide input element and show library button
        elem.style.display = 'none';
        let button = document.createElement('button');
        button.className = 'btn btn-light border border-dark p-1 d-block';
        button.addEventListener('click', this.showImageLibrary);
        button.setAttribute('type', 'button')
        button.innerHTML = `<i class="fal fa-image"></i> Select From Library`
        
        this.miniDisplay = document.createElement('div');
        this.miniDisplay.className='mini-display mt-1';
        this.miniDisplay.id = 'miniDisplay';
        elem.parentElement.appendChild(button);
        elem.parentElement.appendChild(this.miniDisplay);
        this.libraryContainer = document.createElement('div');
        this.libraryContainer.className = 'libraryContainer';
        this.libraryContainer.id = 'libraryContainer';

        this.rowContainer = document.createElement('div');
        this.rowContainer.className='row library bg-light border border-primary';

        this.viewerContainer = document.createElement('div');
        this.viewerContainer.className='viewer col-md-9';
       
        let heading = document.createElement('h5');;
        heading.className = 'text-dark text-center d-flex';
        heading.innerHTML = `
            <span class='d-block col-10 text-center'>
                <i class="fal fa-images"></i>
                Image Library
            </span>
        `
        let doneButton = document.createElement('button');
        doneButton.className='done btn text-success btn-lg col-2';
        doneButton.innerHTML = '<i class="fal fa-check fa-lg"></i> Done'
        doneButton.addEventListener('click', this.doneSelecting);        
        heading.appendChild(doneButton);

        this.viewerContainer.appendChild(heading);

        this.viewer = document.createElement('div');
        this.viewer.className='';

        this.viewer.innerHTML +=`
                        <div class="text-dark text-center m-5">
                            <i class="fas fa-spinner fa-spin"></i>
                            Loading Images
                        </div>
                    ` 

        this.viewerContainer.appendChild(this.viewer);

        this.uploadFormContainer = document.createElement('div');
        this.uploadFormContainer.className='uploadContainer col-md-9 bg-light d-none pb-5 shadow-sm';

        this.uploadForm = document.createElement('form');
        
        this.uploadHeading = document.createElement('h5');
        this.uploadHeading.className = 'text-primary text-center';
        
        this.backButton = document.createElement('button');
        this.backButton.className='btn btn-outline-dark border-0 float-left';
        this.backButton.setAttribute('type', 'button');
        let icon = document.createElement('i');
        icon.className = 'fal fa-arrow-left';
        this.backButton.appendChild(icon);
        this.backButton.appendChild(document.createTextNode(' Back'));
        this.backButton.addEventListener('click', this.showImageLibrary);
        this.uploadHeading.appendChild(this.backButton);

        this.uploadHeading.appendChild(document.createTextNode('Upload Image'));
        this.uploadForm.appendChild(this.uploadHeading);
        this.uploadForm.addEventListener('submit', this.uploadImage);
        let formGroup = document.createElement('div');
        formGroup.className ='form-group mt-4';
        formGroup.innerHTML = `<label for="">Select Image</label>
                            <input type="file" class='form-control' name="images" id="" multiple>`;
        this.uploadForm.appendChild(formGroup);
        let submitBtn = document.createElement('button');
        submitBtn.className = 'btn btn-success';
        submitBtn.setAttribute('type', 'submit');
        submitBtn.innerHTML =  `<i class="fas fa-cloud-upload"></i> Upload`
        this.uploadForm.appendChild(submitBtn);
                    
        this.uploadFormContainer.appendChild(this.uploadForm);
        this.backButton.addEventListener('click', this.hideUploadForm);

        
        this.imageEditor = document.createElement('div');
        this.imageEditor.className='col-12 bg-dark image-editor';
        
        this.sideBar = document.createElement('div');
        this.sideBar.className='sideBar col-md-3 border-left p-0 ';
        
        this.actionBar = document.createElement('div');
        this.actionBar.className='action-bar border-bottom p-1 d-flex justify-content-between ';
        
        this.showUploadFormButton = document.createElement('button');
        this.showUploadFormButton.className = 'btn btn-primary d-block';
        this.showUploadFormButton.addEventListener('click', this.showUploadForm);
        this.showUploadFormButton.innerHTML = `<i class="fal fa-plus"></i>Add Image`
        this.actionBar.appendChild(this.showUploadFormButton);

        this.closeImageLibraryButton = document.createElement('i');
        this.closeImageLibraryButton.className = 'fal fa-times-circle fa-lg text-danger d-block';
        this.closeImageLibraryButton.style = 'align-self: center; cursor:pointer';
        this.closeImageLibraryButton.addEventListener('click', this.closeImageLibrary);
        this.actionBar.appendChild(this.closeImageLibraryButton);

        this.sideBar.appendChild(this.actionBar);

        this.detailView = document.createElement('div');
        this.detailView.className='detailView';
        
        this.previewMsg = document.createElement('div');
        this.previewMsg.className='text-dark text-center mt-5 p-1';
        this.previewMsg.innerHTML = 'Select an image to preview details'
        this.detailView.appendChild(this.previewMsg);

        this.imageEditForm = document.createElement('div');
        this.imageEditForm.className='imageEditForm p-1 d-none';
        this.detailView.appendChild(this.imageEditForm);

        this.sideBar.appendChild(this.detailView);

        this.rowContainer.appendChild(this.viewerContainer);
        this.rowContainer.appendChild(this.uploadFormContainer);
        this.rowContainer.appendChild(this.imageEditor);
        this.rowContainer.appendChild(this.sideBar);

        this.libraryContainer.appendChild(this.rowContainer);
        document.body.appendChild(this.libraryContainer);

        this.limit = limit;
        if(this.selectedImages[0]) this.doneSelecting();
    }


    /**
     *change display of #libraryContainer to block and initiate pullImages
     * @memberof ImageLibrary
     */
    showImageLibrary(){
        this.libraryContainer.style.display='block';
        this.pullImages();
    }

    /**
     *retrieve images from api, save them in this.imageLust and call viewImages()
     *
     * @memberof ImageLibrary
     */
    async pullImages(){
        this.imageList = await fetch('/admin/image/api/list');
        if(this.imageList.ok ){
            this.imageList = await this.imageList.json();
            this.viewImages();
        }else{
            alertify.error(`<i class='fas fa-lg fa-exclamation-triangle'></i> Unable to show image library`);
        }
    }
    
    /**
     *display images based on this.imageList
     *
     * @memberof ImageLibrary
     */
    async viewImages(){
        this.viewer.innerHTML = '';

        if(this.imageList.length == 0){
            let emptyMessage = document.createElement('div');
            emptyMessage.className='emptyMessage';
            emptyMessage.appendChild(document.createTextNode('No Image Found!'));
            this.viewer.appendChild(emptyMessage);
        }else{
            let cardDeck =  document.createElement('div');
            cardDeck.className = 'card-deck justify-content-between';

            for(let image of this.imageList){
                let card = document.createElement('div');
                card.className = 'col-sm-2 imageCard mb-4';
                card.id = `img${image._id}`
                let selected;
                this.selectedImages.forEach(item=>{
                    if(item._id == image._id){
                        selected = true;
                    }
                })
                card.addEventListener('click', ()=>{this.viewImageDetail(image._id)});
                let cardImg = document.createElement('div');
                cardImg.className = `card-img ${selected? 'selected':''}`
                cardImg.innerHTML = `<img class='img-thumbnail gallery-image ' src='${image.thumbnail}'>`
                card.appendChild(cardImg);

                //TODO: convert to vDom
                card.innerHTML += ` 
                    <span class='bg-light text-dark d-block' style='width:100%;overflow:hidden; text-overflow:ellipsis; white-space:nowrap'>${image.title || image.name}</span>
                `
                cardDeck.appendChild(card);
            }
           this.viewer.appendChild(cardDeck);
        }
    }

    /**
     *close imageLibrary
     *
     * @memberof ImageLibrary
     */
    async closeImageLibrary(){
        this.viewer.innerHTML = '';
        this.libraryContainer.style.display='none';
        this.imageEditForm.innerHTML='';
        this.imageEditForm.classList.add('d-none');
        this.previewMsg.classList.remove('d-none');

    }


    /**
     *show new image upload from
     *
     * @memberof ImageLibrary
     */
    async showUploadForm(){
        this.uploadFormContainer.classList.remove('d-none');
    }

    /**
     *hide new image upload form
     *
     * @memberof ImageLibrary
     */
    async hideUploadForm(){
        this.uploadFormContainer.classList.add('d-none');
    }

    /**
     *image upload form handler
     *
     * @param {Object} ev then event object
     * @memberof ImageLibrary
     */
    async uploadImage(ev){
        ev.preventDefault();

        let formData= new FormData(ev.target);
            
        let spinner = showSpinner();//show spinner
    
        let res = await fetch(`${baseUrl}/admin/image/add`, {
            method:'post',
            body:formData,
            credentials: 'include'
        })
        removeSpinner(spinner); //remove spinner
        
        let obj = await res.json();
        if(res.ok){
            showMsg(obj.msg);
            this.pullImages(); //refresh interface
        }else{
            showErrors(obj);
        }

    }

    /**
     *toggles class 'selected' on image
     *
     * @param {String} id 
     * @memberof ImageLibrary
     */
    async toggleSelectImage(id){
        let card = this.libraryContainer.querySelector(`#img${id} .card-img`);
        let selected = false;
        let selectedIndex;

        for(let x of this.selectedImages){ //check if image is selected already
            if(x._id == id){
                selected = true;
                selectedIndex = this.selectedImages.indexOf(x);
                break;
            }
        }

        if(selected){ //remove from selection
            card && card.classList.remove('selected');
            this.selectedImages.splice(selectedIndex, 1);
        }else{
            let image = this.imageList.find(item=>item._id == id);
            this.selectedImages.push(image);
            card && card.classList.add('selected');
        }
    } 

    /**
     *displays image information on right panel
     *
     * @param {String} id image id
     * @memberof ImageLibrary
     */
    async viewImageDetail(id){
        this.imageEditForm.innerHTML='';

        let image;
        this.toggleSelectImage(id);
        image = this.imageList.find(item=>item._id == id);

        let container = document.createElement('div');
        container.className = 'mt-1 mb-2 d-flex justify-content-between';

        let editButton = document.createElement('button');
        editButton.className = 'btn text-primary d-block';
        editButton.addEventListener('click', ()=>{this.editImage(image._id)});
        editButton.innerHTML = `<i class="fal fa-palette"></i> Edit Image`
        container.appendChild(editButton);

        let deleteButton = document.createElement('button');
        deleteButton.className = 'btn text-danger d-block';
        deleteButton.addEventListener('click', ()=>{this.deleteImage(image._id)});
        deleteButton.innerHTML = `<i class="fas fa-trash"></i> Delete Image`
        container.appendChild(deleteButton);
        this.imageEditForm.appendChild(container);

        let updateForm = document.createElement('form');
        updateForm.className = 'btn text-danger d-block';
        updateForm.addEventListener('submit', (event)=>{this.submitUpdateImage(event)});
        updateForm.innerHTML = `<input type="hidden" name="id" value='${image._id}'>
            <div class="form-group mt-4">
                <label for="">Name:</label>
                <div class="text-dark">${image.name} </div>
            </div>
            
            <div class="form-group mt-4">
                <label for="">Alternate Text</label>
                <input type="text" value='${image.alt}' class='form-control' name="alt" id="" placeholder='alternate text'>
            </div>
            
            <div class="form-group mt-4">
                <label for="">Title</label>
                <input type="text" value='${image.title}' class='form-control' name="title" id="" placeholder='Image Title'>
            </div>
            
            <div class="form-group mt-4">
                <label for="">Caption</label>
                <input type="text" value='${image.caption}' class='form-control' name="caption" id="" placeholder="Image caption" >
            </div>
            
            <div class="form-group mt-4">
                <label for="">Labels</label>
                <input type="text" value='${image.labels}' class='form-control' name="labels" id="" placeholder='image labels: product, edited'>
            </div>

            <button class="btn btn-success " type='submit'>
                <i class="fas fa-save"></i> Update
            </button>`
        this.imageEditForm.appendChild(updateForm);

        this.previewMsg.classList.add('d-none');
        this.imageEditForm.classList.remove('d-none');
    }

    /**
     *image information update form handler
     *
     * @param {Object} ev the Event object
     * @memberof ImageLibrary
     */
    async submitUpdateImage(ev){
        ev.preventDefault();

        let formData= new FormData(ev.target);
            
        let spinner = showSpinner();//show spinner
    
        let res = await fetch(`${baseUrl}/admin/api/image/update`, {
            method:'post',
            body:formData,
            credentials: 'include'
        })
        removeSpinner(spinner); //remove spinner
        
        let obj = await res.json();
        if(res.ok){
            showMsg(obj.msg);
        }else{
            showErrors(obj);
        }
    }

    /**
     *displays selected images in mini display below select button 
     * and sets their id as the value of target joined by ,
     *
     * @memberof ImageLibrary
     */
    async doneSelecting(){
        this.closeImageLibrary();
        this.miniDisplay.innerHTML = '';
        let selectedId = [];
        for(let x of this.selectedImages){
            selectedId.push(x._id);

            let box = document.createElement('div');
            box.className ='mini-img-box d-inline-block m-1';
            box.style.position = 'relative';

            let miniImg = document.createElement('img');
            miniImg.className = 'img-thumbnail ';
            miniImg.setAttribute('src', x.icon);

            let removeBtn = document.createElement('button');
            removeBtn.className = 'btn btn-outline-danger remove-btn';
            removeBtn.innerHTML = `
                <i class='fal fa-trash-alt fa-lg'></i>
            `
            removeBtn.onclick = ()=>{
                this.miniDisplay.removeChild(box);
                this.toggleSelectImage(x._id);
                this.doneSelecting();
            }

            box.appendChild(miniImg);
            box.appendChild(removeBtn);
            this.miniDisplay.appendChild(box);
            
        }
        this.target.value = selectedId.join(',');

    }

    /**
     *remove image from this.selectedImages array
     *
     * @param {String} id
     * @memberof ImageLibrary
     */
    async removeSelection(id){
        console.log(this.selectedImages);
        this.selectedImages.forEach((item, index)=>{
            if(item._id == id){
                this.selectedImages.splice(this.selectedImages.indexOf(index), 1);
                console.log('removed');
            }
            console.log(this.selectedImages);
        })
        
    }

    /**
     *sends request to server for deleting image
     * removes image from preview on success
     *
     * @param {String} id
     * @memberof ImageLibrary
     */
    async deleteImage(id){
        let spinner = showSpinner();//show spinner
        
        let res = await fetch(`${baseUrl}/admin/api/image/update/image/delete/${id}`, {
            method:'get',
            credentials: 'include'
        })
        removeSpinner(spinner); //remove spinner
        
        let obj = await res.json();
        if(res.ok){
            showMsg(obj.msg);
            let imageObj = this.imageList.find(item=>item._id == id);
            this.imageList.splice(this.imageList.indexOf(imageObj), 1); //remove from this.imageList
            this.viewImages(); //refresh interface
        }else{
            showErrors(obj);
        }
    }
    async addPreselectImage(preselected){
        this.selectedImages = preselected;
        this.doneSelecting();
    }

}