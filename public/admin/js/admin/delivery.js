

let previousSelect = '';

async function loadCities(ev){
    let country = ev.target.value;

    if(country == previousSelect) return;
    if(country == '' || !country) return;
    
    let res = await fetch(`/api/geo/${country}/city`);
    if(res.ok){
        let cityList = await res.json();
        let exception  = document.getElementById('exception');
        exception.innerHTML = '';

        for(let city of cityList){
            let container = document.createElement('li');

            container.innerHTML = `
                    <input name='${city.name}' type='checkbox'> 
                    <label>${city.name}</label>
            `
            exception.appendChild(container);
            previousSelect = country;
        }
    }else{
        showErrors({'msg':'could not load city list'})
    }

}

let addOrUpdate = 'add';

async function editRegion(id){
    let region = regionList.find(item=>item._id == id); //existing data
    
    let country = region.country;
    if(country == '' || !country) return;

    document.regionForm.name.value = region.name;
    document.regionForm.pickUpLocation.value = region.pickUpLocation;
    document.regionForm.countryName.value = region.countryName;
    document.regionForm.time.value = region.time;
    document.regionForm.regionId.value = region._id;
    addOrUpdate = 'update';

    (async ()=>{let res = await fetch(`/api/geo/${country}/city`); //get city list
        if(res.ok){
            let cityList = await res.json();
            let exception  = document.getElementById('exception');
            exception.innerHTML = '';

            for(let city of cityList){
                let container = document.createElement('li');

                container.innerHTML = `
                        <input name='${city.name}' type='checkbox' ${region.city.includes(city.name)? 'checked' : ''}> 
                        <label>${city.name}</label>
                `
                exception.appendChild(container);
            }

        }else{
            showErrors({'msg':'could not load city list'})
        }
    })();

    chargeTable = region.charge;

    for(let amount in chargeTable){
        let container = document.createElement('tr');
        container.id = 'charge'+amount;
        container.innerHTML = `
            <td>${amount}</td>
            <td>${chargeTable[amount]}</td>
            <td>
                <button class='btn btn-sm btn-outline-danger' onclick = "removeCharge('charge${amount}')">
                    <i class='fal fa-times'></i>
                </button>
            </td>
        `

        document.getElementById('chargeTable').appendChild(container);
    }

}

async function submitDelivery(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);
    formData.append('charge', JSON.stringify(chargeTable));
    
    let spinner = showSpinner();//show spinner

    let res = await fetch(`${baseUrl}/admin/delivery/${addOrUpdate}`, {
        method:'post',
        body:formData,
        credentials: 'include'
    })
    removeSpinner(spinner); //remove spinner
    
    let obj = await res.json();
    if(res.ok){
        showMsg(obj);
        window.location.reload();
    }else{
        showErrors(obj);
    }
}

async function delRegion(id){

    let confirmation = confirm('Delete delivery region?');
    if(!confirmation) return false;

    let spinner = showSpinner();//show spinner

    let res = await fetch(`${baseUrl}/admin/delivery/update/region/delete/${id}`, {
        method:'get',
        credentials: 'include'
    })
    removeSpinner(spinner); //remove spinner
    
    let obj = await res.json();
    if(res.ok){
        showMsg(obj);
        window.location.reload();
    }else{
        showErrors(obj);
    }
}
function selectAllCity(){
    let cities = document.querySelectorAll("#exception li input");
    for(let city of cities){
        city.checked = !city.checked;
    }
}

let chargeTable={};

function addCharge(){
    let amount = document.getElementById("amount");
    let charge = document.getElementById("charge");

    
    //remove existing entry with same amount
    let entryWithSameAmount = document.getElementById('charge'+amount.value);
    if(entryWithSameAmount) document.getElementById('chargeTable').removeChild(entryWithSameAmount);
    if(chargeTable[amount.value]) delete chargeTable[amount.value];
    
    //set in object
    chargeTable[amount.value] = charge.value;

    //show to user
    let container = document.createElement('tr');
    container.id = 'charge'+amount.value;
    container.innerHTML = `
        <td>${amount.value}</td>
        <td>${charge.value}</td>
        <td>
            <button class='btn btn-sm btn-outline-danger' onclick = "removeCharge('charge${amount.value}')">
                <i class='fal fa-times'></i>
            </button>
        </td>
    `

    document.getElementById('chargeTable').appendChild(container);

    //reset inputs
    amount.value = '';
    charge.value = '';
    amount.focus();
}

function removeCharge(id){
    let container = document.getElementById(id);
    let amount = + (id.replace('charge', ''));

    delete chargeTable[amount];
    document.getElementById('chargeTable').removeChild(container);
}