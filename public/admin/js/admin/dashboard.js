async function getPageAnalytics(ev){
    if(ev) ev.preventDefault();
    let formData = new FormData(ev ? ev.target : document.pageForm);
    let type = formData.get('type'); 

    //for initial load
    if(!type) {
        formData.set('type', 'product');
        type = 'product';
    }

    let table = document.getElementById('data');

    let spinner = showSpinner(null,table);//show spinner
    let res = await fetch(`${baseUrl}/admin/api/analytics/pagevisits`, {
        method:'post',
        body:formData,
        credentials: 'include'
    })
    removeSpinner(spinner); //remove spinner

    let data = await res.json();
    if(res.ok){

        table.innerHTML = '';

        for(let visit of data.data){
            let tr = document.createElement('tr');
            let link = visit.item ? `<a href="/admin/${type}/view/${visit.item._id}">${visit.item.name}</a>` : visit._id;
            tr.innerHTML = `
                <td> 
                    ${link}
                </td>
                <td> ${visit.visit} </td>
            `
            table.appendChild(tr);
        }

        //set select page options
        let select = document.getElementById('page');
        select.innerHTML = '';
        for(let i =1 ; i<=data.page.total; i++){
            let option = document.createElement('option');
            option.value = i;
            option.appendChild(document.createTextNode(i));
            select.appendChild(option);
        }
    }else{
        alertify.error('Error retrieving analytic data');
    }
}

getPageAnalytics();

let visitChart;
async function getVisitAnalytics(ev){
    if(ev) ev.preventDefault();

    let formData = new FormData(ev ? ev.target : document.visitForm);
    let spinner = showSpinner(null, document.getElementById('visitChart'));//show spinner

    let visitDataRes = await fetch(`${baseUrl}/admin/api/analytics/user?metric=date`, {
        method:'post',
        body:formData,
        credentials: 'include'
    })
    let newVisitDataRes = await fetch(`${baseUrl}/admin/api/analytics/returninguser?metric=date`, {
        method:'post',
        body:formData,
        credentials: 'include'
    })
    removeSpinner(spinner); //remove spinner

    let visitData = await visitDataRes.json();
    let newVisitData = await newVisitDataRes.json();

    //combine visit and returning visit data
    let graphData = [];
    for(let item of visitData){
        let newUser = newVisitData.find(i=>i.date == item.date);

        let combinedData = {
            date: item.date,
            count: item.count,
            newUserCount: newUser ? newUser.count : 0,
        }
        combinedData.returningCount= newUser ? combinedData.count - combinedData.newUserCount : combinedData.count;

        graphData.push(combinedData);
    }
    if(newVisitDataRes.ok && visitDataRes.ok ){
        if(visitDataRes.length == 0) visitDataRes = [{_id:0, count:0}]
        if(!visitChart){
            visitChart =   new Morris.Line({
                element: 'visitChart',
                
                data: graphData,
    
                xkey: 'date',
                ykeys: ['count', 'returningCount', 'newUserCount'],
                labels:['User', 'Returning User', 'New User'],
                
                lineColors:['#5363bb', '#f6d27d', '#26e4ad']
            })
        }else{
            visitChart.setData(graphData);
        }
       
    }else{
        alertify.error('Error retrieving analytic data');
    }
}
getVisitAnalytics();



async function getBreakDownAnalytics(ev){
    if(ev) ev.preventDefault();

    let formData = new FormData(ev ? ev.target : document.breakDownForm);
    let type = formData.get('type'); 

    //for initial load
    if(!type) {
        formData.set('type', 'platform');
        type = 'platform';
    }

    let spinner = showSpinner(null, document.getElementById('breakDownChart'));//show spinner

    let visitDataRes = await fetch(`${baseUrl}/admin/api/analytics/breakdown?metric=${type}`, {
        method:'post',
        body:formData,
        credentials: 'include'
    })
    removeSpinner(spinner); //remove spinner

    let visitData = await visitDataRes.json();
    visitData = visitData.filter(i=>i._id != null)
    document.getElementById('breakDownChart').innerHTML = '';

    let graphData = [];

    for(let item of visitData){
        let data = {
            label: item[type],
            value: item.count
        }
        graphData.push(data);
    }

    if(visitDataRes.ok ){
        new Morris.Donut({
            element: 'breakDownChart',
            data: graphData,
            lineColor:'#5363bb'
        })
    
    
    }else{
        alertify.error('Error retrieving analytic data');
    }
}
getBreakDownAnalytics();


