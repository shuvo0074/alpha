function showEditForm(){
    $('#updateForm').fadeIn();
    
}

function hideEditForm(){
    $('#updateForm').fadeOut();
} 

let catList = [];
let tagList = [];

function submitProduct(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);
    //get editor html data
    let editorData = editor.getData();
    formData.set('description', editorData);

    //add calculated fields
    formData.append('category', JSON.stringify(catList));
    formData.append('tags', JSON.stringify(tagList));
    formData.append('attributes', JSON.stringify(productAttributes));

    let formObj = {};
    for(x of formData){
        formObj[x[0]] = x[1];
    }

    for(x of formData.keys()){
        let elem = document.getElementsByName(x)[0]
        if(!elem) continue;
        elem.classList.remove('is-invalid');
    }
    let validator = new Validator();
    let errors = validator.addProduct(formObj);
    if(!_.isEmpty(errors)){;
        showErrors(errors);
    }else{
        
        (async function(){
            let spinner = showSpinner();//show spinner
    
            let res = await fetch(`${baseUrl}/admin/product/add`, {
                method:'post',
                body:formData,
                credentials: 'include'
            })
            removeSpinner(spinner); //remove spinner
            
            let obj = await res.json();
            if(res.ok){
                showMsg(obj.msg); 
            }else{
                showErrors(obj);
            }
        })()
    }
}



function submitProductEdit(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);

    //add calculated fields
    formData.append('category', JSON.stringify(catList));
    formData.append('tags', JSON.stringify(tagList));
    formData.append('attributes', JSON.stringify(productAttributes));

    let formObj = {};
    for(x of formData){
        formObj[x[0]] = x[1];
    }
   
    for(x of formData.keys()){
        let elem = document.getElementsByName(x)[0]
        if(!elem) continue;
        elem.classList.remove('is-invalid');
    }
    let validator = new Validator();
    let errors = validator.addProduct(formObj);
    if(false && !_.isEmpty(errors)){;
        showErrors(errors);
    }else{
        
        (async function(){
            let spinner = showSpinner();//show spinner
    
            let res = await fetch(`${baseUrl}/admin/product/update`, {
                method:'post',
                body:formData,
                credentials: 'include'
            })
            removeSpinner(spinner); //remove spinner
            
            let obj = await res.json();
            if(res.ok){
                showMsg(obj.msg);
            }else{
                showErrors(obj);
            }
        })()
    }
}


function submitUpdate(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);
    let formObj = {};
    for(x of formData){
        formObj[x[0]] = x[1];
    }
    let currList = currency.map(item=>{
        return item.name
    })
    let catList = category.map(item=>{
        return item._id
    })
    for(x of formData.keys()){
        let elem = document.getElementsByName(x)[0]
        elem.classList.remove('is-invalid');
    }
    let validator = new Validator();
    let errors = validator.addProduct(formObj, {currency:currList, category:catList});
    if(!_.isEmpty(errors)){;
        showErrors(errors);
    }else{
        
        (async function(){
            let spinner = showSpinner();//show spinner
    
            let res = await fetch(`${baseUrl}/admin/product/update`, {
                method:'post',
                body:formData,
                credentials: 'include'
            })
            removeSpinner(spinner); //remove spinner
            
            let obj = await res.json();
            if(res.ok){
                showMsg(obj.msg);
                window.location.reload();
            }else{
                showErrors(obj);
            }
        })()
    }

}

async function del(image, id){
    let ans = confirm('Are you sure you want to delete this image?');
    if(!ans) return false;

    let spinner = showSpinner();//show spinner
    
    let res = await fetch(`${baseUrl}/admin/product/update/${product._id}/img/delete?image=${image}`, {
        credentials: 'include'
    });
    
    removeSpinner(spinner); //remove spinner
    
    if(!res.ok) return showErrors(await res.json());
    showMsg((await res.json()).msg)
    
    image = document.getElementById('img'+id);
    image.parentElement.removeChild(image);
}

async function makeThumb(path, id){
    let spinner = showSpinner();//show spinner
    let res = await fetch(`${baseUrl}/admin/product/update/${product._id}/img/thumbnail?image=${path}`, {
        credentials: 'include'
    });
    removeSpinner(spinner); //remove spinner
    
    if(!res.ok) return showErrors(await res.json());
    showMsg((await res.json()).msg)
    let otherButton = document.querySelectorAll('#imageEdit .statbtn');
    for(x of otherButton){
        x.className = 'btn btn-info btn-sm statbtn';
        x.innerHTML = `
        <i class="fal fa-image"></i> Set As Thumbnail
        `;
    }
    let imageButton = document.querySelector(`#img${id} .statbtn`);
    imageButton.className = ' btn btn-outline-success btn-sm statbtn';
    imageButton.innerHTML = `
    <i class="fal fa-check"></i> Thumbnail
    `
}

function submitImage(ev){
    ev.preventDefault();
    let formData= new FormData(ev.target);
   
        
    (async function(){
        let spinner = showSpinner();//show spinner
    
        let res = await fetch(`${baseUrl}/admin/product/update/img/add`, {
            method:'post',
            body:formData,
            credentials: 'include'
        })

        removeSpinner(spinner); //remove spinner
        
        let obj = await res.json();
        if(res.ok){
            showMsg(obj.msg);
            window.location.reload();
        }else{
            showErrors(obj);
        }
    })()


}


async function delProduct(ev){
    let ans = confirm('Are you sure you want to delete this product? \n note: this can not be undone');
    if(!ans) return false;
    let spinner = showSpinner();//show spinner
    
    let res = await fetch(`${baseUrl}/admin/product/update/product/delete/${product._id}`, {
        credentials: 'include'
    });
    removeSpinner(spinner); //remove spinner
    
    if(!res.ok) return showErrors(await res.json());
    window.location = '/admin/product/list'
}