class Validator{
    constructor(){
        this.errors={}
    }
    
    adminLogin(flds){
        let {username, password} = flds;
        if(!username) this.errors.username = 'Input your phone number or email'
        if(!password) this.errors.password = 'Input you password'

        return this.errors;
    }

    addProduct(flds, list = {}){
        let {name, unit, brand, category, tags, price} = flds;

        if(!name) this.errors.name = 'Product name can\'t be empty';
        if(!unit) this.errors.unit = 'Insert Unit';

        if(!price) this.errors.price = 'Insert Price';
        if(price && !isFinite(price)) this.errors.price = 'Invalid Price';
        //TODO:ADD MORE VALIDATION 

        return this.errors;
    }
    addCategory(flds, list={}){
        let {name, type, parent} = flds;
        if(!name) this.errors.name = 'Empty Name'
        if(!type) this.errors.type = 'Select Type'
        if(type=='sub' && !parent) this.errors.parent = 'Select Parent category for Sub category'
        return this.errors;
    }
    addBrand(flds){
        let {name} = flds;
        if(!name) this.errors.name = 'Empty Name';
        if(name && name.length < 3) this.errors.name = 'Name should have at least 3 characters'
        if(name && name.length > 30) this.errors.name = 'Name too long'
        return this.errors;
    }
    addAttributes(flds){
        let {name} = flds;
        if(!name) this.errors.name = 'Empty Name';
        if(name && name.length < 2) this.errors.name = 'Name should have at least 2 characters'
        if(name && name.length > 30) this.errors.name = 'Name too long'
        return this.errors;
    }
    addTag(flds){
        let {name} = flds;
        if(!name) this.errors.name = 'Empty Name';
        if(name && name.length < 2) this.errors.name = 'Name should have at least 2 characters'
        if(name && name.length > 30) this.errors.name = 'Name too long'
        return this.errors;
    }
    
}
