
async function submitSiteSettings(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);

    let spinner = showSpinner();//show spinner

    let res = await fetch(`${baseUrl}/admin/siteSettings/update`, {
        method:'post',
        body:formData,
        credentials: 'include'
    })
    removeSpinner(spinner); //remove spinner

    let obj = await res.json();
    if(res.ok){
        showMsg(obj.msg);
    }else{
        showErrors(obj);
    }

}
