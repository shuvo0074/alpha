
async function createEditor(elem, toolbar){
    let editor = await DecoupledDocumentEditor
    .create( elem, {
        toolbar: {
                items: [
                    'undo',
                    'redo',
                    'heading',
                    '|',
                    'fontSize',
                    'fontFamily',
                    'fontColor',
                    'fontBackgroundColor',
                    '|',
                    'bold',
                    'italic',
                    'underline',
                    'highlight',
                    '|',
                    'alignment',
                    '|',
                    'numberedList',
                    'bulletedList',
                    'todoList',
                    '|',
                    'indent',
                    'outdent',
                    '|',
                    'link',
                    'blockQuote',
                    'imageUpload',
                    'insertTable',
                    'mediaEmbed',
                    '|',
                    'code',
                    'codeBlock',
                    'MathType',
                    'ChemType',
                    'removeFormat',
                    'specialCharacters',
                    'pageBreak',
                    '|',
                    'strikethrough',
                    'superscript',
                    'subscript',

                ]
            },
            language: 'en',
            image: {
                toolbar: [
                    'imageTextAlternative',
                    'imageStyle:full',
                    'imageStyle:side'
                ]
            },
            table: {
                contentToolbar: [
                    'tableColumn',
                    'tableRow',
                    'mergeTableCells',
                    'tableCellProperties',
                    'tableProperties'
                ]
            },
            licenseKey: '',
            
        
    });
    
    const toolbarContainer = toolbar || document.querySelector( '#toolbar-container' );

    toolbarContainer.appendChild( editor.ui.view.toolbar.element );
    return editor
    
}