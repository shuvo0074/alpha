
function showSpinner(icon, parent){
    icon = icon? icon : 'fan'; //default parameter doesn't work while null passed
    let spinnerContainer =  document.createElement('div');
    spinnerContainer.className= 'spinnerContainer';
    spinnerContainer.setAttribute('id', 'spinner');
    let html=`
        <i class='fal fa-${icon} fa-spin fa-5x text-dark spinner'></i>
    `
    spinnerContainer.innerHTML = html;

    if(parent){
        spinnerContainer.classList.add('childSpinner')
        parent.appendChild(spinnerContainer);

    } 
    else document.body.appendChild(spinnerContainer);

    return spinnerContainer; //return spinnerContainer for removeSpinner
}

function removeSpinner(spinnerContainer){
    spinnerContainer.parentElement.removeChild(spinnerContainer);
}