
let productList=[];
async function addProduct(ev){
    let viewer = document.getElementById('productViewer');
    let productId = ev.target.value;
    productList.push({_id:productId, quantity:1}); //default quantity
    let div = document.createElement('div');
    let product = products.find(item=>{
        return item._id == productId;
    }) 
    let str  =`
        <div class='border m-1 p-3'>
            <span class='text-primary'>${product.name}</span>
            <span class='float-right'>
                <label>Quantity:</label>
                <input type='number' class='border pl-2' oninput='setQuantity(event, "${productId}")' value='1'>
                <button class='btn btn-outline-danger btn-sm ml-3' onclick='removeProduct(event, "${productId}")'>&times;</button>
            </span>
        </div>
    `
    div.innerHTML = str;
    div.className='mt-3';
    div.setAttribute('id', 'view'+productId);
    viewer.appendChild(div);
}

function submitOrder(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);
    formData.append('products', JSON.stringify(productList)); //append products
    let formObj = {};
    for(let x of formData){
        formObj[x[0]] = x[1];
    }

    let validator = new Validator();
    let errors = {} //validator.addCategory(formObj, {category:catList}); //TODO: add validation
    if(!_.isEmpty(errors)){;
        showErrors(errors);
    }else{
        
        (async function(){
            let res = await fetch(`${baseUrl}/admin/order/add`, {
                method:'post',
                body:formData,
                credentials: 'include'
            })
            let obj = await res.json();
            if(res.ok){
                showMsg(obj.msg);
            }else{
                showErrors(obj);
            }
        })()
    }

}


function setQuantity(ev, id){
    let product = productList.find(item=>{
        return item._id == id;
    })
    product.quantity = ev.target.value 
    console.log(productList);
}

function removeProduct(ev, id){
    let view = document.getElementById('view'+id);
    view.parentElement.removeChild(view); //remove from view

    //find the product in list
    let product = productList.find(item=>{
        return item._id == id;
    })
    productList.splice(productList.indexOf(product), 1) //remove from array
}

async function mark(status){
    let spinner = showSpinner()
    let res = await fetch(`${baseUrl}/admin/order/update/changeStatus/${orderId}?status=${status}`, {
        method:'get',
        credentials: 'include'
    })
    removeSpinner(spinner)
    let obj = await res.json();
    if(res.ok){
        showMsg(obj.msg);
        window.location.reload();   
    }else{
        showErrors(obj);
    }
}
async function deleteOrder(id){
    let confirmation = confirm('Delete Order?');
    if(!confirmation) return false;
    let spinner = showSpinner()
    let res = await fetch(`${baseUrl}/admin/order/update/order/delete/${id}`, {
        method:'get',
        credentials: 'include'
    })
    removeSpinner(spinner)
    let obj = await res.json();
    if(res.ok){
        showMsg(obj.msg);
        window.location='/admin/order/list'   
    }else{
        showErrors(obj);
    }
}


let previousSelect = ''; //prevent from loading every click

async function loadCity(){

    let country = document.order.country.value;
    if(country == previousSelect) return false; //prevent from loading every click

    let citySelect = document.order.city;

    let spinner = showSpinner();//show spinner
    
    let res = await fetch(`/api/geo/${country}/city`);
    removeSpinner(spinner); //remove spinner
    
    if(!res.ok) return showErrors(await res.json());    

    let cityList = await res.json();
    for(let city of cityList){

        let option = document.createElement('option');
        option.setAttribute('value', city.name);
        option.appendChild(document.createTextNode(city.name));

        citySelect.appendChild(option);
    }

    previousSelect = country; //set for next click
}

async function loadCustomerAddress(){
    let useProfileAddress = document.order.useAccountBillingAddress;
    if(!useProfileAddress.checked) return false;

    let customer = document.order.customerId.value;
    if(!customer) return showErrors({customerId:'Select customer'})
    let spinner = showSpinner();//show spinner
    
    let res = await fetch(`/admin/api/customer/${customer}`);
    removeSpinner(spinner); //remove spinner
    if(!res.ok) return showErrors(await res.json());  

    customer = await res.json();

    document.order.firstName.value = customer.firstName;
    document.order.lastName.value = customer.lastName;
    document.order.country.value = customer.country;
    document.order.phone.value = customer.phone;
    document.order.email.value = customer.email;
    await loadCity();
    document.order.city.value = customer.city;
    document.order.address1.value = customer.address1;
    document.order.address2.value = customer.address2;

}
function differentShippingAddress(){
    if(document.order.shipToDifferentAddress.checked){
        $('#shippingAddress').slideDown()
    }else{
        $('#shippingAddress').slideUp()
    }
}