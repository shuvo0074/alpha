
async function submitCustomer(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);

    let spinner = showSpinner();//show spinner

    let res = await fetch(`${baseUrl}/admin/customer/add`, {
        method:'post',
        body:formData,
        credentials: 'include'
    })
    removeSpinner(spinner); //remove spinner
    
    let obj = await res.json();
    if(res.ok){
        showMsg('New Customer Created');
        customers.push(obj.inserted);
        addCustomerToList(obj.inserted);
    }else{
        showErrors(obj);
    }
    
}

function listCustomer(){
    let customerContainer = document.getElementById('customers');
    customerContainer.innerHTML = '';
    customers.forEach(customer=>{

        let row = document.createElement('div');
        row.className = 'row m-2';

        let name = document.createElement('div');
        name.className = 'col-6';

        let link = document.createElement('a');
        link.setAttribute('href', `/admin/customer/view/${customer._id}`);
        name.appendChild(link);
        link.appendChild(document.createTextNode(customer.firstName + ' ' + customer.lastName));

        row.appendChild(link)
        customerContainer.appendChild(row);

    })
}

function addCustomerToList(customer){
    let customerContainer = document.getElementById('customers');
   
    let row = document.createElement('div');
    row.className = 'row m-2 customerRow';

    let name = document.createElement('div');
    name.className = 'col-6';

    let link = document.createElement('a');
    link.setAttribute('href', `/admin/customer/view/${customer._id}`);
    name.appendChild(link);
    link.appendChild(document.createTextNode(customer.firstName + ' ' + customer.lastName));

    row.appendChild(link)
    row.style.display = 'none';
    $(row).fadeIn();
    customerContainer.appendChild(row);
    row.classList.add('customerRow');
}
async function submitCustomerUpdate(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);

    let spinner = showSpinner();//show spinner

    let res = await fetch(`${baseUrl}/admin/customer/update`, {
        method:'post',
        body:formData,
        credentials: 'include'
    })
    removeSpinner(spinner); //remove spinner
    
    let obj = await res.json();
    if(res.ok){
        showMsg('New Customer Created');
        location.reload();
    }else{
        showErrors(obj);
    }
    
}


async function delCustomer(ev){
    let ans = confirm('Are you sure you want to delete this customer? \n note: this can not be undone');
    if(!ans) return false;
    let spinner = showSpinner();//show spinner
    
    let res = await fetch(`${baseUrl}/admin/customer/update/customer/delete/${customer._id}`, {
        credentials: 'include'
    });
    removeSpinner(spinner); //remove spinner
    
    if(!res.ok) return showErrors(await res.json());
    window.location = '/admin/customer/list'
}

let previousSelect = ''; //prevent from loading every click

async function loadCity(){

    let country = document.getElementById('country').value;
    if(country == previousSelect) return false; //prevent from loading every click

    let citySelect = document.getElementById('city');

    let spinner = showSpinner();//show spinner
    
    let res = await fetch(`/api/geo/${country}/city`);
    removeSpinner(spinner); //remove spinner
    
    if(!res.ok) return showErrors(await res.json());    

    let cityList = await res.json();
    for(let city of cityList){

        let option = document.createElement('option');
        option.setAttribute('value', city.name);
        option.appendChild(document.createTextNode(city.name));

        citySelect.appendChild(option);
    }

    previousSelect = country; //set for next click
}