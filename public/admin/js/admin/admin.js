
const baseUrl = ``


function selectCategory(ev){
    let btn = ev.target;
    let categoryId = btn.getAttribute('value');
    if(!catList.includes(categoryId)){
        btn.classList.remove('btn-outline-primary');
        btn.classList.add('btn-primary');
        catList.push(categoryId);
    }else{
        btn.classList.add('btn-outline-primary');
        btn.classList.remove('btn-primary');

        let index = catList.indexOf(categoryId);
        if (index > -1) catList.splice(index, 1);
    }

}

function selectTag(ev){
    let btn = ev.target;
    let categoryId = btn.getAttribute('value');
    if(!tagList.includes(categoryId)){
        btn.classList.remove('btn-outline-primary');
        btn.classList.add('btn-primary');
        tagList.push(categoryId);
    }else{
        btn.classList.add('btn-outline-primary');
        btn.classList.remove('btn-primary');

        let index = tagList.indexOf(categoryId);
        if (index > -1) tagList.splice(index, 1);
    }

}


function showErrors(errors){
    
    for(x in errors){
        
        let elem = document.getElementsByName(x)[0];
        if(!elem || x=='id'){
            alertify.error(errors[x]);
            continue
        }
        elem.classList.add('is-invalid');
        if(!elem.nextElementSibling || elem.nextElementSibling.className != 'invalid-feedback'){
            let div = document.createElement('span');
            div.classList.add('invalid-feedback');
            div.appendChild(document.createTextNode(errors[x]));
            elem.insertAdjacentElement('afterend', div);

        }
        elem.nextElementSibling.innerHTML = errors[x];
    }
}

function showMsg(msg){
    //alertify.set('notifier','position', 'bottom-right');
    alertify.success(msg);
}


function submitLogin(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);
    let formObj = {};
    for(x of formData){
        formObj[x[0]] = x[1];
    }
   
    for(x of formData.keys()){
        let elem = document.getElementsByName(x)[0]
        elem.classList.remove('is-invalid');
    }
    let validator = new Validator();
    let errors = validator.adminLogin(formObj);
    if(!_.isEmpty(errors)){;
        showErrors(errors);
    }else{
        
        (async function (){
            let spinner = showSpinner('fingerprint');//show spinner
    
            let res = await fetch(`${baseUrl}/admin/auth/login`, {
                method:'post',
                body:formData,
                credentials: 'include'
            })
            
            removeSpinner(spinner); //remove spinner

            if(res.ok){
                location = '/admin';
            }else{ 

                let errors = await res.json();
                showErrors(errors);
            }
        })()
        
    }

}

function convertToUI(item){
    for(x in item){ 
        let elem = document.getElementsByName(x);
        if(!elem[0]) continue;
        
        if(x == 'category') {
            let acElem = document.querySelector(`option[value="${item.category}"]`);
            if(acElem) acElem.selected = true;
        }
        if(x == 'image' || x=='images') continue;
        elem[0].value = item[x];
        elem[0].setAttribute('readonly', 'readonly');
    }

    let selects = document.querySelectorAll('select');
    for(select of selects){
        select.style.display = 'none';
        let span = document.createElement('span');
        let value = select.value;
        span.appendChild(document.createTextNode(select.getAttribute('data-value')));
        select.parentElement.appendChild(span);
    }
    let textAreas = document.querySelectorAll('textarea');
    for(textArea of textAreas){
        textArea.style.display = 'none';
        let span = document.createElement('span');
        let value = textArea.innerText;
        span.appendChild(document.createTextNode(value));
        textArea.parentElement.appendChild(span);
    }
}

function convertToForm(){
    let inputs = document.querySelectorAll('table input');
    for(x of inputs){
        x.readOnly = false;
        x.style.background = '#eee !important';
        x.parentElement.style.background = '#eee !important';
    }
    let selects = document.querySelectorAll('table select');
    for(x of selects){
        x.style.display='inline-block';
        x.nextElementSibling.style.display = 'none';
    }
    
    let textAreas = document.querySelectorAll('table textarea');
    for(x of textAreas){
        x.style.display='inline-block';
        x.nextElementSibling.style.display = 'none';
    }

    document.getElementById('submitBtn').style.display = 'inline-block'
    let editBtn = document.getElementById('editBtn');
    editBtn.className = 'btn btn-secondary float-right';
    editBtn.innerHTML = `
    <i class="fal fa-times"></i> cancel
    `
    editBtn.setAttribute('onclick', 'window.location.reload()')
}

function submitSlider(ev){
    ev.preventDefault();
    let formData= new FormData(ev.target);
   
        
    (async function(){
        let spinner = showSpinner();//show spinner
    
        let res = await fetch(`${baseUrl}/admin/slider/add`, {
            method:'post',
            body:formData,
            credentials: 'include'
        })
        removeSpinner(spinner); //remove spinner
        
        let obj = await res.json();
        if(res.ok){
            showMsg(obj.msg);
            window.location.reload();
        }else{
            showErrors(obj);
        }
    })()


}



/**
 * 
 * 
 * jquery functions 
 * 
 * 
 */

$(window).on('load', ()=>{
    $('#newForm input[name=caption]').prop('checked', false);
})
function capt(){
    $('#newForm input[name=heading]').attr('readonly', (index, attr)=>{
        console.log(index,attr)
        return attr == '' || 'readonly' ? attr== undefined? true: false:true; 
    })
    $('#newForm input[name=text]').attr('readonly', (index, attr)=>{
        console.log(index,attr)
        return attr == '' || 'readonly' ? attr== undefined? true: false:true; 
    })

    
}


function submitAttributes(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);
    let formObj = {};
    for(x of formData){
        formObj[x[0]] = x[1];
    }
    
    for(x of formData.keys()){
        let elem = document.getElementsByName(x)[0]
        elem.classList.remove('is-invalid');
    }

    let validator = new Validator();
    let errors = validator.addAttributes(formObj);
    if(!_.isEmpty(errors)){;
        showErrors(errors);
    }else{
        
        (async function(){
            let spinner = showSpinner();//show spinner
    
            let res = await fetch(`${baseUrl}/admin/attributes/add`, {
                method:'post',
                body:formData,
                credentials: 'include'
            })
            removeSpinner(spinner); //remove spinner
            
            let obj = await res.json();
            if(res.ok){
                showMsg(obj.msg);
                location.reload();
            }else{
                showErrors(obj);
            }
        })()
    }

}


function viewEditModal(id, name, target){
    let attr = list.find(item=>{
        return item._id == id;
    })
    let modal = document.createElement('div'); 
    modal.classList.add('formModal');
    modal.setAttribute('id', 'formModal')

    let wrapper = document.createElement('div') //wrapper

    let h4 = document.createElement('h4'); 
    h4.appendChild(document.createTextNode('Edit '+name));
    h4.className = 'tex-left text-dark'

    let close = document.createElement('button') //close button
    close.className = 'btn btn-outline-primary border border-primary float-right'
    close.appendChild(document.createTextNode('×'))
    close.setAttribute('onclick', 'closeModal()')
    h4.appendChild(close)

    let formContainer = document.createElement('div') //form container
    let form = document.createElement('form');
    form.setAttribute('onSubmit', `submitModalUpdate(event,'${target}')`);

    let inputLabel = document.createElement('label'); //name label
    inputLabel.className = 'd-block text-left m-2';
    inputLabel.appendChild(document.createTextNode(name+' Name'))
    
    let input = document.createElement('input'); //name 
    input.setAttribute('value', attr.name);//current name
    input.classList.add('form-control');
    input.setAttribute('name', 'name')

    let textareaLabel = document.createElement('label'); //name label
    textareaLabel.className = 'd-block text-left m-2';
    textareaLabel.appendChild(document.createTextNode(name+' Description'))

    let textarea = document.createElement('textarea'); //description
    textarea.appendChild(document.createTextNode(attr.description)) //current description
    textarea.classList.add('form-control');
    textarea.setAttribute('name', 'description')


    let hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('value', attr._id);
    hiddenInput.setAttribute('name', 'id')

    let button  =document.createElement('button'); //submit button
    button.className = 'btn btn-primary'
    button.appendChild(document.createTextNode('update'))

    form.appendChild(hiddenInput);
    form.appendChild(inputLabel);
    form.appendChild(input);
    form.appendChild(textareaLabel);
    form.appendChild(textarea);
    form.appendChild(button);

    formContainer.appendChild(form);

    wrapper.appendChild(h4);
    wrapper.appendChild(formContainer);

    modal.appendChild(wrapper)

    document.body.appendChild(modal)

    setTimeout(()=>{
        modal.style.opacity = 1;
    }, 200)
}

function submitModalUpdate(ev, target){
    ev.preventDefault();
    let formData= new FormData(ev.target);
    let formObj = {};
    for(x of formData){
        formObj[x[0]] = x[1];
    }
    
    for(x of formData.keys()){
        let elem = document.getElementsByName(x)[0]
        elem.classList.remove('is-invalid');
    }

    let validator = new Validator();
    let errors = validator.addAttributes(formObj);
    if(!_.isEmpty(errors)){;
        showErrors(errors);
    }else{
        
        (async function(){
            let spinner = showSpinner();//show spinner
    
            let res = await fetch(`${baseUrl}${target}`, {
                method:'post',
                body:formData,
                credentials: 'include'
            })
            removeSpinner(spinner); //remove spinner
            
            let obj = await res.json();
            if(res.ok){
                showMsg(obj.msg);
                location.reload();
            }else{
                showErrors(obj);
            }
        })()
    }
}

function closeModal(){
    let modal = document.getElementById('formModal');
    modal.parentElement.removeChild(modal);
}

async function delAttribute(id){
    let ans = confirm('Are you sure you want to delete this attribute? \n note: this can not be undone');
    if(!ans) return false;
    let spinner = showSpinner();//show spinner
    
    let res = await fetch(`${baseUrl}/admin/attributes/update/attributes/delete/${id}`, {
        credentials: 'include'
    });
    removeSpinner(spinner); //remove spinner
    
    if(!res.ok) return showErrors(await res.json());
    window.location.reload();
}



function submitTag(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);
    let formObj = {};
    for(x of formData){
        formObj[x[0]] = x[1];
    }
    
    for(x of formData.keys()){
        let elem = document.getElementsByName(x)[0]
        elem.classList.remove('is-invalid');
    }

    let validator = new Validator();
    let errors = validator.addTag(formObj);
    if(!_.isEmpty(errors)){;
        showErrors(errors);
    }else{
        
        (async function(){
            let spinner = showSpinner();//show spinner
    
            let res = await fetch(`${baseUrl}/admin/tag/add`, {
                method:'post',
                body:formData,
                credentials: 'include'
            })
            removeSpinner(spinner); //remove spinner
            
            let obj = await res.json();
            if(res.ok){
                showMsg(obj.msg);
                location.reload();
            }else{
                showErrors(obj);
            }
        })()
    }

}

async function delTag(id){
    let ans = confirm('Are you sure you want to delete this tag? \n note: this can not be undone');
    if(!ans) return false;
    let spinner = showSpinner();//show spinner
    
    let res = await fetch(`${baseUrl}/admin/tag/update/tag/delete/${id}`, {
        credentials: 'include'
    });
    removeSpinner(spinner); //remove spinner
    
    if(!res.ok) return showErrors(await res.json());
    window.location.reload();
}

let productAttributes=[];
function addAttribute(ev){
    let value = ev.target.value;
    productAttributes.push({
        _id: value
    })

    let attribute = attributes.find(item=>{
        return item._id == value;
    }) 

    let viewer = document.getElementById('attrView');
    let div = document.createElement('div');
    //TODO: add type dynamic fields
    let str  =`
        <div class='shadow-sm border m-1 p-1'>
            <span class='text-primary'>${attribute.name}</span>
            <span class='ml-5'>
                <label>Value:</label>
                <input type='text' class='border-0 p-1' oninput="setAttr(event, '${value}')" value='' placeholder='Black, White, Blue'>
                <button class='btn btn-outline-danger btn-sm ml-3' onclick="removeAttr(event, '${value}')">&times;</button>
            </span>
        </div>
    `
    div.innerHTML = str;
    div.className='mt-3';
    div.setAttribute('id', 'view'+value);
    viewer.appendChild(div);
}

function setAttr(ev, id){
    let attribute = productAttributes.find(item=>{
        return item._id == id;
    })
    attribute.value = ev.target.value 
    console.log(productAttributes);
}

function removeAttr(ev, id){
    console.log(68687687)
    let view = document.getElementById('view'+id);
    view.parentElement.removeChild(view); //remove from view

    //find the product in list
    let product = productAttributes.find(item=>{
        return item._id == id;
    })
    productAttributes.splice(productAttributes.indexOf(product), 1) //remove from array
}

async function submitAdminSettings(ev){
    ev.preventDefault();
    let formData = new FormData(ev.target);
    let spinner = showSpinner();//show spinner
    
    let res = await fetch(`${baseUrl}/admin/settings/update`, {
        method:'post',
        body:formData,
        credentials: 'include'
    })
    removeSpinner(spinner); //remove spinner
    
    let obj = await res.json();
    if(res.ok){
        showMsg(obj.msg);
        location.reload();
    }else{
        showErrors(obj);
    }
    
   
}
