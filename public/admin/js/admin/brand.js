

function submitBrand(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);
    let formObj = {};
    for(x of formData){
        formObj[x[0]] = x[1];
    }
    
    for(x of formData.keys()){
        let elem = document.getElementsByName(x)[0]
        elem.classList.remove('is-invalid');
    }

    let validator = new Validator();
    let errors = validator.addBrand(formObj);
    if(!_.isEmpty(errors)){;
        showErrors(errors);
    }else{
        
        (async function(){
            let spinner = showSpinner();//show spinner
    
            let res = await fetch(`${baseUrl}/admin/brand/add`, {
                method:'post',
                body:formData,
                credentials: 'include'
            })
            removeSpinner(spinner); //remove spinner

            let obj = await res.json();
            if(res.ok){
                showMsg(obj.msg);
            }else{
                showErrors(obj);
            }
        })()
    }

}


function submitBrandUpdate(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);
    let formObj = {};
    for(x of formData){
        formObj[x[0]] = x[1];
    }

    for(x of formData.keys()){
        let elem = document.getElementsByName(x)[0]
        elem.classList.remove('is-invalid');
    }
    let validator = new Validator();
    let errors = validator.addBrand(formObj);
    if(!_.isEmpty(errors)){;
        showErrors(errors);
    }else{
        
        (async function(){
            let spinner = showSpinner();//show spinner
    
            let res = await fetch(`${baseUrl}/admin/brand/update/${brand._id}`, {
                method:'post',
                body:formData,
                credentials: 'include'
            })
            removeSpinner(spinner); //remove spinner
            
            let obj = await res.json();
            if(res.ok){
               window.location.reload();

            }else{
                showErrors(obj);
            }
        })()
    }

}

async function delBrand(ev){
    let ans = confirm('Are you sure you want to delete this brand? \n note: this can not be undone');
    if(!ans) return false;
    let spinner = showSpinner();//show spinner
    
    let res = await fetch(`${baseUrl}/admin/brand/update/brand/delete/${brand._id}`, {
        credentials: 'include'
    });
    removeSpinner(spinner); //remove spinner
    
    if(!res.ok) return showErrors(await res.json());
    window.location = '/admin/brand/list'
}


async function delImageBrand(image, id){
    let ans = confirm('Are you sure you want to delete this image?');
    if(!ans) return false;

    let spinner = showSpinner();//show spinner
    
    let res = await fetch(`${baseUrl}/admin/brand/update/${brand._id}/img/delete?image=${image}`, {
        credentials: 'include'
    });
    
    removeSpinner(spinner); //remove spinner
    
    if(!res.ok) return showErrors(await res.json());
    showMsg((await res.json()).msg)
    
    image = document.getElementById('img'+id);
    image.parentElement.removeChild(image);
}

async function makeThumbBrand(path, id){
    let spinner = showSpinner();//show spinner
    let res = await fetch(`${baseUrl}/admin/brand/update/${brand._id}/img/thumbnail?image=${path}`, {
        credentials: 'include'
    });
    removeSpinner(spinner); //remove spinner
    
    if(!res.ok) return showErrors(await res.json());
    showMsg((await res.json()).msg)
    let otherButton = document.querySelectorAll('#image  .statbtn');
    for(x of otherButton){
        x.className = 'btn btn-info btn-sm statbtn';
        x.innerHTML = `
        <i class="fal fa-image"></i> Set As Thumbnail
        `;
    }
    let imageButton = document.querySelector(`#img${id} .statbtn`);
    imageButton.className = ' btn btn-outline-success btn-sm statbtn';
    imageButton.innerHTML = `
    <i class="fal fa-check"></i> Thumbnail
    `
}

function submitImageBrand(ev){
    ev.preventDefault();
    let formData= new FormData(ev.target);
   
        
    (async function(){
        let spinner = showSpinner();//show spinner
    
        let res = await fetch(`${baseUrl}/admin/brand/update/img/add`, {
            method:'post',
            body:formData,
            credentials: 'include'
        })

        removeSpinner(spinner); //remove spinner
        
        let obj = await res.json();
        if(res.ok){
            showMsg(obj.msg);
            window.location.reload();
        }else{
            showErrors(obj);
        }
    })()


}

