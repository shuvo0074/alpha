
async function delImageCat(image, id){
    let ans = confirm('Are you sure you want to delete this image?');
    if(!ans) return false;

    let spinner = showSpinner();//show spinner
    
    let res = await fetch(`${baseUrl}/admin/category/update/${category._id}/img/delete?image=${image}`, {
        credentials: 'include'
    });
    
    removeSpinner(spinner); //remove spinner
    
    if(!res.ok) return showErrors(await res.json());
    showMsg((await res.json()).msg)
    
    image = document.getElementById('img'+id);
    image.parentElement.removeChild(image);
}

async function makeThumbCat(path, id){
    let spinner = showSpinner();//show spinner
    let res = await fetch(`${baseUrl}/admin/category/update/${category._id}/img/thumbnail?image=${path}`, {
        credentials: 'include'
    });
    removeSpinner(spinner); //remove spinner
    
    if(!res.ok) return showErrors(await res.json());
    showMsg((await res.json()).msg)
    let otherButton = document.querySelectorAll('#imageEdit .statbtn');
    for(x of otherButton){
        x.className = 'btn btn-info btn-sm statbtn';
        x.innerHTML = `
        <i class="fal fa-image"></i> Set As Thumbnail
        `;
    }
    let imageButton = document.querySelector(`#img${id} .statbtn`);
    imageButton.className = ' btn btn-outline-success btn-sm statbtn';
    imageButton.innerHTML = `
    <i class="fal fa-check"></i> Thumbnail
    `
}

function submitImageCat(ev){
    ev.preventDefault();
    let formData= new FormData(ev.target);
   
        
    (async function(){
        let spinner = showSpinner();//show spinner
    
        let res = await fetch(`${baseUrl}/admin/category/update/img/add`, {
            method:'post',
            body:formData,
            credentials: 'include'
        })

        removeSpinner(spinner); //remove spinner
        
        let obj = await res.json();
        if(res.ok){
            showMsg(obj.msg);
            window.location.reload();
        }else{
            showErrors(obj);
        }
    })()
}


function submitCategory(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);
    let formObj = {};
    for(x of formData){
        formObj[x[0]] = x[1];
    }
    let catList = category.map(item=>{
        return item._id
    })

    for(x of formData.keys()){
        let elem = document.getElementsByName(x)[0]
        elem.classList.remove('is-invalid');
    }
    let validator = new Validator();
    let errors = validator.addCategory(formObj, {category:catList});
    if(!_.isEmpty(errors)){;
        showErrors(errors);
    }else{
        
        (async function(){
            let spinner = showSpinner();//show spinner
    
            let res = await fetch(`${baseUrl}/admin/category/add`, {
                method:'post',
                body:formData,
                credentials: 'include'
            })
            removeSpinner(spinner); //remove spinner

            let obj = await res.json();
            if(res.ok){
                showMsg(obj.msg);
                let spinner = showSpinner();//show spinner
    
                let cat = await fetch(`${baseUrl}/admin/api/category/list`, {
                    
                    credentials: 'include'
                });
                removeSpinner(spinner); //remove spinner
                
                if(res.ok){
                    let select = document.getElementById('parentSelect');
                    select.innerHTML = '';
                    let cats = await cat.json();
                    category = cats;
                    cats.forEach(item => {
                        let option = document.createElement('option');
                        option.setAttribute('value', item._id);
                        option.appendChild(document.createTextNode(item.name));
                        select.appendChild(option)
                        console.log(select, option)
                    });
                } else{
                    showErrors(cat.json());
                }
            }else{
                showErrors(obj);
            }
        })()
    }

}



function submitCategoryUpdate(ev){
    ev.preventDefault();

    let formData= new FormData(ev.target);
    let formObj = {};
    for(x of formData){
        formObj[x[0]] = x[1];
    }
   
    for(x of formData.keys()){
        let elem = document.getElementsByName(x)[0]
        elem.classList.remove('is-invalid');
    }
    let validator = new Validator();
    let errors = validator.addCategory(formObj);
    if(!_.isEmpty(errors)){;
        showErrors(errors);
    }else{
        
        (async function(){
            let spinner = showSpinner();//show spinner
    
            let res = await fetch(`${baseUrl}/admin/category/update/${category._id}`, {
                method:'post',
                body:formData,
                credentials: 'include'
            })
            removeSpinner(spinner); //remove spinner
            
            let obj = await res.json();
            if(res.ok){
               window.location.reload();

            }else{
                showErrors(obj);
            }
        })()
    }

}


function submitCategoryImage(ev){
    ev.preventDefault();
    let formData= new FormData(ev.target);
   
        
    (async function(){
        let spinner = showSpinner();//show spinner
    
        let res = await fetch(`${baseUrl}/admin/category/update/img/change`, {
            method:'post',
            body:formData,
            credentials: 'include'
        })
        removeSpinner(spinner); //remove spinner
        
        let obj = await res.json();
        if(res.ok){
            showMsg(obj.msg);
            window.location.reload();
        }else{
            showErrors(obj);
        }
    })()
}

async function delCategory(ev){
    let ans = confirm('Are you sure you want to delete this product? \n note: this can not be undone');
    if(!ans) return false;

    let spinner = showSpinner();//show spinner

    let res = await fetch(`${baseUrl}/admin/category/update/category/delete/${category._id}`, {
        credentials: 'include'
    });

    removeSpinner(spinner); //remove spinner

    if(!res.ok) return showErrors(await res.json());
    window.location = '/admin/category/list'
}