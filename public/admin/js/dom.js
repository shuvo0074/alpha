/**
 * @class DOM
 * for creating html elements using javascript
 * 
 */
export default class DOM {

    /**
     * @method create Create DOM element 
     * @param {Object} elem Element definition Object with tagname as property and definition with as value the definition should be an object with attribute name as property and value as value it can also have a child property which will contain child elements
     *  @returns {Object|Array} DOM element with child appended, array if multiple supplied in elem;
     */
    create(elem){
        let elementList = [];
        for(let x in elem){
            let element = this._createNodeRecursively(x, elem[x]);
            elementList.push(element);
        }
        if(elementList.length == 1) return elementList[0];
        return elementList;
    }

    /**
     * 
     * @private
     * @method _createNodeRecursively creates DOM nodes from tag name and definition object
     * @param {String} name Name/tagName of the element
     * @param {Object} node definition of the element, an object with attributes name as property name and value as value 
     * @returns {Object} the DOM element Object
     */
    _createNodeRecursively(name, node){

        if(!node.child){ //exit condition : element has no  child 
            let elem = document.createElement(name); //create element by name
            for(let x in node){
                
                if(x == 'text'){ // text is not attribute, contains text node
                    elem.appendChild(document.createTextNode(node[x])) // create and append  text
                    continue;
                }

                elem.setAttribute(x, node[x]); //create attribute for each property
            }
            return elem; 
        }
        //now element has child elements

        let elem = document.createElement(name);
        //set attributes first
        for(let x in node){
            if(x == 'child') continue; // child property is not attribute, for listing child nodes
            
            if(x == 'text'){ // text is not attribute, contains text node
                elem.appendChild(document.createTextNode(node[x])) // create and append  text
                continue;
            }

            elem.setAttribute(x, node[x]);
        } 

        //create child elements
        for(let x in node.child){
            elem.appendChild(this._createNodeRecursively(x, node.child[x])); 
        }

        return elem;

    }
    
}