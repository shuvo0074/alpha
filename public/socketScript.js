

const msgContainer = document.getElementById("msgContainer");

class Chat {
	constructor() {
		// 
		this.socket = io({ query: "client=user" });
		this.events = []
		this.listeners = {};
	}

	registerListener(event, listener){
		this.socket.addEventListener(event, listener);
		this.events.push(event);
		this.listeners[event] = listener;
	}

	attachAllListeners(){
		for(let event in events){
			if(this.handlers[event]) this.socket.addEventListener(event, this.listeners[event])
		}
	}
	detachAllListeners(){
		for(let event in events){
			if(this.handlers[event]) this.socket.reemoveEventListener(event, this.listeners[event])
		}
	}

	send(event, message) {
		this.socket.emit(event, message);
	}

}

let chat = new Chat();


chat.registerListener('text', (data)=>{
	msgContainer.appendChild(createBlock('Text', data));
})
chat.registerListener('userConnect', (data)=>{
	msgContainer.appendChild(createBlock('Connection', data))
})

function createBlock(heading, text){
	let div = document.createElement('div');
	let h5 = document.createElement('h5');
	h5.appendChild(document.createTextNode(heading));
	div.appendChild(h5);
	div.appendChild(document.createTextNode(text));
	return div;
}

function handleSubmit(e) {
	e.preventDefault();
	let msg = document.getElementById('msg');
	chat.send('text', msg.value);
	// msg.value = "";
	msg.focus();
}