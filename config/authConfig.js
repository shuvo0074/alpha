// import dependencies
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const con = require("../lib/db");

require("dotenv").config();

const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.JWT_SECRET_KEY;

module.exports = (passport) => {
  passport.use(
    new JwtStrategy(opts, async (jwt_payload, done) => {
      const userId = jwt_payload._id;
      let db = await con.db();
      try {

        let user;
        let {userType} = jwt_payload;
        // find and attach user info conditionaly
        if(userType === 'customer') {
          user = await db.collection("customer").findOne({ _id: con.ObjectID(userId) });
        } else if(userType === 'dealer') {
          user = await db.collection('dealer').findOne({_id: new con.ObjectID(userId)});
        }

        user = {
          userType,
          ...user
        }
        
        if (user) return done(null, user);
        return done(null, false);
      } catch (error) {
        console.log(error);
        res.status(401).json(error);
      }
    })
  );
};
