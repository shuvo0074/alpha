const domainMap = require('./../../config/domainMap.json');
const router = require('express').Router();
const fs = require('fs');
const con = require('./../../lib/db.js');
const packageLimit = require('./../../config/packageLimit.json');
const path = require('path')

router.get('/', async(req, res)=>{
    res.end(`System Settings ${req.headers.host}`);
})

router.post('/', async (req, res)=>{
    let {owner, domain, db, publicDir, package} = req.fields;

    //all the properties are required  
    if(!owner || !con.ObjectID.isValid(owner)){
        return res.status(400).json({response:'Invalid Owner'});
    }
    else if(!domain){
        return res.status(400).json({response:'Missing domain'});
    }
    else if(!db){
        return res.status(400).json({response:'Missing dbName'});
    }
    else if(!publicDir){
        return res.status(400).json({response:'Missing publicDir'});
    }
    else if(!package){
        return res.status(400).json({response:'Missing Package'});
    }

    if(domainMap[domain]) return res.status(400).json({response:'site with same domain already exists'});

    let theme = path.join(__dirname, '../../' ,'views', 'themes', 'theme1');

    domain = domain.replace('www.', ''); //config always in non-www format

    let packageInfo = packageLimit[package];
    if(!packageInfo) return res.status(400).json({response:'Invalid Package'});
    packageInfo.name = package;

    let newSiteConfig = {
        owner, 
        domain,
        db,
        publicDir,
        theme,
        package:packageInfo,
    }

    domainMap[domain] = newSiteConfig;

    //save change
    await fs.writeFile('./config/domainMap.json', JSON.stringify(domainMap)); 

    //create public directory
    await fs.mkdir(path.join(__dirname, '../../', 'user', publicDir));
    await fs.mkdir(path.join(__dirname, '../../', 'user', publicDir, 'public'));
    await fs.mkdir(path.join(__dirname, '../../', 'user', publicDir, 'themes'));

    res.json({response:'New site registered successfully'});


})

module.exports = router;