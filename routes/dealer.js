const router = require('express').Router();
const con = require('./../lib/db.js');
// const customerAuth = require('../lib/auth/customerAuth.js');
const authRouter = require('./dealer/auth/post.js');
const apiRouter = require('./dealer/api.js');


//authentication routers
router.use('/auth', authRouter);


router.use('/api', apiRouter);


module.exports = router;
