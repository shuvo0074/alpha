const router = require("express").Router();
const ejs = require("ejs");
const path = require("path");
const con = require("../lib/db.js");

//routers import
const adminAuth = require("../lib/auth/adminAuth.js");
const authRouter = require("./admin/auth.js");
const apiRouter = require('./admin/api.js');

//authentication routers
router.use("/auth", authRouter);
router.use(/\/((?!auth).)*/, adminAuth);

//api
router.use('/api', apiRouter);


// serve SPA
router.get('*', async(req, res)=>{
    res.render('admin/index.html');
})

module.exports = router;
