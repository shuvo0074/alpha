const router = require('express').Router();

router.use(require('./auth/post.js'));
router.use(require('./auth/get.js'));

module.exports = router;
