const router = require('express').Router();

router.get('/logout', async(req, res)=>{
    try{
        req.session.destroy();
        res.json({success:'Logged out'})
    }catch(err){
        console.log(err);
        res.status(500).json({
            error:'Sorry, something went wrong!'
        })
    }
})

module.exports = router;