const router = require("express").Router();
const Validator = require("../../../validator/customer.js");
const _ = require("lodash");
const con = require("../../../lib/db.js");
const fs = require("fs");
const util = require("util");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const autoMail = require("./../../../lib/autoMail.js");
const autoSMS = require("./../../../lib/autoSMS.js")
const { sendMail } = require('./../../../lib/mail.js');
const { sendSms } = require('./../../../lib/sms.js');

const { notify } = require("./../../../lib/adminNotification.js");
const { generateRandomString } = require('./../../../lib/utility.js');


// const passport = require("passport");

require("dotenv").config();

router.post("/login", async (req, res) => {
  try {
    //validation
    let { username, password } = req.fields;
    let validator = new Validator();
    let errors = await validator.customerLogin(req.fields);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);


    //get user info
    let db = await con.db();
    let user = await db.collection("customer").findOne({
      $or: [{ phone: username }, { email: { $regex: new RegExp("^" + username, "i") } }, { username: username }],
    });

    if (!user) return res.status(400).json({ username: "No user found with given email or phone" }); //no such user
    //compare password
    let result = await bcrypt.compare(password, user.password);
    if (!result) return res.status(400).json({ password: "Incorrect password" });

    //remove password from jwt payload
    delete user.password;
    const payload = {
      ...user,
      userType: 'customer',
    }

    // payload, secretKey, token expiry date(30days)
    const token = jwt.sign(payload, process.env.JWT_SECRET_KEY, {
      expiresIn: 30 * 24 * 3600,
    });

    // req.session.customer = {};
    // req.session.customer._id = user._id;
    // req.session.customer.domain = req.hostOptions.domain;

    return res.json({
      success: "login is successful",
      token: `Bearer ${token}`,
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Sorry, something went wrong!" });
  }
});


router.post('/recover', async (req, res) => {
  try {

    let { username } = req.fields;

    if (!username) return res.status(400).json({ username: "Invalid Username" });

    let db = await con.db();
    const customer = await db.collection('customer').findOne({
      $or: [{ phone: username }, { email: { $regex: new RegExp("^" + username, "i") } }, { username: username }],
    })


    // check if customer exists
    if (!customer) return res.status(400).json({ username: `Invalid username` });

    if (!customer.email && !customer.phone) return res.status(400).json(
      { phone: "no email or phone number found in customer profile" }
    );

    let resetToken = generateRandomString(6, 100);
    // console.log(resetToken);

    let dateNow = new Date();
    let expire = new Date(dateNow.setHours(dateNow.getHours() + 3));


    let resetData = {
      customer: customer._id,
      token: resetToken,
      expire, // 3 hour
      added: new Date()
    }

    await db.collection('recoveryToken').insertOne(resetData);


    //sms
    let SMSContent = {
      recipient: customer.phone,
      text: `${resetToken} is your password recovery code. If you did not request a password change you can safely ignore this sms.
      `
    }

    //mail
    let mailContent = {
      recipient: customer.email,
      subject: "Reset Password",
      html: `
      <div style="font-family:sans-serif">
        Dear <b>${customer.firstName || ""} ${customer.lastName || ""}</b>,<br> <br>
          Your password recovery code is 
          <br><br><br> <br>
          <div style="text-align:center;">
          <b  style="padding:10px 15px; font-size:3em;  color:#f1982b; border: 1px dashed #d2d2d2">
            ${resetToken}
          </b>
          </div>
          <br> <br><br> <br>
          <span style="color:#333333">If you did not request a password change you can safely ignore this email.</span>
        </div>
    `,
    }

    //*send
    sendSms(SMSContent); // same
    sendMail(mailContent); //no need to await 

    // censor contact info
    let hideEmail = function (email) {
      return email.replace(/(.{2})(.*)(?=@)/,
        function (gp1, gp2, gp3) {
          for (let i = 0; i < gp3.length; i++) {
            gp2 += "*";
          } return gp2;
        });
    };

    let hideNumber = function (number) {
      let regex = /(?<!^.?).(?!.?$)/g;
      return number.replace(regex, '*');

    }


    return res.json({
      message: "Password reset code sent",
      customer: {
        firstName: customer.firstName,
        lastName: customer.lastName
      },
      email: hideEmail(customer.email),
      phone: hideNumber(customer.phone)
    })

  } catch (error) {
    console.log(error);
    res.status(500).json({ error: error.message });
  }
})

router.post('/recover/validate', async (req, res) => {
  try {
    // accountVerify is optional
    // reuse this function for verify account also
    const { resetToken, accountVerify } = req.fields;
    let db = await con.db();

    let resetData = await db.collection('recoveryToken').findOne({
      $and: [
        { token: resetToken },
        {expire:{$gte: new Date()}}
      ]
    });

    if (!resetData) return res.status(400).json({ resetToken: "Invalid reset code" });

    let customer = await db.collection('customer').findOne({ _id: new con.ObjectID(resetData.customer) });

    if (!customer) return res.status(400).json({ user: "No user found!" });

    // account verify need no further action
    // so delete the token from recoveryToken collection
    if(accountVerify) {
      await db.collection('recoveryToken').deleteOne({
      $and: [
        {token: resetToken},
        {_id: new con.ObjectID(customer._id)}
      ]})
    } 

    return res.json({
      success: "Code Valid",
      status: "verified" // for account validation
    })
  } catch (error) {
    return res.status(500).json({ error: error.message });
  }
})

router.post('/recover/reset', async (req, res) => {
  try {
    const { resetToken, newPassword, confirmPassword } = req.fields;

    let db = await con.db();

    let resetData = await db.collection('recoveryToken').findOne({
      $and: [
        { token: resetToken },
        {expire:{$gte: new Date()}}
      ]
    });

    if (!resetData) return res.status(400).json({ resetToken: "Invalid reset code" });

    let customer = await db.collection('customer').findOne({ _id: resetData.customer });
    if (!customer) return res.status(400).json({ resetToken: "Invalid reset code" });

    // validate password
    if (!newPassword) return res.status(400).json({ password: "Password can not be empty" });
    if (!confirmPassword) return res.status(400).json({ confirmPassword: "Confirm password can not be empty" });

    if (newPassword !== confirmPassword) return res.status(400).json({ password: "Passwords do not match" });

    // hash password
    let salt = await bcrypt.genSalt(10);
    let hash = await bcrypt.hash(newPassword, salt);

    let updatedCustomer = {
      password: hash,
      updated: new Date(),
    }

    let result = await db.collection('customer').updateOne(
      { _id: customer._id },
      { $set: updatedCustomer }
    )

    res.json({
      success: "Password changed successfully"
    })



    //delete used token
    await db.collection('recoveryToken').deleteOne({ token: resetToken });

  } catch (error) {
    return res.status(500).json({ error: error.message });
  }
})

router.post("/register", async (req, res) => {
  try {
    let {
      firstName, lastName,
      country, city, address1, address2, zipCode,
      phone, email,
      additionalInfo,
      password, password2, dealerCode,
      gender
    } = req.fields;

    //validation
    let validator = new Validator();
    let errors = await validator.customerRegister(req.fields);

    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    let db = await con.db();

    //checking if account with phone exists
    let exists = await db.collection("customer").findOne({ phone });
    if (exists) return res.status(400).json({ phone: "There is already an account with this phone number" });

    //checking if account with email exists
    if (email && email != "" && email != " ") {
      exists = await db.collection("customer").findOne({ email: { $regex: new RegExp("^" + email, "i") } });
      if (exists) return res.status(400).json({ email: "There is already an account with this email" });
    }

    //encrypt password
    let salt = await bcrypt.genSalt(10);
    let hash = await bcrypt.hash(password, salt);


    let newCustomer = {
      firstName, lastName,
      country, city,
      address1, address2, zipCode,
      phone, email,
      gender,
      additionalInfo,
      password: hash,
      added: new Date(),
    };


    // *dealer code
    if (dealerCode) {
      let dealer = await db.collection('dealer').findOne({ code: dealerCode.toUpperCase() });
      if (!dealer) return res.status(400).json({ dealerCode: "Invalid dealer code" });

      newCustomer.dealer = dealer._id;
    }


    let result = await db.collection("customer").insertOne(newCustomer);

    // for email verification
    let verificationKey = generateRandomString(6, 100);
    let dateNow = new Date();
    let expire = new Date(dateNow.setHours(dateNow.getHours() + 3));


    let confirmData = {
      customer: `${result.ops[0]._id}`,
      token: verificationKey,
      expire, // 3 hour
      added: new Date()
    }

    await db.collection('recoveryToken').insertOne(confirmData);

    delete newCustomer.password;

    const payload = {
      ...newCustomer,
      userType: 'customer',
    }

    // payload, secretKey, token expiry date(7days)
    const token = jwt.sign(payload, process.env.JWT_SECRET_KEY, {
      expiresIn: 30 * 24 * 3600,
    });


    res.json({
      inserted: result.ops,
      token: `Bearer ${token}`,
      status: "pending",
    });
    

    // send confirmation email
    let mailContent = {
      recipient: newCustomer.email,
      subject: "Confirm Email",
      html: `
      <div style="font-family: sans-serif;">
        <p>Hello ${firstName || ""} ${lastName || ""},</p>
        <p>Welcome to <a title="${req.hostName}" href="${req.hostName}" target="_blank" rel="noopener">${req.hostName}</a>.
          We are almost done creating your account. You can use this account to login into  
          <a title="${req.hostName}" href="${req.hostName}" target="_blank" rel="noopener">${req.hostName}</a>.
        </p>
        <p>Please use this secret code to verify your email.</p>
        <p style="text-align: center; border: 3px solid #317399; padding: 10px 30px; width: 100px; margin: 0 auto;">
          ${verificationKey}
        </p>
        <p>&nbsp;</p>
        <p style="font-weight: bold;">This code will be valid for only three hours. Please verify within time</p>
        <p>If you didn't request to register from this email, you can safely ignore this message</p>
      </div>
    `,
    };

    sendMail(mailContent);

    //*notify
    //notify admin
    let notification = {
      type: "customer",
      heading: "New Customer Registered",
      text: `${firstName} ${lastName} registered as a customer`,
      eventId: result.insertedId,
    };
    notify(notification);

    //send confirmation
    autoMail.eventMail("newCustomer", newCustomer.email, { user: newCustomer });
    autoSMS.eventSMS("newCustomer", newCustomer.phone, { user: newCustomer });

  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Sorry, something went wrong!" });
  }
});


router.post('/account/resendEmail', async (req, res) => {
  try {

    let customer;
		let authToken = req.headers.authorization;
		if (authToken) {
			try {
				customer = await jwt.verify(authToken.split(' ')[1], process.env.JWT_SECRET_KEY);
			} catch (error) {
				console.log(error)
			  return res.status(403).json({ authorization: "Unauthorized access" });
			}
		} else {
      return res.status(400).json({user: "Send the token!"});
    }

    let db = await con.db();

    // delete existing token
    await db.collection('recoveryToken').deleteOne({customer: customer._id})

    // for email verification
    let verificationKey = generateRandomString(6, 100);
    let dateNow = new Date();
    let expire = new Date(dateNow.setHours(dateNow.getHours() + 3));


    let confirmData = {
      customer: customer._id,
      token: verificationKey,
      expire, // 3 hour
      added: new Date()
    }

    await db.collection('recoveryToken').insertOne(confirmData);
    
    let mailContent = {
      recipient: customer.email,
      subject: "Confirm Email",
      html: `
      <div style="font-family: sans-serif;">
        <p>Hello ${customer.firstName || ""} ${customer.lastName || ""},</p>
        <p>Welcome to <a title="${req.hostName}" href="${req.hostName}" target="_blank" rel="noopener">${req.hostName}</a>.
          We are almost done creating your account. You can use this account to login into  
          <a title="${req.hostName}" href="${req.hostName}" target="_blank" rel="noopener">${req.hostName}</a>.
        </p>
        <p>Please use this secret code to verify your email.</p>
        <p style="text-align: center; border: 3px solid #317399; padding: 10px 30px; width: 50px; margin: 0 auto;">
          ${verificationKey}
        </p>
        <p>&nbsp;</p>
        <p style="font-weight: bold;">This code will be valid for only three hours. Please verify within time</p>
        <p>If you didn't request to register from this email, you can safely ignore this message</p>
      </div>
    `,
    };

    sendMail(mailContent);

    return res.json({
      success: "New email sent",
      status: "pending"
    })

  } catch(error) {
    console.log(error);
    res.status(500).json({error: "Sorry, something went wrong!"});
  }
})

module.exports = router;



