const router = require('express').Router();
const passport = require('passport')
const isCustomer = require('../../lib/auth/customerAuth.js')


router.use('/wishlist', passport.authenticate('jwt', {session: false}), isCustomer, require('./api/wishlist.js'));
router.use('/order', passport.authenticate('jwt', {session: false}), isCustomer, require('./api/order.js'));
router.use('/profile', passport.authenticate('jwt', {session: false}), isCustomer, require('./api/profile.js'));
router.use("/address", passport.authenticate("jwt", {session: false}), isCustomer, require("./api/address.js"));
router.use("/cart", passport.authenticate("jwt", {session: false}), isCustomer, require("./api/cart.js"));
router.use("/feedback", passport.authenticate("jwt", {session: false}), isCustomer, require("./api/feedback.js"));


module.exports = router;
 