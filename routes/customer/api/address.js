const router = require('express').Router();


router.use('/add',require("./address/add.js"));
router.use('/', require("./address/get.js"));
router.use('/update', require("./address/update.js"));

module.exports = router;