const router = require("express").Router();
const con = require("../../../../lib/db");
const Validator = require("../../../../validator/customer");
const _ = require("lodash");

/*
 General guide for this file
    * Export the router
    * Register this file into /customer/api/address.js
*/

/*
    @route - /customer/api/address/update
    @method - POST
    @desc - update payload to customer address object
*/
router.post("/", async (req, res) => {
  try {
    // check for customer id validity
    let customer = req.user;
    if (!con.ObjectID.isValid(customer._id))
      return res.status(400).json({ customerId: "Invalid Customer id" });

    let {
      title,
      firstName, lastName,
      country, city, address1, address2, zipCode,
      phone, email,
    } = req.fields;

    // validation
    let validator = new Validator();
    let errors = await validator.customerAddress(req.fields);

    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    // fetching and checking if this customer exist in db
    let customerId = new con.ObjectID(customer._id);
    let db = await con.db();
    let foundCustomer = await db.collection("customer").findOne({ _id: customerId });

    let existingAdressTitles = Object.keys(foundCustomer.address);

    // check if address exceeds more than 5
    if (existingAdressTitles.length > 5)
      return res.status(400).json({ address: "Max address limit exceeded" });

    //check address with same title existence
    if (!existingAdressTitles.includes(title))
      return res.status(400).json({ title: `${title} address not found` });

    let newAddress = {
      firstName, lastName,
      country, city, address1, address2, zipCode,
      phone, email,
    };

    await db.collection("customer").updateOne(
        { _id: customerId },
        { $set: { [`address.${title}`]: newAddress } }
      );

    return res.status(200).json({ updated: { [title]: newAddress } });
  } catch (err) {
    console.log(err);
    // is this status code right, need some help at res.status()
    res.status(500).json({
      error: "Failed to add/update address",
    });
  }
});


/*
    @route - /customer/api/address/remove
    @method - DELETE
    @desc - remove all address info except default address
*/

// router.post("/delete", async (req, res) => {

//     try {
//         let customerId = req.session.customer._id;
        
//         // fetching customer document
//         let db = await con.db();
//         let id = new con.ObjectID(customerId)
//         let result = await db.collection("customer").findOne({_id: id});

//         if(!result.address) return res.status(404).json({msg: "No address found"})

//         let defaultAddress = result.address.default
//         // res.send(result.address.default)
//         await db.collection("customer").updateOne(
//           {_id: id}, 
//           {$set: {address: {"default": defaultAddress}}}
//         )

//         res.status(200).json({success: "Addresses are deleted successfully"});
//     } catch(err) {
//         console.log(err);
//         res.status(500).json({error: "Failed to delete addresses"})
//     }
// })

/*
    @route - /customer/api/address/remove/:label
    @method - DELETE
    @desc - delete specific address info by label
*/
router.post("/delete/:title", async (req, res) => {
  try {
    let customerId = req.user._id;
    let title = req.params.title;

    // fetching customer doc
    let db = await con.db();
    customerId = new con.ObjectID(customerId);

    let result = await db.collection("customer").findOne({ _id: customerId });
    if (!result.address[title])
      return res.status(404).json({ error: "No address found with this title" });

    await db.collection("customer").updateOne(
      { _id: customerId }, 
      { $unset: { [`address.${title}`]: "" } }
    );

    res.status(200).json({ success: "Address deleted " });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Failed to delete addresses" });
  }
});

module.exports = router;
