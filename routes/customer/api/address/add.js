const router = require("express").Router();
const Validator = require("../../../../validator/customer");
const _ = require("lodash");
const con = require("../../../../lib/db");

/*
 General guide for this file
    * Export the router
    * Register this file into /customer/api/address.js
*/

/*
** Temporary notes
This api will take effect on
1. any api that requires address, need to change it to address.default 

/*
    @route - [/customer/api/address/add, /customer/api/address/update
    @method - POST
    @desc - add/update address into customer profile(batch)
*/
router.post("/", async (req, res) => {
  try {
    let customer = req.user

    let {
      title,
      firstName, lastName,
      country, city, address1, address2, zipCode,
      phone, email
    } = req.fields;

    // validation
    let validator = new Validator();
    let errors = await validator.customerAddress(req.fields);

    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    // fetching and checking if this customer exist in db
    let customerId = new con.ObjectID(customer._id);
    let db = await con.db();
    let foundCustomer = await db.collection("customer").findOne({ _id: customerId });

    let existingAddressTitles = (foundCustomer && foundCustomer.address)? 
      Object.keys(foundCustomer.address) : [];

    // check if address exceeds more than 5
    if (existingAddressTitles.length > 5)
      return res.status(400).json({ address: "Max address limit exceeded" });

    //check address with same title existence
    if (existingAddressTitles.includes(title))
      return res.status(400).json({ title: `${title} address already exists` });

    let newAddress = {
      firstName, lastName,
      country, city, address1, address2, zipCode,
      phone, email,
    };

    await db.collection("customer").updateOne(
        { _id: customerId },
        { $set: { [`address.${title}`]: newAddress } }
      );

    return res.status(200).json({ updated: { [title]: newAddress } });
  } catch (err) {
    console.log(err);
    res.status(500).json({error: "Sorry, something went wrong"});
  }
});

module.exports = router;
