const router = require("express").Router();
const con = require("../../../../lib/db");

/*
 General guide for this file
    * Export the router
    * Register this file into /customer/api/address.js
*/

/*
    @route - /customer/api/address
    @method - GET
    @desc - get all address info
*/

router.get("/", async (req, res) => {

    try {
        let customerId = req.user._id;

        // fetching customer document
        let db = await con.db();
        let result = await db.collection("customer").findOne({_id: new con.ObjectID(customerId)});

        return res.json(result.address);
    } catch(err) {
        console.log(err);
        res.status(500).json({error: "Sorry something went wrong"});
    }
});


/*
    @route - /customer/api/address/:name
    @method - GET
    @desc - get specific address info by address label
*/
router.get("/:title", async (req, res) => {
    try {
        let customerId = req.user._id;
        let title = req.params.title;

        // fetch address info by label
        let db = await con.db();
        let result = await db.collection("customer").findOne({_id: new con.ObjectID(customerId)});

        if(!result.address[title]) 
            return res.status(404).json({error: "No address found with this title"})

        return res.status(200).json(result.address[title])

    } catch(err) {
        console.log(err);
        res.status(500).json({error: "Sorry something went wrong"});
    }
})

module.exports = router;