const router = require('express').Router();
const Validator = require('../../../validator/customer.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");

require('dotenv').config();

router.get('/', async (req, res) => {

  // console.log(req.headers, 123);

  let customer = req.user;
  let db = await con.db();

  if (!customer || !customer._id) {
    return res.status(403).json({ error: 'log in to continue' });
  }

  let userInfo = await db.collection('customer').findOne({ _id: new con.ObjectID(customer._id) });
  if (!userInfo) res.status(404).json({ user: "No user found!" });

  delete userInfo.password;

  res.json(userInfo);

});

router.post('/update', async (req, res) => {
  try {
    let customer = req.user;
    if (!customer) return res.status(403).json({ '403': 'Access Forbidden' });

    let { firstName, lastName, country, city, address1, address2, zipCode, phone, email, additionalInfo, dealerCode } = req.fields;


    //validation
    let validator = new Validator();
    let errors = await validator.customerUpdate(req.fields);

    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    let db = await con.db();

    let newCustomer = {
      firstName, lastName, country, city, address1, address2, zipCode, phone, email, additionalInfo,
      updated: new Date(),
    }


    // *dealer code
    if (dealerCode) {
      let dealer = await db.collection('dealer').findOne({ code: dealerCode.toUpperCase() });
      if (!dealer) return res.status(400).json({ dealerCode: "Invalid dealer code" });

      newCustomer.dealer = dealer._id;
    }

    let result = await db.collection('customer').updateOne(
      { _id: new con.ObjectID(customer._id) },
      { $set: newCustomer }
    );

    const payload = {
      _id: customer._id, dealer: newCustomer.dealer,
      firstName, lastName, country, city, address1, address2, zipCode, phone, email, additionalInfo, 
      userType: "customer"
    }

    const token = jwt.sign(payload, process.env.JWT_SECRET_KEY, { expiresIn: 30 * 24 * 1000 })

    res.json({
      success: 'Profile Updated!',
      token: `Bearer ${token}`
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      error: 'Sorry, something went wrong!',
    });
  }
});


/**
 * change password route
 * 
 */
router.post('/changePassword', async (req, res) => {
  try {

    //validation
    let { password, newPassword, newPassword2 } = req.fields;
    let validator = new Validator();

    let errors = await validator.customerChangePassword(req.fields);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    let customer = req.user;
    if (!customer) return res.status(400).json({ customer: 'not signed in' });
    let customerId = customer._id;

    //get user info
    let db = await con.db();
    let user = await db.collection('customer').findOne({ _id: new con.ObjectID(customerId) });

    if (!user) return res.status(400).json({ customer: 'invalid customer' }); //no such user

    //compare password
    let result = await bcrypt.compare(password, user.password);
    if (!result) return res.status(400).json({ password: 'Incorrect password' });

    //hash new password
    let salt = await bcrypt.genSalt(10);
    let hash = await bcrypt.hash(newPassword, salt);

    await db.collection('customer').updateOne({ _id: new con.ObjectID(customerId) }, { $set: { password: hash } });

    res.json({ success: 'Password Changed Successfully' });

  } catch (err) {
    console.log(err);
    res.status(500).json({
      error: 'Sorry, something went wrong!'
    });
  }
});

module.exports = router;
