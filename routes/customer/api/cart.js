const router = require('express').Router();


const addRouter = require('./cart/add.js')
const getRouter = require('./cart/get.js')
const updateRouter = require('./cart/update.js')

router.use('/add', addRouter);
router.use('/update', updateRouter)
router.use('/', getRouter);



module.exports = router;

