const router = require("express").Router();
const con = require("../../../lib/db.js");
const jwt = require("jsonwebtoken");
const Validator = require("../../../validator/customer.js");
const _ = require('lodash');
const feedbackFunctions = require('./../../../lib/dbFunctions/public/feedback.js');


router.post('/', async (req, res) => {

  try {

    //customer info
    let customer = await jwt.verify(req.headers.authorization.split(' ')[1], process.env.JWT_SECRET_KEY);
       

    let {filter = {}, resolve = {}, options = {}} = req.fields;

    let feedbackList = await feedbackFunctions.getFeedback(
      {
        customer: customer._id,
        product: filter.product,
        _id: filter._id,
        status: filter.status
      },
      {
        product: resolve.product,
        customer: resolve.customer
      },
      options
    );

    return res.json(feedbackList);

  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: 'Sorry, something went wrong.' })
  }
})


router.post('/add', async (req, res) => {

  try {

    //get customer
    let customer = await jwt.verify(req.headers.authorization.split(' ')[1], process.env.JWT_SECRET_KEY);
      
    //validate
    let validator = new Validator;
    let errors = await validator.addFeedback(req.fields);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);


    let { title, message, product } = req.fields;
    let feedback = await feedbackFunctions.createFeedback({
      title,
      message,
      product,
      customer: customer._id
    });

    return res.json(feedback);

  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: 'Sorry, something went wrong.' })
  }
})


router.post('/update', async (req, res) => {

  try {

    //get customer
    let customer = await jwt.verify(req.headers.authorization.split(' ')[1], process.env.JWT_SECRET_KEY);
      

    let {_id ,  title, message, product, } = req.fields;
    if(!con.ObjectID.isValid(_id)) return res.status(400).json({_id: "invalid id"});


    //validate
    let validator = new Validator;
    let errors = await validator.addFeedback(req.fields);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);


    let feedback = await feedbackFunctions.updateFeedback({
      _id,
      title,
      message,
      product,
      customer: customer._id
    });

    return res.json(feedback);

  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: 'Sorry, something went wrong.' })
  }
})

router.post('/delete', async (req, res) => {

  try {

    //get customer
    let customer = await jwt.verify(req.headers.authorization.split(' ')[1], process.env.JWT_SECRET_KEY);
      

    let {_id} = req.fields;
    if(!con.ObjectID.isValid(_id)) return res.status(400).json({_id: "invalid id"});



    let feedback = await feedbackFunctions.deleteFeedback({
      _id,
      customer: customer._id
    });

    return res.json({success: "feedback deleted"});

  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: 'Sorry, something went wrong.' })
  }
})

module.exports = router;