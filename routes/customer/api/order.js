const router = require('express').Router();
const Validator = require('../../../validator/admin.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const orderFunctions = require('./../../../lib/dbFunctions/customer/orderFunctions.js')


//adding order is not done through this route
// navigate to route/pubic/api/cart/post.js for 'cart/checkout' route
// *adding order is done through '/api/cart/checkout/' POST route

router.get('/', async(req, res)=>{
    let customer = req.user;
    let orders = await orderFunctions.getOrderList(customer._id, req.query);
    res.json(orders);
});
router.get('/:id', async(req, res)=>{
    let id = req.params.id;
    let customer = req.user._id;

    let db = await con.db();
		
    let aggregationPipeLine = [
        {$match: {customer: new con.ObjectID(customer)}},
        {$match: {_id: new con.ObjectID(id)}},
        {$lookup:{
            from: "product",
            localField: "products._id",
            foreignField: "_id",
            as: "product",
        }},
        {$unwind:{
            path:'$product',
            preserveNullAndEmptyArrays:true,
        }},
        {$lookup:{
            from: "images",
            localField: "product.cover",
            foreignField: "_id",
            as: "product.cover",
        }},
        {$unwind:{
            path:'$product.cover',
            preserveNullAndEmptyArrays:true,
        }},
        {
        $group: {
            _id: '$_id',
            product: { $push: '$product' },
            doc: { $first: '$$ROOT' }
        }
        },
        { $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
        { $project: { doc: 0 } },
        {$lookup:{
            from: "customer",
            localField: "customer",
            foreignField: "_id",
            as: "customer",
        }},
        {$unwind:{
            path:'$customer',
            preserveNullAndEmptyArrays:true,
        }},
    ]

    let order =await db.collection('order').aggregate(aggregationPipeLine).toArray();
    order = order[0];
    // console.log(order)
    if(!order) return res.status(400).json({id: 'no order with given id'});

    //combine product & products
    order.products.forEach((i, index, array)=>{
        let productInfo = order.product.find(item=>{ if(i._id && item._id) return item._id.toHexString() == i._id.toHexString()})
        i = { ...productInfo, ...i},
        array[index] = i;
    })

    delete order.product;


    let siteInfo = await db.collection('settings').findOne({name:'site'}); //for invoice
    siteInfo  =siteInfo? siteInfo : {};
    siteInfo.domain = req.hostOptions.domain;

    res.json({order})
});

module.exports = router;
