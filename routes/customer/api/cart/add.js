const router = require('express').Router();
const Validator = require('../../../../validator/customer.js');
const _ = require('lodash');
const con = require('../../../../lib/db.js');

router.post('/', async (req, res) => {
	try {

		//validate item array
		let items = req.fields.items;
		if (!Array.isArray(items)) return res.status(400).json({ items: "invalid formation" });

		let db = await con.db();
		let customerId = req.user._id;
		let cart = await db.collection('cart').findOne({ customer: new con.ObjectID(customerId) });

		//create new if not found
		if (!cart) cart = { customer: new con.ObjectID(customerId), items: [] }
		cart.updated = new Date();

		let newItemIds = [];
		let responseItems = {};

		for (item of items) {
			//validate id
			if (!item.product || !con.ObjectID.isValid(item.product))
				return res.status(400).json({ error: 'Invalid Product Id' });
			if (!item.quantity || item.quantity < 1) item.quantity = 1;

			let cartItem = cart.items.find(i => {
				return (i.product.toHexString() == item.product
					&& i.variation.toHexString() == item.variation);
			})

			if (cartItem) {
				//increase quantity for existing item
				cartItem.quantity += +item.quantity;

				//adding as object to avoid duplicity in response 
				responseItems[item.product + item.variation] = { ...cartItem, merged: true };
			} else {
				//push for adding as new item
				newItemIds.push(new con.ObjectID(item.product));
			}

		}

		// get new items info from db
		let products = await db.collection('product').aggregate([
			{ $match: { _id: { $in: newItemIds } } },
			{ $project: { name: 1, cover: 1, price: 1, attributes: 1, url: 1, pricing: 1, price: 1 } },
			{ $lookup: { from: 'images', localField: 'cover', foreignField: '_id', as: 'cover', } },
			{ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } }
		]).toArray();

		for (let product of products) {

			let item = items.find(i => i.product == product._id.toHexString());

			//set to default variation if no variation is chosen
			if (!item.variation || !con.ObjectID.isValid(item.variation)) {
				item.variation = product.price.defaultVariation;
			}


			let variation = product.pricing.find(i =>
				i._id.toHexString ? i._id.toHexString() == item.variation : i._id == item.variation
			);
			
			if (!variation) return res.status(400).json({ variationId: "Invalid variation id" });

			//new cart item
			let cartItem = {
				quantity: +item.quantity,
				product: product._id,
				variation: new con.ObjectID(item.variation),
				name: product.name,
				cover: product.cover,
				price: variation.price.offer || variation.price.regular,
				totalPrice: (variation.price.offer || variation.price.regular) * item.quantity,
				url: product.url
			};
			cart.items.push(cartItem);
			responseItems[item.product + item.variation] = { ...cartItem, merged: false };
		}

		let result = await db.collection('cart').updateOne(
			{ customer: new con.ObjectID(customerId) },
			{ $set: cart },
			{ upsert: true }
		);

		return res.json(Object.values(responseItems));

	} catch (err) {
		console.log(err)
		res.status(500).json({ error: "Sorry something went wrong!" });
	}
})

module.exports = router;
