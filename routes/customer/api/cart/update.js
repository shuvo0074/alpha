const router = require('express').Router();
const _ = require('lodash');
const con = require('../../../../lib/db.js');


router.post('/quantity', async (req, res) => {
  try {
    // *validate ids
    let {product, variation, quantity} = req.fields;
    if(!con.ObjectID.isValid(product)) return res.status(400).json({product: "invalid product id"});
    if(!con.ObjectID.isValid(variation)) return res.status(400).json({variation: "invalid variation id"});

    // *validate quantity
    quantity = +quantity;
    if(!isFinite(quantity) || quantity < 1) return res.status(400).json({quantity: "set a valid quantity"})


    let db = await con.db();

    //get existing cart
    let customerId = req.user._id;

    let cart = await db.collection('cart').findOne({customer: new con.ObjectID(customerId)});
    if(!cart || !cart.items || cart.items.length == 0) return res.status(400).json({cart: "cart is empty"});

    //find item
    let cartItem = cart.items.find( (i, index)=> {
      return (i.product == product && i.variation.toHexString() == variation)
    })

    if(!cartItem) return res.status(400).json({product: "product with given variation not found in cart"});

    cartItem.quantity = quantity;
    cartItem.totalPrice = cartItem.price * quantity;

    cart.updated = new Date();

    let result = await db.collection('cart').updateOne({customer: new con.ObjectID(customerId)}, 
    {$set: cart});

    return res.json({updated: cartItem});

  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: 'Sorry, something went wrong' });
  }
});


router.post('/remove', async (req, res) => {
  try {
    // *validate ids
    let {product, variation} = req.fields;
    if(!con.ObjectID.isValid(product)) return res.status(400).json({product: "invalid product id"});
    if(!con.ObjectID.isValid(variation)) return res.status(400).json({variation: "invalid variation id"});

    let db = await con.db();

    //get existing cart
    let customerId = req.user._id;

    let cart = await db.collection('cart').findOne({customer: new con.ObjectID(customerId)});
    if(!cart || !cart.items || cart.items.length == 0) return res.status(400).json({cart: "cart is empty"});

    //find item
    let cartItemIndex;
    let cartItem = cart.items.find( (i, index)=> {

      if(i.product == product && i.variation.toHexString() == variation){
        cartItemIndex = index; 
        return true;
      }

    })

    if(!cartItem) return res.json({notFound: "item not in cart"});

    cart.items.splice(cartItemIndex, 1); //remove item form cart

    cart.updated = new Date();
    
    let result = await db.collection('cart').updateOne({customer: new con.ObjectID(customerId)}, 
    {$set: cart});

    return res.json({success: "item removed from cart"});

  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: 'Sorry, something went wrong' });
  }
});

router.get('/clear', async (req, res) => {
  try {

    let db = await con.db();

    //get customer information
    let customerId = req.user._id ;

    let result = await db.collection('cart').updateOne({customer: new con.ObjectID(customerId)},
    {$set:{
      items:[],
      updated: new Date()
    }});
    
    if(result.matchedCount == 0) return res.status(400).json({cart: "cart is empty"});

    return res.json({success: "Cart cleared"});

  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: 'Sorry, something went wrong' });
  }
});

module.exports = router;
