const router = require('express').Router();
const _ = require('lodash');
const con = require('../../../../lib/db.js');

/**
 * cart view api
 * return cart from database if logged in
 * otherwise return cart from session
 *
 * @returns {Object} cart
 */

router.get('/', async (req, res) => {
  try {

    let db = await con.db();
    let customerId = req.user._id;
    let cart = await db.collection('cart').findOne({ customer: new con.ObjectID(customerId) });
    
    if(!cart || !cart.items) return res.json([]);
    return res.json(cart.items);

  } catch (err) {
    console.log(err)
    res.status(500).json({ error: 'Sorry, something went wrong' });
  }
});

module.exports = router;
