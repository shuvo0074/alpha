const router = require('express').Router();

router.use('/',  require('./wishlist/add.js'));
router.use('/add', require('./wishlist/get.js'));
router.use('/update', require('./wishlist/update.js'));

module.exports = router;