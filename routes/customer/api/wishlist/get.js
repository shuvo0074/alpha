const router = require('express').Router();
const _ = require('lodash')
const con = require('../../../../lib/db.js')
const fs = require('fs');
const util = require('util');

fs.readFile = util.promisify(fs.readFile);
fs.writeFile = util.promisify(fs.writeFile);
fs.unlink = util.promisify(fs.unlink);

/**
 * @route get wishlist
 * @method get
 * @return array of items
 */

router.get('/', async (req, res)=>{
    try{
        let db = await con.db();

        let customerId = req.user._id;

        //get list
        let wishlist = await db.collection('wishlist').findOne({customer: new con.ObjectID(customerId)});
        if(!wishlist) return res.json([]);

        res.json(wishlist.items);
   }catch(err){
       console.log(err);
       res.status(500).json({msg:err.message})
   }

})


module.exports = router;