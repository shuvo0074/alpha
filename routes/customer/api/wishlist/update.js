const router = require('express').Router();
const Validator = require('../../../../validator/customer.js');
const _ = require('lodash');
const con = require('../../../../lib/db.js');
const fs = require('fs');
const util = require('util');

router.get('/remove/all', async (req, res) => {
  try {
    let db = await con.db();

    //get customer information
    let customerId = req.user._id;

    await db
      .collection('wishlist')
      .updateOne(
        { customer: new con.ObjectID(customerId) },
        { $set: { items: [] } }
      );

    res.json({ msg: 'wishlist updated' });
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: err.message });
  }
});

/**
 * @route wishlist remove
 * @param {String} id : id of product to remove
 * @method GET
 */
router.get('/remove/:id', async (req, res) => {
  try {
    let itemId = req.params.id;
    let db = await con.db();

    if (!itemId || !con.ObjectID.isValid(itemId))
      return res.status(400).json({ items: 'Invalid Item Id' });

    //get customer information
    let customerId = req.user._id;

    await db.collection('wishlist').updateOne(
      { customer: new con.ObjectID(customerId) },
      {
        $pull: {
          items: new con.ObjectID(itemId),
        },
      }
    );

    res.json({ msg: 'wishlist updated' });
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: err.message });
  }
});

module.exports = router;