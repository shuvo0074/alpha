const router = require('express').Router();
const Validator = require('../../../../validator/customer.js');
const _ = require('lodash');
const con = require('../../../../lib/db.js');
const fs = require('fs');
const util = require('util');

/**
 *
 * @route wishlist add
 *
 * fields:
 * @field item: id of product
 * @method POST
 *
 */

router.post('/', async (req, res) => {
  try {
    let { item } = req.fields;
    let db = await con.db();

    if (!item || !con.ObjectID.isValid(item))
      return res.status(400).json({ items: 'Invalid Item' });

    let product = await db
      .collection('product')
      .findOne({ _id: new con.ObjectID(item) });
    if (!product) return res.status(400).json({ items: 'Item not found' });

    //get customer information
    let customerId = req.user._id;

    await db.collection('wishlist').updateOne(
      { customer: new con.ObjectID(customerId) },
      {
        $push: {
          items: new con.ObjectID(item),
        },
      },
      { upsert: true }
    );

    res.json({ msg: 'wishlist updated' });
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: err.message });
  }
});

module.exports = router;