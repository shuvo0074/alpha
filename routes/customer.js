const router = require("express").Router();
// const customerAuth = require('../lib/auth/customerAuth.js');
const authRouter = require("./customer/auth.js");
const apiRouter = require("./customer/api.js");

//authentication routers
router.use("/auth", authRouter);
// router.use(/\/((?!auth).)*/, customerAuth);

router.use("/api", apiRouter);

module.exports = router;
