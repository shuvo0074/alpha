const router = require('express').Router();

const productRouter = require('./api/product.js')
const categoryRouter = require('./api/category.js')
const brandRouter = require('./api/brand.js')
const tagRouter = require('./api/tag.js')
const componentRouter = require('./api/component.js')
const searchRouter = require('./api/search.js')
const analyticsRouter = require('./api/analytics.js')
const geoRouter = require('./api/geo.js')
const deliveryRouter = require('./api/delivery.js')
const pageRouter = require('./api/page.js')
const checkoutRouter = require('./api/checkout.js')
const siteRouter = require('./api/site.js')
const feedbackRouter = require('./api/feedback.js');
const contactRouter = require('./api/contact.js');
const statsRouter = require('./api/statistics.js');
// const customProductRouter = require('./api/custom.js');

const paymentIPNRouter = require('./api/paymentIPN.js')
router.use(categoryRouter);
router.use(productRouter);
router.use(categoryRouter);
router.use(brandRouter);
router.use(tagRouter);
router.use(componentRouter);
router.use(searchRouter);
router.use(analyticsRouter);
router.use(geoRouter);
router.use(deliveryRouter);
router.use(pageRouter);
router.use(checkoutRouter);
router.use(siteRouter);
router.use(feedbackRouter);
router.use(contactRouter);
router.use(statsRouter);
router.use(paymentIPNRouter);
// router.use('/custom/type', customProductRouter);

module.exports = router;
