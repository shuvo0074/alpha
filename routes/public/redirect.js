const router = require('express').Router();


router.post('/order/success', async (req, res)=>{
    res.redirect('/order/success')
})

router.post('/order/fail', async (req, res)=>{
    res.redirect('/order/fail')
})

router.post('/order/cancel', async (req, res)=>{
    res.redirect('/order/cancel')
})


router.get('/onionhat', async(req, res)=>{
    res.redirect('/category/onion-(tcb-imported)')
})
module.exports = router;
