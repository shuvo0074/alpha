const router = require('express').Router();
const con = require('../../../../lib/db.js');
const dbFunctions = require('../../../../lib/dbFunctions/publicFunctions.js');

/**
 * get category list
 *
 *
 */
router.get('/category', async (req, res) => {
  let categoryList = await dbFunctions.getCategoryList({
    limit: 2,
    page: 2,
    image: 'true',
    subCategory: true,
    cover: false,
    type: 'sub'
  });
  res.json(categoryList);
});

/**
 * get single category
 */

router.get('/category/id/:id', async (req, res) => {
  const id = req.params.id;
  let category = await dbFunctions.getCategory(id, {
    image: true
  });
  res.json(category);
});

/**
 * get single
 *
 */

router.get('/category/image/:id', async (req, res) => {
  let id = req.params.id;
  let image = await dbFunctions.getCategoryImage(id);
  res.json(image);
});

/**
 * get parent category by id
 *
 */

router.get('/category/parentCategory/:id', async (req, res) => {
  let id = req.params.id;

  let parentCategory = await dbFunctions.getCategoryParent(id);
  res.json(parentCategory);
});

/**
 * get sub category by id
 *
 */

router.get('/category/subCategory/:id', async (req, res) => {
  let id = req.params.id;
  let subCategory = await dbFunctions.getSubCategory(id);
  res.json(subCategory);
});

/**
 * get category products by id
 *
 */

router.get('/category/product/:id', async (req, res) => {
  let id = req.params.id;
  let products = await dbFunctions.getCategoryProduct(id);
  res.json(products);
});

/**
 * get single category with url
 *
 */

router.get(
  ['/category/:parentName/:name', '/category/:name'],
  async (req, res) => {
    let url = req.path;
    let category = await dbFunctions.getCategoryByUrl(url, {
      image: true
    });
    res.json(category);
  }
);

module.exports = router;
