const router = require('express').Router();
const path = require('path');
const ejs = require('ejs');
const fs = require('fs');
const con = require('../../../lib/db.js');
const moment = require('moment')
const {getFormattedDate} = require('./../../../lib/utility.js');
const userAgent = require('express-useragent');
const cookie  =require('cookie');
const uuid = require('uuid');

router.get('/analytics', async (req, res)=>{


    //type
    let validTypes =  ['page', 'category', 'product', 'brand', 'tag', 'page', 'post', 'postCategory', 'postTag'];
    let type = req.query.type;
    // if(!type) return res.status(400).json({type:"missing parameter type"});
    if(!validTypes.includes(type)) return res.status(400).json({type:"Invalid type"});

    //item 
    let item = req.query.item;
    if(!item) return res.status(400).json({item:"missing parameter item"});
    if(!con.ObjectID.isValid(item)) return res.status(400).json({item: "invalid item id"});
 
    //validate item
    let db = await con.db();
    item  = await db.collection(type).findOne({_id: new con.ObjectID(item)});
    if(!item) return res.status(400).json({item: "item not found"});


    //get analytics token
    const cookies = cookie.parse(req.headers.cookie || '');
    
    let dataToken = cookies.dataToken;

    //set new cookie token
    if(!dataToken){
        dataToken = uuid.v4(); 
        res.setHeader('Set-Cookie', cookie.serialize('dataToken', String(dataToken), {
            httpOnly: true,
            maxAge: 60 * 60 * 24 * 31 * 12 * 10 // 10 years
        }));
    }

    //check returning 
    let returning = false;
    let previousData = await db.collection('analytics').findOne({token: dataToken});
    if(previousData){
        returning = true;
    }

    //user information
    let remoteAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress; //get user ip

    
    let browser, os, platform, device, deviceModel, osVersion;

    if(req.query.browser && req.query.browser == "app"){
        browser = req.query.browser;
        os = req.query.os;
        platform= req.query.os == "android"? 'Android' : req.query.os == "ios"?  "iPhone" : null ;
        device = req.query.device;
        deviceModel = req.query.deviceModel;
        osVersion = req.query.osVersion;
    }else{
        let ua = userAgent.parse( req.headers['user-agent']) ;
        browser = ua.browser;
        os = ua.os;
        platform = ua.platform;
        device = ua.isDesktop? 'Desktop' : ua.isMobile ? 'Mobile' : ua.isTablet ? 'Tablet' : ua.isSmartTV ?
                    'TV' : 'Others';
    }

    
    let {longitude, latitude} = req.query.coords || {};

    let location = {
        type: "Point",
        coordinates: [longitude, latitude]
    }


    let analyticData = {
        date: new Date(), 
        type,  
        remoteAddress, 
        browser, 
        os, 
        platform, 
        returning, 
        device,
        deviceModel,
        osVersion,
        token: dataToken,
        item: item._id,
        location
    }
    if(req.user){
        analyticData.user = req.user._id;
    }

    let result = await db.collection(`analytics`).insertOne(analyticData); 
    return res.status(200).end();

})



router.post('/analytics', async (req, res)=>{


    //type
    let validTypes =  ['page', 'category', 'product', 'brand', 'tag', 'page', 'post', 'postCategory', 'postTag'];
    let type = req.fields.type;
    // if(!type) return res.status(400).json({type:"missing parameter type"});
    if(!validTypes.includes(type)) return res.status(400).json({type:"Invalid type"});

    //item 
    let item = req.fields.item;
    if(!item) return res.status(400).json({item:"missing parameter item"});
    if(!con.ObjectID.isValid(item)) return res.status(400).json({item: "invalid item id"});
 
    //validate item
    let db = await con.db();
    item  = await db.collection(type).findOne({_id: new con.ObjectID(item)});
    if(!item) return res.status(400).json({item: "item not found"});


    //get analytics token
    const cookies = cookie.parse(req.headers.cookie || '');
    
    let dataToken = cookies.dataToken;

    //set new cookie token
    if(!dataToken){
        dataToken = uuid.v4(); 
        res.setHeader('Set-Cookie', cookie.serialize('dataToken', String(dataToken), {
            httpOnly: true,
            maxAge: 60 * 60 * 24 * 31 * 12 * 10 // 10 years
        }));
    }

    //check returning 
    let returning = false;
    let previousData = await db.collection('analytics').findOne({token: dataToken});
    if(previousData){
        returning = true;
    }

    //user information
    let remoteAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress; //get user ip

    
    let browser, os, platform, device, deviceModel, osVersion;

    if(req.fields.browser && req.fields.browser == "app"){
        browser = req.fields.browser;
        os = req.fields.os;
        platform= req.fields.os == "android"? 'Android' : req.fields.os == "ios"?  "iPhone" : null ;
        device = req.fields.device;
        deviceModel = req.fields.deviceModel;
        osVersion = req.fields.osVersion;
    }else{
        let ua = userAgent.parse( req.headers['user-agent']) ;
        browser = ua.browser;
        os = ua.os;
        platform = ua.platform;
        device = ua.isDesktop? 'Desktop' : ua.isMobile ? 'Mobile' : ua.isTablet ? 'Tablet' : ua.isSmartTV ?
                    'TV' : 'Others';
    }

    let {longitude, latitude} = req.fields.coords || {};

    let location = {
        type: "point",
        coordinates: [longitude, latitude]
    }

    let analyticData = {
        date: new Date(), 
        type,  
        remoteAddress, 
        browser, 
        os, 
        platform, 
        returning, 
        device,
        deviceModel,
        osVersion,
        token: dataToken,
        item: item._id,
        location
    }
    if(req.user){
        analyticData.user = req.user._id;
    }

    let result = await db.collection(`analytics`).insertOne(analyticData); 
    return res.status(200).end();

})
module.exports = router;
