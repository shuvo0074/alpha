const router = require('express').Router();
const con = require('../../../lib/db.js');
const payWithSSLCommerz = require("./../../../lib/payWithSSLCommerz.js");
const { db } = require('../../../lib/db.js');

router.post('/payment/ipn/sslcommerz', async (req, res) => {
  try{
    let paymentConfig = req.hostOptions.payment; //payment configuration from domainMap.json
    let verifiedInfo = await payWithSSLCommerz.verify(req.fields, paymentConfig); //verify request
    
    //update database
    let db = await con.db();
    let result = await db.collection('order').updateOne(
      {_id: verifiedInfo.orderId}, 
      {$set: {payment: verifiedInfo.payment}}
    )

    return res.json({status: "verified"});

  }catch(err){
    console.log(err);
    return res.status(500).json({status: 'could not verify'});
  }

})

module.exports = router;