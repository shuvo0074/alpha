const router = require('express').Router();
const path = require('path');
const ejs = require('ejs');
const fs = require('fs');
const con = require('../../../lib/db.js');
const tagFunctions = require('./../../../lib/dbFunctions/public/tagFunctions.js')

/** 
 * tag list api
 * @param limit number of items per response
 * @param page number of pages to skip (limit*page items)
 * @param sort field name to sort by
 * @param sortOrder 1 for ASC, -1 for DESC sort
 * 
 * @returns {Array} tags
*/
router.get('/tag', async (req, res)=>{
    let db = await con.db();
    let tags = await tagFunctions.getTagList(req.query);
    res.json(tags);
    
})

router.get('/tag/:tagId/product', async (req, res)=>{
    let tagId = req.params.tagId;
    let products;
    if(con.ObjectID.isValid(tagId)  && !tagId.includes('/') ){ //id passed, query by id
        products = await tagFunctions.getTagProduct(tagId, req.query)
    }else{ //query by url
        products = await tagFunctions.getTagProduct(req.path,req.query)
    }

    res.json(products)
    
})

/** 
 * category detail api
 * @param image do not include if -1 or false
 * @param subcategory do not include if -1 or false
 * 
 * 
*/
router.get('/tag/:id', async (req, res)=>{
    let tagId = req.params.id;
    let tag;
    if(con.ObjectID.isValid(tagId)  && !tagId.includes('/')){ //id passed, query by id
        tag = await tagFunctions.getTag(tagId, req.query)
    }else{ //query by url
        tag = await tagFunctions.getTag(req.path, req.query)
    }
    res.json(tag)
})




module.exports = router;
