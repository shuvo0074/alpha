const router = require('express').Router();
const con = require('../../../../lib/db.js');
const _ = require("lodash");
const Validator = require('../../../../validator/public.js');

router.post('/', async(req, res) => {
    try {
			let { type, size, color, frontImage, backImage } = req.fields;
			
			// validation
			const validator = new Validator();
			let errors = await validator.validateCustomProductType(req.fields);	

			// console.log(con.ObjectID.isValid(backImage))

			// return if it hits any error
			if(!_.isEmpty(errors)) return res.status(400).json(errors);
			// console.log(errors)

			let db = await con.db();
			let newCustomType = {
				type, size, color, 
				frontImage: new con.ObjectID(frontImage),
				backImage: new con.ObjectID(backImage),
			}

			// const result = await db.collection("customType").findOne({frontImage: new con.ObjectID("5f7ad730887abe27dc11207c")})
			const result = await db.collection('customType').insertOne(newCustomType);
			return res.json(result.ops);
		} catch(error) {
			return res.status(500).json(error.message)
		}
})

module.exports = router;