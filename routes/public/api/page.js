const router = require('express').Router();
const path = require('path');
const ejs = require('ejs');
const fs = require('fs');
const con = require('../../../lib/db.js');
const pageFunctions = require('../../../lib/dbFunctions/public/pageFunctions.js');

/**
 * page list api
 * @param limit number of items per response
 * @param page number of pages to skip (limit*page items)
 * @param sort field name to sort by
 * @param sortOrder 1 for ASC, -1 for DESC sort
 *
 *
 */
router.get('/page', async (req, res) => {
  let pages = await pageFunctions.getPageList(req.query);
  res.json(pages);
});


/**
 * page detail api
 */
router.get('/page/:id', async (req, res) => {
  let id = req.params.id;
  let brand;
  if (con.ObjectID.isValid(id) && !id.includes('-')) {
    //id passed, query by id
    brand = await pageFunctions.getPage(id, req.query);
  } else {
    //query by url
    brand = await pageFunctions.getPage(req.path, req.query);
  }

  res.json(brand);
});


module.exports = router;
