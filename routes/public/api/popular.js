const router = require('express').Router();
const con = require('../../../lib/db.js');
const paginate = require('../../../lib/paginate.js');
// const { max } = require('lodash');

router.get("/", async (req, res) => {
    try {

        //sort by most visited
        req.query.sort = "count";
        req.query.sortOrder = -1;

        let { category, brand, tags } = req.query;

        let aggregatePipeLine = [];

        if (category && con.ObjectID.isValid(category)) {
            aggregatePipeLine.push({
                $match: { "category": new con.ObjectID(category) }
            })
        }

        if (brand && con.ObjectID.isValid(brand)) {
            aggregatePipeLine.push({
                $match: { "brand": new con.ObjectID(brand) }
            })
        }

        if (tags && con.ObjectID.isValid(tags)) {
            aggregatePipeLine.push({
                $match: { "tags": new con.ObjectID(tags) }
            })
        }

        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "cover",
                foreignField: "_id",
                as: "cover"
            }
        })

        aggregatePipeLine.push({
            $unwind: {
                path: "$cover",
                preserveNullAndEmptyArrays: true
            }
        })


        aggregatePipeLine.push({
            $lookup: {
                from: "analytics",
                localField: "_id",
                foreignField: "item",
                as: "analyticsCount"
            }
        })

        aggregatePipeLine.push({
            $unwind: {
                path: "$analyticsCount",
                preserveNullAndEmptyArrays: true,
            }
        })

        aggregatePipeLine.push({
            $group: {
                _id: "$_id",
                count: { $sum: 1 },
                doc: { $first: '$$ROOT' }
            }
        })
        aggregatePipeLine.push({
            $replaceRoot: {
                newRoot: { $mergeObjects: ['$doc', '$$ROOT'] }
            }
        });
        aggregatePipeLine.push({ 
            $project: { doc: 0 } 
        });

        let popularProduct = await paginate('product', aggregatePipeLine, [], req.query)
        res.json({ popularProduct })

    } catch (error) {
        console.log(error);
        res.status(500).json({ error: error.message });
    }
})

module.exports = router;