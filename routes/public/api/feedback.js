const router = require('express').Router();
const con = require('../../../lib/db.js');
const feedbackFunctions = require('../../../lib/dbFunctions/public/feedback.js');

router.post('/feedback', async (req, res) => {

  try {

    let {filter = {}, resolve = {}, options = {}} = req.fields;

    let feedbackList = await feedbackFunctions.getFeedback(
      {
        product: filter.product,
        _id: filter._id,
        status: 'approved'
      },
      {
        product: resolve.product,
        customer: resolve.customer
      },
      options
    );

    return res.json(feedbackList);

  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: 'Sorry, something went wrong.' })
  }
})


module.exports = router;
