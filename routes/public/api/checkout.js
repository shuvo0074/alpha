const router = require('express').Router();
const Validator = require('../../../validator/customer.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const bcrypt = require('bcrypt');
const autoMail = require('../../../lib/autoMail.js');
const autoSMS = require('./../../../lib/autoSMS.js');
const { notify } = require('../../../lib/adminNotification.js');
const deliveryFunction = require('../../../lib/dbFunctions/public/deliveryFunctions.js');
const { generateRandomString } = require("../../../lib/utility.js");
const defaultOrderStatus = require("./../../../config/defaultOrderStatus.json");
const jwt = require("jsonwebtoken");
const http = require('http');


const { 
	parseCustomerId, 
	calculateTotalPrice, 
	getProducts, 
	getShippingAddress, 
	getDeliveryRegion, 
	getDeliveryCharge, 
	createAccount, 
	applyCoupon, 
	applyPayment, 
	notifyOrder,
	getProductIds,
	getProductsInfo
} = require('./../../../lib/checkoutFunctions.js');



router.post('/checkout', async (req, res) => {
	try {


		//* customer information

		// get customer id 
		let customerId = await parseCustomerId(req);
		let customerDetails;

		// verify & get information from db
		let db = await con.db();
		if(con.ObjectID.isValid(customerId)){
			customerDetails = await db.collection('customer').findOne({ _id: new con.ObjectID(customerId) })
		}

		// return error if requested as logged in user and no customer id
		if (!con.ObjectID.isValid(customerId) && !req.fields.createAccount ) {
			return res.status(403).json({ authorization: "Unauthorized access" });
		}



		//* shipping address

		// validate
		let validator = new Validator();
		errors = await validator.checkoutShipping(req.fields);
		if (!_.isEmpty(errors)) return res.status(400).json({ error: 'Incomplete Shipping Address', checkoutError: errors });

		// populate
		let { shippingAddress, coupon, paymentMethod, delivery } = req.fields;
		shippingAddress = getShippingAddress(shippingAddress);



		//* create new account with shipping address information
		let accountCreated = false, token; //token will be added to response later if accountCreated = true 
		
		if (req.fields.createAccount) {

			let customer = await createAccount(shippingAddress, req.fields);
			accountCreated = true;

			customerId = customer._id;
			customerDetails = customer;

			// payload, secretKey, token expiry date(7days)
			customer.userType = 'customer';
			token = jwt.sign(customer, process.env.JWT_SECRET_KEY, {
				expiresIn: 30 * 24 * 3600
			});

		}



		//* product
		let productIds = await getProductIds(req.fields.items); // get ordered product ids
		let productsInfo = await getProductsInfo(productIds); // get ordered product information
		let products = await getProducts(productsInfo, req.fields.items); // get products cross verified

		if(!products[0]) {
			return res.status(400).json({product: "select at least one valid product"})
		}

		let totalPrice = await calculateTotalPrice(products);


		//* delivery option
		if (!delivery || !con.ObjectID.isValid(delivery)) {
			return res.status(400).json({ delivery: 'Select delivery option' });
		}
		// verify from db
		delivery = await getDeliveryRegion(delivery);
		if (!delivery) return res.status(400).json({ delivery: 'Invalid delivery option' });

		let deliveryCharge = getDeliveryCharge(delivery, totalPrice);


		//* format order
		let statusConfig = await db.collection('settings').findOne({ name: 'orderStatus' });

		let order = {
			_id: new con.ObjectID(),
			added: new Date(),
			products,
			shippingAddress,
			status: statusConfig && statusConfig.defaultStatus || defaultOrderStatus[0],
			totalPrice,
			deliveryCharge,
			deliveryRegion: delivery,
			customer: customerId ? new con.ObjectID(customerId) : null,
			shortCode: generateRandomString(6, "110")
		};


		//* apply coupon
		if (coupon) {

			//needs to be logged in to use coupon
			if (!customerId) return res.status(400).json({ customer: 'Sign in to use coupon' });

			//apply coupon
			order = await applyCoupon(coupon, customerId, order);
		}


		//*payment
		let customerInfo = customerDetails || shippingAddress;
		let paymentConfig = req.hostOptions.payment;
		let payment = await applyPayment(order, customerInfo, paymentMethod, paymentConfig, productsInfo);
		order.payment = payment;
		
		//* save to db
		let result = await db.collection('order').insertOne(order);
		delete result.ops[0].payment.paymentId;

		//* send response
		let responseObj = { inserted: result.ops }
		

		if (accountCreated && token) responseObj.token = `Bearer ${token}`;
		res.json(responseObj);


		//* notify order
		notifyOrder(shippingAddress, customerDetails, order);

	} catch (err) {
		if(err.statusCode){ //custom error
			return res.status(err.statusCode).json(err.response);
		}else{
			console.log(err)
			return res.status(500).json({ error: 'Sorry something went wrong' });
		}
		
	}
});



router.post('/validateCoupon', async (req, res) => {
	try {

		// let customerId = req.session.customer ? req.session.customer._id : null;
		let customerId = req.user ? req.user._id : null;

		let db = await con.db();

		// *product
		let { items, coupon } = req.fields;
		let productIds = [], products = [], totalPrice = 0;

		if (!items) return res.status(400).json({ items: "select items" });
		if (!Array.isArray(items)) return res.status(400).json({ "items": "invalid items formation" });

		if (!coupon) return res.status(400).json({ coupon: "enter coupon" });

		// validate and add id to list
		for (let item of items) {
			let { product, variation } = item;
			if (!con.ObjectID.isValid(product)) return res.status(400).json({ product: "invalid product id" });
			if (!con.ObjectID.isValid(variation)) return res.status(400).json({ variation: "invalid variation id" });

			productIds.push(new con.ObjectID(product));
		}

		//get products from db
		let productsInfo = await db.collection('product').find({ _id: { $in: productIds } }).toArray();

		//calculate price
		for (let product of productsInfo) {
			let orderedItem = items.find(i => product._id.toHexString() == i.product);
			let variation = product.pricing.find(i =>
				i && i._id && i._id.toHexString ? i._id.toHexString() == orderedItem.variation : i && i._id && i._id == orderedItem.variation
			);

			if (!variation) return res.status(400).json({ variation: `Invalid variation for product ${product.name}` });

			let unitPrice = + (variation.price.offer || variation.price.regular);
			let price = unitPrice * +orderedItem.quantity;
			totalPrice += price; 	//increase total order price

			products.push({
				_id: product._id,
				variation: variation._id,
				unitPrice,
				price,
				quantity: +orderedItem.quantity,
			})

		}

		// cross check all items existence in poducts

		for (let orderedItem of items) {
			let product = productsInfo.find(i => orderedItem.product == i._id.toHexString());

			if (!product) return res.status(400).json({ item: "invalid product" });

			let variation = product.pricing.find(i => i._id.toHexString() == orderedItem.variation);

			if (!variation) return res.status(400).json({ variation: `Invalid variation for product ${product.name}` });

			let unitPrice = + (variation.price.offer || variation.price.regular);
			let price = unitPrice * +orderedItem.quantity;
			totalPrice += price; 	//increase total order price

			products.push({
				_id: product._id,
				variation: variation._id,
				unitPrice,
				price,
				quantity: +orderedItem.quantity,
			})

		}


		let order = {
			products,
			totalPrice,
			customer: customerId ? new con.ObjectID(customerId) : null
		};

		if (coupon) {

			coupon = await db.collection('coupon').findOne({
				$and: [
					{ code: { $regex: new RegExp("^" + coupon.toLowerCase(), "i") } },
					{ startDate: { $lte: new Date() } },
					{ endDate: { $gte: new Date() } }
				]
			});

			//validate coupon
			if (!coupon) return res.status(409).json({ coupon: "Invalid  coupon" });

			//usage limit total
			let couponUsage = await db.collection('couponUsage').find({ coupon: coupon._id }).toArray();
			if (coupon.maxUseTotal && couponUsage && couponUsage.length > coupon.maxUseTotal) return res.status(409).json({ coupon: "Coupon is not available anymore" });


			//usage limit by user
			if (customerId) {
				let couponUsageByUser = couponUsage.filter(i => i.customer.toHexString() == customerId);
				if (coupon.maxUsePerUser && couponUsage && couponUsageByUser.length > coupon.maxUsePerUser) return res.status(409).json({ coupon: "Coupon max use reached" });
			}

			//order matching
			if (coupon.minimumOrder && order.totalPrice < coupon.minimumOrder) {
				return res.status(409).json({ coupon: `minimum order for coupon ${coupon.name} is ${coupon.minimumOrder}` });
			}
			if (coupon.maximumOrder && order.totalPrice > coupon.maximumOrder) {
				return res.status(409).json({ coupon: `maximum order for coupon ${coupon.name} is ${coupon.maximumOrder}` });
			}
			if (coupon.orderedProducts) {
				for (let requiredProduct of coupon.orderedProducts) {
					let productInOrder = order.products.find(i => i._id.toHexString() == requiredProduct._id.toHexString())
					if (!productInOrder) return res.status(409).json({
						coupon: `you did not buy all required product to apply coupon ${coupon.name}`
					})
				}
			}


			let discountAmount;
			if (coupon.amount) { //price discount

				if (coupon.amountType == "fixed") {
					discountAmount = coupon.amount;
					order.discountPrice = order.totalPrice - coupon.amount
				} else if (coupon.amountType == "percentage") {
					order.discountPrice = order.totalPrice - (order.totalPrice / 100 * coupon.amount);
					discountAmount = order.totalPrice - order.discountPrice
				}

				order.discountAmount = +discountAmount;
			}

			// order.freeDelivery = false;
			// if (coupon.freeDelivery) {
			// 	order.freeDelivery = true;
			// }

			// if (coupon.freeProducts) {
			// 	let freeProductList = [];
			// 	let freeProductIds = coupon.freeProducts.map(i => new con.ObjectID(i._id));
			// 	let freeProducts = await db.collection('product').find({ _id: { $in: freeProductIds } }).toArray();

			// 	for (let product of coupon.freeProducts) {

			// 		let productDetails = freeProducts.find(i => i._id.toHexString() == product._id.toHexString());
			// 		if (!productDetails) continue;
			// 		let variation = productDetails.pricing.find(i => i._id.toHexString() == product.variation.toHexString());
			// 		let unitPrice = + (variation.price.offer || variation.price.regular);
			// 		let price = unitPrice * +product.quantity;

			// 		freeProductList.push({ ...productDetails, quantity: product.quantity })
			// 	}
			// 	order.freeProducts = freeProductList;
			// }

		}

		return res.json(order)

	} catch (err) {
		console.log(err);
		return res.status(500).json({ error: 'Sorry something went wrong' });
	}
});

router.get("/statusList", async (req, res) => {
	let db = await con.db();


	let statusList = await db.collection('settings').findOne({ name: "orderStatus" });

	if (!statusList || !statusList[0]) statusList = defaultOrderStatus; //default if not found

	return res.json(statusList);

})

module.exports = router;


