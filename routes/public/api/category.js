const router = require('express').Router();
const con = require('../../../lib/db.js');
const categoryFunctions = require('./../../../lib/dbFunctions/public/categoryFunctions.js');
const _ = require("lodash");

/**
 * category list api
 * @param subCategory include subcategory in response if true
 * @param image include image in response if true
 * @param limit number of items per response
 * @param page number of pages to skip (limit*page items)
 * @param sort field name to sort by
 * @param sortOrder 1 for ASC, -1 for DESC sort
 *
 *
 */
router.get('/category', async (req, res) => {
  
  let categories = await categoryFunctions.getCategoryList(req.query);
  res.json(categories);
});

router.get( ['/category/:categoryName/product','/category/:parentName/:categoryName/product'], async (req, res) => {
    
//TODO: add full hierarchy in url

  let categoryName = req.params.categoryName;
  let products;
  if (con.ObjectID.isValid(categoryName) && !categoryName.includes('-')) {
    //id passed, query by id
    // console.log(req.path);
    products = await categoryFunctions.getCategoryProduct( categoryName, req.query );
  } else {
    //query by url
    // console.log(req.path);

    products = await categoryFunctions.getCategoryProduct(req.path,  req.query);
  }

  res.json(products);
});

/**
 * category detail api
 * @param image do not include if -1 or false
 * @param subcategory do not include if -1 or false
 *
 *
 */
router.get( ['/category/:categoryName', '/category/:parentName/:categoryName'], async (req, res) => {

    let categoryName = req.params.categoryName;
    // let { cover, subCategory, image } = req.query;
    let category;
    if (con.ObjectID.isValid(categoryName) && !categoryName.includes('-')) {
      //id passed, query by id
    console.log(11, categoryName)

      category = await categoryFunctions.getCategory(categoryName, req.query);
    } else {
      //query by url
      console.log(22, categoryName)

      category = await categoryFunctions.getCategory(req.path, req.query);
    }

    res.json(category);
});

module.exports = router;
