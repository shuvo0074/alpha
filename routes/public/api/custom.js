const router = require('express').Router();

const addRouter = require('./custom/add.js');
const getRouter = require('./custom/get.js');


router.use('/add', addRouter);
router.use('/', getRouter);

module.exports = router;