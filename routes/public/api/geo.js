const router = require('express').Router();
const path = require('path');
const ejs = require('ejs');
const fs = require('fs');
const con = require('../../../lib/db.js');
const geoFunctions = require('./../../../lib/dbFunctions/public/geoFunctions.js')

/**
 * get continent list
*/
router.get('/geo/continent', async (req, res)=>{
    let continentList = await geoFunctions.getContinentList(req.query);
    res.json(continentList);
})
/**
 * get continent details
 * @param {string} id id, code or name 
 */
router.get('/geo/continent/:id', async (req, res)=>{
    let id = req.params.id;
    if(!id) return res.status(400).json({id: 'id must be country id, code or name'})
    let continent = await geoFunctions.getContinent(id, req.query);
    res.json(continent);
})

/**
 * get country list
 * @query {boolean} continent ?include information  
*/
router.get('/geo/country', async (req, res)=>{
    let countryList = await geoFunctions.getCountryList(req.query);
    res.json(countryList);
})

/**
 * get country details
 * @param {string} id id, code or name 
 *  @query {boolean} continent ?include information  
 */
router.get('/geo/country/:id', async (req, res)=>{
    let id = req.params.id;
    if(!id) return res.status(400).json({id: 'id must be country id, code or name'})
    let country = await geoFunctions.getCountry(id, req.query);
    res.json(country);
})

/**
 * get city list of country 
 * @param {string} country id, code or name of country  
 * @query {boolean} country ?include country information  
 */
router.get('/geo/:country/city', async (req, res)=>{
    let country = req.params.country;
    let cityList = await geoFunctions.getCityList(country, req.query);
    res.json(cityList);
})

/**
 * get city detail
 * @param {string} id id or name of city  
 * @query {boolean} country ?include country information  
 */
router.get('/geo/city/:id', async (req, res)=>{
    let id = req.params.id;
    let city = await geoFunctions.getCity(id, req.query);
    res.json(city);
})


module.exports = router;