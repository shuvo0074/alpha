const router = require('express').Router();
const path = require('path');
const ejs = require('ejs');
const fs = require('fs');
const con = require('../../../lib/db.js');
const deliveryFunctions = require('./../../../lib/dbFunctions/public/deliveryFunctions.js');
/**
 * get region list
 * @returns {Array} array of all available regions
 */
router.get('/delivery', async (req, res) => {
  let regionList = await deliveryFunctions.getAllRegion();
  res.json(regionList);
});


/**
 * get delivery region data by customer address in profile
 * @returns {Object} delivery region data
 */
router.get('/delivery/customer', async (req, res) => {
  let customer = req.user;
  if (!customer)
    return res.status(400).json({ customer: 'customer not logged in' });

  let region = await deliveryFunctions.getCustomerRegion(customer._id);
  res.json(region);
});

/**
 * get region by country
 * @param {string} country country name, id or code
 * @returns {Object} Region data
 */

router.get('/delivery/country/:country', async (req, res) => {
  let country = req.params.country;
  if (!country)
    return res.status(400).json({ country: 'country name required' });

  let region = await deliveryFunctions.getRegionByCountry(country);
  res.json(region);
});

/**
 * get delivery region by country and city
 * @param {String} country country name, id or code
 * @param {String} city city name
 * @return {Object} delivery region data
 */
router.get('/delivery/city/:city', async (req, res) => {
  let city = req.params.city;

  if (!city) return res.status(400).json({ city: 'city name required' });

  let region = await deliveryFunctions.getRegionByCity(city);
  res.json(region);
});

router.get('/delivery/name/:name', async (req, res) => {
  let name = req.params.name;

  if (!name) return res.status(400).json({ city: 'city name required' });

  let region = await deliveryFunctions.getRegionByName(name);
  res.json(region);
});


router.get('/delivery/:id', async(req, res)=>{
  let id = req.params.id;
  if(!id || ! con.ObjectID.isValid(id)) return res.status(400).json({id:'invalid id'});

  let region = await deliveryFunctions.getRegion(id);
  res.json(region);
})

module.exports = router;
