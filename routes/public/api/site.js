const router = require('express').Router();
const con = require('../../../lib/db.js');

/** 
 * tag list api
 * @param limit number of items per response
 * @param page number of pages to skip (limit*page items)
 * @param sort field name to sort by
 * @param sortOrder 1 for ASC, -1 for DESC sort
 * 
 * @returns {Array} tags
*/
router.get('/site/seo', async (req, res) => {
    let db = await con.db();
    let seoSettings = await db.collection('settings').findOne({ name: 'seo' });
    res.json(seoSettings);
})



module.exports = router;
