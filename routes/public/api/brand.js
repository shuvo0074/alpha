const router = require('express').Router();
const path = require('path');
const ejs = require('ejs');
const fs = require('fs');
const con = require('../../../lib/db.js');
const brandFunctions = require('../../../lib/dbFunctions/public/brandFunctions.js');

/**
 * category list api
 * @param subCategory include subcategory in response if true
 * @param image include image in response if true
 * @param limit number of items per response
 * @param page number of pages to skip (limit*page items)
 * @param sort field name to sort by
 * @param sortOrder 1 for ASC, -1 for DESC sort
 *
 *
 */
router.get('/brand', async (req, res) => {
  let { image, limit, page } = req.query;
  let brands = await brandFunctions.getBrandList(req.query);
  res.json(brands);
});

router.get('/brand/:brandName/product', async (req, res) => {
  let brandName = req.params.brandName;
  let products;
  if (con.ObjectID.isValid(brandName)  && !brandName.includes('/')) {
    //id passed, query by id
    products = await brandFunctions.getBrandProduct(brandName, req.query);
  } else {
    //query by url
    products = await brandFunctions.getBrandProduct(req.path, req.query);
  }

  res.json(products);
});

/**
 * category detail api
 * @param image do not include if -1 or false
 * @param subcategory do not include if -1 or false
 *
 *
 */
router.get('/brand/:id', async (req, res) => {
  let id = req.params.id;
  let brand;
  if (con.ObjectID.isValid(id) && !id.includes('/')) {
    //id passed, query by id
    brand = await brandFunctions.getBrand(id, req.query);
  } else {
    //query by url
    brand = await brandFunctions.getBrand(req.path, req.query);
  }

  res.json(brand);
});


module.exports = router;
