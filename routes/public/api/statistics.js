const router = require('express').Router();
const con = require('../../../lib/db.js');


router.get('/stats', async (req, res) => {
  try {
    let db = await con.db();
    console.log("HERE")

    let userCount = await db.collection('customer').countDocuments();
    let eventCount = await db.collection('product').countDocuments();
    let ticketCount = await db.collection('cart').countDocuments();

    return res.json({
      userCount, eventCount, ticketCount
    })
  } catch(error) {
    console.log(error);
    return res.status(500).json()
  }
})

module.exports = router;