const router = require('express').Router();
const con = require('../../../lib/db.js');
const feedbackFunctions = require('../../../lib/dbFunctions/public/feedback.js');
const mail = require('../../../lib/mail.js');

router.post('/contact', async (req, res) => {

  try {

    let {subject, message, phone, email, name} = req.fields;

    //validation
    if(!subject ) return res.status(400).json({subject: "Enter subject"})
    if(!message ) return res.status(400).json({message: "Enter message"})
    if(!name ) return res.status(400).json({name: "Enter name"})
    if(!phone ) return res.status(400).json({phone: "Enter phone number"})


    let db = await con.db()
    let adminInfo = await db.collection('settings').findOne({name:'site'});
    if(!adminInfo || !adminInfo.adminEmail) return res.status(406).json({error: "can not receive form at this moment"});

    subject = `Contact Form - ` + subject

    message += `
    Contact Name: ${name}, <br/>
    Contact Phone : ${phone}, <br/>
    Contact Email: ${email || "N/A"}, <br/>
    Contact Time: ${new Date()}
    <br>
    ` + message + `
    <br> <br> 
    <hr>
    <br> <br> 
    Sent from Alpha E-commerce Engine <br/>
    Developed by <a href='https://lotustech.dev'>Lotus Technology Development</a> 
    `

    mail.sendMail({
      recipient: adminInfo.adminEmail,
      subject,
      html: message
    })

    return res.json({success: "form accepted"})

  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: 'Sorry, something went wrong.' })
  }
})


module.exports = router;
