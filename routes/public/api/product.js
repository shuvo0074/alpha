const router = require("express").Router();
const path = require("path");
const ejs = require("ejs");
const fs = require("fs");
const con = require("../../../lib/db.js");
const productFunctions = require("./../../../lib/dbFunctions/public/productFunctions.js");
const productQuery = require("./../../../lib/dbFunctions/public/productQuery.js");

const _ = require('lodash');

router.post("/product/query", async (req, res) => {

  let filter = req.fields.filter;
  let resolve = req.fields.resolve;

  let products = await productQuery(filter, resolve);
  res.json(products);
});

router.get("/product", async (req, res) => {
  let db = await con.db();

  let products = await productFunctions.getProductList(req.query);
  res.json(products);
});

router.get("/product/offer", async(req, res)=>{
  
  let products = await productFunctions.getOfferProducts(req.query);
  res.json(products);
})

router.get("/product/related/:id", async (req, res) => {
  try {
    let db = await con.db();
    let id = req.params.id;
    if (!con.ObjectID.isValid(id)) return res.status(400).json({ id: "invalid id" });

    let relatedProducts = await productFunctions.getRelatedProducts(id, req.query);
    res.json(relatedProducts);
  }catch(err){
    console.log(err);
    res.status(500).json({error: err.message})
  }
});


router.get(
  ["/product/:productId", "/product/:categoryName/:productName"],
  async (req, res) => {
    let productId = req.params.productId;
    let product;
    if (con.ObjectID.isValid(productId) && !productId.includes('/')) {
      //id passed, query by id
      product = await productFunctions.getProduct(productId, req.query);
    } else {
      //query by url
      product = await productFunctions.getProduct(req.path, req.query);
    }
    res.json(product);
  }
);


/*
  - incoming data should be like this
  {
    '0': { brand: '5f52c99f6d22121d7876277d' },
    '1': { category: '5f4a96ef6848714beb61b0de' },
    '2': { category: '5f4bbcb12916495951517e73' },
    '3': { tags: '5f43811bc386034ed97a0c00' }
  }

  - searchObj after processing req.fields
  [
  { brand: 5f52c99f6d22121d7876277d },
  { category: 5f4a96ef6848714beb61b0de },
  { category: 5f4a993d6848714beb61b0ee },
  { tags: 5f43811bc386034ed97a0c00 }
  ]

*/

router.post("/product/filter", async(req, res) => {
  try {
    
    let filterLabels = Object.values(req.fields);
    let searchObj = [];

    for(let label of filterLabels) {
      let id = Object.values(label).join();

      if(con.ObjectID.isValid(id)) {
        let validId = {[Object.keys(label).join()]: new con.ObjectID(id)}
        searchObj.push(validId);
      }
    }
  
    let products = await productFunctions.getProductsByFilter(searchObj, req.query);
    
    return res.json(products);
  } catch(error) {
    // console.log(error);
    res.status(500).json({error: error.message})
  }
})

module.exports = router;
