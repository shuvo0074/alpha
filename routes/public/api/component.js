const router = require('express').Router();
const path = require('path');
const ejs = require('ejs');
const fs = require('fs');
const con = require('../../../lib/db.js');
const componentFunctions = require('../../../lib/dbFunctions/public/componentFunctions.js')

/** 
 * component list api
 * 
 * @option {String} image 
 * @returns {Array} components with resolved images
 * 
*/
router.get('/component', async (req, res)=>{
    let components = await componentFunctions.getComponentList(req.query)
    res.json(components);
})


/** 
 * component detail api
 * @param {String} id id or name of component 
 * @options image included unless specified -1 or false
 * 
 * 
*/
router.get('/component/:id', async (req, res)=>{
    let id = req.params.id;
    let component = await componentFunctions.getComponent(id, req.query)
    res.json(component)
})




module.exports = router;
