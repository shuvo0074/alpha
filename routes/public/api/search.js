const router = require('express').Router();
const path = require('path');
const ejs = require('ejs');
const fs = require('fs');
const con = require('../../../lib/db.js');
const searchFunctions = require('../../../lib/dbFunctions/public/searchFunctions.js')

/** 
 * product search api
 * @param {String} query search query
 * @param {String} searchCategory optional, ObjectID of category to search in 
 * @param {String} searchBrand optional, ObjectID of brand to search in 
 * @param {String} searchTag optional, ObjectID of tag to search in 
 * 
 * @returns {Object} paginated search result
*/
router.get('/search', async (req, res)=>{
    let db = await con.db();
    let query = req.query.query;
    let products = await searchFunctions.searchProduct(query, req.query, req.query);
    res.json(products);

})

router.get('/search/suggest', async(req, res)=>{
    let query = req.query.query;
    let keywords = await searchFunctions.searchProductSuggestion(query, req.query, req.query);
    res.json(keywords);
})

// router.get('/search/:collection', async (req, res) => {
//     let {collection} = req.params;
//     let {location, dateRange}
// })


module.exports = router;
