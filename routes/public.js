const router = require('express').Router();
const path = require('path');
const ejs = require('ejs');
const fs = require('fs');
const con = require('../lib/db.js');

const postGetRouter = require('./admin/post/get.js');
const postCategoryGetRouter = require('./admin/post/category/get.js');
const postTagGetRouter = require('./admin/post/tags/get.js');

// const productRouter = require('./public/product.js');
// const categoryRouter = require('./public/category.js');
// const cartRouter = require('./public/cart.js');
const apiRouter = require('./public/api.js');
const popularProductRouter = require('./public/api/popular.js');

const redirectRouter = require('./public/redirect.js');


router.use('/api/product/popular', popularProductRouter);
router.use('/api/post/tag', postTagGetRouter);
router.use('/api/post/category', postCategoryGetRouter);
router.use('/api/post', postGetRouter);
router.use('/api', apiRouter);
router.use(redirectRouter);

router.use


router.get("*", async(req,res)=>{

  res.render(path.join(req.userDir, req.hostOptions.theme, 'index.html'), (err, data)=>{
    if(err){
      return res.status(404).end('Requested page not found');
    }  

    res.send(data);
    res.end();
  })
})







// router.use(productRouter);
// router.use(categoryRouter);
// router.use('/cart', cartRouter);

/**
 * home page
 *
 * priority
 * 1. index.html
 * 2. home.html
 */
// router.get('/', async (req, res) => {
//   let home = path.join(req.themePath, 'home.html');
//   let index = path.join(req.themePath, 'index.html');
//   if (fs.existsSync(index)) {
//     res.render('index.html');
//   }
//   else if (fs.existsSync(home)) {
//     res.render('home.html');
//   } else {
//     //todo: replace with custom page
//     res.end('home template not found');
//   }
// });

module.exports = router;
