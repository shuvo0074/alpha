const router = require('express').Router();
const Validator = require('../../../validator/customer.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");
const dealerFunctions = require('./../../../lib/dbFunctions/dealerFunctions.js');

require('dotenv').config();

router.get('/', async (req, res) => {

  let dealer = req.user;
  
  if (!dealer || !dealer._id){
    return res.status(403).json({ error: 'log in to continue' });
  }

  let customers = await dealerFunctions.getDealerCustomer(dealer._id, req.query);
  res.json(customers);

});

module.exports = router;
