const router = require('express').Router();
const Validator = require('../../../validator/customer.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");

require('dotenv').config();

router.get('/', async (req, res) => {


  let dealer = req.user;
  let db = await con.db();
  let aggregatePipeLine = [];

  aggregatePipeLine.push({ $match: { _id: new con.ObjectID(dealer._id )}});
  aggregatePipeLine.push({
    $lookup: {
      from: 'dealerArea',
      localField: 'area',
      foreignField: '_id',
      as: 'area'
    }
  })
  
  if (!dealer || !dealer._id){
    return res.status(403).json({ error: 'log in to continue' });
  }

  // let userInfo = await db.collection('dealer').findOne({ _id: new con.ObjectID(dealer._id) });
  let userInfo = await db.collection('dealer').aggregate(aggregatePipeLine).toArray()
  if(!userInfo) res.status(404).json({user: "No dealer found!"});

  delete userInfo[0].password;

  res.json(userInfo[0]);

});

module.exports = router;
