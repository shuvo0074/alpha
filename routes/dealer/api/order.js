const router = require('express').Router();
const Validator = require('../../../validator/customer.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");
const dealerFunctions = require('../../../lib/dbFunctions/dealerFunctions.js');

require('dotenv').config();

router.get('/', async (req, res) => {

  let dealer = req.user;
  
  if (!dealer || !dealer._id){
    return res.status(403).json({ error: 'log in to continue' });
  }

  let orders = await dealerFunctions.getDealerOrderListWithCommission(dealer._id, req.query);
  res.json(orders);

});

router.get('/:orderId', async (req, res) => {

  let dealer = req.user;
  
  if (!dealer || !dealer._id){
    return res.status(403).json({ error: 'log in to continue' });
  }

  let orderId = req.params.orderId;
  if(!con.ObjectID.isValid(orderId)) return res.status(400).json({orderId: "invalid order id"});

  let order = await dealerFunctions.getDealerOrderDetailWithCommission(dealer._id, orderId, req.query);
  res.json(order);

});


module.exports = router;
