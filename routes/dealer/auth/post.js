const router = require("express").Router();
const Validator = require("../../../validator/customer.js");
const _ = require("lodash");
const con = require("../../../lib/db.js");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

// const passport = require("passport");

require("dotenv").config();

router.post("/login", async (req, res) => {
  try {
    //validation
    let { username, password } = req.fields;
    let validator = new Validator();
    let errors = await validator.customerLogin(req.fields);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    //get user info
    let db = await con.db();
    let user = await db.collection("dealer").findOne({
      $or: [{ phone: username }, { email: username }, { username: username }],
    });

    if (!user) return res.status(400).json({ username: "No user found with given email or phone" }); //no such user
    //compare password
    let result = await bcrypt.compare(password, user.password);
    if (!result) return res.status(400).json({ password: "Incorrect password" });

    //remove password from jwt payload
    delete user.password;
    const payload = {
      ...user,
      userType: 'dealer',
    }

    // payload, secretKey, token expiry date(30days)
    const token = jwt.sign(payload, process.env.JWT_SECRET_KEY, { 
      expiresIn: 30 * 24 * 3600,
    });

    // req.session.customer = {};
    // req.session.customer._id = user._id;
    // req.session.customer.domain = req.hostOptions.domain;

    return res.json({
      success: "login is successful",
      token: `Bearer ${token}`,
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "Sorry, something went wrong!" });
  }
});


router.post("/register", async (req, res) => {
  try {
    let {
      firstName, lastName,
      country, city, address1, address2, zipCode,
      phone, email,
      additionalInfo,
      password, password2,
    } = req.fields;

    //validation
    let validator = new Validator();
    let errors = await validator.customerRegister(req.fields);

    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    let db = await con.db();

    //checking if account with phone exists
    let exists = await db.collection("dealer").findOne({ phone });
    if (exists)
      return res
        .status(400)
        .json({ phone: "There is already an account with this phone number" });

    //checking if account with email exists
    if (email && email !== "" && email !== " ") {
      exists = await db.collection("dealer").findOne({ email });
      if (exists)
        return res
          .status(400)
          .json({ email: "There is already an account with this email" });
    }

    if(password !== password2) return res.status(400).json({password: "Password does not match"});

    //encrypt password
    let salt = await bcrypt.genSalt(10);
    let hash = await bcrypt.hash(password, salt);

    let newDealer = {
      firstName, lastName,
      country, city,
      address1, address2, zipCode,
      phone, email,
      additionalInfo,
      password: hash,
      added: new Date(),
    };

    let result = await db.collection("dealer").insertOne(newDealer);

    let id = result.insertedId;
    req.session.customer = id; //set as logged in
    delete newDealer.password; //object will go through other modules, delete pass
    newDealer._id = id;

    const payload = {
      ...newDealer,
      userType: 'dealer',
    }

     // payload, secretKey, token expiry date(30days)
     const token = jwt.sign(payload, process.env.JWT_SECRET_KEY, { 
      expiresIn: 30 * 24 * 3600,
    });


    res.json({ 
      inserted: result.ops,  
      token: `Bearer ${token}`
    });

    // //notify admin
    // let notification = {
    //   type: "dealer",
    //   heading: "New dealer Registered",
    //   text: `${firstName} ${lastName} registered as a dealer`,
    //   link: `/admin/dealer/view/${result.insertedId}?ref=notification`,
    // };
    // notify(notification);

    // //send mail
    // autoMail.eventMail("newDealer", newDealer.email, {
    //   dealer: newDealer,
    // });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Sorry, something went wrong!" });
  }
});


// router.post('/recover', async (req, res) => {
//   try {

//     let { username } = req.fields;

//     if (!username ) return res.status(400).json({ username: "Invalid Username" });

//     let db = await con.db();
//     const customer = await db.collection('customer').findOne({
//       $or: [{ phone: username }, { email: username }, { username: username }],
//     })

    
//     // check if customer exists
//     if (!customer) return res.status(400).json({ username: `Invalid username` });
    
//     if(!customer.email) return res.status(400).json({email:"no email found in customer profile"});

//     let resetToken = `${Math.floor(Math.random() * 1000000)}`;


//     let resetData = {
//       customer: customer._id,
//       token:resetToken,
//       expire: new Date() + 3 * 3600000, // 3 hour
//       added: new Date()
//     }

//     await db.collection('recoveryToken').insertOne(resetData);


//     let mailContent = {
//       recipient: customer.email,
//       subject: "Reset Password",
//       html: `
//       <div style="font-family:sans-serif">
//         Dear <b>${customer.firstName || ""} ${customer.lastName || ""}</b>,<br> <br>
//           You password recovery code is 
//           <br><br><br> <br>
//           <div style="text-align:center;">
//           <b  style="padding:10px 15px; font-size:3em;  color:#f1982b; border: 1px dashed #d2d2d2">
//             ${resetToken}
//           </b>
//           </div>
//           <br> <br><br> <br>
//           <span style="color:#333333">If you did not request a password change you can safely ignore this email.</span>
//         </div>
//     `,
//     }

//     let hideEmail = function(email) {
//       return email.replace(/(.{2})(.*)(?=@)/,
//         function(gp1, gp2, gp3) { 
//           for(let i = 0; i < gp3.length; i++) { 
//             gp2+= "*"; 
//           } return gp2; 
//         });
//     };

//     sendMail(mailContent); //no need to await 

//     return res.json({
//       message: "Password reset mail sent",
//       customer: {
//         firstName: customer.firstName,
//         lastName: customer.lastName
//       },
//       email: hideEmail(customer.email)
//     })

//   } catch (error) {
//     console.log(error);
//     res.status(500).json({ error: error.message });
//   }
// })

// router.post('/recover/reset', async (req, res) => {
//   try {
//     const { resetToken, newPassword, confirmPassword } = req.fields;


//     let db = await con.db();

//     let resetData = await db.collection('recoveryToken').findOne({
//       $and:[
//         {token:resetToken},
//         // {expire:{$lte: new Date()}}
//       ]
//     });

//     if (!resetData) return res.status(400).json({ resetToken: "Invalid reset code" });

//     let customer =await db.collection('customer').findOne({_id: resetData.customer});
//     if(!customer) return res.status(400).json({ resetToken: "Invalid reset code" });

//     // validate password
//     if (!newPassword) return res.status(400).json({ password: "Password can not be empty" });
//     if (!confirmPassword) return res.status(400).json({ confirmPassword: "Confirm password can not be empty" });

//     if (newPassword !== confirmPassword) return res.status(400).json({ password: "Passwords do not match" });

//     // hash password
//     let salt = await bcrypt.genSalt(10);
//     let hash = await bcrypt.hash(newPassword, salt);

//     let updatedCustomer = {
//       password:hash,
//       updated: new Date(),
//     }

//     let result = await db.collection('customer').updateOne(
//       { _id: customer._id },
//       { $set: updatedCustomer }
//     )

//     return res.json({
//       success: "Password changed successfully"
//     })
//   } catch (error) {
//     return res.status(500).json({ error: error.message });
//   }
// })

module.exports = router;



