const router = require('express').Router();
const passport = require('passport')


router.use('/profile', passport.authenticate('jwt', {session: false}), require('./api/profile.js'));
router.use('/customer', passport.authenticate('jwt', {session: false}), require('./api/customer.js'));
router.use('/order', passport.authenticate('jwt', {session: false}), require('./api/order.js'));


module.exports = router;