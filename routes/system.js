const router = require('express').Router();
const registerRouter = require('./system/register.js');
const systemAuth = require('../lib/auth/systemAuth.js');

router.use((req, res, next)=>{
    let host = req.headers.host.replace('www.', '');
    if(host){ //todo: add permanent domain
        console.log(host);
        next()
    }else{
        res.redirect('/404');
    }
})
router.get('/check', (req, res)=>{res.end('working')});

router.use(systemAuth);
router.use('/register', registerRouter);

module.exports = router;