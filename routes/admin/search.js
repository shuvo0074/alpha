const router = require('express').Router();
const _ = require('lodash')
const con = require('../../lib/db.js')
let searchFunction = require('./../../lib/dbFunctions/public/searchFunctions.js');

router.get('/', async (req, res)=>{
    let query = req.query;
	query.page = query.page && query.page>0? query.page : 1;
    query.limit = query.limit && query.limit>0? query.limit : 50;
    
    let result = await searchFunction.searchProduct(query.query, query);

    res.render('admin/search.html', {result, ...query});
})

module.exports = router;
