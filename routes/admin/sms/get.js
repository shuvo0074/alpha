const router = require('express').Router();
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
const paginate = require('./../../../lib/paginate.js') 

router.get('/configuration', async(req, res)=>{
    let db = await con.db();
    let settings = await db.collection('settings').findOne({name:'sms'}) || {};

    res.json(settings);
})


router.get('/inbox', async(req, res)=>{

    let SMSList = await paginate('sentSMS', [], [], req.query); //get sent SMS list
    return res.json(SMSList);
});

router.get('/inbox/:id', async(req, res)=>{
    
    let id = req.params.id; //sent SMS id
    
    if(!id || !con.ObjectID.isValid(id)){
        res.locals.msg= {
            type:'error',
            message:'Invalid Id'
        };
        res.redirect(req.session.back || 'back')
    }

    let db = await con.db()
    let SMS = await db.collection('sentSMS').findOne({_id: new con.ObjectID(id)});
    if(!SMS) return res.status(400).json({id: "no sms with given id"});

    res.json(SMS);
})

router.get('/autoevents', async(req, res)=>{
    let db = await con.db();
    let settings = await db.collection('settings').findOne({name:'autoSMS'}) || {};

    res.json(settings);
})

router.get('/autoevents/templates', async(req, res)=>{
 
    let db = await con.db();
    let templates = await db.collection('SMSTemplates').find().toArray() ;

    res.json(templates);
})


router.get('/autoevents/templates/:event', async(req, res)=>{
    
    let event = req.params.event;

    let db = await con.db();
    let template = await db.collection('SMSTemplates').findOne({event}) ;

    res.json(template);
})


module.exports = router;
