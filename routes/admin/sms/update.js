const router = require('express').Router();
const Validator = require('../../../validator/admin.js');
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
const path = require('path');
const mail = require('../../../lib/mail.js');


router.post('/', async (req, res) => {

	try {
		let {
			parameters, authentication,
            protocol, method, url, contentType,
            authHeader
		} = req.fields;

		let db = await con.db();

		// *validation
		let validator = new Validator();
		let errors = await validator.smsSettings(req.fields);
		if (!_.isEmpty(errors)) return res.status(400).json(errors)

		let newSettings = {
			parameters, authentication,
            protocol, method, url, contentType,
            authHeader
		}

		await db.collection('settings').updateOne({ name: 'sms' }, { $set: newSettings }, { upsert: true });

		res.json({ success: 'sms settings saved ' });

	} catch (err) {
		console.log(err);
		res.status(500).json({ error: err.message })
	}

})



//set in which events auto SMS should be sent
// for both admin and customer separately

router.post('/autoevents', async (req, res)=>{

    try{
       
        let { newCustomer = {}, order = {}, orderStatus = {}, newDealer = {} } = req.fields;

        let db = await con.db();
  
        //validation
        //force boolean value
        let settings = {
            newCustomer:{
                admin: newCustomer.admin? true : false, //ensure boolean
                user: newCustomer.user? true : false
            },
            order:{
                admin: order.admin? true : false,
                user: order.user? true : false
            },
            orderStatus:{
                admin: orderStatus.admin? true : false,
                user: orderStatus.user? true : false
            },
            newDealer:{
                admin: newDealer.admin? true : false,
                user: newDealer.user? true : false
            },
        }
		
        await db.collection('settings').updateOne({name:'autoSMS'},{$set:settings}, {upsert:true});
  
        res.json({success:'configuration updated'});
  
    }catch(err){
        console.log(err);
        res.status(500).json({error:err.message}) 
    }
  
  })

// set SMS templates to send 
// for both admin and customer separately

router.post('/template', async (req, res)=>{

    try{
        let { event, user= {},  admin={}} = req.fields;
        
        let db = await con.db();
  
        
        //validation
        let validator = new Validator();
        let errors = await validator.SMSTemplate(req.fields);
        if(!_.isEmpty(errors)){
            return res.status(400).json(errors)
        }
       
        let template = {
            event, 
            user:{
                body: user.body
            },
            admin:{
                body: admin.body
            },

            lastModified: new Date()
        }


        await db.collection('SMSTemplates').updateOne({event:event},{$set:template}, {upsert:true});
  
        return res.json({success:'template saved'});
  
    }catch(err){
        console.log(err);
        res.status(500).json({error:err.message}) 
    }
  
  })


module.exports = router;