const router = require("express").Router();
const Validator = require("../../../validator/admin.js");
const _ = require("lodash");
const con = require("../../../lib/db.js");
const fs = require("fs");
const util = require("util");
const path = require("path");
const sms = require("./../../../lib/sms.js");


router.post("/send", async (req, res) => {
  try {
    let { recipient, text } = req.fields;

    //validation
    let validator = new Validator();
    let errors = await validator.sendSms(req.fields);
    if (!_.isEmpty(errors)) {
      return res.status(400).json(errors);
    }

    try{

      let response = await sms.sendSms({ recipient, text, event: "compose sms" });
      return res.json({ success: response });

    }catch(err){

      console.log(err);
      return res.status(400).json({ sms: "failed to send sms", sendError: err.message });
    }

  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: err.message });
  }
});

router.post("/delete/:id", async (req, res) => {
  try {
    let id = req.params.id; //sent SMS id
    if (!id || !con.ObjectID.isValid(id)) return res.status(400).json({ id: "invalid id" });

    let db = await con.db();
    await db.collection("sentSMS").deleteOne({ _id: new con.ObjectID(id) });

    res.json({ msg: "SMS Deleted" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: err.message });
  }
});

module.exports = router;
