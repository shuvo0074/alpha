const router = require('express').Router();
const Validator = require('../../../validator/customer.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const fs = require('fs');
const util = require('util');
const path = require('path');
const { notify } = require('../../../lib/adminNotification.js');
const meter = require('./../../../lib/meterFunctions.js');
const bcrypt = require('bcrypt');
const autoMail = require('./../../../lib/autoMail.js');
const autoSMS = require('./../../../lib/autoSMS.js');


router.post('/', async (req, res) => {
  try {

    //*resource check
    let missingResource = await meter.usedResource(req, 'db');
    if (missingResource) 
      return res.status(400).json({Resource: `Usage Limit Exceeded For ${missingResource.join(', ')}`});

    let { 
      firstName, lastName,
      country, city, address1, address2, zipCode,
      phone, email, additionalInfo, password
    } = req.fields;

    // *validation
    let validator = new Validator();
    let errors = await validator.customerRegister(req.fields);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    let db = await con.db();

    //checking if account with phone exists
    let exists = await db.collection('customer').findOne({ phone });
    if (exists) 
      return res.status(400).json({ phone: 'There is already an account with this phone number' });

    //checking if account with email exists
    exists = await db.collection('customer').findOne({ email });
    if (exists)
      return res.status(400).json({ email: 'There is already an account with this email' });

    // *encrypt password
    let salt = await bcrypt.genSalt(10);
    let hash = await bcrypt.hash(password, salt);

    // *model customer
    let customer = {
      firstName, lastName,
      country, city, address1, address2, zipCode,
      phone, email, additionalInfo,
      password: hash, 
      added: new Date(),
      updated: new Date()    }

    let result = await db.collection('customer').insertOne(customer);
    
    delete result.ops[0].password;
    res.json({inserted: result.ops});

    //notify admin
    let notification = {
      type: 'customer',
      heading: 'New Customer Registered',
      text: `${customer.firstName} ${customer.lastName} registered as a customer`,
      eventId: result.insertedId,
    };
    notify(notification);

    //send consfirmation
    autoMail.eventMail("newCustomer", customer.email, {user: customer});
    autoSMS.eventSMS("newCustomer", customer.phone, {user: customer});
    

  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: err.message });
  }
});

module.exports = router;
