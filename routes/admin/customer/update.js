const router = require('express').Router();
const Validator = require('../../../validator/customer.js');
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
const path = require('path');
const bcrypt = require('bcrypt');

router.post('/:id', async (req, res) => {
	try {

		// *id validation
		let id = req.params.id;
		if(!id || !con.ObjectID.isValid(id)) return res.status(400).json({id: "invalid id"});

		let {
			firstName, lastName,
			country, city, address1, address2, zipCode,
			phone, email, additionalInfo, password
		} = req.fields;

		// *validation
		let validator = new Validator();
		let errors = await validator.customerUpdate(req.fields);
		if (!_.isEmpty(errors)) return res.status(400).json(errors);

		let db = await con.db();

		// *model customer
		let customer = {
			firstName, lastName,
			country, city, address1, address2, zipCode,
			phone, email, additionalInfo,
			updated: new Date()
		}

		// * password : only update if changed by admin
		if(password){
			let salt = await bcrypt.genSalt(10);
			let hash = await bcrypt.hash(password, salt);
			customer.password = hash;
		}
		
		//save to db
		let result = await db.collection('customer').updateOne({ _id: new con.ObjectID(id) }, { $set: customer });
		
		//existence check
		if(result.matchedCount == 0) return res.status(400).json({id: "no customer with given id"});

		delete customer.password;
		return res.json({updated:customer});

	} catch (err) {
		console.log(err);
		res.status(500).json({ error: err.message })
	}

})


router.post('/delete/:id', async (req, res) => {
	try {
		let id = req.params.id; //product id
		if (!id || !con.ObjectID.isValid(id)) return res.status(400).json({ msg: 'Invalid customer Id' });

		let db = await con.db();
		let result = await db.collection('customer').deleteOne({ _id: new con.ObjectID(id) });

		if(result.deletedCount < 1) return res.status(400).json({id: "no customer with given id"});

		return res.json({ success: 'Customer deleted' });

	} catch (err) {
		console.log(err);
		res.status(500).json({ msg: err.message });
	}
})

module.exports = router;