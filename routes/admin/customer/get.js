const router = require("express").Router();
const _ = require("lodash");
const con = require("../../../lib/db.js");
const fs = require("fs");
const util = require("util");
const paginate = require("./../../../lib/paginate.js");
const orderFunctions = require("./../../../lib/dbFunctions/customer/orderFunctions.js");

router.get("/", async (req, res) => {
  try {
    let db = await con.db();

    let aggregatePipeLine = [
      {
        $lookup: {
          from: "order",
          localField: "_id",
          foreignField: "customer",
          as: "orders"
        }
      },
      {
        $addFields: {
          "orderCount": { $size: "$orders" },
        }
      },
      {
        $unwind: {
          path: "$orders",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $group: {
          _id: "$_id",
          "totalOrderPrice": { $sum: "$orders.totalPrice" },
          "doc": { $first: "$$ROOT" }
        }
      },
      {
        $replaceRoot: {
          newRoot: {
            $mergeObjects: ["$doc", "$$ROOT"]
          }
        }
      },
      {
        $project: {
          doc: 0,
          orders: 0,
          password: 0
        }
      },
      {
        $lookup: {
          from: "dealer",
          localField: "dealer",
          foreignField: "_id",
          as: "dealer"
        }
      },
      {
        $unwind: {
          path: "$dealer",
          preserveNullAndEmptyArrays: true
        }
      },
    ]

    let customers = await paginate("customer", aggregatePipeLine, [], req.query);

    return res.json(customers);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: err.message });
  }
});

router.get("/:id", async (req, res) => {
  try {
    // id validation
    let id = req.params.id;
    if (!con.ObjectID.isValid(id)) return res.status(400).end("Invalid Customer Id"); //TODO: set proper response
    
    let db = await con.db();

    let aggregatePipeLine = [
      {
        $match:{
          _id: new con.ObjectID(id)
        }
      },
      {
        $lookup: {
          from: "order",
          localField: "_id",
          foreignField: "customer",
          as: "orders"
        }
      },
      {
        $addFields: {
          "orderCount": { $size: "$orders" },
        }
      },
      {
        $unwind: {
          path: "$orders",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $group: {
          _id: "$_id",
          "totalOrderPrice": { $sum: "$orders.totalPrice" },
          orders: {$push:"$orders"},
          "doc": { $first: "$$ROOT" },
        }
      },
      {
        $replaceRoot: {
          newRoot: {
            $mergeObjects: ["$doc", "$$ROOT"]
          }
        }
      },
      {
        $project: {
          doc: 0,
          password: 0
        }
      },
      {
        $lookup: {
          from: "dealer",
          localField: "dealer",
          foreignField: "_id",
          as: "dealer"
        }
      },
      {
        $unwind: {
          path: "$dealer",
          preserveNullAndEmptyArrays: true
        }
      },
    ]

    let customer = await db.collection("customer").aggregate(aggregatePipeLine).toArray();
    customer =  customer[0];

    if(!customer) return res.status(404).json({id: "no customer with given id"});

    return res.json(customer);

  } catch (err) {
    console.log(err);
    res.redirect("/500");
  }
});

router.get("/:id/order", async (req, res) => {
  try {
    // *validate customer id
    let customerId = req.params.id;
    if (!con.ObjectID.isValid(customerId))
      return res.status(400).json({ id: "invalid customer id" });

    // * get orders by customer
    let orders = await orderFunctions.getOrderList(customerId, req.query);

    return res.json(orders);
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: err.message });
  }
});

/*
	@route - /admin/customer/:id/order/:oder_id
	@desc - get specific customer order by id
	@method - GET
*/
router.get("/:id/order/:orderId", async (req, res) => {
  try {
    // validate order id
    let orderId = req.params.orderId;
    if (!con.ObjectID.isValid(orderId))
      return res.status(400).json({ id: "Invalid order id" });
    // check for valid customer id
    let customerId = req.params.id;
    if (!con.ObjectID.isValid(customerId))
      return res.status(400).json({ id: "Invalid customer id" });

    orderId = new con.ObjectID(orderId);
    customerId = new con.ObjectID(customerId);

    let db = await con.db();
    let order = await db.collection("order").findOne({ $and: [{ _id: orderId }, { customer: customerId }] });

    if (!order) return res.status(400).json({ orderId: "No order found for this user" });

    return res.json(order);
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Something went wrong!" });
  }
});

module.exports = router;
