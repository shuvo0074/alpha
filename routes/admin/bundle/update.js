const router = require('express').Router();
const Validator = require('../../../validator/admin.js');
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
const urlGenerator = require('../../../lib/urlGenerator.js');
const meter = require('./../../../lib/meterFunctions.js');


router.post('/:id', async (req, res) => {
	try {
		// *resource check ...
		let missingResource = await meter.usedResource(req, 'db');
		if (missingResource) {
			return res.status(400).json({
				Resource: `Usage Limit Exceeded For ${missingResource.join(', ')}`
			});
		}

		let db = await con.db();

		let bundleId = req.params.id;
		if(!con.ObjectID.isValid(bundleId)) return res.status(400).json({id: "invalid id"})

		// *validate
		let validator = new Validator();
		let errors = await validator.bundle(req.fields);
		if (!_.isEmpty(errors)) return res.status(400).json(errors);

		let { name, products,
			category, primaryCategory, brand, tags,
			price, startDate, endDate, description, url } = req.fields;

		//primary category
		primaryCategory = primaryCategory || category[0];
		primaryCategory = await db.collection('category').findOne({ _id: new con.ObjectID(primaryCategory) });

		if (!primaryCategory) return res.status(400).json({ primaryCategory: "Select valid primary category" });

		url = await urlGenerator.productUrl(url || name, primaryCategory.name, bundleId);

		// *model bundle
		let bundle = {
			name, description,
			category: category && category.map(i => new con.ObjectID(i)),
			primaryCategory: primaryCategory._id,
			brand: brand && new con.ObjectID(brand),
			tags: tags && tags.map(i => new con.ObjectID(i)),
			startDate: startDate ? new Date(startDate) : new Date(),
			endDate: endDate && new Date(endDate),
			actualPrice: 0,
			price: {
				regular: +price.regular,
				offer: price.offer && +price.offer,
			},
			url,
			type: 'bundle',
			product: [],
		}

		// *products
		let productIds = products.map(i => new con.ObjectID(i._id));
		let productsInfo = await db.collection('product').find({ _id: { $in: productIds } }).toArray();

		for (let product of products) {
			let productInfo = productsInfo.find(i => i._id.toHexString() == product._id);
			if (!productInfo) return res.status(400).json({ product: "product not found" });

			let variation = productInfo.pricing.find(i => i._id.toHexString() == product.variation);
			if (!variation) return res.status(400).json({ product: "variation not found" });

			let productInBundle = {
				_id: productInfo._id,
				variation: variation._id,
				quantity: product.quantity,
				price: (variation.price.regular * product.quantity),
				unitPrice: +variation.price.regular,
				updated: new Date()
				
			}

			bundle.actualPrice += productInBundle.price;
			bundle.product.push(productInBundle);
		}


		let result = await db.collection("product").updateOne(
			{ _id: new con.ObjectID(bundleId) }, 
			{ $set: bundle }
		);

		if(result.matchedCount == 0) return res.status(400).json({id: "no bundle with given id"});

		return res.json(bundle);


	} catch (err) {
		console.log(err);
		res.status(500).json({ msg: err.message });
	}
});



router.post('/delete/:id', async (req, res) => {
	let id = req.params.id;
	if (!id) return res.status(400).json({ id: 'Invalid Id' });

	let db = await con.db();

	//remove category from collection;
	let result = await db.collection('product').deleteOne({ _id: new con.ObjectID(id) });

	res.json({ success: 'bundle deleted!' })
})
module.exports = router;