const router = require('express').Router();
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');

//TODO by category, brand, detail aggregation


router.get('/', async(req, res)=>{
    let db = await con.db();
    let coupons = await db.collection('product').find({type:'bundle'}).toArray();

    return res.json(coupons);
})


router.get('/:id', async(req, res)=>{

    // *id validation
    let id = req.params.id;
    if(!con.ObjectID.isValid(id)) return res.status(400).json({id: "Invalid id"});

    let db = await con.db();
    let coupon = await db.collection('product').findOne({_id: new con.ObjectID(id)});

    if(!coupon) return res.status(400).json({id: "no bundle with given id"});

    return res.json(coupon);
})


module.exports = router;
