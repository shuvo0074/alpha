const router = require('express').Router();
const Validator = require('../../../validator/admin.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const fs = require('fs');
const util = require('util');
const meter = require('./../../../lib/meterFunctions.js');
const urlGenerator = require('./../../../lib/urlGenerator.js');

router.post('/', async (req, res) => {
  try {
    // *resource check ...
    let missingResource = await meter.usedResource(req, 'db');
    if (missingResource) {
      return res.status(400).json({
        Resource: `Usage Limit Exceeded For ${missingResource.join(', ')}`
      });
    }

    let db = await con.db();

    // *validate
    let validator = new Validator();
    let errors = await validator.bundle(req.fields);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    let { name, products, 
      image, cover,
      category, primaryCategory, brand, tags, 
      price, description, url } = req.fields;

     //primary category
    primaryCategory = primaryCategory || category[0];
    primaryCategory = await db.collection('category').findOne({_id: new con.ObjectID(primaryCategory)});

    if(!primaryCategory) return res.status(400).json({primaryCategory: "Select valid primary category"});

    url = await urlGenerator.productUrl(url || name, primaryCategory.name);

    // *image ...
    // convert image ids to objectIds
    let validImages = [];
    if (image){
      for (let x in image) {
        if (!con.ObjectID.isValid(image[x])) {
          continue
        };
        validImages.push(new con.ObjectID(image[x]));
      }
    }
    //cover image
    cover = (cover && con.ObjectID.isValid(cover) )? new con.ObjectID(cover) : (validImages && validImages[0])? validImages[0]:null;
    //TODO: validate images and cover from DB


    // *model bundle
    let bundle = {
      name, description,
      category: category && category.map(i => i && new con.ObjectID(i)).filter(i=>i),
      primaryCategory: primaryCategory._id, 
      brand: brand && new con.ObjectID(brand),
      tags: tags && tags.map(i => i && new con.ObjectID(i)).filter(i=>i),
      actualPrice: 0,
      price:{
        regular: +price.regular,
        offer: price.offer && +price.offer,
      },
      url,
      image:validImages, 
      cover,
      type:'bundle',
      product: [],
      added: new Date(),
      updated: new Date()    }

    // *products
    let productIds = products.map(i => new con.ObjectID(i._id));
    let productsInfo = await db.collection('product').find({ _id: { $in: productIds } }).toArray();
    
    for (let product of products) {
      let productInfo = productsInfo.find(i => i._id.toHexString() == product._id);
      if (!productInfo) return res.status(400).json({ product: "product not found" });

      let variation = productInfo.pricing.find(i => i._id && i._id.toHexString() == product.variation);
      if (!variation) return res.status(400).json({ product: "variation not found" });

      let productInBundle = {
        _id: productInfo._id,
        variation: variation._id,
        quantity: product.quantity,
        price: (variation.price.regular * product.quantity),
        unitPrice: +variation.price.regular,
      }

      bundle.actualPrice += productInBundle.price;
      bundle.product.push(productInBundle);
    }

    let result = await db.collection('product').insertOne(bundle);


    //resolve cover for response
    let coverDetail = cover && await db.collection('images').findOne({_id: cover});
    if(result.ops && result.ops[0]) result.ops[0].cover = coverDetail;


    return res.json(result.ops);


  } catch (err) {
    console.log(err);
    res.status(500).json({ error: err.message });
  }
});

module.exports = router;
