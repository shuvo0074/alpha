const router = require('express').Router();
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');

fs.readFile = util.promisify(fs.readFile);
fs.writeFile = util.promisify(fs.writeFile);


router.get('/site', async(req, res)=>{
    let db = await con.db();
    let settings = await db.collection('settings').findOne({name:'site'}) || {};

    res.json(settings);
})
router.get('/invoice', async(req, res)=>{
    let db = await con.db();
    let settings = await db.collection('settings').findOne({name:'invoice'}) || {};

    res.json(settings);
})
router.get('/seo', async(req, res)=>{
    let db = await con.db();
    let settings = await db.collection('settings').findOne({name:'seo'}) || {};

    res.json(settings);
})


router.get('/sitemap', async(req, res)=>{
    let domain  = req.hostOptions.domain;
    let sitemap = {
        sitemapIndex: `${domain}/sitemap.xml`,
        sitemapCategory: `${domain}/sitemap-category.xml`,
        sitemapBrand: `${domain}/sitemap-brand.xml`,
        sitemapTag: `${domain}/sitemap-tag.xml`,
        sitemapProduct: `${domain}/sitemap-product.xml`,
        sitemapPost: `${domain}/sitemap-post.xml`,
        sitemapPage: `${domain}/sitemap-page.xml`,
    }

    res.json(sitemap);  
})


module.exports = router;
