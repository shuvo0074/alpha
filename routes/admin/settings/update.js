const router = require('express').Router();
const Validator = require('../../../validator/admin.js');
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
const path = require('path');
const mail = require('./../../../lib/mail.js');

router.post('/site', async (req, res) => {

    try {
        let { title, adminName, adminEmail, adminPhone } = req.fields;

        let db = await con.db();

        // *validation
        let validator = new Validator();
        let errors = await validator.siteSettings(req.fields);
        if (!_.isEmpty(errors)) {
            return res.status(400).json(errors)
        }


        let newSettings = { title, adminName, adminEmail, adminPhone }
        let result = await db.collection('settings').updateOne({ name: 'site' }, { $set: newSettings }, { upsert: true });

        return res.json({ updated: newSettings });

    } catch (err) {
        console.log(err);
        res.status(500).json({ msg: err.message })
    }

})


router.post('/logo', async (req, res) => {

    try {
        let { logo, icon, logoApp } = req.files;

        let db = await con.db();

        //logo and icon will be used very frequently, directly overwrite images
        //if logo and icon is selected then rewrite otherwise skip

        // *logo
        if (logo && logo.size > 0) {

            let logoImg = await fs.promises.readFile(logo.path);
            if (!logoImg) return res.status(400).json({ 'logo': 'invalid logo' });

            await fs.promises.writeFile(path.join(req.publicDir, 'images', 'logo.png'), logoImg)
        }

        // *app logo
        if (logoApp && logoApp.size > 0) {

            let logoImg = await fs.promises.readFile(logoApp.path);
            if (!logoImg) return res.status(400).json({ 'logoApp': 'invalid app logo' });

            await fs.promises.writeFile(path.join(req.publicDir, 'images', 'logo-app.png'), logoImg)
        }

        // *icon
        let iconImg;
        if (icon && icon.size > 0) {

            iconImg = await fs.promises.readFile(icon.path);
            if (!iconImg) return res.status(400).json({ 'icon': 'invalid icon' });

            await fs.promises.writeFile(path.join(req.publicDir, 'favicon.ico'), iconImg)

        }

        res.json({ success: 'settings updated' });

    } catch (err) {
        console.log(err);
        res.status(500).json({ msg: err.message })
    }

})

router.post('/invoice', async (req, res) => {

    try {
        let { invoiceTitle, address, phone, email, additionalText } = req.fields;

        let db = await con.db();

        //validation
        let validator = new Validator();
        let errors = await validator.invoiceSettings(req.fields);
        if (!_.isEmpty(errors)) {
            return res.status(400).json(errors)
        }

        let newSettings = {
            invoiceTitle,
            address,
            phone,
            email,
            additionalText
        }
        let result = await db.collection('settings').updateOne({ name: 'invoice' }, { $set: newSettings }, { upsert: true });

        return res.json({ updated: newSettings });

    } catch (err) {
        console.log(err);
        res.status(500).json({ msg: err.message })
    }

})


router.post('/seo', async (req, res) => {

    try {
        let { metaTitle, metaTags, metaDescription, injectHTML } = req.fields;

        let db = await con.db();

        //validation
       
        let newSettings = {
            metaTitle, 
            metaTags, 
            metaDescription,
            injectHTML
        }
        let result = await db.collection('settings').updateOne({ name: 'seo' }, { $set: newSettings }, { upsert: true });

        return res.json({ updated: newSettings });

    } catch (err) {
        console.log(err);
        res.status(500).json({ msg: err.message })
    }

})


router.post("/sitemap", async (req, res) => {
    let domain = req.hostOptions.domain;
    let index = `<?xml version="1.0" encoding="UTF-8"?>

    <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    <sitemap>
        <loc>http://${domain}/sitemap-category.xml</loc>
    </sitemap>

    
    <sitemap>
        <loc>http://${domain}/sitemap-brand.xml</loc>
    </sitemap>

    
    <sitemap>
        <loc>http://${domain}/sitemap-tag.xml</loc>
    </sitemap>

    
    <sitemap>
        <loc>http://${domain}/sitemap-product.xml</loc>
    </sitemap>

    
    <sitemap>
        <loc>http://${domain}/sitemap-post.xml</loc>
    </sitemap>

    
    <sitemap>
        <loc>http://${domain}/sitemap-page.xml</loc>
    </sitemap>


    </sitemapindex>
    `
    await fs.promises.writeFile(path.join(req.publicDir, 'sitemap.xml'), index);



    //*common header & footer
    let commonHeader = `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    `

    let commonFooter = `
    </urlset>
    `

    let db = await con.db();
    
    //*generate category sitemap
    let categories = await db.collection('category').find({}).project({url:1, updated:1, added:1}).toArray();
    
    let categoryUrls = categories.map(i=>{
        return `
        <url>

            <loc>https://${domain}${i.url}</loc>
    
            <lastmod>${i.updated || i.added || ''}</lastmod>
    
            <changefreq>weekly</changefreq>
    
        </url>
     `
    })

    let categorySiteMap = 
    `${commonHeader} ${categoryUrls.join('')} ${commonFooter}`

    await fs.promises.writeFile(path.join(req.publicDir, 'sitemap-category.xml'), categorySiteMap);
    
    //*generate brand sitemap
    let brands = await db.collection('brand').find({}).project({url:1, updated:1, added:1}).toArray();
    
    let brandUrls = brands.map(i=>{
        return `
        <url>

            <loc>https://${domain}${i.url}</loc>
    
            <lastmod>${i.updated || i.added || ''}</lastmod>
    
            <changefreq>weekly</changefreq>
    
        </url>
     `
    })

    let brandSiteMap = 
    `${commonHeader} ${brandUrls.join('')} ${commonFooter}`

    await fs.promises.writeFile(path.join(req.publicDir, 'sitemap-brand.xml'), brandSiteMap);

    
    //*generate tag sitemap
    let tags = await db.collection('tag').find({}).project({url:1, updated:1, added:1}).toArray();
    
    let tagUrls = tags.map(i=>{
        return `
        <url>

            <loc>https://${domain}${i.url}</loc>
    
            <lastmod>${i.updated || i.added || ''}</lastmod>
    
            <changefreq>weekly</changefreq>
    
        </url>
     `
    })

    let tagSiteMap = 
    `${commonHeader} ${tagUrls.join('')} ${commonFooter}`

    await fs.promises.writeFile(path.join(req.publicDir, 'sitemap-tag.xml'), tagSiteMap);

    //*generate product sitemap
    let products = await db.collection('product').find({}).project({url:1, updated:1, added:1}).toArray();
    
    let productUrls = products.map(i=>{
        return `
        <url>

            <loc>https://${domain}${i.url}</loc>
    
            <lastmod>${i.updated || i.added || ''}</lastmod>
    
            <changefreq>weekly</changefreq>
    
        </url>
     `
    })

    let productSiteMap = 
    `${commonHeader} ${productUrls.join('')} ${commonFooter}`

    await fs.promises.writeFile(path.join(req.publicDir, 'sitemap-product.xml'), productSiteMap);

    //*generate post sitemap
    let posts = await db.collection('post').find({}).project({url:1, updated:1, added:1}).toArray();
    
    let postUrls = posts.map(i=>{
        return `
        <url>

            <loc>https://${domain}${i.url}</loc>
    
            <lastmod>${i.updated || i.added || ''}</lastmod>
    
            <changefreq>weekly</changefreq>
    
        </url>
     `
    })

    let postSiteMap = 
    `${commonHeader} ${postUrls.join('')} ${commonFooter}`

    await fs.promises.writeFile(path.join(req.publicDir, 'sitemap-post.xml'), postSiteMap);



    //*generate page sitemap
    let pages = await db.collection('page').find({}).project({url:1, updated:1, added:1}).toArray();
    
    let pageUrls = pages.map(i=>{
        return `
        <url>

            <loc>https://${domain}${i.url}</loc>
    
            <lastmod>${i.updated || i.added || ''}</lastmod>
    
            <changefreq>weekly</changefreq>
    
        </url>
     `
    }) 

    let pageSiteMap = 
    `${commonHeader} ${pageUrls.join('')} ${commonFooter}`

    await fs.promises.writeFile(path.join(req.publicDir, 'sitemap-page.xml'), pageSiteMap);

    return res.json({
        sitemapIndex: `${domain}/sitemap.xml`,
        sitemapCategory: `${domain}/sitemap-category.xml`,
        sitemapBrand: `${domain}/sitemap-brand.xml`,
        sitemapTag: `${domain}/sitemap-tag.xml`,
        sitemapProduct: `${domain}/sitemap-product.xml`,
        sitemapPost: `${domain}/sitemap-post.xml`,
        sitemapPage: `${domain}/sitemap-page.xml`,
    })


})
module.exports = router;