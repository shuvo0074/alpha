const router = require("express").Router();

const categoryRouter = require("./category.js");
const productRouter = require("./product.js");
const brandRouter = require("./brand.js");
const attributesRouter = require("./attributes.js");
const tagRouter = require("./tag.js");
const searchRouter = require("./search.js");
const offerRouter = require("./offer.js");
const couponRouter = require("./coupon.js")
const bundleRouter = require("./bundle.js")

const orderRouter = require("./order.js");
const deliveryRouter = require("./delivery.js");
const customerRouter = require("./customer.js");

const pageRouter = require("./page.js");
const postRouter = require('./post.js');
const feedbackRouter  = require('./feedback.js');

const imageRouter = require("./imageLibrary.js");
const sitemapRouter = require("./sitemap.js");

const componentRouter = require("./component.js");
const settingsRouter = require("./settings.js");
const themeRouter = require("./theme.js");

const credentialRouter = require("./credential.js");
const adminRolesRouter = require('./adminRoles.js');

const emailRouter = require("./email.js");
const smsRouter = require("./sms.js");
const notificationRouter = require("./notification.js");

const analyticsRouter = require('./analytics.js');

const migrateRouter = require("./migrate.js");


//all admin
router.use("/image", imageRouter);
router.use("/credential", credentialRouter);

//catalogue 
router.use("/category", categoryRouter);
router.use("/brand", brandRouter);
router.use("/product", productRouter);
router.use("/attributes", attributesRouter);
router.use("/tag", tagRouter);
router.use("/search", searchRouter);

//feedback
router.use("/feedback", feedbackRouter);


router.use("/coupon", couponRouter);
router.use("/bundle", bundleRouter);
router.use("/offer", offerRouter);

//super 
router.use("/component", componentRouter);
router.use("/settings", settingsRouter);
router.use("/theme", themeRouter);
router.use("/email", emailRouter);
router.use("/sms", smsRouter);
router.use("/adminRoles", adminRolesRouter);

//analytics
router.use("/analytics", analyticsRouter);

//blog
router.use("/page", pageRouter);
router.use("/post", postRouter)

//delivery 
router.use("/delivery", deliveryRouter);

//order
router.use("/order", orderRouter);
router.use("/notification", notificationRouter);
router.use("/customer", customerRouter);

//none
router.use("/migrate", migrateRouter);
router.use("/sitemap", sitemapRouter);

module.exports = router;