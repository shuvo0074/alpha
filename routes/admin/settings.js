const router = require('express').Router();
const Validator =  require('../../validator/admin.js');
const _ = require('lodash')
const con = require('../../lib/db.js')

const updateRouter = require('./settings/update.js');
const getRouter = require('./settings/get.js');

router.use('/update', updateRouter); 
router.use('/', getRouter); 

module.exports = router;
