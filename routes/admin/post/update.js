const router = require('express').Router();
const con = require('../../../lib/db.js')
const Validator = require('../../../validator/admin.js');
const _ = require('lodash')
const urlGenerator = require('../../../lib/urlGenerator.js')

router.post('/:postId', async (req, res) => {

  try {
    let postId = req.params.postId;
    if (!con.ObjectID.isValid(postId)) return res.status(400).json({ id: "Invalid post id" });

    let {
      name, body, url, category,
      tags, primaryCategory,
      servingSize,
      preparationTime, cookingTime, requiredProducts,
      metaTitle,
      metaTags,
      metaDescription, bn = {}, displayOrder
    } = req.fields;

    let db = await con.db();

    let list = {}

    // validation
    let categories = await db.collection('postCategory').find({}).sort({ name: 1 }).toArray();
    list.category = categories.map(item => {
      return item._id.toHexString();
    });

    let products = await db.collection('product').find({}).sort({ name: 1 }).toArray();
    list.products = products.map(product => {
      return product._id.toHexString();
    })

    primaryCategory = primaryCategory || category[0];
    primaryCategory = categories.find(
      item => item._id.toHexString() == primaryCategory
    );

    let tag = await db.collection('postTag').find({}).sort({ name: 1 }).toArray();
    list.tags = tag.map(item => {
      return item._id.toHexString();
    });

    let validator = new Validator();
    let errors = await validator.addPost(req.fields, list);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    url = await urlGenerator.postUrl(url || name, primaryCategory.name, postId);

    let requiredProductList = [];

    for (let product of requiredProducts) {
      if (!con.ObjectID.isValid(product._id)) return res.status(400).json({ requiredProducts: "invalid id" })
      product._id = new con.ObjectID(product._id);

      if (!con.ObjectID.isValid(product.variation)) return res.status(400).json({ requiredProducts: "invalid variation id" })
      product.variation = new con.ObjectID(product.variation);
      product.quantity = +product.quantity || 1;

      requiredProductList.push(product);
    }


    let postItem = {
      name, body, url,
      category: category && category.map(i => new con.ObjectID(i)),
      tags: tags && tags.map(i => new con.ObjectID(i)),
      primaryCategory: new con.ObjectID(primaryCategory._id),
      servingSize,
      preparationTime,
      cookingTime,
      requiredProducts: requiredProductList,
      metaTitle,
      metaTags,
      metaDescription,
      displayOrder,
      bn: {
        name: bn.name,
        body: bn.body,
        servingSize: bn.servingSize,
        preparationTime: bn.preparationTime,
        cookingTime: bn.cookingTime,

        metaTitle: bn.metaTitle,
        metaTags: bn.metaTags,
        metaDescription: bn.metaDescription,
      },
      added: new Date(),
      updated: new Date(),
    }

    // update document
    let result = await db.collection('post').updateOne(
      { _id: new con.ObjectID(postId) },
      { $set: postItem }
    );

    if (result.matchedCount == 0) return res.status(400).json({ post: "post with given id found!" });

    let aggregatePipeLine = [
      { $match: { _id: new con.ObjectID(postId) } },

      {
        $lookup: {
          from: 'images',
          localField: 'image',
          foreignField: '_id',
          as: 'image'
        }
      },

      {
        $lookup: {
          from: 'images',
          localField: 'cover',
          foreignField: '_id',
          as: 'cover'
        }
      },

      { $unwind: { path: "$cover", preserveNullAndEmptyArrays: true } }
    ];

    let updatedPost = await db.collection('post').aggregate(aggregatePipeLine).toArray();
    return res.json({ updated: updatedPost });

  } catch (error) {
    console.log(error);
    res.status(500).json({ error: error.message });
  }
})

router.post('/delete/:postId', async (req, res) => {
  try {
    let postId = req.params.postId;
    if (!postId) return res.status(400).json({ msg: "Invalid Post Id" });

    const db = await con.db();

    //delete the product
    let result = await db.collection("post").deleteOne({ _id: new con.ObjectID(postId) });

    //existence check
    if (result.deletedCount < 1)
      return res.status(400).json({ id: "No post found with given id" });

    return res.json({ success: "Post deleted successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: error.message });
  }
})



module.exports = router;