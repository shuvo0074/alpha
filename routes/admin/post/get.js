const router = require('express').Router();
const con = require('../../../lib/db.js');
const Validator = require('../../../validator/admin.js');
const _ = require('lodash')
const urlGenerator = require('../../../lib/urlGenerator.js')
const postFunctions = require('../../../lib/dbFunctions/adminFunctions.js');
const convertParams = require('../../../lib/utility.js')

router.get('/', async (req, res) => {
    try {
        
        const posts = await postFunctions.getPostsList(req.query);
        return res.json(posts)
    } catch(error) {
        console.log(error);
        res.status(500).json({error: error.message})
    }
});

router.get('/:postId', async (req, res) => {
    try {
        const postId = req.params.postId;
        
        // if(!con.ObjectID.isValid(postId)) return res.status(400).json({postId: "Invalid post id"});

        // let post = await postFunctions.getPost(postId, {category, tag, image, primaryCategory});
        // console.log(post)
        // return res.json({post})

        let post;
        if (con.ObjectID.isValid(postId)  && !postId.includes('/') ) {
          //id passed, query by id
          post = await postFunctions.getPost(postId, req.query);
        } else {
          //query by url
          post =  await postFunctions.getPost(req.path, req.query);
        }
        res.json(post)
    } catch(error) {
        console.log(error);
        res.status(500).json({error: error.message});
    }
})



module.exports = router;