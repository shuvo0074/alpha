const router = require('express').Router();
const con = require('../../../../lib/db.js')
const Validator = require('../../../../validator/admin.js')
const urlGenerator = require('../../../../lib/urlGenerator.js')
const _ = require('lodash')

router.post('/:catId', async (req, res) => {
	try {


		let id = req.params.catId;

		if (!id || !con.ObjectID.isValid(id))
			return res.status(400).json({ id: "invalid id" });


		let db = await con.db();

		let { name, parent, description, url, metaTitle, metaTags, metaDescription,  bn={} } = req.fields;

		let list = {}; //for validation

		//category list
		let categories = await db.collection('postCategory').find({}).toArray();
		list.category = categories.map(item => {  //list of category _id for validation
			return item._id.toHexString();
		});

		//basic validation
		let validator = new Validator();
		let errors = await validator.addCategory(req.fields, list);
		if (!_.isEmpty(errors)) return res.status(400).json(errors);

		//TODO: add more validation


		// *parent category ...
		if (!con.ObjectID.isValid(parent)) parent = null;
		else parent = await db.collection('postCategory').findOne({ _id: new con.ObjectID(parent) });

		// *category type
		let type = (parent && parent._id) ? 'sub' : 'top';

		// *url
		url = await urlGenerator.postCategoryUrl(url || name, parent && parent.name, id);

		// *model category
		let category = {
			name,
			type,
			parent: parent && parent._id,
			description,
			url,

			metaTitle,
			metaTags,
			metaDescription,

			bn: {
				name: bn.name,
				description: bn.description,

				metaTitle: bn.metaTitle,
				metaTags: bn.metaTags,
				metaDescription: bn.metaDescription
			},
			updated: new Date()

		}

		let result = await db.collection("postCategory").updateOne({ _id: new con.ObjectID(id) }, { $set: category });

		//item existence check
		if (result.matchedCount == 0)
			return res.status(400).json({ itemId: `no category with given id` });

		return res.json({
			updated: category,
		});
	
	} catch (error) {
		console.log(error);
		return res.status(500).json({ error: error.message })
	}
})


router.post("/icon/:id", async (req, res) => {
  try {

    let id = req.params.id;
    if (!id || !con.ObjectID.isValid(id))
      return res.status(400).json({ id: "invalid id" });


    let db = await con.db();


    // *icon
    let { icon } = req.files;
    let iconPublicPath;

    if (icon && icon.size < 0) return res.status(400).json({icon: "upload icon"});

    //*checkMime 
    let mimeErrors = await checkMime.image([icon]);
    if (mimeErrors.length > 0) {
      return res.status(400).json(mimeErrors);
    }

    let fileName = `${name}_icon_${icon.name}`;
    let filePath = path.join(req.publicDir, 'images', 'icons');

    let iconData = await fs.promises.readFile(icon.path);
    let iconFullPath = path.join(filePath, fileName);

    //create directory if not exists
    if (!fs.existsSync(filePath)) await fs.promises.mkdir(filePath, { recursive: true });
    //write to disk
    await fs.writeFile(iconFullPath, iconData);


    //public path
    iconPublicPath = path.join('/images', 'icons', fileName);



    let category = { 
	  icon: iconPublicPath,
	  updated: new Date()
	  
    }

    let result = await db.collection("postCategory").updateOne({ _id: new con.ObjectID(id) }, { $set: category });

    //item existence check
    if (result.matchedCount == 0) return res.status(400).json({ itemId: `no category with given id` });

    return res.json({ updated: category});

  } catch (err) {
    console.log(err);
    res.status(500).json({ error: err.message });
  }
});


router.post("/delete/:id", async (req, res) => {
  let id = req.params.id;
  if (!id || !con.ObjectID.isValid(id))
    return res.status(400).json({ id: "Invalid id" });
  let db = await con.db();

  //remove category from db;
  let result = await db.collection("postCategory").deleteOne({ _id: new con.ObjectID(id) });

  if (result.deletedCount < 1)return res.status(400).json({ id: "no category with given id" });

  return res.json({ success: "Category deleted!" });
});

module.exports = router;