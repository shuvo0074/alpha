const router = require('express').Router();
const con = require('../../../../lib/db.js');
const Validator = require('../../../../validator/admin.js');
const _ = require('lodash')
const urlGenerator = require('../../../../lib/urlGenerator.js');
const convertParams = require('../../../../lib/utility.js');
const postFunctions = require('../../../../lib/dbFunctions/adminFunctions.js');


router.get('/', async (req, res) => {
    try {
        let {parentCategory, limit, page, sort, order} = req.query;
        parentCategory = convertParams.convertParams(parentCategory)
        let categories = await postFunctions.getPostCategory({parentCategory, limit, page, sort, order})
        
        return res.json(categories)
    } catch(error) {
        console.log(error);
        res.status(500).json({error: error.message})
    }
})

router.get('/:catId', async (req, res) => {
    try {
        const catId = req.params.catId;
        if(!con.ObjectID.isValid(catId)) return res.status(400).json({catId: "Invalid category id"});

        let {cover, image, parentCategory} = req.query;
        cover = convertParams.convertParams(cover);
        image = convertParams.convertParams(image);
        parentCategory = convertParams.convertParams(parentCategory);

        let category = await postFunctions.getCategory(catId, {cover, image, parentCategory})
        return res.json(category)

    } catch(error) {
        console.log(error);
        res.status(500).json({error: error.message});
    }
})

router.get('/:catId/post', async (req, res) => {
    try {
       
        let category = req.params.catId;
        if(!con.ObjectID.isValid(category)) return res.status(400).json({id: "invalid id"});

        const posts = await postFunctions.getPostsList(req.query, {category});

        return res.json(posts)

    } catch(error) {
        console.log(error);
        res.status(500).json({error: error.message});
    }
})


module.exports = router;