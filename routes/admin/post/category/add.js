const router = require('express').Router();
const con = require('../../../../lib/db.js');
const Validator = require('../../../../validator/admin.js');
const _ = require('lodash')
const urlGenerator = require('../../../../lib/urlGenerator.js');

router.post('/', async (req, res) => {
  try {

    let db = await con.db();
    let { name, parent, description, image, cover, url, metaTitle, metaTags, metaDescription, bn = {} } = req.fields;

    let list = {}; //for validation

    //category list
    let categories = await db.collection('postCategory').find({}).toArray();
    list.category = categories.map(item => {  //list of category _id for validation
      return item._id.toHexString();
    });

    //basic validation
    let validator = new Validator();
    let errors = await validator.addCategory(req.fields, list);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    //TODO: add more validation


    // *parent category ...
    if (!con.ObjectID.isValid(parent)) parent = null;
    else parent = await db.collection('postCategory').findOne({ _id: new con.ObjectID(parent) });

    // *category type
    let type = (parent && parent._id) ? 'sub' : 'top';

    // *image ...
    // convert image ids to objectIds
    let validImages = [];
    if (image) {
      image = JSON.parse(image)
      for (let x in image) {
        if (!con.ObjectID.isValid(image[x])) {
          console.log(image[x]);
          continue
        };
        validImages.push(new con.ObjectID(image[x]));
      }
    }
    //cover image
    cover = (cover && con.ObjectID.isValid(cover)) ? new con.ObjectID(cover) : (validImages && validImages[0]) ? validImages[0] : null;
    //TODO: validate images and cover from DB


    // *icon
    let { icon } = req.files;
    let iconPublicPath;

    if (icon && icon.size > 0) {
      //*checkMime 
      let mimeErrors = await checkMime.image([icon]);
      if (mimeErrors.length > 0) {
        return res.status(400).json(mimeErrors);
      }

      let fileName = `${name}_icon_${icon.name}`;
      let filePath = path.join(req.publicDir, 'images', 'icons');

      let iconData = await fs.promises.readFile(icon.path);
      let iconFullPath = path.join(filePath, fileName);

      //create directory if not exists
      if (!fs.existsSync(filePath)) await fs.promises.mkdir(filePath, { recursive: true });
      //write to disk
      await fs.writeFile(iconFullPath, iconData);


      //public path
      iconPublicPath = path.join('/images', 'icons', fileName);
    }


    // *url
    url = await urlGenerator.postCategoryUrl(url || name, parent && parent.name);

    // *model category
    let category = {
      name,
      type,
      parent: parent && parent._id,
      description,
      image: validImages,
      cover,
      icon: iconPublicPath,
      url,

      metaTitle,
      metaTags,
      metaDescription,

      bn: {
        name: bn.name,
        description: bn.description,

        metaTitle: bn.metaTitle,
        metaTags: bn.metaTags,
        metaDescription: bn.metaDescription
      },
      added: new Date(),
      updated: new Date()

    }
    // *saving to db ...
    let result = await db.collection('postCategory').insertOne(category);


    //resolve cover for response
    let coverDetail = cover && await db.collection('images').findOne({ _id: cover });
    if (result.ops && result.ops[0]) result.ops[0].cover = coverDetail;


    return res.json(result.ops);

  } catch (error) {
    console.log(error);
    res.status(500).json({ error: error.message })
  }
})



module.exports = router;