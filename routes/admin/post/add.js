const router = require('express').Router();
const con = require('../../../lib/db.js');
const Validator = require('../../../validator/admin.js');
const _ = require('lodash')
const urlGenerator = require('../../../lib/urlGenerator.js')

router.post('/', async (req, res) => {

  try {
    let {
      name, body, url, category,
      tags, cover, image, primaryCategory,
      servingSize, cookingTime, preparationTime, requiredProducts, 
      metaTitle,
      metaTags,
      metaDescription,  bn={}, displayOrder
    } = req.fields;

    let db = await con.db();

    let list = {}

    // validation
    let categories = await db.collection('postCategory').find({}).sort({ name: 1 }).toArray();
    list.category = categories.map(item => {
      return item._id.toHexString();
    });

    let products = await db.collection('product').find({}).sort({ name: 1 }).toArray();
    list.products = products.map(product => {
      return product._id.toHexString();
    })

    primaryCategory = primaryCategory || category[0];
    primaryCategory = categories.find(
      item => item._id.toHexString() == primaryCategory
    );

    let tag = await db.collection('postTag').find({}).sort({ name: 1 }).toArray();
    list.tags = tag.map(item => {
      return item._id.toHexString();
    });

    let validator = new Validator();
    let errors = await validator.addPost(req.fields, list);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    url = await urlGenerator.postUrl(url || name, primaryCategory.name);

     // *image ... 
    // convert image ids to objectIds
    let validImages = [];
    if (image){
      for (let x in image) {
        if (!con.ObjectID.isValid(image[x])) {
          continue
        };
        validImages.push(new con.ObjectID(image[x]));
      }
    }
    //cover image
    cover = (cover && con.ObjectID.isValid(cover) )? new con.ObjectID(cover) : (validImages && validImages[0])? validImages[0]:null;
    //TODO: validate images and cover from DB


    let requiredProductList = [];

    for(let product of requiredProducts){
      if(!con.ObjectID.isValid(product._id)) return res.status(400).json({requiredProducts: "invalid id"})
      product._id = new con.ObjectID(product._id);

      if(!con.ObjectID.isValid(product.variation)) return res.status(400).json({requiredProducts: "invalid variation id"})
      product.variation = new con.ObjectID(product.variation);
      product.quantity = +product.quantity || 1;

      requiredProductList.push(product);
    }

    let postItem = {
      name, body, url,
      category: category && category.map(i => i && new con.ObjectID(i)).filter(i=>i),
      tags: tags && tags.map(i => i && new con.ObjectID(i)).filter(i=>i),
      cover,
      image: validImages,
      primaryCategory: new con.ObjectID(primaryCategory._id),
      servingSize,
      preparationTime,
      cookingTime,
      requiredProducts: requiredProductList,
      metaTitle,
      metaTags,
      metaDescription,
      displayOrder,
      bn: {
        name: bn.name,
        body: bn.body,
        servingSize: bn.servingSize,
        preparationTime: bn.preparationTime,
        cookingTime: bn.cookingTime,

        metaTitle: bn.metaTitle,
        metaTags: bn.metaTags,
        metaDescription: bn.metaDescription,
      },
      added: new Date(),
      updated: new Date(),
    }

    // insert to db
    let result = await db.collection('post').insertOne(postItem);

    
    //resolve cover for response
    let coverDetail = cover && await db.collection('images').findOne({_id: cover});
    if(result.ops && result.ops[0]) result.ops[0].cover = coverDetail;

    return res.json({ inserted: result.ops });

  } catch (error) {
    console.log(error);
    res.status(500).json({ error: error.message })
  }
})



module.exports = router;