const router = require('express').Router();
const con = require('../../../../lib/db.js');
const Validator = require('../../../../validator/admin.js');
const _ = require('lodash')
const urlGenerator = require('../../../../lib/urlGenerator.js');

router.post('/', async (req, res) => {
	try {


		let db = await con.db();
		let { name, description,  bn={} } = req.fields;

		//basic validation
		let validator = new Validator();
		let errors = await validator.addTag(req.fields);
		if (!_.isEmpty(errors)) return res.status(400).json(errors);

		//saving to db
		let result = await db.collection('postTag').insertOne({
			name,
			description,
			bn: {
				name: bn.name,
				description: bn.description
			},
			added: new Date(),
			updated: new Date()
		});

		return res.json({ inserted: result.ops });


	} catch (error) {
		console.log(error);
		res.status(500).json({ error: error.message })
	}
})



module.exports = router;