const router = require('express').Router();
const con = require('../../../../lib/db.js');
const Validator = require('../../../../validator/admin.js');
const _ = require('lodash')
const urlGenerator = require('../../../../lib/urlGenerator.js');
const convertParams = require('../../../../lib/utility.js');
const postFunctions = require('../../../../lib/dbFunctions/adminFunctions.js');


router.get('/', async (req, res) => {
    try {
        let { limit, page, sort, order } = req.query;
        let tags = await postFunctions.getPostTags({ limit, page, sort, order })

        return res.json(tags)
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: error.message })
    }
});

router.get('/:tagId', async (req, res) => {
    try {
        const tagId = req.params.tagId;
        let { limit, page, sort, order, image, cover } = req.query;

        image = convertParams.convertParams(image);
        cover = convertParams.convertParams(cover);


        let tag;
        if (con.ObjectID.isValid(tagId)  && !tagId.includes('/') ) {
            //id passed, query by id
            tag = await postFunctions.getTag(tagId, req.query);
        } else {
            //query by url
            tag = await postFunctions.getTag(req.path, req.query);
        }
        res.json(tag)
    } catch (error) {
        console.log(error);
        return res.status(500).json({error: error.message})
    }
})

router.get('/:tagId/post', async (req, res) => {
    try {
       
        let tag = req.params.tagId;
        if(!con.ObjectID.isValid(tag)) return res.status(400).json({id: "invalid id"});

        const posts = await postFunctions.getPostsList(req.query, {tag});

        return res.json(posts)

    } catch(error) {
        console.log(error);
        res.status(500).json({error: error.message});
    }
})


module.exports = router;