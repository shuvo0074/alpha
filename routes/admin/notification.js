const router = require('express').Router();
const notificationFunctions = require('./../../lib/adminNotification.js');
const con = require('./../../lib/db.js');
const paginate = require('./../../lib/paginate.js');


router.get('/', async (req, res)=>{

    let aggregationPipeLine = [];
    let status = req.query.status;
    
    if(status == "read") status = true;
    else if(status == "unread") status = false;

    if(status){
        aggregationPipeLine.push({
            $match:{read: status }
        })
    }

    let notifications = await paginate('adminNotification', aggregationPipeLine, [], req.query);
    return res.json(notifications);
})

router.post('/markread', async(req, res)=>{
    let {id} = req.fields;
    if(!Array.isArray(id)) return res.status(400).json({id: "expected parameter id to be Array"});

    // * validate id
    for(let i in id){
        if(!con.ObjectID.isValid(id[i])) return res.status(400).json({id: "invalid id"});
        id[i] = new con.ObjectID(id[i]);
    }
    await notificationFunctions.markRead(id);
    return res.json({success: "notifications marked as read"})
})

router.post('/markreadall', async(req, res)=>{
    await notificationFunctions.markReadAll();
    return res.json({success: "notifications marked as read"})
})



router.post('/markunread', async(req, res)=>{
    let {id} = req.fields;
    if(!Array.isArray(id)) return res.status(400).json({id: "expected parameter id to be Array"});

    // * validate id
    for(let i in id){
        if(!con.ObjectID.isValid(id[i])) return res.status(400).json({id: "invalid id"});
        id[i] = new con.ObjectID(id[i]);
    }

    await notificationFunctions.markUnread(id);
    return res.json({success: "notifications marked as unread"})


})

router.post('/markunreadall', async(req, res)=>{
    notificationFunctions.markUnreadAll();
    return res.json({success: "notifications marked as unread"})

})



router.get('/:id', async (req, res)=>{

    // *validate id
    let id = req.params.id;
    if(!con.ObjectID.isValid(id)) return res.status(400).json({id: "invalid id" });

    // *get from db
    let db = await con.db();
    let notification = await db.collection('adminNotification').findOne({_id: new con.ObjectID(id)});
    if(!notification) return res.status(404).json({id: 'notification not found'});

    res.json(notification);

    //mark as read
    notificationFunctions.markRead([new con.ObjectID(id)]);
})

module.exports = router;