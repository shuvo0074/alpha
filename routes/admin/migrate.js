const router = require('express').Router();
const _ = require('lodash');
const con = require('../../lib/db.js');
const fs = require('fs');
const jimp = require('jimp');
const path = require('path');


router.use('/from0.1.1', async (req, res) => {
  const db = await con.db();

  let product = await db.collection('product').find({}).toArray();
  let category = await db.collection('category').find({}).toArray();
  let brand = await db.collection('brand').find({}).toArray();


  let images = [];

  //user defined image size settings
  let {full = {}, medium={}, thumbnail={}, icon={}} = await db.collection('settings').findOne({name:'imageSize'}) || {};
        
  (function(){
    //make sure sizes are valid
    if((!full.width || full.width == 'auto') && (!full.height || full.height == 'auto')) {
      full.width = 1024;
      full.height = jimp.AUTO;
    }
    if(full.width == 'auto') full.width = jimp.AUTO;
    if(full.height == 'auto') full.height = jimp.AUTO;
    if((!medium.width || medium.width == 'auto') && (!medium.height || medium.height == 'auto')) {
      medium.width = 500;
      medium.height = jimp.AUTO;
    }
    if(medium.width == 'auto') medium.width = jimp.AUTO;
    if(medium.height == 'auto') medium.height = jimp.AUTO;
    if((!thumbnail.width || thumbnail.width == 'auto') && (!thumbnail.height || thumbnail.height == 'auto')) {
      thumbnail.width = 150;
      thumbnail.height = jimp.AUTO;
    }
    if(thumbnail.width == 'auto') thumbnail.width = jimp.AUTO;
    if(thumbnail.height == 'auto') thumbnail.height = jimp.AUTO;
    if((!icon.width || icon.width == 'auto') && (!icon.height || icon.height == 'auto')) {
      icon.width = 50;
      icon.height = jimp.AUTO;
    }
    if(icon.width == 'auto') icon.width = jimp.AUTO;
    if(icon.height == 'auto') icon.height = jimp.AUTO;
  })()
  
  brand.forEach(async x=>{
    if(!x.image || typeof x.image != 'string') return;
    let imgPath =path.join(__dirname , '../../public', x.image);
    console.log(imgPath);
    if(!fs.existsSync(imgPath)) return;
    console.log('brand2', x.name);

    let pathPrefix = req.publicDir;
    let imagePath = path.join('/images', 'library');


        let fileName =  (Math.random()*1000000).toFixed(0)+'-'+x.name;
        fileName = fileName.replace(/ /g, '-').replace(/%20/g, '-') //replace whitespace & %20 with - 
        //ensure .jpg extension
        fileName = fileName.split('.');
        fileName[fileName.length] = 'jpg';
        fileName = fileName.join('.');

        let originalPath = path.join( imagePath, 'original', fileName)
        let fullPath = path.join( imagePath, 'full', fileName)
        let mediumPath = path.join( imagePath, 'medium', fileName)
        let thumbnailPath = path.join( imagePath, 'thumbnail', fileName)
        let iconPath = path.join(imagePath, 'icon', fileName)
        
        if(!fs.existsSync(imgPath)) return;

        //save to disk
        let data = await fs.readFile(imgPath);
console.log(originalPath, fileName);

        //*original size
        await fs.writeFile(path.join(pathPrefix, originalPath), data); //?pay special attention here while rewriting paths

        jimp.read(data)
        .then(data => {
            return data
            //reduce  image quality
            // .quality(75)

            //full size 1024 x auto
            .resize(+full.width, +full.height)
            .write(path.join(pathPrefix, fullPath))
            
            //medium size 300 x auto
            .resize(+medium.width, +medium.height) 
            .write(path.join(pathPrefix,mediumPath))

            //thumbnail size 150 x auto
            .resize(+thumbnail.width, +thumbnail.height) 
            .write(path.join(pathPrefix,thumbnailPath))

            //icon size 50 x auto
            .resize(+icon.width, +icon.height) 
            .write(path.join(pathPrefix,iconPath))

        }).catch(err=>{
            console.log(err);
        })
        
        let imageObj = {
            name:x.name+'image'+(Math.random()*1000).toFixed(0),
            original: originalPath,
            full: fullPath,
            medium: mediumPath,
            thumbnail: thumbnailPath,
            icon: iconPath,
            title:'',
            alt:'',
            labels:[],
            caption:'',
            added: new Date()
        }
        let result = await db.collection('images').insertOne(imageObj);
        await db.collection('brand').updateOne({_id: new con.ObjectID(x._id)}, {$set:{
          image: [result.insertedId],
          cover: result.insertedId,
        }})
  });
  
  category.forEach(async x=>{
    
    if(!x.image || typeof x.image != 'string') return;
    let imgPath =path.join(__dirname , '../../public', x.image);
    console.log(imgPath);
    
    if(!fs.existsSync(imgPath)) return;
    console.log('category2', x.name);

    let pathPrefix = req.publicDir;
    let imagePath = path.join('/images', 'library');


        let fileName =  (Math.random()*1000000).toFixed(0)+'-'+x.name;
        fileName = fileName.replace(/ /g, '-').replace(/%20/g, '-') //replace whitespace & %20 with - 
        //ensure .jpg extension
        fileName = fileName.split('.');
        fileName[fileName.length] = 'jpg';
        fileName = fileName.join('.');

        let originalPath = path.join( imagePath, 'original', fileName)
        let fullPath = path.join( imagePath, 'full', fileName)
        let mediumPath = path.join( imagePath, 'medium', fileName)
        let thumbnailPath = path.join( imagePath, 'thumbnail', fileName)
        let iconPath = path.join(imagePath, 'icon', fileName)
        
        if(!fs.existsSync(imgPath)) return;

        //save to disk
        let data = await fs.readFile(imgPath);

        //*original size
        await fs.writeFile(path.join(pathPrefix, originalPath), data); //?pay special attention here while rewriting paths

        jimp.read(data)
        .then(data => {
            return data
            //reduce  image quality
            // .quality(75)

            //full size 1024 x auto
            .resize(+full.width, +full.height)
            .write(path.join(pathPrefix, fullPath))
            
            //medium size 300 x auto
            .resize(+medium.width, +medium.height) 
            .write(path.join(pathPrefix,mediumPath))

            //thumbnail size 150 x auto
            .resize(+thumbnail.width, +thumbnail.height) 
            .write(path.join(pathPrefix,thumbnailPath))

            //icon size 50 x auto
            .resize(+icon.width, +icon.height) 
            .write(path.join(pathPrefix,iconPath))

        }).catch(err=>{
            console.log(err);
        })
        
        let imageObj = {
            name:x.name+'image'+(Math.random()*1000).toFixed(0),
            original: originalPath,
            full: fullPath,
            medium: mediumPath,
            thumbnail: thumbnailPath,
            icon: iconPath,
            title:'',
            alt:'',
            labels:[],
            caption:'',
            added: new Date()
        }
        let result = await db.collection('images').insertOne(imageObj);
        await db.collection('category').updateOne({_id: new con.ObjectID(x._id)}, {$set:{
          image: [result.insertedId],
          cover: result.insertedId,
        }})
  });
  
  product.forEach(async x=>{
    await db.collection('product').updateOne({_id: x._id}, {$set:{image:[]}});

    if(!x.image || !Array.isArray(x.image)) return;
    
    x.image.forEach(async img=>{
      let imgPath =path.join(__dirname , '../../public', img);
      console.log(imgPath);
      
      if(!fs.existsSync(imgPath)) return;
      console.log('product2', x.name);

    let pathPrefix = req.publicDir;
    let imagePath = path.join('/images', 'library');


        let fileName =  (Math.random()*1000000).toFixed(0)+'-'+x.name;
        fileName = fileName.replace(/ /g, '-').replace(/%20/g, '-') //replace whitespace & %20 with - 
        //ensure .jpg extension
        fileName = fileName.split('.');
        fileName[fileName.length] = 'jpg';
        fileName = fileName.join('.');

        let originalPath = path.join( imagePath, 'original', fileName)
        let fullPath = path.join( imagePath, 'full', fileName)
        let mediumPath = path.join( imagePath, 'medium', fileName)
        let thumbnailPath = path.join( imagePath, 'thumbnail', fileName)
        let iconPath = path.join(imagePath, 'icon', fileName)
        
        if(!fs.existsSync(imgPath)) return;

        //save to disk
        let data = await fs.readFile(imgPath);

        //*original size
        await fs.writeFile(path.join(pathPrefix, originalPath), data); //?pay special attention here while rewriting paths

        jimp.read(data)
        .then(data => {
            return data
            //reduce  image quality
            // .quality(75)

            //full size 1024 x auto
            .resize(+full.width, +full.height)
            .write(path.join(pathPrefix, fullPath))
            
            //medium size 300 x auto
            .resize(+medium.width, +medium.height) 
            .write(path.join(pathPrefix,mediumPath))

            //thumbnail size 150 x auto
            .resize(+thumbnail.width, +thumbnail.height) 
            .write(path.join(pathPrefix,thumbnailPath))

            //icon size 50 x auto
            .resize(+icon.width, +icon.height) 
            .write(path.join(pathPrefix,iconPath))

        }).catch(err=>{
            console.log(err);
        })
        
        let imageObj = {
            name:x.name+'image'+(Math.random()*1000).toFixed(0),
            original: originalPath,
            full: fullPath,
            medium: mediumPath,
            thumbnail: thumbnailPath,
            icon: iconPath,
            title:'',
            alt:'',
            labels:[],
            caption:'',
            added: new Date()
        }
        let result = await db.collection('images').insertOne(imageObj);
         db.collection('product').updateOne({_id: new con.ObjectID(x._id)}, {$push:{
          image: result.insertedId,
        }})

        if(x.cover == img){
           db.collection('product').updateOne({_id: new con.ObjectID(x._id)}, {$set:{
            cover: result.insertedId,
          }})
        }
      })
    })
  
  
  await db.collection('product').updateMany({}, [
    //combine prices into single object
    { $set: { price: { regular: '$price', offer: '$offerPrice' } } },
    { $unset: 'offerPrice' } //remove offerPice
  ]);

  //create searching index
  // await db.collection('product').dropIndex('$**_text'); //drop if exists ? :/
  await db
    .collection('product')
    .createIndex({ title: 'text', description: 'text' });

  res.json({ msg: 'migration completed' });

});

/**
 * migrate from alpha 0.1.1 to latest
 * changes:
 * *database Modeling
 * price :{regular, offer}
 *
 *
 */
router.use('/from0.1.2', async (req, res) => {
  let db = await con.db();
  await db.collection('product').updateMany({}, [
    //combine prices into single object
    { $set: { price: { regular: '$price', offer: '$offerPrice' } } },
    { $unset: 'offerPrice' } //remove offerPice
  ]);

  //create searching index
  await db.collection('product').dropIndex('$**_text'); //drop if exists ? :/
  await db
    .collection('product')
    .createIndex({ title: 'text', description: 'text' });

  res.json({ msg: 'migration completed' });
});

router.use('/itemurl', async(req,res)=>{
  let db = await con.db();

  //product
  let products = await db.collection('product').find({}).toArray();

  for(let product of products){
    let url = product.url;
    let name = product.name;

    if(url) continue;


    let  primaryCategory = product.primaryCategory || product.category[0];
    primaryCategory = await db.collection('category').findOne({_id: new con.ObjectID(primaryCategory)});

    let primaryCategoryName = primaryCategory.name;

    url = path.join('/product', primaryCategoryName, name);
    
    url = url
      .replace(/\ /g, '-')
      .replace(/\\/g, '/')
      .toLowerCase();

    async function findUnusedUrl(url, count = '') {
      let existingProductWithUrl = await db
        .collection('product')
        .findOne({ url: url + count });
      if (!existingProductWithUrl) return url + count;
      return findUnusedUrl(url, +count + 1);
    }

    url = await findUnusedUrl(url);

    db.collection('product').updateOne(
      {_id: new con.ObjectID(product._id)},
      {$set:{url}}  
    );
  }

  //category
  let categories = await db.collection('category').find({}).toArray();
  for(let category of categories){
    let url = category.url;
    let name = category.name;

    if(url) continue;


    let parent;
    if(con.ObjectID.isValid(category.parent))
      parent = await db.collection('category').findOne({_id: new con.ObjectID(category.parent)});

    
    //find url
    if (parent) {
      url = path.join('/category', `${parent.name ? parent.name : ''}`, name);
    } else {
      url = path.join('/category', name);
    }

    url = url
      .replace(/\ /g, '-')
      .replace(/\\/g, '/')
      .toLowerCase(); //remove whitespace

    async function findUnusedUrl(url, count = '') {
      let existingProductWithUrl = await db
        .collection('category')
        .findOne({ url: url + count });
      if (!existingProductWithUrl) return url + count;
      return findUnusedUrl(url, +count + 1);
    }
    url = await findUnusedUrl(url);

    db.collection('category').updateOne(
      {_id: new con.ObjectID(category._id)},
      {$set:{url}}
    )

  }
  res.end('url updated')

})

router.use('/correctUrl', async (req, res)=>{

  function removeSymbols(str){
    //replace non-url friendly characters with - 
    return str.replace(/[\ <>#%{}|\\^~\[\]`;\/?:@=&]/g, '-').toLowerCase();
  }

  let db = await con.db();
  let products = await db.collection('product').find({}).toArray();
  
  let counter = 0;
  for(let product of products){
    counter++
    product.category = product.category.filter(item=> item);
    await db.collection('product').update({_id: new con.ObjectID(product._id)}, 
    {$set:{
      name: removeSymbols(product.name),
      category: product.category,
    }}
    )

    res.write(` ${(counter/products.length * 100).toFixed(0)}%   `);
  }

  res.end(`
  ====================================================================
  done
  `)
})

module.exports = router;
