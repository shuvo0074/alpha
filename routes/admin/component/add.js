const router = require('express').Router();
const Validator =  require('../../../validator/admin.js');
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');

router.post('/', async (req, res)=>{
   try{
        let db = await con.db();

        let {groupName, items} = req.fields;

        //two components can not have same name 
        let exists = await db.collection('component').findOne({name:groupName});
        if(exists) return res.status(400).json({name:'Component '+groupName+' already exists'})

        let component = {
            name: groupName,
            items: []
        }

        //validate each item
        for(let item of items){

            //convert image ids
            if(item.image){
                let images = [];
                for(let id of item.image){
                    if(!con.ObjectID.isValid(id)) continue;
                    images.push(new con.ObjectID(id));
                }
                item.image = images; 
            }
            //validate properties
            for(prop in item){

                //only allow string and numbers as values
                if(prop == 'image') continue;
                if(typeof item[prop] != 'string' && typeof item[prop] != 'number'){
                    delete prop;
                }
            }
            component.items.push(item);
        }

        let result = await db.collection('component').insertOne(component);

        return res.json({inserted: result.ops});

   }catch(err){
       console.log(err);
       res.status(500).json({msg:err.message})
   }

})


module.exports = router;