const router = require('express').Router();
const Validator =  require('../../../validator/admin.js');
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');

fs.readFile = util.promisify(fs.readFile);
fs.writeFile = util.promisify(fs.writeFile);
fs.unlink = util.promisify(fs.unlink);

router.post('/:id', async (req, res)=>{

    try{
        let id = req.params.id;
        if(!con.ObjectID.isValid(id)) return res.status(400).json({id: "invalid id"});

        let db = await con.db();

        let {groupName, items} = req.fields;

        let component = {
            name: groupName,
            items: []
        }

        //validate each item
        for(let item of items){

            //convert image ids
            if(item.image){
                let images = [];
                for(let id of item.image){
                    if(!con.ObjectID.isValid(id)) continue;
                    images.push(new con.ObjectID(id));
                }
                item.image = images; 
            }
            //validate properties
            for(prop in item){

                //only allow string and numbers as values
                if(prop == 'image') continue;
                if(typeof item[prop] != 'string' && typeof item[prop] != 'number'){
                    delete prop;
                }
            }
            component.items.push(item);
        }

        let result = await db.collection('component').updateOne({_id: new con.ObjectID(id)},{$set:component});
        
        if(result.matchedCount == 0) 
            return res.status(400).json({id: "no  component with given id"});

        return res.json({updated : component});
 
    }catch(err){
        console.log(err);
        res.status(500).json({msg:err.message})
    }
 
 })

router.post('/delete/:id', async(req, res)=>{
    let id = req.params.id;
    if(!id) return res.status(400).json({id:'Invalid Id'});

    let db= await con.db();

    //remove component from collection;
    let result = await db.collection('component').deleteOne({_id: new con.ObjectID(id)});
    
    //existence check
    if(result.deletedCount == 0) return res.status(400).json({id: "no component with given id"});

    return res.json({success:'Component Deleted!'})
})

module.exports = router;