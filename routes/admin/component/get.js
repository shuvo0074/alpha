const router = require('express').Router();
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
const componentFunctions = require('./../../../lib/dbFunctions/public/componentFunctions.js');

fs.readFile = util.promisify(fs.readFile);
fs.writeFile = util.promisify(fs.writeFile);


router.get('/', async(req, res)=>{
    let components = await componentFunctions.getComponentList({image:true});
    return res.json({components});
})

router.get('/:id', async(req, res)=>{
    let id = req.params.id;
    if(!con.ObjectID.isValid(id)) return res.status(400).json({id: "invalid id"});

    let components = await componentFunctions.getComponent(id, {image:true});
    return res.json({components});
})



module.exports = router;
