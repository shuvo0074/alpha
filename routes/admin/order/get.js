const router = require('express').Router();
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
const paginate = require('./../../../lib/paginate.js');
const orderFunctions = require('./../../../lib/dbFunctions/admin/orderFunctions.js');

router.get('/', async (req, res) => {

	let orders = await orderFunctions.getOrderList(req.query);
	return res.json(orders);
})

router.get('/search', async (req, res) => {

	let orders = await orderFunctions.searchOrder(req.query);
	return res.json(orders);
})


router.get('/:id', async (req, res) => {
	try {
		let id = req.params.id;
		if (!id || !con.ObjectID.isValid(id)) return res.end('invalid id');

		let db = await con.db();

		let aggregationPipeLine = [
			{ $match: { _id: new con.ObjectID(id) } },
			{
				$lookup: {
					from: "product",
					localField: "products._id",
					foreignField: "_id",
					as: "product",
				}
			},
			{
				$unwind: {
					path: '$product',
					preserveNullAndEmptyArrays: true,
				}
			},
			{
				$lookup: {
					from: "images",
					localField: "product.cover",
					foreignField: "_id",
					as: "product.cover",
				}
			},
			{
				$unwind: {
					path: '$product.cover',
					preserveNullAndEmptyArrays: true,
				}
			},
			{
				$group: {
					_id: '$_id',
					product: { $push: '$product' },
					doc: { $first: '$$ROOT' }
				}
			},
			{ $replaceRoot: { newRoot: { $mergeObjects: ['$doc', '$$ROOT'] } } },
			{ $project: { doc: 0 } },
			{
				$lookup: {
					from: "customer",
					localField: "customer",
					foreignField: "_id",
					as: "customer",
				}
			},
			{
				$unwind: {
					path: '$customer',
					preserveNullAndEmptyArrays: true,
				}
			},
			{
				$lookup: {
					from: "dealer",
					localField: "customer.dealer",
					foreignField: "_id",
					as: "customer.dealer",
				}
			},
			{
				$unwind: {
					path: '$dealer',
					preserveNullAndEmptyArrays: true,
				}
			}
		]

		let order = await db.collection('order').aggregate(aggregationPipeLine).toArray();
		order = order[0];
		if (!order) return res.status(400).json({ id: 'no order with given id' });

		//combine product & products
		order.products.forEach((i, index, array) => {
			let productInfo = order.product.find(item => { if (i._id && item._id) return item._id.toHexString() == i._id.toHexString() })
			i = { ...productInfo, ...i },
				array[index] = i;
		})

		delete order.product;


		let siteInfo = await db.collection('settings').findOne({ name: 'site' }); //for invoice
		siteInfo = siteInfo ? siteInfo : {};
		siteInfo.domain = req.hostOptions.domain;

		res.json({ order })

	} catch (err) {
		console.log(err);
		res.status(500).json({ error: err.message })
	}
})

module.exports = router;
