const router = require('express').Router();
const _ = require('lodash');
const con = require('../../../lib/db.js');
const productFunctions = require('./../../../lib/dbFunctions/public/productFunctions.js')
const deliveryFunction = require('./../../../lib/dbFunctions/public/deliveryFunctions.js')
const { generateRandomString } = require("../../../lib/utility.js");
const Validator = require('../../../validator/customer.js');
const defaultOrderStatus = require("./../../../config/defaultOrderStatus.json");
const autoMail = require('../../../lib/autoMail.js');



router.post('/', async (req, res) => {
  try {

    let db = await con.db();

    let customerId = req.fields.customerId;
    if (customerId && !con.ObjectID.isValid(customerId)) return res.status(400).json({ customerId: 'Invalid Customer id' })
    else{
      let customer = await db.collection('customer').findOne({ _id: new con.ObjectID(customerId) });
      if (!customer) return res.status(400).json({ customerId: 'No customer with given id' });  
    }
  
		let { shippingAddress, coupon } = req.fields;

		let validator = new Validator();
		errors = await validator.checkoutShipping(req.fields);
		if (!_.isEmpty(errors)) return res.status(400).json({ error: 'Incomplete Shipping Address', checkoutError: errors });

		let { longitude, latitude } = shippingAddress.coords || {};
		let location = {
			type: "Point",
			coordinates: [longitude, latitude]
		}

		shippingAddress = {
			firstName: shippingAddress.firstName,
			lastName: shippingAddress.lastName,
			country: shippingAddress.country,
			city: shippingAddress.city,
			address1: shippingAddress.address1,
			address2: shippingAddress.address2,
			zipCode: shippingAddress.zipCode,
			phone: shippingAddress.phone,
			email: shippingAddress.email,
			additionalInfo: shippingAddress.additionalInfo,
			location
		};

		payment = {
			paymentMethod: "COD"
		}; //TODO

		// *product
		let items = req.fields.items;
		let productIds = [], products = [], totalPrice = 0;

		// validate and add id to list
		for (let item of items) {
			let { product, variation } = item;
			if (!con.ObjectID.isValid(product)) return res.status(400).json({ product: "invalid product id" });
			if (!con.ObjectID.isValid(variation)) return res.status(400).json({ variation: "invalid variation id" });

			productIds.push(new con.ObjectID(product));
		}

		//get products from db
		let productsInfo = await db.collection('product').find({ _id: { $in: productIds } }).toArray();

		//calculate price
		for (let product of productsInfo) {
			let orderedItem = items.find(i => product._id.toHexString() == i.product);
			let variation = product.pricing.find(i => {

				if (!i._id) return res.status(400).json({ variation: `Invalid variation for product ${product.name}` });

				return i._id.toHexString ?
					(i._id.toHexString() == orderedItem.variation)
					:
					(i._id == orderedItem.variation)

			});

			if (!variation) return res.status(400).json({ variation: `Invalid variation for product ${product.name}` });

			let unitPrice = + (variation.price.offer || variation.price.regular);
			let price = unitPrice * +orderedItem.quantity;
			totalPrice += price; 	//increase total order price

			products.push({
				_id: product._id,
				variation: variation._id,
				unitPrice,
				price,
				quantity: +orderedItem.quantity,
			})

		}

		//delivery option
		let { delivery } = req.fields;
		if (!delivery || !con.ObjectID.isValid(delivery)) return res.status(400).json({ delivery: 'Select delivery option' });

		//verify from db
		delivery = await deliveryFunction.getRegion(delivery);
		if (!delivery) return res.status(400).json({ delivery: 'Invalid delivery option' });

		let deliveryAmount = Object.keys(delivery.charge);
		deliveryAmount.sort((a, b) => a - b);

		let deliveryCharge;

		//get delivery charge according to totalPrice;

		if (totalPrice < deliveryAmount[0]) {		//lower than minimum order
			return res.status(400).json({ delivery: 'Minimum order amount is ' + deliveryAmount[0] });
		} else if (totalPrice >= deliveryAmount[deliveryAmount.length - 1]) { //higher than defined amount
			deliveryCharge = delivery.charge[deliveryAmount.length - 1];
		} else {
			for (let index in deliveryAmount) {

				if (totalPrice >= deliveryAmount[index] && totalPrice < deliveryAmount[+index + 1]) {
					deliveryCharge = delivery.charge[deliveryAmount[index]];
					break;
				}
			}
		}


		//create new account
		if (req.fields.createAccount) {
			let registerError = {};
			let { password, password2 } = req.fields;

			//validation 
			if (!password) registerError.password = 'Enter password';
			if (!password2) registerError.password2 = 'Repeat password';
			if (password !== password2) registerError.password2 = 'Passwords not matching';

			if (!_.isEmpty(registerError)) return res.status(400).json({ error: 'Could not register new customer', registerError });


			//checking if account with phone exists
			let exists = await db.collection("customer").findOne({ phone: shippingAddress.phone });
			if (exists) return res.status(400).json({ phone: "There is already an account with this phone number" });

			//checking if account with email exists
			if (shippingAddress.email && shippingAddress.email != "" && shippingAddress.email != " ") {
				exists = await db.collection("customer").findOne({ email: { $regex: new RegExp("^" + shippingAddress.email, "i") } });
				if (exists) return res.status(400).json({ email: "There is already an account with this email" });
			}


			//hash password
			let salt = await bcrypt.genSalt(10);
			let hash = await bcrypt.hash(password, salt);

			let result = await db.collection('customer').insertOne({
				...shippingAddress,
				password: hash,
				added: new Date(),
				updated: new Date()
			});

			let customer = result && result.ops && result.ops[0] && result.ops[0];
			customerId = customer && customer._id;
		}

		let statusConfig = await db.collection('settings').findOne({ name: 'orderStatus' });

		let order = {
			added: new Date(),
			products,
			shippingAddress,
			payment,
			status: statusConfig && statusConfig.defaultStatus || defaultOrderStatus[0],
			totalPrice,
			deliveryCharge,
			deliveryRegion: delivery,
			customer: customerId ? new con.ObjectID(customerId) : null,
			shortCode: generateRandomString(6, "110")
		};

		if (coupon) {
			if (!customerId) return res.status(400).json({ customer: 'Sign in to use coupon' });

			coupon = await db.collection('coupon').findOne({
				$and: [
					{ code: coupon.toUpperCase() },
					{ startDate: { $lte: new Date() } },
					{ endDate: { $gte: new Date() } }
				]
			});

			//validate coupon
			if (!coupon) return res.status(409).json({ coupon: "Invalid  coupon" });

			//usage limit total
			let couponUsage = await db.collection('couponUsage').find({ coupon: coupon._id }).toArray();
			if (coupon.maxUseTotal && couponUsage && couponUsage.length > coupon.maxUseTotal) return res.status(409).json({ coupon: "Coupon is not available anymore" });

			//usage limit by user
			let couponUsageByUser = couponUsage && couponUsage.filter(i => i.customer.toHexString() == customerId);
			if (coupon.maxUsePerUser && couponUsage && couponUsageByUser.length > coupon.maxUsePerUser) return res.status(409).json({ coupon: "Coupon max use reached" });

			//order matching
			if (coupon.minimumOrder && order.totalPrice < coupon.minimumOrder) {
				return res.status(409).json({ coupon: `minimum order for coupon ${coupon.name} is ${coupon.minimumOrder}` });
			}
			if (coupon.maximumOrder && order.totalPrice > coupon.maximumOrder) {
				return res.status(409).json({ coupon: `maximum order for coupon ${coupon.name} is ${coupon.maximumOrder}` });
			}
			if (coupon.orderedProducts) {
				for (let requiredProduct of coupon.orderedProducts) {
					let productInOrder = order.products.find(i => i._id.toHexString() == requiredProduct._id.toHexString())
					if (!productInOrder) return res.status(409).json({
						coupon: `you did not buy all required product to apply coupon ${coupon.name}`
					})
				}
			}


			let discountAmount;
			if (coupon.amount) { //price discount

				if (coupon.amountType == "fixed") {
					discountAmount = coupon.amount;
					order.discountPrice = order.totalPrice - coupon.amount
				} else if (coupon.amountType == "percentage") {
					order.discountPrice = order.totalPrice - (order.totalPrice / 100 * coupon.amount);
					discountAmount = order.totalPrice - order.discountPrice
				}

			}
			if (coupon.freeDelivery) {
				order.freeDelivery = true;
			}


			order.coupon = coupon.code;

			await db.collection('couponUsage').insertOne({
				coupon: coupon._id,
				customer: new con.ObjectID(customerId),
				discount: discountAmount,
				freeDelivery: order.freeDelivery,
				freeProducts: order.freeProducts,
				date: new Date(),
			})
		}


    let result = await db.collection('order').insertOne(order);
    order._id = result.insertedId;

    res.json({ inserted: result.ops });

    // send mail to customer and admin
    autoMail.eventMail('order', shippingAddress.email, { customer: shippingAddress, order });

  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: err.message });
  }
});



module.exports = router;
