const router = require('express').Router();
const con = require('../../../lib/db.js')


router.get('/:orderId', async (req, res)=>{
    try {

        let orderId = req.params.orderId;
        if(!con.ObjectID.isValid(orderId)) return res.status(400).json({ orderId: "invalid  order id" }); 


        let db = await con.db();
        let notes = await db.collection('orderNotes').find({order: new con.ObjectID(orderId)}).toArray();

        return res.json(notes);
    } catch (err) {
        console.log(err);
        res.status(500).json({ error: err.message });
    }
})

router.post('/add', async (req, res) => {
    try {
        let db = await con.db();
        let { summary, note, order } = req.fields;

        // * validation
        if (!con.ObjectID.isValid(order)) return res.status(400).json({ order: "invalid  order id" });
        if (!note) return res.status(400).json({ note: "insert note" });
        // TODO: cross chec order with db

        //modal note
        let noteObj = {
            summary: summary || '',
            note,
            order: new con.ObjectID(order)
        }

        let result = await db.collection('orderNotes').insertOne(noteObj);

        return res.json(result.ops);
    } catch (err) {
        console.log(err);
        res.status(500).json({ msg: err.message });
    }
});


router.post('/update/:id', async (req, res) => {
    let id = req.params.id;

    // *validation
    // validate order id
    if (!con.ObjectID.isValid(id)) return res.status(400).json({ id: 'Invalid Id' });

    let db = await con.db();
    let { summary, note, order } = req.fields;

    if (!con.ObjectID.isValid(order)) return res.status(400).json({ order: "invalid  order id" });

    // * validation
    if (!note) return res.status(400).json({ note: "insert note" });

  
    //modal note
    let noteObj = {
        summary: summary || '',
        note,
        order: new con.ObjectID(order)
    }

    let result = await db.collection('orderNotes').updateOne(
        {_id: new con.ObjectID(id)},
        {$set: noteObj}    
    )

    if(result.matchedCount == 0) return res.status(400).json({ id: "no note with given id" });

    return res.json({updated: noteObj});
})


router.post('/update/delete/:id', async (req, res) => {
    let id = req.params.id;
    if (!con.ObjectID.isValid(id)) return res.status(400).json({ id: 'Invalid Id' });

    let db = await con.db();


    //remove order from collection;
    let result = await db.collection('orderNotes').deleteOne({ _id: new con.ObjectID(id) });

    if (result.deletedCount == 0) return res.status(400).json({ id: "no note with given id" });


    return res.json({ success: 'note Deleted!' })
})
module.exports = router;