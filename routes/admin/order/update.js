const router = require('express').Router();
const Validator = require('../../../validator/customer.js');
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const productFunctions = require('./../../../lib/dbFunctions/public/productFunctions.js')
const autoMail = require('./../../../lib/autoMail.js');
const autoSMS = require('./../../../lib/autoSMS.js');
const defaultOrderStatus = require("./../../../config/defaultOrderStatus.json");


router.post('/:id', async (req, res) => {
	try {

		return res.json({ "400": "updating order is disabled" });

	} catch (err) {
		console.log(err);
		res.status(500).json({ msg: err.message });
	}
});


router.post('/:id/status', async (req, res) => {
	let id = req.params.id;
	let status = req.query.newstatus;

	// *validation
	// validate order id
	if (!con.ObjectID.isValid(id)) return res.status(400).json({ id: 'Invalid Id' });

	let db = await con.db();

	//getting order & customer details for notification + validation
	let order = await db.collection('order').aggregate([
		{ $match: { _id: new con.ObjectID(id) } },
		{
			$lookup: {
				from: 'customer',
				localField: 'customer',
				foreignField: "_id",
				as: "customer"
			}
		}
	]).toArray();
	order = order[0];
	if (!order) return res.status(400).json({ id: 'no order with given id' });


	// console.log(order);

	let orderStatusList = await db.collection('settings').find({ name: "orderStatus" });
	orderStatusList = orderStatusList && orderStatusList[0] ? orderStatusList : defaultOrderStatus;

	let statusObj = orderStatusList.find(i => i.name == status);

	//update status
	let result = await db.collection('order').updateOne(
		{ _id: new con.ObjectID(id) },
		{
			$set: { status: statusObj }
		});

	res.json({ success: 'Status Updated' });

	order.status = statusObj;

	// *send noification
	autoMail.eventMail(
		'orderStatus',
		order.customer.email || order.shippingAddress.email,
		{ user: _.isEmpty(order.customer)? order.shippingAddress : order.customer, order }
	);
	autoSMS.eventSMS(
		'orderStatus',
		order.customer.phone || order.shippingAddress.phone,
		{ user: _.isEmpty(order.customer)? order.shippingAddress : order.customer , order }
	);
})


router.get('/order/delete/:id', async (req, res) => {
	let id = req.params.id;
	if (!con.ObjectID.isValid(id)) return res.status(400).json({ id: 'Invalid Id' });

	let db = await con.db();


	//remove order from collection;
	let result = await db.collection('order').deleteOne({ _id: new con.ObjectID(id) });

	if (result.deletedCount == 0)
		return res.status(400).json({ id: "no order with given id" });


	return res.json({ msg: 'Order Deleted!' })
})
module.exports = router;