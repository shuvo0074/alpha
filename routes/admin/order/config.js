const router = require('express').Router();
const Validator = require('../../../validator/customer.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const productFunctions = require('../../../lib/dbFunctions/public/productFunctions.js')

const defaultOrderStatus = require("./../../../config/defaultOrderStatus.json");

router.post('/status', async (req, res) => {
  try {
    let { status } = req.fields;
    if (!status || !Array.isArray(status)) return res.status(400).json({ status: "Invalid formation, status should be array" })

    let defaultStatus, cancelStatus;

    for (let item of status) {
      if (!item) continue; // avoid empty strings
      if (!item.name) return res.status(400).json("enter status name");
      if (!item.bn.name) return res.status(400).json("enter bangla name");
      if (!item.stage) return res.status(400).json("enter status stage number");

      if(item.default) defaultStatus = item;
    }

    if (!defaultStatus) defaultStatus = status[0];

    let db = await con.db();

    await db.collection('settings').updateOne(
      { name: 'orderStatus' },
      { $set: { status, defaultStatus } },
      { upsert: true }
    );

    return res.json({ success: 'settings updated' });

  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: err.message });
  }
});

router.get('/status', async (req, res) => {
  try {

    let db = await con.db();

    let settings = await db.collection('settings').findOne({ name: 'orderStatus' });

    if (settings.length == 0) settings = defaultOrderStatus; //default if not set by admin

    return res.json(settings);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: err.message });
  }
})



module.exports = router;
