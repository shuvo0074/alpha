const router = require('express').Router();
const Validator =  require('../../validator/admin.js');
const _ = require('lodash')
const con = require('../../lib/db.js');
const bcrypt = require('bcrypt');

router.post('/register', async (req, res)=>{
    try{
        

        let {name, phone, email='', password, password2, access} = req.fields;
        //validation
        let validator = new Validator();
        let errors = await validator.adminRegister(req.fields);
        if(!_.isEmpty(errors)) return res.status(400).json(errors);

        let db = await con.db();

        //checking if account with phone exists
        let exists = await db.collection('admin').findOne({phone});
        if(exists) return res.status(409).json({phone: "Admin with phone number already registered"});

        // *role
        let roles = [   'getCatalogue', 'postCatalogue', 
                        'getDelivery', 'postDelivery', 
                        'getOrder', 'postOrder',
                        'getBlog', 'postBlog', 
                        'getPage', 'postPage',
                        'analytics', 'superAdmin',
                        'accounts'
                    ]

        let allowedRoles = [];
        for(let role of roles){
            if(access.includes(role)) allowedRoles.push(role);
        }

        let salt = await bcrypt.genSalt(10);
        let hash = await bcrypt.hash(password, salt);

        let admin = {
            name,
            phone,
            email,
            password: hash,
            role: allowedRoles
        }

        let result = await db.collection('admin').insertOne(admin);
        delete result.ops[0].password
        return res.json(result.ops);

    }catch(err){
        console.log(err);
        res.status(500).json({
            error: err.message
        })
    }
})


router.post('/update/:id', async (req, res)=>{
    try{
        
        let id = req.params.id;
        if(!con.ObjectID.isValid(id)) return res.status(400).json({id: "invalid id"});

        let {name, phone, email='', password, password2, access} = req.fields;
        //validation
        let validator = new Validator();
        let errors = await validator.adminRegister(req.fields);
        if(!_.isEmpty(errors)) return res.status(400).json(errors);

        let db = await con.db();

        // *role
        let roles = [   'getCatalogue', 'postCatalogue', 
                        'getDelivery', 'postDelivery', 
                        'getOrder', 'postOrder',
                        'getBlog', 'postBlog', 
                        'getPage', 'postPage',
                        'analytics', 'superAdmin',
                        'accounts'
                    ]

        let allowedRoles = [];
        for(let role of roles){
            if(access.includes(role)) allowedRoles.push(role);
        }

        let admin = {
            name,
            phone,
            email,
            role: allowedRoles
        }
        // console.log(allowedRoles);

        if(password){
            
            let salt = await bcrypt.genSalt(10);
            let hash = await bcrypt.hash(password, salt);
            admin.password = hash;
        }

        let result = await db.collection('admin').updateOne({_id: new con.ObjectID(id)}, {$set: admin});

        // delete result.ops[0].password
        delete admin.password;
        return res.json({updated: admin});

    }catch(err){
        console.log(err);
        res.status(500).json({
            error: err.message
        })
    }
})


router.get('/', async (req, res)=>{
    try{
        let db = await con.db()
       
        let adminList = await db.collection('admin').find({}).project({password:0}).toArray();
        res.json(adminList);

    }catch(err){
        console.log(err);
        res.status(500).json({
            error: err.message
        })
    }
})

router.get('/:id', async (req, res)=>{
    try{
        
        let id = req.params.id;
        if(!con.ObjectID.isValid(id)) return res.status(400).json({id: "invalid id"});

        let db = await con.db()

        let admin = await db.collection('admin').findOne({_id: new con.ObjectID(id)});
        delete admin.password
        res.json(admin);


    }catch(err){
        console.log(err);
        res.status(500).json({
            error: err.message
        })
    }
})


router.get('/delete/:id', async (req, res)=>{
    try{
        let id = req.params.id;
        let {role} = req.session.admin;
        
        if(!con.ObjectID.isValid(id)) return res.status(400).json({id: "invalid id"});

        let db = await con.db();

        if(role.includes('superAdmin')) {
            let result = await db.collection('admin').deleteOne({_id: new con.ObjectID(id)});
            if(result.deletedCount === 0) return res.status(400).json({id: "no admin with given id"});
            return res.json({success:"admin deleted"});
        } else {
            return res.status(403).json({admin: "not authorized to delete admin"});
        }

    }catch(err){
        console.log(err);
        res.status(500).json({ error: err.message });
    }
})



module.exports = router;