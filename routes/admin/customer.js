const router = require('express').Router();
const Validator =  require('../../validator/admin.js');
const _ = require('lodash')
const con = require('../../lib/db.js')

const addRouter = require('./customer/add.js');
const updateRouter = require('./customer/update.js');
const getRouter = require('./customer/get.js')

router.use('/add', addRouter);
router.use('/update', updateRouter);
router.use('/', getRouter);

module.exports = router;
