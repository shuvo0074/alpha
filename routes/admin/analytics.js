const router = require("express").Router();
const analytics = require("../../lib/dbFunctions/admin/analyticsFunctions.js");

// router.get("/visitCount", async (req, res) => {
//   let type = req.query.type;
//   if (!type) return res.status(400).json({ type: "select type" });

//   let analyticData = await analytics.pageVisits(type, req.query);
//   res.json(analyticData);
// });

// router.get("/breakdown", async (req, res) => {
//   let metric = req.query.metric || "date";

//   let analyticData = await analytics.numberOfUser(metric, req.query);
//   res.json(analyticData);
// });
// router.get("/returninguser", async (req, res) => {
//   let metric = req.query.metric || "date";

//   let analyticData = await analytics.newUser(metric, req.query);
//   res.json(analyticData);
// });

router.get("/item", async (req, res) => {
  let analyticData = await analytics.getItemAnalytics( req.query);
  res.json(analyticData);
});

router.get("/user", async (req, res) => {  
    let analyticData = await analytics.getUserAnalytics(req.query);
    res.json(analyticData);
  });

module.exports = router;
