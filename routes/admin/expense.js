const router = require('express').Router();
const Validator =  require('../../validator/admin.js');
const _ = require('lodash')
const con = require('../../lib/db.js')

const addRouter = require('./expense/add.js');
const updateRouter = require('./expense/update.js');
const getRouter = require('./expense/get.js')
// const salaryRouter = require('./expense/salary.js')

router.use('/add', addRouter);
router.use('/update', updateRouter);
// router.use('/salaryReport', salaryRouter);
router.use('/', getRouter);

module.exports = router;
