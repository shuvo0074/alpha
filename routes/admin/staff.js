const router = require('express').Router();
const Validator =  require('../../validator/admin.js');
const _ = require('lodash')
const con = require('../../lib/db.js')

const addRouter = require('./staff/add.js');
const updateRouter = require('./staff/update.js');
const getRouter = require('./staff/get.js')
const salaryRouter = require('./staff/salary.js')

router.use('/add', addRouter);
router.use('/update', updateRouter);
router.use('/salaryReport', salaryRouter);
router.use('/', getRouter);

module.exports = router;
