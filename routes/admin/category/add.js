const router = require('express').Router();
const Validator = require('../../../validator/admin.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const meter = require('./../../../lib/meterFunctions.js');
const urlGenerator = require('./../../../lib/urlGenerator.js');
const fs = require('fs');
const path = require('path');
const checkMime = require('./../../../lib/checkMime.js');

router.post('/', async (req, res) => {
  try {
    // *resource check ...
    let missingResource = await meter.usedResource(req, 'db', 'disk');
    if (missingResource) {
      return res.status(400).json({
        Resource: `Usage Limit Exceeded For ${missingResource.join(', ')}`
      });
    } 


    
    console.log({
      fields: req.fields,
      files: req.files
    })
    
    let db = await con.db();
    let { name, parent, description, image, cover, url, metaTitle, metaTags, metaDescription,  bn, displayOrder} = req.fields;

    try{
      bn = bn? JSON.parse(bn) : {}
    }catch(err){
      //
    }

    console.log(bn, 000 ,  typeof bn, bn.name, 000)
    let list = {}; //for validation

    //category list
    let categories = await db.collection('category').find({}).toArray();
    list.category = categories.map(item => {  //list of category _id for validation
      return item._id.toHexString();
    });

    //basic validation
    let validator = new Validator();
    let errors = await validator.addCategory(req.fields, list);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    //TODO: add more validation


    // *parent category ...
    if (!con.ObjectID.isValid(parent)) parent = null;
    else parent = await db.collection('category').findOne({ _id: new con.ObjectID(parent) });

    // *category type
    let type = (parent && parent._id) ? 'sub' : 'top';

    // *image ...
    // convert image ids to objectIds
    let validImages = [];
    if (image) {
      for (let x in image) {
        if (!con.ObjectID.isValid(image[x])) {
          continue
        };
        validImages.push(new con.ObjectID(image[x]));
      }
    }
    //cover image
    cover = (cover && con.ObjectID.isValid(cover)) ? new con.ObjectID(cover) : (validImages && validImages[0]) ? validImages[0] : null;
    //TODO: validate images and cover from DB

    // *icon
    let { icon } = req.files;
    let iconPublicPath;

    if (icon && icon.size > 0) {
      //*checkMime 
      let mimeErrors = await checkMime.image([icon]);
      if (mimeErrors.length > 0) {
        return res.status(400).json(mimeErrors);
      }

      let fileName = `category_icon_${icon.name}`;
      let filePath = path.join(req.publicDir, 'images', 'icons');

      let iconData = await fs.promises.readFile(icon.path);
      let iconFullPath = path.join(filePath, fileName);

      //create directory if not exists
      if (!fs.existsSync(filePath)) await fs.promises.mkdir(filePath, { recursive: true });
      //write to disk
      await fs.writeFile(iconFullPath, iconData);


      //public path
      iconPublicPath = path.join('/images', 'icons', fileName);
    }


    // *url
    url = await urlGenerator.categoryUrl(url || name, parent && parent.name);

    // *model category
    let category = {
      name,
      type,
      parent: parent && parent._id,
      description,
      image: validImages,
      cover,
      icon: iconPublicPath,
      url,

      metaTitle,
      metaTags,
      metaDescription,

      bn: {
        name: bn.name,
        description: bn.description,
        
        metaTitle: bn.metaTitle,
        metaTags: bn.metaTags,
        metaDescription: bn.metaDescription
      },
      
      added: new Date(),
      updated: new Date(),
      displayOrder: +displayOrder
    }
    // *saving to db ...
    let result = await db.collection('category').insertOne(category);


    //resolve cover for response
    let coverDetail = cover && await db.collection('images').findOne({_id: cover});
    if(result.ops && result.ops[0]) result.ops[0].cover = coverDetail;

    return res.json({
      inserted: result.ops,
    });

  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: err.message });
  }
});

// router.post("/:id/icon", async(req, res) => {
//   try {
//     // destruct category id and icon file
//     let {icon} = req.files;
//     let id = req.params.id;

//     // check if there's any icon
//     if(!icon.size) return res.status(400).json({icon: "No icon selected"});

//     // check id validity
//     if(!con.ObjectID.isValid(id)) return res.status(400).json({id: "Invalid category id"});

//     // check mimetypes
//     if(icon.type !== "image/png" &&
//       icon.type !== "image/svg+xml" &&
//       icon.type !== "image/jpg" && 
//       icon.type !== "image/jpeg"
//     ) return res.status(400).json({icon: "Invalid icon type"});

//     // set fileName and path
//     // let pathPrefix = req.publicDir;
//     let fileName = `${uuidv4()}__${icon.name}`;
//     let iconPath = path.join(req.publicDir, '/icons');

//     // check for existing icon
//     const db = await con.db();
//     let category = await db.collection("category").findOne({_id: new con.ObjectID(id)});
//     if(fs.existsSync(category.icon)) {
//       fs.unlinkSync(category.icon)
//     }

//     // save to filesystem
//     let filePath = path.join(iconPath, "/", fileName);
//     if (!fs.existsSync(iconPath)) fs.promises.mkdir(iconPath, { recursive: true });
//     // read file into buffer
//     let data = fs.readFileSync(icon.path);
//     // write buffer to disk
//     fs.writeFileSync(filePath, data);

//     // save filepath to db
//    console.log(filePath)
//     await db.collection("category").updateOne(
//       {_id: new con.ObjectID(id)},
//       {$set: {icon: filePath}}
//     )

//     return res.json({icon: "Icon saved into disk successfully"});
//   } catch (err) {
//     console.log(err);
//     res.status(500).json({error: "Something went wrong"});
//   }
// })

module.exports = router;
