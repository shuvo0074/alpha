const router = require('express').Router();
const Validator =  require('../../validator/admin.js');
const _ = require('lodash')
const con = require('../../lib/db.js')

const addRouter = require('./order/add.js');
const updateRouter = require('./order/update.js');
const getRouter = require('./order/get.js')
const configRouter = require('./order/config.js')
const notesRouter = require('./order/notes.js')

router.use('/add', addRouter);
router.use('/update', updateRouter);
router.use('/', getRouter)
router.use('/config', configRouter);
router.use('/notes', notesRouter);
module.exports = router;
