const router = require('express').Router();
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
const productFunctions = require('./../../../lib/dbFunctions/public/productFunctions.js');

router.get('/csv', async (req, res) => {
    let db = await con.db();
    let products = await db.collection('product').find({}).toArray();
    
    let minifiedProductList = [];

    for (product of products) {
        for (variation of product.pricing) {
            minifiedProductList.push({
                _id: product._id.toHexString(),
                name: product.name.replace(/,/g, ' '),
                variation: variation._id,
                variationAttributes: variation.attribute && Object.values(variation.attribute).join(' ').replace(/,/g, ' '),
                regularPrice: variation.price.regular,
                offerPrice: variation.price.offer,
                inStock: product.inStock
            })
        }
    }


    let csvStr = "";
    csvStr += Object.keys(minifiedProductList[0]).join(',');
    csvStr += '\n';

    minifiedProductList.forEach(i => {
        csvStr += Object.values(i)
        csvStr += '\n'
    });

    res.writeHead(200, {
        'Content-Type': 'application/force-download',
        'Content-disposition': 'attachment; filename=product.csv'
    });

    res.end(csvStr);
})

module.exports = router;
