const router = require("express").Router();
const Validator = require("../../../validator/admin.js");
const _ = require("lodash");
const con = require("../../../lib/db.js");
const fs = require("fs");
const util = require("util");
const path = require("path");
const urlGenerator = require("./../../../lib/urlGenerator.js");
const csvToJSON = require('csvtojson');

/*
  @route - /admin/product/update/stock/:id
  @desc - manages stock 
  @method POST
*/
router.post("/stock/:productId", async (req, res) => {
	try {
		let productId = req.params.productId;

		let { inStock } = req.fields;

		inStock = inStock ? true : false;

		if (!con.ObjectID.isValid(productId)) return res.status(400).json({ id: "Invalid id" });

		let db = await con.db();


		let result = await db.collection('product').updateOne(
			{ _id: new con.ObjectID(productId) },
			{
				$set: {
					inStock
				}
			}
		)

		if (result.matchedCount == 0) return res.status(400).json({ id: "no product with given id" });

		return res.json({ succes: "stock updated" });

	} catch (err) {
		console.log(err);
		res.status(500).json({ error: "Something went wrong!" });
	}
});

// /*
// 	@route - /admin/product/update/stock/available
// 	@desc - show if inventory stocked out
// 	@method - POST
// */
// router.post("/stock/:productId/available", async (req, res) => {
// 	try {
// 		let { label, available } = req.fields;
// 		let productId = req.params.productId;

// 		if (!con.ObjectID.isValid(productId))
// 			return res.status(400).json({ id: "Invalid id" });
// 		productId = new con.ObjectID(productId);

// 		let db = await con.db();
// 		let updatedStock = await db.collection("product").updateOne(
// 			{ _id: productId },
// 			{ $set: { [`pricing.${label}.stock.stockout`]: available } }
// 		);

// 		if (!updatedStock.matchedCount > 0) return res.status(404).json({ product: "Product not found" });

// 		return res.json({ success: "Stock availability updated" });
// 	} catch (err) {
// 		console.log(err);
// 		res.status(500).json({ error: "Something went wrong!" });
// 	}
// });

router.post('/pricing/:id', async (req, res) => {
	try {
		let id = req.params.id;
		if (!con.ObjectID.isValid(id)) return res.status(400).json({ id: "invalid id" });

		let db = await con.db();

		let { pricing } = req.fields;

		//default variation
		let defaultVariation = 0; //first variation default if none selected 
		pricing.forEach((i, index) => {
			if (i.default) defaultVariation = index;
			if (!con.ObjectID.isValid(i._id)) return res.status(400).json({ pricing: "invalid variation id" });
			i._id = new con.ObjectID(i._id); //assign id to each variation
		})

		//default price
		let price = {
			regular: pricing[defaultVariation].price.regular,
			offer: pricing[defaultVariation].price.offer,
			defaultVariation: pricing[defaultVariation]._id
		};

		//default attribute
		let attribute = pricing[defaultVariation].attribute;

		let partialUpdate = {
			pricing,
			price,
			attribute
		}

		let result = await db.collection('product').updateOne(
			{ _id: new con.ObjectID(id) },
			{ $set: partialUpdate }
		);

		if (result.matchedCount == 0) return res.status(400).json({ id: "no product with given id" });

		return res.json({ updated: partialUpdate });

	} catch (err) {
		console.log(err);
		res.status(500).json({ error: err.message });
	}
})

router.post("/:id", async (req, res) => {
	try {
		// *id validation
		let id = req.params.id;
		if (!id || !con.ObjectID.isValid(id))
			return res.status(400).json({ id: "Invalid id" });

		let {
			name, model, unit,
			description, shortDescription,
			url,
			brand, category, primaryCategory, tags, pricing, bn = {}, displayOrder,
			metaDescription, metaTitle, metaTags
		} = req.fields;

		let db = await con.db();

		// *validation
		let list = {};

		//category list
		let categories = await db.collection("category").find({}).sort({ name: 1 }).toArray();
		list.category = categories.map((item) => {
			return item._id.toHexString();
		});

		//brand list
		let brandList = await db.collection("brand").find({}).toArray();
		list.brand = brandList.map((item) => {
			return item._id.toHexString();
		});

		//tags list
		let tagsList = await db.collection("tag").find({}).toArray();
		list.tags = tagsList.map((item) => {
			return item._id.toHexString();
		});

		//attributes list
		let attrList = await db.collection("attributes").find({}).toArray();
		list.attr = attrList.map((item) => {
			return item._id.toHexString();
		});

		//basic validation
		let validator = new Validator();
		let errors = await validator.addProduct(req.fields, list);
		if (!_.isEmpty(errors)) return res.status(400).json(errors);

		// *category
		for (let x in category) {
			if (con.ObjectID.isValid(category[x]))
				category[x] = new con.ObjectID(category[x]);
		}

		// *primary category
		primaryCategory = primaryCategory || category[0];
		primaryCategory = categories.find(
			(item) => item._id.toHexString() == primaryCategory
		);

		// *tags
		for (let x in tags) {
			if (con.ObjectID.isValid(tags[x])) tags[x] = new con.ObjectID(tags[x]);
		}

		// *brand
		brand = brand ? new con.ObjectID(brand) : null;


		// *pricing

		//default variation
		let defaultVariation = 0; //first variation default if none selected 

		if (!pricing || !pricing[0]) return res.status(400).json({ pricing: "pricing can not be empty" });

		pricing.forEach((i, index) => {
			if (i.default) defaultVariation = index;
			i._id = new con.ObjectID(); //assign id to each variation
			i.maximumPurchaseLimit = isFinite(+i.maximumPurchaseLimit) ? +i.maximumPurchaseLimit : null;
		})

		//default price
		let price = {
			regular: +pricing[defaultVariation].price.regular,
			offer: +pricing[defaultVariation].price.offer,
			defaultVariation: pricing[defaultVariation]._id
		};

		//default attribute
		let attribute = pricing[defaultVariation].attribute;

		// *url
		url = await urlGenerator.productUrl(url || name, primaryCategory.name, id);

		// *model product
		let product = {
			name, model, unit,
			description, shortDescription,
			url,
			brand, category, primaryCategory: primaryCategory._id, tags,
			price, attribute, pricing, type: 'product',
			metaDescription, metaTitle, metaTags,
			bn: {
				name: bn.name,
				unit: bn.unit,
				description: bn.description,
				shortDescription: bn.shortDescription,
				metaTitle: bn.metaTitle,
				metaTags: bn.metaTags,
				metaDescription: bn.metaDescription,
			},
			updated: new Date(),
			displayOrder: +displayOrder

		};

		//save to db
		let result = await db.collection("product").updateOne({ _id: new con.ObjectID(id) }, { $set: product });

		//existence check
		if (result.matchedCount == 0) {
			return res.status(400).json({ id: "No product with given id" });
		}

		return res.json({ updated: product });
	} catch (err) {
		console.log(err);
		res.status(500).json({ error: err.message });
	}
});

router.post("/delete/:id", async (req, res) => {
	try {
		let id = req.params.id; //product id
		if (!id) return res.status(400).json({ msg: "Invalid Product Id" });

		const db = await con.db();

		//delete the product
		let result = await db.collection("product").deleteOne({ _id: new con.ObjectID(id) });

		//existence check
		if (result.deletedCount < 1)
			return res.status(400).json({ id: "no product with given id" });

		return res.json({ success: "Product Deleted" });
	} catch (err) {
		console.log(err);
		return res.status(500).json({ error: err.message });
	}
});


router.post('/bulk/csv', async (req, res) => {

	try {
		let csv = req.files.csv;
		let startTime = new Date();


		if (!csv) return res.status(400).json({ csv: 'Upload CSV' });

		let products;
		try {
			products = await csvToJSON().fromFile(csv.path);
		} catch (err) {
			res.status(500).json({ error: err.message });
		}

		let db = await con.db();


		for (let product of products) {
			let index = products.indexOf(product);

			// validate

			if (!con.ObjectID.isValid(product._id)) return errorResponse("_id", 'invalid product id', index);
			if (!product.name) return errorResponse("name", "name can not be empty", index);
			if (!con.ObjectID.isValid(product.variation)) return errorResponse("variation", "Invalid variation id", index);
			if (!product.regularPrice || !isFinite(+product.regularPrice)) return errorResponse("regularPrice", 'price not valid', index)
			if (product.offerPrice && !isFinite(+product.offerPrice)) return errorResponse("offerPrice", 'price not valid', index)
			if (!(Object.keys(product).includes('inStock'))) return errorResponse('inStock', 'missing stock option', index)

			// get from db
			let productInDB = await db.collection('product').findOne({ _id: new con.ObjectID(product._id) });
			if (!productInDB) return errorResponse('_id', 'no product with given id', index);

			// update name
			productInDB.name = product.name;
			productInDB.inStock = product.inStock == 'false' ? false : true;

			// get variation
			let variation = productInDB.pricing.find(i => i._id.toHexString() == product.variation);
			if (!variation) return errorResponse('variation', 'wrong variation id', index);

			variation.price.regular = +product.regularPrice;
			variation.price.offer = +product.offerPrice || null;

			if(productInDB.price.defaultVariation == product.variation){
				productInDB.price.regular = +product.regularPrice;
				productInDB.price.offer = +product.offerPrice;
			}

			db.collection("product").updateOne({ _id: productInDB._id }, { $set: productInDB });

		}

		let operationTime = new Date() - startTime;
		return res.json({ success: "products updated" });


		function errorResponse(field, msg, index) {
			return res.status(400).json({ [field]: index + ": " + msg });
		}
	} catch (err) {
		console.log(err);
		res.status(500).json({error: err.message});
	}

})

module.exports = router;
