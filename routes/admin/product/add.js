const router = require('express').Router();
const Validator = require('../../../validator/admin.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const fs = require('fs');
const util = require('util');
const path = require('path');
const meter = require('./../../../lib/meterFunctions.js');
const urlGenerator = require('./../../../lib/urlGenerator.js');

router.post('/', async (req, res) => {
  try {

    // *resource check
    let missingResource = await meter.usedResource(req, 'db', 'product');
    if (missingResource) {
      return res.status(400).json({
        Resource: `Usage Limit Exceeded For ${missingResource.join(', ')}`
      });
    } //TODO: convert to middleware

    let { name, model, unit, 
      description, shortDescription,
      url,
      image, cover, 
      brand, category, primaryCategory, tags, 
      pricing,
      metaTitle,
      metaTags,
      metaDescription,  bn={}, displayOrder
    } = req.fields;


    let db = await con.db();

    // *validation 
    let list = {};

    //category list
    let categories = await db.collection('category').find({}).sort({ name: 1 }).toArray();
    list.category = categories.map(item => {
      return item._id.toHexString();
    });

    //brand list
    let brandList = await db.collection('brand').find({}).toArray();
    list.brand = brandList.map(item => {
      return item._id.toHexString();
    });

    //tags list
    let tagsList = await db.collection('tag').find({}).toArray();
    list.tags = tagsList.map(item => {
      return item._id.toHexString();
    });

    //attributes list
    let attrList = await db.collection('attributes').find({}).toArray();
    list.attr = attrList.map(item => {
      return item._id.toHexString();
    });

    //basic validation
    let validator = new Validator();
    let errors = await validator.addProduct(req.fields, list);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    // *category
    for (let x in category) {
      if(con.ObjectID.isValid(category[x])) 
        category[x] = new con.ObjectID(category[x]);
    }

    //primary category
    primaryCategory = primaryCategory || category[0];
    primaryCategory = categories.find(
      item => item._id.toHexString() == primaryCategory
    );
    
    // *tags
    for (let x in tags) {
      if(con.ObjectID.isValid(tags[x])) 
        tags[x] = new con.ObjectID(tags[x]);
    }

    // *brand
    brand = brand? new con.ObjectID(brand) : null;

    // *pricing

    //default variation
    let defaultVariation = 0; //first variation default if none selected 
    
    if(!pricing || !pricing[0]) return res.status(400).json({pricing: "pricing can not be empty"});

    pricing.forEach((i, index)=> {
      if(i.default)defaultVariation = index;
      i._id = new con.ObjectID(); //assign id to each variation
      i.maximumPurchaseLimit = isFinite(+i.maximumPurchaseLimit)? +i.maximumPurchaseLimit : null;
    })
    
    //default price
    let price = {
      regular: +pricing[defaultVariation].price.regular,
      offer: +pricing[defaultVariation].price.offer,
      defaultVariation: pricing[defaultVariation]._id
    };

    //default attribute
    let attribute = pricing[defaultVariation].attribute;

    // *url
    url = await urlGenerator.productUrl(url || name, primaryCategory.name);
   
    // *image ...
    // convert image ids to objectIds
    let validImages = [];
    if (image){
      for (let x in image) {
        if (!con.ObjectID.isValid(image[x])) {
          continue
        };
        validImages.push(new con.ObjectID(image[x]));
      }
    }
    //cover image
    cover = (cover && con.ObjectID.isValid(cover) )? new con.ObjectID(cover) : (validImages && validImages[0])? validImages[0]:null;
    //TODO: validate images and cover from DB

    let product = { 
      name, model, unit, 
      description, shortDescription,
      url,
      brand, category, primaryCategory: primaryCategory._id, tags, 
      price, attribute,  
      pricing,
      image:validImages, 
      cover,
      type:'product',
      metaTitle,
      metaTags,
      metaDescription,
      bn:{
        name: bn.name,
        unit: bn.unit,
        description:bn.description,
        shortDescription: bn.shortDescription,
        metaTitle: bn.metaTitle,
        metaTags: bn.metaTags,
        metaDescription: bn.metaDescription,
      },
      added: new Date(),
      updated: new Date(),
      displayOrder: +displayOrder,
      inStock: true
    };
    

    //save to db
    let result = await db.collection('product').insertOne(product);

    //resolve cover for response
    let coverDetail = cover && await db.collection('images').findOne({_id: cover});
    if(result.ops && result.ops[0]) result.ops[0].cover = coverDetail;
    
    console.log(result.ops)

    return res.json({ inserted: result.ops});

  } catch (err) {
    console.log(err);
    res.status(500).json({ error: err.message });
  }
});

module.exports = router;