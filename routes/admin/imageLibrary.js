const router = require('express').Router();
const Validator =  require('../../validator/admin.js');
const _ = require('lodash')
const con = require('../../lib/db.js')

const addRouter = require('./imageLibrary/add.js');
const updateRouter = require('./imageLibrary/update.js');
const getRouter = require('./imageLibrary/get.js');

const manageRouter = require('./imageLibrary/manage.js'); //manage all item images

router.use('/add', addRouter);
router.use('/update', updateRouter);
router.use('/', getRouter)
router.use('/', manageRouter);
module.exports = router; 
