const router = require('express').Router();
const Validator =  require('../../validator/admin.js');
const _ = require('lodash')
const con = require('../../lib/db.js')

const addRouter = require('./dealer/add.js');
const updateRouter = require('./dealer/update.js');
const getRouter = require('./dealer/get.js')
const areaRouter = require("./dealer/area.js");

router.use('/area', areaRouter)
router.use('/add', addRouter);
router.use('/update', updateRouter);
router.use('/', getRouter);

module.exports = router;
