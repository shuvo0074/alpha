const router = require('express').Router();
const generateRouter = require('./sitemap/generate.js');

const generate = require('./../../lib/sitemap/generate.js');

router.get('/generate', async (req, res)=>{
    await generate.productSitemap(req);
    res.end('done');
});


module.exports = router;
