const router = require('express').Router();
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');


router.get('/', async(req, res)=>{
    let db = await con.db();
    let deliveryRegions = await db.collection('deliveryRegion').find({}).toArray(); //get all categories

    return res.json(deliveryRegions);
})


router.get('/:id', async(req, res)=>{

    // *id validation
    let id = req.params.id;
    if(!con.ObjectID.isValid(id)) return res.status(400).json({id: "Invalid id"});

    let db = await con.db();
    let deliveryRegion = await db.collection('deliveryRegion').findOne({_id: new con.ObjectID(id)}); //get all categories

    if(!deliveryRegion) return res.status(400).json({id: "no region with given id"});

    return res.json(deliveryRegion);
})


module.exports = router;
