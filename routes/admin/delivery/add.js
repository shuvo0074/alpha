const router = require('express').Router();
const Validator = require('../../../validator/admin.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const fs = require('fs');
const util = require('util');
const meter = require('./../../../lib/meterFunctions.js');
const geo = require('../../../lib/dbFunctions/public/geoFunctions.js')
router.post('/', async (req, res) => {
  try {
    let missingResource = await meter.usedResource(req, 'db');
    if (missingResource) {
      return res.status(400).json({
        Resource: `Usage Limit Exceeded For ${missingResource.join(', ')}`
      });
    }

    let db = await con.db();
    let { name, pickUpLocation, country, city, time, charge} = req.fields;

    //basic validation
    let validator = new Validator();
    let errors = await validator.addDeliveryRegion(req.fields);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    //validate country
    country = await geo.getCountry(country);
    if(!country) return res.status(400).json({country:'Invalid Country'});

    //check for existing settings
    let existingSetting = await db.collection('deliveryRegion').findOne({name: name});
    if(existingSetting) return res.status(400).json({name:'Delivery Specification already with name'+ name});

    //TODO: add validation for country and city existence

    for(let amount in charge){
        if(!isFinite(+amount) || !isFinite(+(charge[amount]))) 
          return res.status(400).json({charge:'invalid charge'}); 
    }

    let deliveryScheme ={
      name,
      pickUpLocation,
      country:country._id,
      countryCode:country.code,
      countryName : country.name,
      city,
      charge,
      time,
      added: new Date(),
      updated: new Date()
    }


    //saving to db
    let result = await db.collection('deliveryRegion').insertOne(deliveryScheme);

    return res.json({ inserted: result.ops });
    
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: err.message });
  }
});

module.exports = router;
