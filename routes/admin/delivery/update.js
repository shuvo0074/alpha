const router = require('express').Router();
const Validator =  require('../../../validator/admin.js');
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
const meter = require('./../../../lib/meterFunctions.js');
const geo = require('../../../lib/dbFunctions/public/geoFunctions.js')



router.post('/:id', async(req, res)=>{
    try {

      // id validation
      let regionId = req.params.id;
      if(!con.ObjectID.isValid(regionId)) return res.status(400).json({regionId:'Invalid Region Id'});

      let missingResource = await meter.usedResource(req, 'db');
      if (missingResource) {
        return res.status(400).json({
          Resource: `Usage Limit Exceeded For ${missingResource.join(', ')}`
        });
      }

    let db = await con.db();
    let { name, pickUpLocation, country, time, charge, city } = req.fields;

    //basic validation
    let validator = new Validator();
    let errors = await validator.addDeliveryRegion(req.fields);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    //validate country
    country = await geo.getCountry(country);
    if(!country) return res.status(400).json({country:'Invalid Country'});

    for(let amount in charge){
        console.log(+amount, charge[amount])
        if(!isFinite(+amount) || !isFinite(+(charge[amount])))
        return res.status(400).json({charge:'invalid charge'}); 
    }

    let deliveryScheme ={
      name,
      pickUpLocation,
      country:country._id,
      countryCode:country.code,
      countryName : country.name,
      city,
      charge,
      time
    }

    //saving to db
    let result = await db.collection('deliveryRegion').updateOne({_id: new con.ObjectID(regionId)},{$set:deliveryScheme});
    
    if(result.matchedCount == 0 ) return res.status(400).json({id: "no delivery region with given id"})

    return res.json({ updated: deliveryScheme });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: err.message });
  }
})

router.post('/delete/:id', async(req, res)=>{
  let id = req.params.id;
  if(!id || !con.ObjectID.isValid(id)) return res.status(400).json({id:'Invalid Id'});

  let db= await con.db();

  //remove attribute from collection;
  let result = await db.collection('deliveryRegion').deleteOne({_id: new con.ObjectID(id)});
  
  res.json({success:'Region Deleted!'})
})



// router.get('/region/delete/:id', async(req, res)=>{
//     let id = req.params.id;
//     if(!id || !con.ObjectID.isValid(id)) return res.status(400).json({id:'Invalid Id'});

//     let db= await con.db();

//     //remove attribute from collection;
//     let result = await db.collection('deliveryRegion').deleteOne({_id: new con.ObjectID(id)});
    
//     res.json({success:'Region Deleted!'})
// })

module.exports = router;