const router = require('express').Router();
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const path = require('path');
const fileType = require("file-type");
const sharp = require("sharp");
const meter = require('./../../../lib/meterFunctions.js');
const urlGenerator = require('./../../../lib/urlGenerator.js');
const checkMime = require('./../../../lib/checkMime.js');

router.post('/', async (req, res) => {
	try {

		// *resource check ...
		let missingResource = await meter.usedResource(req, 'db', 'disk');
		if (missingResource) 
			return res.status(400).json({Resource: `Usage Limit Exceeded For ${missingResource.join(', ')}`});

		let db = await con.db()

		// *checking for images
		let images = req.files.images;
		console.log(images);
		images = (Array.isArray(images)) ? images : [images];
		if (!images[0] || images[0].size == 0) return res.status(400).json({ images: 'No image selected', files: req.files });

		//* check mime type
		let mimeErrors = await checkMime.image(images); 
		if (mimeErrors.length > 0) {
			return res.status(400).json(mimeErrors);
		}

		//user defined image size settings
		let {
			//default sizes
			full = { width: 1024, fit: "fill" },
			medium = { width: 500, fit: "fill" },
			thumbnail = { width: 150, fit: "fill" },
			icon = { width: 50, fit: "fill" }
		} = await db.collection('settings').findOne({ name: 'imageSize' }) || {};

		// *save images to disk...

		let pathPrefix = req.publicDir;
		let imagePath = path.join('/images', 'library');
		console.log("pathPrefix",pathPrefix);
		console.log("image path", imagePath);

		//ensure all folder exists
		let directory_original = path.join(pathPrefix, imagePath, 'original');
		let directory_full = path.join(pathPrefix, imagePath, 'full');
		let directory_medium = path.join(pathPrefix, imagePath, 'medium');
		let directory_icon = path.join(pathPrefix, imagePath, 'icon');
		let directory_thumbnail = path.join(pathPrefix, imagePath, 'thumbnail');
		if (!fs.existsSync(directory_original)) await fs.promises.mkdir(directory_original, { recursive: true });
		if (!fs.existsSync(directory_full)) await fs.promises.mkdir(directory_full, { recursive: true });
		if (!fs.existsSync(directory_medium)) await fs.promises.mkdir(directory_medium, { recursive: true });
		if (!fs.existsSync(directory_icon))  await fs.promises.mkdir(directory_icon, { recursive: true });
		if (!fs.existsSync(directory_thumbnail)) await fs.promises.mkdir(directory_thumbnail, { recursive: true });


		let insertList = [];

		//resize and save images
		let writeToDisk = images.map(image => {

			let fileName = (Math.random() * 1000000).toFixed(0) + '-' + image.name;
			fileName = urlGenerator.removeNonUrlCharacters(fileName);

			let originalPath = path.join(imagePath, 'original', fileName)

			let fileNameJpg = fileName.split('.');
			fileNameJpg[fileNameJpg.length - 1] = 'jpg';
			fileNameJpg = fileNameJpg.join('.');

			let fullPath = path.join(imagePath, 'full', fileNameJpg)
			let mediumPath = path.join(imagePath, 'medium', fileNameJpg)
			let thumbnailPath = path.join(imagePath, 'thumbnail', fileNameJpg)
			let iconPath = path.join(imagePath, 'icon', fileNameJpg)

			//push to db insert list
			let imageObj = {
				name: image.name,
				original: originalPath,
				full: fullPath,
				medium: mediumPath,
				thumbnail: thumbnailPath,
				icon: iconPath,
				title: '',
				alt: '',
				labels: [],
				caption: '',
				added: new Date()
			}
			insertList.push(imageObj);

			let data = fs.readFileSync(image.path);

			//original size
			fs.writeFileSync(path.join(pathPrefix, originalPath), data); //?pay special attention here while rewriting paths

			return sharp(image.path) //return promise for Promise.all
				.toFormat('jpeg')
				.jpeg({
					quality: 70,
					chromaSubsampling: '4:4:4'
				})
				.resize({
					width: full.width,
					height: full.height,
					// fit: full.fit,
				})
				.toFile(path.join(pathPrefix, fullPath), (err) => { if (err) console.log(err) })
				.resize({
					width: medium.width,
					height: medium.height,
					// fit: medium.fit,
				})
				.toFile(path.join(pathPrefix, mediumPath), (err) => { if (err) console.log(err) })
				.resize({
					width: thumbnail.width,
					height: thumbnail.height,
					// fit: thumbnail.fit,
				})
				.toFile(path.join(pathPrefix, thumbnailPath), (err) => { if (err) console.log(err) })
				.resize({
					width: icon.width,
					height: icon.height,
					// fit: icon.fit,
				})
				.toFile(path.join(pathPrefix, iconPath), (err) => { if (err) console.log(err) });

		});

		Promise.all(writeToDisk).then()

		let insertResult = await db.collection('images').insertMany(insertList);
		return res.json({inserted: insertResult.ops });

	} catch (err) {
		console.log(err);
		res.status(500).json({ error: err.message })
	}
})

module.exports = router;
