const router = require('express').Router();
const _ = require('lodash')
const con = require('../../../lib/db.js');

//collections that can have image attached 
let validCollection = ['category', 'product', 'brand', 'page', 'tag', "post", "postCategory"];

router.post('/attach/:collection/:itemId', async (req, res) => { // attach multiple images route
	try {

		
		// *validation
		//check if collection is valid
		let collection = req.params.collection;
		if (!validCollection.includes(collection))
			return res.status(400).json({ collection: `${collection} can not have images` });

		//validate itemId
		let itemId = req.params.itemId;
		if (!itemId || !con.ObjectID.isValid(itemId))
			return res.status(400).json({ itemId: `Invalid ${collection} Id` });

		let { image } = req.fields;
		if (!image) return res.status(400).json({ 'image': 'no image selected to add' });

		//convert to objectIds && objectID validation
		for (let x in image) {
			if (!con.ObjectID.isValid(image[x]))
				return res.status(400).json({ 'image': `Invalid image id ${image[x]}` });

			image[x] = new con.ObjectID(image[x]);
		}

		//save to db
		let db = await con.db();
		let result = await db.collection(collection).updateOne(
			{ _id: new con.ObjectID(itemId) },
			{ $push: { image: { $each: image } } }
		)


		//itemID existence check
		if(result.matchedCount == 0)  
			return res.status(400).json({itemId: `no ${collection} with given id`});

		return res.json({ success: `${image.length} images attached to ${collection}` });

	} catch (err) {
		console.log(err);
		res.status(500).json({ error: err.message });
	}
});

router.post('/attach/:imageId/:collection/:itemId', async (req, res) => { // attach single images route
	try {

	
		//check if collection is valid
		let collection = req.params.collection;
		if (!validCollection.includes(collection))
			return res.status(400).json({ collection: `${collection} can not have images` });

		let imageId = req.params.imageId;
		let itemId = req.params.itemId;

		//check if given ids are valid
		if (!itemId || !con.ObjectID.isValid(itemId))
			return res.status(400).json({ itemId: `Invalid ${collection} id!` }); // invalid item id

		if (!imageId || !con.ObjectID.isValid(imageId))
			return res.status(400).json({ imageId: `Invalid Image Id!` });

		//convert to objectIds && objectID validation
		imageId = new con.ObjectID(imageId);

		//save to db
		let db = await con.db();
		let result = await db.collection(collection).updateOne(
			{ _id: new con.ObjectID(itemId) },
			{ $push: { image: imageId } }
		)
		
		//itemID existence check
		if(result.matchedCount == 0)  
			return res.status(400).json({itemId: `no ${collection} with given id`});


		return res.json({ success: `Image attached to ${collection}` });

	} catch (err) {
		console.log(err);
		res.status(500).json({ error: err.message });
	}
});

router.post('/detach/:imageId/:collection/:itemId', async (req, res) => {
	try {

		//check if collection is valid
		let collection = req.params.collection;
		if (!validCollection.includes(collection))
			return res.status(400).json({ collection: `${collection} can not have images` });

		let imageId = req.params.imageId;
		let itemId = req.params.itemId;

		//check if given ids are valid
		if (!itemId || !con.ObjectID.isValid(itemId))
			return res.status(400).json({ itemId: `Invalid ${collection} id!` }); // invalid item id

		if (!imageId || !con.ObjectID.isValid(imageId))
			return res.status(400).json({ imageId: `Invalid Image Id!` });


		// database remove image from item image list 
		const db = await con.db()
		let result = await db.collection(collection).updateOne(
			{ _id: new con.ObjectID(itemId) },
			{ $pull: { image: new con.ObjectID(imageId) } }
		)


		//itemID existence check
		if(result.matchedCount == 0)  
			return res.status(400).json({itemId: `no ${collection} with given id`});

		if(result.modifiedCount == 0) // image not in list 
			return res.status(400).json({image: `selected image are not attached to ${collection} with given id`});
		
		return res.json({ success: `Image detached from ${collection}` })

	} catch (err) {
		console.log(err);
		res.status(500).json({ error: err.message });
	}
});


router.post('/detach/:collection/:itemId', async (req, res) => { //detach multiple image  
	try {

		//check if collection is valid
		let collection = req.params.collection;
		if (!validCollection.includes(collection))
			return res.status(400).json({ collection: `${collection} can not have images` });

		let itemId = req.params.itemId;

		//validate itemId
		if (!itemId || !con.ObjectID.isValid(itemId))
			return res.status(400).json({ itemId: `Invalid ${collection} id!` }); // invalid item id

		let { image } = req.fields;
		if (!image) return res.status(400).json({ 'image': 'no image selected to remove' });

		//convert to objectIds && objectID validation
		for (let x in image) {
			if (!con.ObjectID.isValid(image[x]))
				return res.status(400).json({ 'image': `Invalid image id ${image[x]}` });

			image[x] = new con.ObjectID(image[x]);
		}

		// database remove image from item image list 
		const db = await con.db()
		let result = await db.collection(collection).updateOne(
			{ _id: new con.ObjectID(itemId) },
			{ $pull: { image: { $in: image } } }

		)

		//itemID existence check
		if(result.matchedCount == 0)  
			return res.status(400).json({itemId: `no ${collection} with given id`});

		if(result.modifiedCount == 0) // image not in list 
			return res.status(400).json({image: `selected images are not attached to ${collection} with given id`});

		return res.json({ success: `${image.length} images detached from ${collection}` })

	} catch (err) {
		console.log(err);
		res.status(500).json({ error: err.message });
	}
});

router.post('/cover/:imageId/:collection/:itemId', async (req, res) => {
	try {

		let collection = req.params.collection;

		if (!validCollection.includes(collection))
			return res.status(400).json({ collection: `${collection} can not have images` });

		let imageId = req.params.imageId;
		let itemId = req.params.itemId;

		if (!itemId || !con.ObjectID.isValid(itemId))
			return res.status(400).json({ itemId: `Invalid ${collection} id!` }); // invalid item id

		if (!imageId || !con.ObjectID.isValid(imageId))
			return res.status(400).json({ imageId: `Invalid Image Id!` });


		const db = await con.db()

		//check image existence
		let image = await db.collection('images').findOne({ _id: new con.ObjectID(imageId) });
		if (!image) return res.status(400).json({ imageId: 'No image with given id' });

		// database set imageId as cover
		let result = await db.collection(collection).updateOne(
			{ _id: new con.ObjectID(itemId) }, 
			{ $set: { cover: new con.ObjectID(imageId) } }
		)

		//itemID existence check
		if(result.matchedCount == 0)  
			return res.status(400).json({itemId: `no ${collection} with given id`});

		return res.json({ success: `Image set as thumbnail for ${collection}` })

	} catch (err) {
		console.log(err);
		res.status(500).json({ error: err.message });
	}
});


module.exports = router;