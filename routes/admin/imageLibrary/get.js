const router = require('express').Router();
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
const paginate = require('./../../../lib/paginate.js');

router.get('/', async(req, res)=>{
    let db = await con.db();
    let images = await paginate('images', [], [], req.query);
    
    return res.json(images);
})

router.get('/settings', async(req, res)=>{
    try{
        const db = await con.db();
        let settings =  await db.collection('settings').findOne({name: 'imageSize'});
        res.json(settings);
    }
    catch(err){
        console.log(err);
        res.status(500).json({error: err.message});
    }
})

router.get('/:id', async(req, res)=>{
    let id = req.params.id;
    if(!id || !con.ObjectID.isValid(id)) return res.status(400).json({id: "invalid id"});
    
    let db = await con.db();
    let image = await db.collection('images').findOne({_id: new con.ObjectID(id)});
    
    if(!image) return res.status(404).json({id: 'image not found'});
    return res.json(image);
})


module.exports = router;
