const router = require('express').Router();
const Validator =  require('../../../validator/admin.js');
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
const path =  require('path');
const jimp = require('jimp');

router.post('/', async (req, res)=>{

  try{
    
    let {id, alt, title, caption, labels} = req.fields;
    let db = await con.db();

    //id validation
    if(!id || !con.ObjectID.isValid(id)) return res.status(400).json({id:'Invalid Id'})

    let newImageInfo = {
      alt, title, caption, labels
    }

    let result = await db.collection('images').updateOne({_id: new con.ObjectID(id)},{$set:newImageInfo});    
    
		//item existence check
		if(result.matchedCount == 0)  
      return res.status(400).json({itemId: `no image with given id`});

    
    return res.json({ updated: newImageInfo});

  }catch(err){
    console.log(err);
    res.status(500).json({error : err.message})
  }
 
})

//TODO: add multiple delete support

router.post('/delete/:id', async(req, res)=>{
  try{

    let id = req.params.id;
    if(!id || !con.ObjectID.isValid(id)) return res.status(400).json({id:'Invalid Id'});

    let db= await con.db();

    //component info
    let image = await db.collection('images').findOne({_id: new con.ObjectID(id)});
    if(!image) return res.status(400).json({id:'Image not found'});
    
    //delete images
    let imagePath = {};
    imagePath.thumbnail = image.thumbnail && path.join('public', req.hostOptions.publicDir, image.thumbnail);
    imagePath.original = image.original && path.join('public', req.hostOptions.publicDir, image.original);
    imagePath.icon = image.icon && path.join('public', req.hostOptions.publicDir, image.icon);
    imagePath.medium = image.medium && path.join('public', req.hostOptions.publicDir, image.medium);
    imagePath.full = image.thumbnail && path.join('public', req.hostOptions.publicDir, image.full);

    for(let item in imagePath){
      if(fs.existsSync(imagePath[item])){
        fs.unlink(imagePath[item]);
      }
    }

    let result = await db.collection('images').deleteOne({_id: new con.ObjectID(id)});
    
    return res.json({success:'Image Deleted!'});
  }
  catch(err){
    res.status(500).json({error: err.message});
  }
})

//TODO: add fit: fill, cover, inside etc
router.post('/settings', async (req, res)=>{

  try{
      
    let db = await con.db();
    
    //validation 
    let errors = {};

    // number type validation
    let numFields = ["iconWidth", "iconHeight", "thumbnailWidth", "thumbnailHeight", "mediumWidth", "mediumHeight", "fullWidth", "fullHeight"];
    for(fld of numFields){
      req.fields[fld] = req.fields[fld] ? Number(req.fields[fld]) : undefined;
      if(req.fields[fld] && !isFinite(req.fields[fld])) return res.status(400).json({[fld]:`${fld} must be a number`})
    }


    let {iconWidth, iconHeight, 
      thumbnailWidth, thumbnailHeight,
      mediumWidth, mediumHeight, 
      fullWidth, fullHeight} = req.fields;
    

    if( iconWidth && iconWidth < 5) errors.iconWidth = 'Icon width should be at least 5px'
    if( iconHeight && iconHeight < 5) errors.iconWidth = 'Icon height should be at least 5px'

    if( thumbnailWidth && thumbnailWidth < 20) errors.thumbnailWidth = 'Thumbnail width should be at least 20px'
    if( thumbnailHeight && thumbnailHeight < 20) errors.thumbnailHeight = 'Thumbnail height should be at least 20px'

    if( mediumWidth && mediumWidth < 50) errors.mediumWidth = 'Medium width should be at least 50px'
    if( mediumHeight && mediumHeight < 50) errors.mediumHeight = 'Medium height should be at least 50px'

    if( fullWidth && fullWidth < 100) errors.fullWidth = 'Full width should be at least 100px'
    if( fullHeight && fullHeight < 100) errors.fullHeight = 'Full height should be at least 100px'

    if(!_.isEmpty(errors)) return res.status(400).json(errors);
    let newSize = {
      full:{
        height: fullHeight,
        width: fullWidth
      },
      medium:{
        height: mediumHeight,
        width: mediumWidth
      },
      thumbnail:{
        height: thumbnailHeight,
        width: thumbnailWidth,
      },
      icon:{
        height: iconHeight,
        width: iconWidth
      }
    }
    let updateResult = await db.collection('settings').updateOne({name: 'imageSize'},{$set:newSize}, {upsert:true});

    res.json({updated: newSize});

  }catch(err){
    console.log(err);
    res.status(500).json({error:err.message})
  }
 
})

module.exports = router;