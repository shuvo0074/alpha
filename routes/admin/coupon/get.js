const router = require('express').Router();
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');


router.get('/', async(req, res)=>{
    let db = await con.db();
    // let coupons = await db.collection('coupon').find({}).toArray();

    let aggregatePipeLine = [
        // {$match: {_id: new con.ObjectID(id)}},
        {$lookup:{
            from: "images",
            localField: "cover",
            foreignField: "_id",
            as: "cover"
        }}
    ]
    aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } });

    let coupons = await db.collection('coupon').aggregate(aggregatePipeLine).toArray();
    // if(!coupon) return res.status(400).json({id: "no coupon with given id"});
    
    return res.json(coupons);
})


router.get('/:id', async(req, res)=>{

    // *id validation
    let id = req.params.id;
    if(!con.ObjectID.isValid(id)) return res.status(400).json({id: "Invalid id"});

    let {orderedProducts} = req.query;

    let db = await con.db();
    // let coupon = await db.collection('coupon').findOne({_id: new con.ObjectID(id)});
    let aggregatePipeLine = [
        {$match: {_id: new con.ObjectID(id)}},
        {$lookup:{
            from: "images",
            localField: "cover",
            foreignField: "_id",
            as: "cover"
        }}
    ]
    aggregatePipeLine.push({ $unwind: { path: '$cover', preserveNullAndEmptyArrays: true } });


    
  
    if (orderedProducts != -1) {
        aggregatePipeLine.push({ $unwind: { path: '$orderedProducts', preserveNullAndEmptyArrays: true } });

        aggregatePipeLine.push({
            $lookup: {
                from: "product",
                localField: "orderedProducts._id",
                foreignField: "_id",
                as: "orderedProducts.detail",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$orderedProducts.detail', preserveNullAndEmptyArrays: true } });
 
        //lookup category cover images
        aggregatePipeLine.push({
            $lookup: {
                from: "images",
                localField: "orderedProducts.detail.cover",
                foreignField: "_id",
                as: "orderedProducts.detail.cover",
            }
        });
        aggregatePipeLine.push({ $unwind: { path: '$orderedProducts.detail.cover', preserveNullAndEmptyArrays: true } });
        
        aggregatePipeLine.push({
            $group:{
                _id: "$_id",
                orderedProducts: {$push: "$orderedProducts"},
                doc: {$first: "$$ROOT"}
            }
        })

        aggregatePipeLine.push({
            $replaceRoot: {newRoot: {$mergeObjects: ["$doc", "$$ROOT"]}}
        })

        aggregatePipeLine.push({ $project: { doc: 0 } });

        
    }

    let coupon = await db.collection('coupon').aggregate(aggregatePipeLine).toArray();
    if(!coupon[0]) return res.status(400).json({id: "no coupon with given id"});
    
    coupon = coupon && coupon [0];

    return res.json(coupon);
})



module.exports = router;
