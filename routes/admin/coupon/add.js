const router = require('express').Router();
const Validator = require('../../../validator/admin.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const fs = require('fs');
const util = require('util');
const meter = require('./../../../lib/meterFunctions.js');
const urlGenerator = require('./../../../lib/urlGenerator.js');

router.post('/', async (req, res) => {
  try {

    // *resource check
    let missingResource = await meter.usedResource(req, 'db');
    if (missingResource) {
      return res.status(400).json({
        Resource: `Usage Limit Exceeded For ${missingResource.join(', ')}`
      });
    }

    let { name, code,
      minimumOrder, maximumOrder, orderedProducts,
      startDate, endDate,
      amountType, amount, freeDelivery, freeProducts, maxUseTotal, maxUsePerUser,
      cover } = req.fields;

    // *validate 
    let validator = new Validator();
    let errors = validator.coupon(req.fields);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    //TODO: validate ordered & free products

    //TODO: add variation
    if (orderedProducts) {
      for (let product of orderedProducts) {
        product._id = new con.ObjectID(product._id);
        if (product.variation) product.variation = new con.ObjectID(product.variation);
      }
    }
    if (freeProducts) {
      for (let product of freeProducts) {
        product._id = new con.ObjectID(product._id);
        product.variation = new con.ObjectID(product.variation);
      }
    }

    startDate = startDate ? new Date(startDate) : new Date();

    let startDateClone = new Date(startDate.getTime());
    endDate = endDate ? new Date(endDate) : new Date(startDateClone.setMonth(startDateClone.getMonth() + 1)); //one month default

    cover = con.ObjectID.isValid(cover) ? new con.ObjectID(cover) : null;

    let coupon = {
      name,
      code:code.toUpperCase(),
      minimumOrder, maximumOrder, orderedProducts,
      amountType, amount, freeDelivery, freeProducts,
      maxUseTotal, maxUsePerUser,
      added: new Date(),
      updated: new Date(),
      startDate, endDate,
      cover
    }

    let db = await con.db();
    let result = await db.collection('coupon').insertOne(coupon);

    //resolve cover for response
    let coverDetail = cover && await db.collection('images').findOne({ _id: cover });
    if (result.ops && result.ops[0]) result.ops[0].cover = coverDetail;

    return res.json(result.ops);

  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: err.message });
  }
});

module.exports = router;
