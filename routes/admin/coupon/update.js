const router = require('express').Router();
const Validator = require('../../../validator/admin.js');
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');


router.post('/:id', async (req, res) => {
    try {
        let id = req.params.id;
        if(!con.ObjectID.isValid(id)) return res.status(400).json({id: "Invalid Id"});
        
        let { name, code,
            minimumOrder, maximumOrder, orderedProducts,
            startDate, endDate,
            amountType, amount, freeDelivery, freeProducts, cover } = req.fields;

        
        // *validate 
        let validator = new Validator();
        let errors = validator.coupon(req.fields);
        if (!_.isEmpty(errors)) return res.status(400).json(errors);
        cover = con.ObjectID.isValid(cover) ? new con.ObjectID(cover) : null;

        //TODO: validate ordered & free products

        let coupon = {
            name, 
            code: code.toUpperCase(),
            minimumOrder, maximumOrder, orderedProducts,
            startDate: startDate ? new Date(startDate) : new Date(),
            endDate: endDate ? new Date(endDate) : null,
            amountType, amount, freeDelivery, freeProducts, cover,
            updated: new Date(),
        }

        let db = await con.db();
        let result = await db.collection('coupon').updateOne({_id: new con.ObjectID(id)}, {$set:coupon});

        if(result.matchedCount == 0) return res.status(400).json({id: "no coupon with given id"});

        return res.json({updated: coupon});

    } catch (err) {
        console.log(err);
        res.status(500).json({ msg: err.message });
    }
});



router.post('/delete/:id', async (req, res) => {
    let id = req.params.id;
    if (!id) return res.status(400).json({ id: 'Invalid Id' });

    let db = await con.db();

    //remove category from collection;
    let result = await db.collection('coupon').deleteOne({ _id: new con.ObjectID(id) });

    if(result.deletedCount === 0) return res.status(404).json({ msg: "No coupon found!" })

    res.json({ success: `Coupon deleted!` })
})
module.exports = router;