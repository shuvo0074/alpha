const router = require('express').Router();
const _ = require('lodash')
const con = require('../../../lib/db.js')

router.get("/", async (req, res) => {
	try {
		const db = await con.db();

		let aggregatePipe = [
			{$match: {attributes: {$ne: null}}},
			{$project: { attributes: 1, _id:0}},
			{$unwind: "$attributes"},
			{$group:{
				_id: null,
				attributes: {$push: "$attributes"}
			}},
			{$project: {attributes:1, _id: 0}}
		]

		// get attributes from db
		let attrs = await db.collection("product").aggregate(aggregatePipe).toArray();
		attrs = attrs[0] && attrs[0].attributes || [];

		// normalize attributes
		let attrList = {};
		attrs.forEach((attr, index)=>{
			for(let key in attr){
				if(!attrList[key]) attrList[key] = new Set(); // Set() to avoid duplicate value
				attrList[key].add(attr[key]);
			}
		});

		//convert set to Array for json
		for(let key in attrList){
			attrList[key] = [...attrList[key]]
		}
		return res.json(attrList);
	} catch (err) {
		console.log(err);
		return res.status(500).json({error: err.message});
	}
})

router.get("/category/:id", async (req, res) => {
	try {
		const db = await con.db();

		//validate Id
		let categoryId = req.params.id;
		if(!con.ObjectID.isValid(categoryId)) return res.status(400).json({id: "invalid id"});

		let aggregatePipe = [
			{$match: {attributes: {$ne: null}}},
			{$project: { attributes: 1, _id:0}},
			{$unwind: "$attributes"},
			{$group:{
				_id: null,
				attributes: {$push: "$attributes"}
			}},
			{$project: {attributes:1, _id: 0}}
		]

		//TODO: add recursive support

		// get attributes from db
		let attrs = await db.collection("product").aggregate(aggregatePipe).toArray();
		attrs = attrs[0] && attrs[0].attributes || [];

		// normalize attributes
		let attrList = {};
		attrs.forEach((attr, index)=>{
			for(let key in attr){
				if(!attrList[key]) attrList[key] = new Set(); // Set() to avoid duplicate value
				attrList[key].add(attr[key]);
			}
		});

		//convert set to Array for json
		for(let key in attrList){
			attrList[key] = [...attrList[key]]
		}
		return res.json(attrList);
	} catch (err) {
		console.log(err);
		return res.status(500).json({error: err.message});
	}
})


router.get("/product/:id", async (req, res) => {
	try {
		const db = await con.db();

		//validate Id
		let categoryId = req.params.id;
		if(!con.ObjectID.isValid(categoryId)) return res.status(400).json({id: "invalid id"});

		let aggregatePipe = [
			{$match: {_id: new con.ObjectID(categoryId)}},
			{$match: {attributes: {$ne: null}}},
			{$project: { attributes: 1, _id:0}},
			{$unwind: "$attributes"},
			{$group:{
				_id: null,
				attributes: {$push: "$attributes"}
			}},
			{$project: {attributes:1, _id: 0}}
		]
		//TODO: add recursive support

		// get attributes from db
		let attrs = await db.collection("product").aggregate(aggregatePipe).toArray();
		attrs = attrs[0] && attrs[0].attributes || [];

		// normalize attributes
		let attrList = {};
		attrs.forEach((attr, index)=>{
			for(let key in attr){
				if(!attrList[key]) attrList[key] = new Set(); // Set() to avoid duplicate value
				attrList[key].add(attr[key]);
			}
		});

		//convert set to Array for json
		for(let key in attrList){
			attrList[key] = [...attrList[key]]
		}
		return res.json(attrList);
	} catch (err) {
		console.log(err);
		return res.status(500).json({error: err.message});
	}
})

module.exports = router;
