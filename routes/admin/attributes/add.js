const router = require('express').Router();
const Validator = require('../../../validator/admin.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const fs = require('fs');
const util = require('util');
const meter = require('./../../../lib/meterFunctions.js');
/*
router.post('/', async (req, res) => {
  try {

    //*resource check
    let missingResource = await meter.usedResource(req, 'db');
    if (missingResource) 
      return res.status(400).json({Resource: `Usage Limit Exceeded For ${missingResource.join(', ')}`});

    let db = await con.db();
    let { name, description } = req.fields;

    //basic validation
    //TODO: add validation for input types
    let validator = new Validator();
    let errors = await validator.addAttributes(req.fields);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);


    // *model attribute
    let attr = {
      name,
      description
    }
    //saving to db
    let result = await db.collection('attributes').insertOne(attr);

    return res.json( {inserted: result.ops });
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: err.message });
  }
});
*/
module.exports = router;
