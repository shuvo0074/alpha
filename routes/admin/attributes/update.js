const router = require('express').Router();
const Validator =  require('../../../validator/admin.js');
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');

/*
router.post('/:id', async(req, res)=>{
    try{

        //id validation
        let id = req.params.id;
        if(!id) res.status(400).json({msg:'Invalid Id!'});

        let db = await con.db()
        let {name, description} = req.fields;

        //basic validation
        //TODO: add validation for input type
        let validator = new Validator();
        let errors = await validator.addAttributes(req.fields);
        if(! _.isEmpty(errors)) return res.status(400).json(errors);
        
        // *model attribute
        let attr = {
            name, 
            description
        }

        //saving to db
        let result = await db.collection('attributes').updateOne({_id: new con.ObjectID(id)}, {$set:attr});
        
        //item existence check
        if(result.matchedCount == 0)  
            return res.status(400).json({itemId: `no brand with given id`});

        return res.json({updated : attr});
    }catch(err){
        console.log(err);
        return res.status(500).json({error:err.message})
    }
})


router.post('/delete/:id', async(req, res)=>{
    try{
        let id = req.params.id;
        if(!id) return res.status(400).json({id:'Invalid Id'});

        let db= await con.db();

        //remove attribute from collection;
        let result = await db.collection('attributes').deleteOne({_id: new con.ObjectID(id)});
        
        //item existence check
        if(result.deletedCount < 1)  
            return res.status(400).json({itemId: `no attribute with given id`});

        return res.json({success:'Attribute Deleted!'})

    }catch(err){
        console.log(err);
        return res.status(500).json({error: err.message});
    }
})
*/
module.exports = router;