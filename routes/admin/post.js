const router = require('express').Router();

const addRouter = require('./post/add.js');
const getRouter = require('./post/get.js');
const updateRouter = require('./post/update.js');
const addCategoryRouter = require('./post/category/add.js');
const getCategoryRouter = require("./post/category/get.js");
const updateCategoryRouter = require('./post/category/update.js');
const addTagRouter = require('./post/tags/add.js');
const getTagRouter = require('./post/tags/get.js');
const updateTagRouter = require("./post/tags/update.js")
// const updateRouter = require('./product/update.js');
// const getRouter = require('./product/get.js')


router.use('/category', getCategoryRouter);
router.use('/category/add', addCategoryRouter);
router.use('/category/update', updateCategoryRouter);

router.use('/tag', getTagRouter);
router.use('/tag/add', addTagRouter);
router.use('/tag/update', updateTagRouter)

router.use('/', getRouter);
router.use('/add', addRouter);
router.use('/update', updateRouter);

// router.use('/update', updateRouter);
// router.use('/', getRouter);

module.exports = router;