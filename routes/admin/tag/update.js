const router = require('express').Router();
const Validator = require('../../../validator/admin.js');
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');

router.post('/:id', async (req, res) => {
	try {

		// validate id
		let id = req.params.id;
		if (!id || !con.ObjectID.isValid(id)) return res.status(400).json({ msg: 'Invalid Id!' });

		let db = await con.db()
		let { name, description, bn = {}, metaTitle, metaTags,metaDescription, } = req.fields;

		//  *basic validation
		let validator = new Validator();
		let errors = await validator.addTag(req.fields);
		if (!_.isEmpty(errors)) return res.status(400).json(errors);

		// *model tag
		let tag = {
			name, description,
			metaTitle,
			metaTags,
			metaDescription,
			bn: {
				name: bn.name,
				description: bn.description,
				metaTitle: bn.metaTitle,
				metaTags: bn.metaTags,
				metaDescription: bn.metaDescription
			},
			updated: new Date()

		}

		//saving to db
		let result = await db.collection('tag').updateOne({ _id: new con.ObjectID(id) }, { $set: tag });

		//item existence check
		if (result.matchedCount == 0)
			return res.status(400).json({ itemId: `no tag with given id` });

		return res.json({ updated: tag });

	} catch (err) {
		console.log(err);
		return res.status(500).json({ error: err.message })
	}
})


router.post('/delete/:id', async (req, res) => {
	try {
		let id = req.params.id;
		if (!id) return res.status(400).json({ id: 'Invalid Id' });

		let db = await con.db();

		//remove attribute from collection;
		let result = await db.collection('tag').deleteOne({ _id: new con.ObjectID(id) });

		//item existence check
		if (result.deletedCount < 1)
			return res.status(400).json({ itemId: `no tag with given id` });

		return res.json({ success: 'Tag Deleted!' })

	} catch (err) {
		console.log(err);
		return res.status(500).json({ error: err.message });
	}
})

module.exports = router;