const router = require('express').Router();
const Validator = require('../../../validator/admin.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const fs = require('fs');
const util = require('util');
const meter = require('./../../../lib/meterFunctions.js');

router.post('/', async (req, res) => {
  try {

    // *resource check ...
		let missingResource = await meter.usedResource(req, 'db');
		if (missingResource) 
			return res.status(400).json({Resource: `Usage Limit Exceeded For ${missingResource.join(', ')}`});


    let db = await con.db();
    let { name, description,  bn={}, metaTitle, metaTags, metaDescription, } = req.fields;

    //basic validation
    let validator = new Validator();
    let errors = await validator.addTag(req.fields);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    //saving to db
    let result = await db.collection('tag').insertOne({
      name,
      description,
      metaTitle,
      metaTags,
      metaDescription,
      bn:{
        name: bn.name,
        description:bn.description,
        metaTitle: bn.metaTitle,
        metaTags: bn.metaTags,
        metaDescription: bn.metaDescription
      },
      added: new Date(),
      updated: new Date()
    });

    return res.json(result.ops);

  } catch (err) {
    console.log(err);
    res.status(500).json({ error: err.message });
  }
});

module.exports = router;
