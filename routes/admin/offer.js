const router = require('express').Router();
const Validator =  require('../../validator/admin.js');
const _ = require('lodash')
const con = require('../../lib/db.js')

const addRouter = require('./offer/add.js');
const updateRouter = require('./offer/update.js');
const getRouter = require('./offer/get.js')

router.use('/add', addRouter);
router.use('/update', updateRouter);
router.use('/', getRouter)
module.exports = router;
