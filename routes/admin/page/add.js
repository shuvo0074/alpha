const router = require('express').Router();
const Validator = require('../../../validator/admin.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const fs = require('fs');
const util = require('util');
const meter = require('./../../../lib/meterFunctions.js');
const urlGenerator = require('./../../../lib/urlGenerator.js');

router.post('/', async (req, res) => {
  try {
    let missingResource = await meter.usedResource(req, 'db', 'disk', 'page');
    if (missingResource) {
      return res.status(400).json({
        Resource: `Usage Limit Exceeded For ${missingResource.join(', ')}`
      });
    }

    let db = await con.db();

    let { name, content, cover, url , metaTitle, metaTags, metaDescription,  bn={}} = req.fields;

    if (!name) return res.status(400).json({ name: 'name can not be empty' });
    if (!content) return res.status(400).json({ content: 'empty page will not be created' });

    if(!con.ObjectID.isValid(cover)) return res.status(400).json({cover: "invalid cover image"});

    url = await urlGenerator.pageUrl( url || name);

    let newPage = {
      name,
      content,
      url,
      cover: new con.ObjectID(cover),
      added: new Date(),
      lastModified: new Date(),
      
      metaTitle,
      metaTags,
      metaDescription,

      bn: {
        name: bn.name,
        content: bn.content,
        
        metaTitle: bn.metaTitle,
        metaTags: bn.metaTags,
        metaDescription: bn.description
      }
    };

    let result = await db.collection('page').insertOne(newPage);

    //resolve cover for response
    let coverDetail = cover && await db.collection('images').findOne({_id: cover});
    if(result.ops && result.ops[0]) result.ops[0].cover = coverDetail;

    res.json(result.ops);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: err.message });
  }
});

module.exports = router;