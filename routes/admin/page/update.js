const router = require('express').Router();
const Validator = require('../../../validator/admin.js');
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
const urlGenerator = require('./../../../lib/urlGenerator.js');

router.post('/:id', async (req, res) => {
    try {
        let id = req.params.id;
        if (!id || !con.ObjectID.isValid(id)) return res.status(400).json({ id: 'Invalid Page Id' });
        let db = await con.db()

        let page = await db.collection('page').findOne({ _id: new con.ObjectID(id) });
        if (!page) return res.status(400).json({ id: 'Page Not Found' });

        let { name, content, url , cover,  metaTitle, metaTags, metaDescription,  bn={}} = req.fields;

        if (!name) return res.status(400).json({ name: 'name can not be empty' });
        if (!content) return res.status(400).json({ content: 'empty page will not be created' });
        if(!con.ObjectID.isValid(cover)) return res.status(400).json({cover: "invalid cover image"});

        url = await urlGenerator.pageUrl( url || name, id);
        let updatedPage = {
            name,
            content,
            url,
            lastModified: new Date(),
            cover: new con.ObjectID(cover),

            metaTitle,
            metaTags,
            metaDescription,

            bn: {
                name: bn.name,
                content: bn.content,

                metaTitle: bn.metaTitle,
                metaTags: bn.metaTags,
                metaDescription: bn.metaDescription
            },
            updated: new Date()

        }

        let result = await db.collection('page').updateOne({ _id: new con.ObjectID(id) }, { $set: updatedPage });

        res.json({ updated: updatedPage });

    } catch (err) {
        console.log(err);
        res.status(500).json({ msg: err.message })
    }


})

router.post('/delete/:id', async (req, res) => {
    let id = req.params.id;
    if (!id) return res.status(400).json({ id: 'Invalid Id' });

    let db = await con.db();

    //remove category from collection;
    let result = await db.collection('page').deleteOne({ _id: new con.ObjectID(id) });

    res.json({ msg: 'Page Deleted!' })
})
module.exports = router;