const router = require('express').Router();
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const pageFunctions = require("./../../../lib/dbFunctions/public/pageFunctions.js");
const util = require('util');

router.get('/', async (req, res) => {
	try {
		// let db = await con.db();
		let pages = await pageFunctions.getPageList(req.query);

		res.json(pages);

	} catch (err) {
		console.log(err);
		res.status(500).json({ error: err.message });
	}
})

router.get('/:id', async (req, res) => {
	try {
		let id = req.params.id;
		if (!con.ObjectID.isValid(id)) return res.end('/invalid id');

		let page = await pageFunctions.getPage(id, req.query);

		res.json(page);

	} catch (err) {
		console.log(err);
		res.status(500).json({ error: err.message });
	}
})

module.exports = router;
