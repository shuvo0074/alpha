const router = require('express').Router();
const Validator =  require('../../validator/admin.js');
const _ = require('lodash')
const con = require('../../lib/db.js')

const addRouter = require('./category/add.js');
const updateRouter = require('./category/update.js');
const getRouter = require('./category/get.js')

router.use('/add', addRouter);
router.use('/update', updateRouter);
router.use('/', getRouter)
module.exports = router;
