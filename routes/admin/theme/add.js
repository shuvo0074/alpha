const router = require('express').Router();
const Validator =  require('../../../validator/admin.js');
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
let extract = require('extract-zip');
const path = require('path');
const fileType = require('file-type');

extract = util.promisify(extract);

router.post('/', async(req, res)=>{
    try{
        //TODO: add size, type validation
        let db = await con.db();

        let {thumbnail, folder} = req.files;
        let {name} = req.fields;

        // *validation
        if(!folder || folder.size == 0) return res.status(400).json({folder: "add theme folder"}); 

        let mimeType = await fileType.fromFile(folder.path); //mime check

        if(mimeType.mime != 'application/zip') 
            return res.status(400).json({folder: "invalid file type, only zip file is supported"}); 

        if(!name) return res.status(400).json({name: "add theme name"});

        //existence check
        let existingTheme = await db.collection('themes').findOne({name});
        if(existingTheme){
            return res.status(400).json({folder:'There is already a theme with this name'});
        }

        
        let themePath = path.join(req.userDir, 'themes')
        await extract(folder.path, {dir: themePath});

        let thumbPath;
        if(thumbnail && thumbnail.size > 0){
            let data = await fs.readFile(thumbnail.path);
            let imageName = folder.name+'-thumb'+'-'+thumbnail.name;
            thumbPath = path.join('/images', imageName)
            await fs.writeFile(path.join(req.publicDir, thumbPath), data);
        }

        let theme = {
            name,
            thumbnail: thumbPath,
            path: path.join('themes', folder.name.replace('.zip', '')),
            added: new Date(),
        }
        
        let result = await db.collection('themes').insertOne(theme);
        theme._id = result.insertedId;
        res.json({success:'Theme Uploaded', inserted:result.ops});

    }catch(err){
        console.log(err);
        res.status(500).json({msg:err.message})
    }


})

module.exports = router;
