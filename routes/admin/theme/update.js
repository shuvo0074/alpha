const router = require('express').Router();
const Validator =  require('../../../validator/admin.js');
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
const rimraf = require('rimraf');
const domainMap = require('./../../../config/domainMap.json');
const path = require("path");

router.post('/activate/:id', async (req, res)=>{
	try{
		let id = req.params.id;
		if(!id) return res.end('invalid id');

		//theme detail
        let db = await con.db();
        let theme = await db.collection('themes').findOne({_id: new con.ObjectID(id)});
		if(!theme) return res.status(400).json({msg:'No theme found'}); //!rewrite with proper redirection

		let config = domainMap[req.hostOptions.domain];
		if(!config) config = domainMap["*"];
		if(!config) return res.status(400).json({host:"host not valid"});
		
		config.theme = theme.path; //set in memory;
		
		await fs.writeFile('./config/domainMap.json', JSON.stringify(domainMap)); //save in json
		res.json({success:'theme Activated'})

	}catch(err){
		console.log(err);
		res.redirect('/500')
	}
})

router.post('/delete/:id', async (req, res)=>{
    try{
        let id = req.params.id;

        if(!id || !con.ObjectID.isValid(id)) return res.status(400).json({id: "invalid id"});
        
        let db = await con.db();
        let theme = await db.collection('themes').findOne({_id : new con.ObjectID(id)});
        if(!theme)return res.status(400).json({id: "no theme with given id"});
        
        //delete from disk
        rimraf.sync(path.join(req.userDir, theme.path));
    
        //delete from db
        await db.collection('themes').deleteOne({_id : new con.ObjectID(id)});
    
        return res.json({success: 'theme deleted'});

    }catch(err){
        console.log(err);        
        res.status(500).json({error: err.message});
    }
   
})

module.exports = router;