const router = require('express').Router();
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
const path = require('path');


router.get('/', async(req, res)=>{

    let db = await con.db();
	let themes = await db.collection('themes').find({}).toArray();
    res.json(themes);
})

router.get('/:id', async(req, res)=>{
	let id = req.params.id;
	if(!con.ObjectID.isValid(id)) return res.status(400).json({id: "invalid id"});

    let db = await con.db();
	let theme = await db.collection('themes').findOne({_id: new con.ObjectID(id)});
    return res.json(theme);
})

module.exports = router;
