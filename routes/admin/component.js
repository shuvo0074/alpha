const router = require('express').Router();
const Validator =  require('../../validator/admin.js');
const _ = require('lodash')
const con = require('../../lib/db.js')

const addRouter = require('./component/add.js');
const updateRouter = require('./component/update.js');
const getRouter = require('./component/get.js');


router.use('/add', addRouter);
router.use('/update', updateRouter); 
router.use('/', getRouter); 

module.exports = router;
