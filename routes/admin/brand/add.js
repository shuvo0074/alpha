const router = require('express').Router();
const Validator = require('../../../validator/admin.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const fs = require('fs');
const util = require('util');
const meter = require('./../../../lib/meterFunctions.js');
const urlGenerator = require('./../../../lib/urlGenerator.js');

router.post('/', async (req, res) => {
  try {

    // *resource check ...
    let missingResource = await meter.usedResource(req, 'db', 'disk');
    if (missingResource) 
      return res.status(400).json({ Resource: `Usage Limit Exceeded For ${missingResource.join(', ')}`}); 

    let db = await con.db();
    let { name, description, image, cover, url, metaTitle, metaTags, metaDescription, bn={} } = req.fields;
    
    let list = {}; //for validation

    //brand list
    let brands = await db.collection('brand').find({}).toArray();
    list.brand = brands.map(item => {  //list of brand _id for validation
      return item._id.toHexString();
    });

    //basic validation
    let validator = new Validator();
    let errors = await validator.addBrand(req.fields, list);
    if(! _.isEmpty(errors)) return res.status(400).json(errors);

    
    // *image ...
    // convert image ids to objectIds
    let validImages = [];
    if (image){
      for (let x in image) {
        if (!con.ObjectID.isValid(image[x])) {
          continue
        };
        validImages.push(new con.ObjectID(image[x]));
      }
    }
    //cover image
    cover = (cover && con.ObjectID.isValid(cover) )? new con.ObjectID(cover) : (validImages && validImages[0])? validImages[0]:null;

    // *url
    url = await urlGenerator.brandUrl(url || name);

    // *model brand
    let brand = { 
      name,
      description, 
      image:validImages,
      cover,
      url,

      metaTitle,
      metaTags,
      metaDescription,

      bn: {
        name: bn.name,
        description: bn.description,
        
        metaTitle: bn.metaTitle,
        metaTags: bn.metaTags,
        metaDescription: bn.metaDescription
      },
      added: new Date(),
      updated: new Date()
    }

    // *saving to db ...
    let result = await db.collection('brand').insertOne(brand);


    //resolve cover for response
    let coverDetail = cover && await db.collection('images').findOne({_id: cover});
    if(result.ops && result.ops[0]) result.ops[0].cover = coverDetail;

    console.log(cover, coverDetail)
    return res.json({
      inserted: result.ops,
   });
   
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: err.message });
  }
});

module.exports = router;