const router = require("express").Router();
const Validator = require("../../../validator/admin.js");
const _ = require("lodash");
const con = require("../../../lib/db.js");
const fs = require("fs");
const util = require("util");
const urlGenerator = require("./../../../lib/urlGenerator.js");
const meter = require("./../../../lib/meterFunctions.js");

/*
	@routes /admin/brand/update/delete
	@desc Bulk delete brand
	@method POST
*/

router.post("/delete", async (req, res) => {
  try {
    let ids = [];
    Object.values(req.fields).filter((id) => {
      if (con.ObjectID.isValid(id)) {
        ids.push(new con.ObjectID(id));
      }
    });

    const db = await con.db();
    const deletedProduct = await db
      .collection("brand")
      .deleteMany({ _id: { $in: ids } });

    return res.json({
      success: `${deletedProduct.deletedCount} product deleted from category`,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json(err);
  }
});

router.post("/:id", async (req, res) => {
  try {
    // *resource check ...
    let missingResource = await meter.usedResource(req, "db");
    if (missingResource)
      return res.status(400).json({
        Resource: `Usage Limit Exceeded For ${missingResource.join(", ")}`,
      });

    let id = req.params.id;
    if (!id || !con.ObjectID.isValid(id))
      return res.status(400).json({ id: "Invalid Id" });

    let { name, description, url, metaTitle, metaTags, metaDescription,  bn={} } = req.fields;

    // *validation
    let list = {};
    let db = await con.db();

    //brand list
    let brands = await db.collection("brand").find({}).toArray();
    list.brand = brands.map((item) => {
      //list of brand _id for validation
      return item._id.toHexString();
    });

    // *url
    url = await urlGenerator.brandUrl(url || name, id);

    // *model brand
    let brand = {
      name,
      description,
      url,

      metaTitle,
      metaTags,
      metaDescription,

      bn: {
        name: bn.name,
        description: bn.description,
        
        metaTitle: bn.metaTitle,
        metaTags: bn.metaTags,
        metaDescription: bn.metaDescription
      }
    };

    let result = await db
      .collection("brand")
      .update({ _id: new con.ObjectID(id) }, { $set: brand });

    //item existence check
    if (result.matchedCount == 0)
      return res.status(400).json({ itemId: `no brand with given id` });

    return res.json({ updated: brand });
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: err.message });
  }
});

router.post("/delete/:id", async (req, res) => {
  try {
    let id = req.params.id;
    if (!id || !con.ObjectID.isValid(id))
      return res.status(400).json({ id: "Invalid Id" });

    let db = await con.db();

    //remove brand from collection;
    let result = await db
      .collection("brand")
      .deleteOne({ _id: new con.ObjectID(id) });

    //item existence check
    if (result.deletedCount < 1)
      return res.status(400).json({ itemId: `no brand with given id` });

    return res.json({ success: "Brand Deleted!" });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: err.message });
  }
});

module.exports = router;
