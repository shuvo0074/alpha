const router = require('express').Router();
const Validator =  require('../../validator/admin.js');
const _ = require('lodash')
const con = require('../../lib/db.js')

const updateRouter = require('./email/update.js');
const sendRouter = require('./email/send.js');
const getRouter = require('./email/get.js')

router.use('/configure', updateRouter);
router.use('/inbox', sendRouter);
router.use('/', getRouter)
module.exports = router;
