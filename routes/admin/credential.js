const router = require('express').Router();
const Validator = require('../../validator/admin.js');
const _ = require('lodash')
const con = require('../../lib/db.js')
const bcrypt = require('bcrypt');

router.get('/', async (req, res) => {
    let db = await con.db();
    let admin = await db.collection('admin').findOne({ _id: new con.ObjectID(req.session.admin._id) });
    if (!admin) { //admin not logged in
        req.session.destroy(); //assure session destroy
        res.status(400).json({ admin: 'admin not logged in' })
    }
    delete admin.password;
    return res.json(admin);
})

router.post('/update', async (req, res) => {
    try {
        let adminId = req.session.admin._id;
        let db = await con.db();

        if (!con.ObjectID.isValid(adminId)) {
            req.session.destroy();
            return res.status(400).json({ admin: 'Invalid admin' })
        }


        //get admin info
        let admin = await db.collection('admin').findOne({ _id: new con.ObjectID(adminId) })
        let { name, phone, email, password, newPass, newPassRepeat } = req.fields;


        //validation
        if (newPass && newPass != newPassRepeat) return res.status(400).json({ newPassRepeat: 'password not matching' })
        if (!name) return res.status(400).json({ name: 'Insert Phone Number' });
        if (!phone) return res.status(400).json({ phone: 'Insert Phone Number' });
        if (!email) return res.status(400).json({ email: 'Insert Email' });
        if (!password) return res.status(400).json({ password: 'Insert password' });


        let passwordMatch = await bcrypt.compare(password, admin.password);
        if (!passwordMatch) return res.status(403).json({ password: 'incorrect password' }); //todo: change with proper response

        //checking if account with phone exists
        let exists = await db.collection("admin").findOne({ phone });
        if (exists && exists._id && exists._id.toHexString() != adminId) return res.status(400).json({ phone: "There is already an admin with this phone number" });

        //checking if account with email exists
        if (email && email != "" && email != " ") {
            exists = await db.collection("customer").findOne({ email: { $regex: new RegExp("^" + email, "i") } });
            if (exists && exists._id && exists._id.toHexString() != adminId) return res.status(400).json({ email: "There is already an admin with this email" });
        }


        let updatedAdmin = {
            name, phone, email
        }

        if (newPass) {
            let hash = await bcrypt.hash(newPass, 10); //encrypt password
            console.log(hash)
            updatedAdmin.password = hash;
        }

        await db.collection('admin').updateOne({ _id: new con.ObjectID(adminId) }, { $set: updatedAdmin })
        res.json({ success: 'Admin Settings Updated' });

    } catch (err) {
        console.log(err);
        res.status(500).json({ error: err.message });
    }
})

module.exports = router;
