const router = require('express').Router();
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
const paginate = require('./../../../lib/paginate.js') 

router.get('/configuration', async(req, res)=>{
    let db = await con.db();
    let settings = await db.collection('settings').findOne({name:'email'}) || {};

    res.json(settings);
})

// router.get('/composer', async(req, res)=>{
//     let db = await con.db();
//     let settings = await db.collection('settings').findOne({name:'email'}) || {};

//     res.render('admin/email-composer.html', {email:settings});
// })

router.get('/inbox', async(req, res)=>{

    let emails = await paginate('sentEmail', [], [], req.query); //get sent emails
    return res.json(emails);
});

router.get('/inbox/:id', async(req, res)=>{
    
    let id = req.params.id; //sent email id
    
    if(!con.ObjectID.isValid(id)) return res.json({id: "invalid id"})

    let db = await con.db()
    let email = await db.collection('sentEmail').findOne({_id: new con.ObjectID(id)});
    if(!email) return res.json({id: "no email sent with gien id"})

    return res.json(email);
})

router.get('/autoevents', async(req, res)=>{
    let db = await con.db();
    let settings = await db.collection('settings').findOne({name:'autoEmail'}) || {};

    res.json(settings);
})

router.get('/autoevents/templates', async(req, res)=>{
 
    let db = await con.db();
    let templates = await db.collection('emailTemplates').find().toArray() ;

    res.json(templates);
})


router.get('/autoevents/templates/:event', async(req, res)=>{
    
    let event = req.params.event;

    let db = await con.db();
    let template = await db.collection('emailTemplates').findOne({event}) ;

    res.json(template);
})


module.exports = router;
