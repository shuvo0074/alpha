const router = require('express').Router();
const Validator =  require('../../../validator/admin.js');
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
const path = require('path');
const mail = require('./../../../lib/mail.js'); 


router.post('/', async (req, res)=>{

  try{
      let { 
          fromEmail, fromName, 
          host, encryption, port, 
          authentication, smtpUsername, smtpPassword
        } = req.fields;
      
      let db = await con.db();

      // *validation
      let validator = new Validator();
      let errors = await validator.emailSettings(req.fields);
      if(!_.isEmpty(errors)) return res.status(400).json(errors)

      if(!mail.validateEmail(fromEmail)) return res.status(400).json({fromEmail:'invalid email'})


      let newSettings = {
          fromEmail, fromName,
          host, encryption, port, 
          authentication, smtpUsername, smtpPassword 
        }

      try{
          let verify = await mail.verifySMTP({host, encryption, port, authentication, smtpUsername, smtpPassword});
      }catch(err){
          return res.status(400).json({smtp:'smtp configuration is not correct', verifyError:err.message});
      }
      await db.collection('settings').updateOne({name:'email'},{$set:newSettings}, {upsert:true});

      res.json({success:'Valid SMTP configuration, settings saved '});

  }catch(err){
      console.log(err);
      res.status(500).json({error:err.message}) 
  }

})


//set in which events auto emails should be sent
// for both admin and customer separately

router.post('/autoevents', async (req, res)=>{

    try{
       
        let { newCustomer = {}, order = {}, orderStatus = {}, newDealer = {} } = req.fields;

        let db = await con.db();
  
        //validation
        //force boolean value
        let settings = {
            newCustomer:{
                admin: newCustomer.admin? true : false, //ensure boolean
                user: newCustomer.user? true : false
            },
            order:{
                admin: order.admin? true : false,
                user: order.user? true : false
            },
            orderStatus:{
                admin: orderStatus.admin? true : false,
                user: orderStatus.user? true : false
            },
            newDealer:{
                admin: newDealer.admin? true : false,
                user: newDealer.user? true : false
            },
        }

        await db.collection('settings').updateOne({name:'autoEmail'},{$set:settings}, {upsert:true});
  
        res.json({success:'configuration updated'});
  
    }catch(err){
        console.log(err);
        res.status(500).json({error:err.message}) 
    }
  
  })

// set email templates to send 
// for both admin and customer separately

router.post('/template', async (req, res)=>{

    try{
        let { event, user= {},  admin={}} = req.fields;
        
        let db = await con.db();
  
        
        //validation
        let validator = new Validator();
        let errors = await validator.emailTemplate(req.fields);
        if(!_.isEmpty(errors)){
            return res.status(400).json(errors)
        }
       
        let template = {
            event, 
            user:{
                subject: user.subject,
                body: user.body
            },
            admin:{
                subject: admin.subject,
                body: admin.body,
            },

            lastModified: new Date()
        }

        // await db.collection('emailTemplates').deleteOne({event:event} );

        await db.collection('emailTemplates').updateOne({event:event},{$set:template}, {upsert:true});
  
        return res.json({success:'template saved'});
  
    }catch(err){
        console.log(err);
        res.status(500).json({error:err.message}) 
    }
  
  })

  
module.exports = router;