const router = require('express').Router();
const Validator =  require('../../../validator/admin.js');
const _ = require('lodash')
const con = require('../../../lib/db.js')
const fs = require('fs');
const util = require('util');
const path = require('path');
const mail = require('./../../../lib/mail.js'); 


router.post('/send', async (req, res)=>{

  try{
    let {recipient, subject, html, cc, bcc, text} = req.fields;

    
    //validation
    let validator = new Validator();
    let errors = await validator.sendEmail(req.fields);
    if(!_.isEmpty(errors)){
        return res.status(400).json(errors)
    }

    try{
        let response = await mail.sendMail({
          recipient, cc, bcc,
          subject, 
          text, 
          html,
          event:'compose email'})
    }catch(err){
        return res.status(400).json({smtp:'failed to send email', sendError:err.message});
    }
    
    res.json({msg:'Email sent'});

  }catch(err){
      console.log(err);
      res.status(500).json({msg:err.message}) 
  }

})

router.post('/delete/:id', async (req, res)=>{

  try{
      let id = req.params.id; //sent email id
      if(!id || !con.ObjectID.isValid(id)) return res.status(400).json({id: 'invalid id'});

      let db = await con.db();
      await db.collection('sentEmail').deleteOne({_id: new con.ObjectID(id)});

      res.json({msg:'Email Deleted'});

  }catch(err){
      console.log(err);
      res.status(500).json({msg:err.message}) 
  }

})

module.exports = router;