const router = require("express").Router();
const Validator = require("../../../validator/admin.js");
const _ = require("lodash");
const con = require("../../../lib/db.js");
const fs = require("fs");
const util = require("util");
const path = require("path");
const urlGenerator = require("../../../lib/urlGenerator.js");
const { ObjectID } = require("../../../lib/db.js");

/*
	@routes /admin/category/update/delete
	@desc Bulk delete category
	@method POST
*/

// {
//   id: [34543, 54543, 5345]
// }


router.post("/delete", async (req, res) => {
  try {
    let ids = req.fields.id;
    
    //convert to ObjectId
    ids.forEach((item, index)=>{
      if(!con.ObjectID.isValid(item)) return;
      ids[index] = new con.ObjectID(item);
    })

    const db = await con.db();
    const deletedCategory = await db.collection("category").deleteMany({ _id: { $in: ids } });

    return res.json({
      success: `${deletedCategory.deletedCount} category deleted from category`,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json(err);
  }
});

router.post("/:id", async (req, res) => {
  try {
    let id = req.params.id;
    if (!id || !con.ObjectID.isValid(id))
      return res.status(400).json({ id: "invalid id" });
    let db = await con.db();

    let { name, parent, description, url } = req.fields;
    let list = {}; //for validation

    //category list
    let categories = await db.collection("category").find({}).toArray();
    list.category = categories.map((item) => {
      //list of category _id for validation
      return item._id.toHexString();
    });

    //current category
    let currentCategory = categories.find((i) => i._id.toHexString() == id);
    if (!currentCategory)
      return res.status(400).json({ id: "no category with given id" });

    //basic validation
    let validator = new Validator();
    let errors = await validator.addCategory(req.fields, list);
    if (!_.isEmpty(errors)) return res.status(400).json(errors);

    // *parent category ...
    if (!con.ObjectID.isValid(parent)) parent = null;
    else
      parent = await db
        .collection("category")
        .findOne({ _id: new con.ObjectID(parent) });
    parent = parent && parent._id;

    // *category type
    let type = parent && parent._id ? "sub" : "top";

    // *url
    url = await urlGenerator.categoryUrl( url || name, parent && parent.name, id);

    // *model category
    let category = {
      name,
      type,
      parent: parent && parent._id,
      description,
      url,
      updated: new Date()

    };

    let result = await db.collection("category").updateOne({ _id: new con.ObjectID(id) }, { $set: category });

    //item existence check
    if (result.matchedCount == 0)
      return res.status(400).json({ itemId: `no category with given id` });

    return res.json({
      updated: category,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: err.message });
  }
});

router.post("/delete/:id", async (req, res) => {
  let id = req.params.id;
  if (!id || !con.ObjectID.isValid(id))
    return res.status(400).json({ id: "Invalid id" });
  let db = await con.db();

  //remove category from db;
  let result = await db
    .collection("category")
    .deleteOne({ _id: new con.ObjectID(id) });

  if (result.deletedCount < 1)
    return res.status(400).json({ id: "no category with given id" });

  return res.json({ success: "Category deleted!" });
});

module.exports = router;
