const router = require('express').Router();
const Validator = require('../../../validator/admin.js');
const _ = require('lodash');
const con = require('../../../lib/db.js');
const meter = require('./../../../lib/meterFunctions.js');
const urlGenerator = require('./../../../lib/urlGenerator.js');

let examplePayload = {
  name: "",
  category : [324234, 4234234],
  brand: [35345],
  tag: [2434234, 23423],
  product: [234324, 454546],
  excludedProducts: [24234234, 423423432, 4324324234],
  startDate: "",
  endDate: "",
  type: "fixed",
  amount: "50",
  
}

router.post('/', async (req, res) => {
  try {

    let { name, category, brand, tag, product, excludedProducts,
      startDate, endDate, type, amount} = req.fields;

    //basic validation
    let validator = new Validator();
    let errors = await validator.offer(req.fields);
    if(! _.isEmpty(errors)) return res.status(400).json(errors);
    
    let offer = {
      name, startDate, endDate, type, amount,
      category, brand, tag, product, excludedProducts,
    }

  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: err.message });
  }
});

module.exports = router;
