const router = require('express').Router();
const Validator =  require('../../validator/admin.js');
const _ = require('lodash')
const con = require('../../lib/db.js');
const bcrypt = require('bcrypt');


router.get('/login', async(req, res)=>{
    res.render('admin/login.html');
})


router.post('/login', async (req, res)=>{
    try{
        //validation
        let {username, password} = req.fields;
        let validator = new Validator();
        let errors = await validator.adminLogin(req.fields);
        if(!_.isEmpty(errors)) return res.status(400).json(errors);

        //get user info
        let db = await con.db();
        let user = await db.collection('admin').findOne({
            "$or": [{"phone": username}, {"email": username}]
        });
        if(!user) return res.status(400).json({username:'No user found with given email or phone'}); //no such user
       
        //compare password 
        let result = await bcrypt.compare(password, user.password);
        if(!result) return res.status(400).json({password:'Incorrect password'});

        //log in
        req.session.admin = {_id:user._id, host:req.hostOptions.domain, role: user.role};
        res.json({success:'user logged in'});
    }catch(err){
        console.log(err)
        res.status(500).json({response:'Could Not Process request'})
    }

 
})

router.get('/logout', async(req, res)=>{
    req.session.destroy();
    return res.redirect('/admin/auth/login')
})

module.exports = router;