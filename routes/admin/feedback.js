const router = require("express").Router();
const con = require("../../lib/db.js");
const jwt = require("jsonwebtoken");
const _ = require('lodash');
const feedbackFunctions = require('../../lib/dbFunctions/public/feedback.js');


router.post('/', async (req, res) => {

  try {

    let {filter = {}, resolve = {}, options = {}} = req.fields;

    let feedbackList = await feedbackFunctions.getFeedback(
      {
        customer: filter.customer,
        product: filter.product,
        _id: filter._id,
        status: filter.status
      },
      {
        product: resolve.product,
        customer: resolve.customer
      },
      options
    );

    return res.json(feedbackList);

  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: 'Sorry, something went wrong.' })
  }
})


router.post('/updateStatus', async (req, res) => {

  try {


    let {_id, status} = req.fields;

    let validStatus = ["pending", "approved", "hidden"]
    //validation
    if(!con.ObjectID.isValid(_id)) return res.status(400).json({_id: "invalid id"});
    if(!validStatus.includes(status)) return res.status(400).json({status: "invalid status"});

    let feedback = await feedbackFunctions.updateFeedbackStatus({_id, status});

    return res.json(feedback);

  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: 'Sorry, something went wrong.' })
  }
})

router.post('/delete', async (req, res) => {

  try {


    let {_id} = req.fields;
    if(!con.ObjectID.isValid(_id)) return res.status(400).json({_id: "invalid id"});

    let feedback = await feedbackFunctions.deleteFeedback({_id});

    return res.json({success: "feedback deleted"});

  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: 'Sorry, something went wrong.' })
  }
})

module.exports = router;