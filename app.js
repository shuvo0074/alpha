const express = require("express");
const app = express();

const path = require("path");
const session = require("./lib/session.js");

const logger = require("morgan");
const ejs = require("ejs");
const cors = require("cors");
const formidable = require("express-formidable");
const router = require("./router.js");
const customStatic = require("./lib/customStatic.js");
const hostHandler = require("./lib/hostHandler.js");
const compression = require("compression");

const helmet = require("helmet");

const passport = require("passport");

//convert fs functions to async
const fs = require("fs");
const util = require("util");

//TODO: remove these && enforce fs.promises in next major rebuild
fs.readFile = util.promisify(fs.readFile);
fs.writeFile = util.promisify(fs.writeFile);
fs.unlink = util.promisify(fs.unlink);
fs.mkdir = util.promisify(fs.mkdir);
fs.readdir = util.promisify(fs.readdir);

app.use(helmet());

//authentication
app.use(passport.initialize());
require("./config/authConfig")(passport);


//log reqs that dont contain . supposed to filter out static files req
// *week logic, but okay 
function pureLog(req, res, next) {
  if (req.path.includes(".")) next();
  else return logger("dev")(req, res, next);
}


app.use(compression()); //for compressing responses

//for logging requests
app.use(pureLog);

//view engine setup
app.engine(".html", ejs.renderFile);
app.set("view engine", "html");
app.set("views", path.join(__dirname, "views")); //admin view

//cors
app.use(cors({ origin: true, credentials: true }));

app.use(express.static(path.join(__dirname, "public"))); //admin static files

// session
app.use(session);

//parsers
app.use(formidable({ multiples: true }));
// app.use(cookieParser());


//domain based options
app.use(hostHandler.setHostOptions); //set db and req.hostOptions
app.use(hostHandler.setHostDirectories); //host options for public, asset and theme

//statics
app.use(customStatic.serveStatic); //public assets files
app.use(customStatic.asset.serveStatic); //theme assets files

//router
app.use(router);

//system
// app.use("/system", systemRouter);

//404
app.get("*", (req, res) => {
  res.status(404).end(`
  <h1 align='center'>404 : Page not found: ${req.url}</h1>
  `);
});

module.exports = app;