let con = require('../lib/db.js');
let _ = require('lodash');
let mail = require('./../lib/mail.js');

class Validator {
	constructor() {
		this.errors = {}
	}
	//admin register form
	async adminRegister(flds) {
		let { name, phone, email, password, password2 } = flds;
		if (!name) this.errors.name = 'Fill up name';
		if (name && name.length < 5) this.errors.name = 'Name must be at least 5 characters long'
		if (name && name.length > 40) this.errors.name = 'Name too long'
		if (!phone) this.errors.phone = 'Input phone number'
		if (phone && phone.length < 11 || phone && phone.length > 14) this.errors.phone = 'Phone number should have exact 11 characters'
		if (!password) this.errors.password = 'Input password'
		if (!password2) this.errors.password2 = 'Repeat Password'
		if (password && password2 && password != password2) this.errors.password2 = 'Repeat password does not match';
		if (email && email.length > 30) this.errors.email = 'Email too long'
		if (email && (!email.includes('@') || !email.includes('.'))) this.errors.email = 'Invalid Email';

		return this.errors;
	}
	async adminLogin(flds) {
		let { username, password } = flds;
		if (!username) this.errors.username = 'Input your phone number or email'
		if (!password) this.errors.password = 'Input you password'

		return this.errors;
	}

	async addPostCategory(flds) {
		let {
			name, description, cover, 
			image, parentCategory, url
		} = flds;


		if (!name) this.errors.name = 'Product name can not be empty';
		// if(!description) this.errors.description = "Description can not be empty";
		// if(!url) this.errors.url = "URL can not be empty";

		if(parentCategory && !con.ObjectID.isValid(parentCategory)) this.errors.parentCategory = "Invalid parent category"

		return this.errors;
	}

	async addPostTags (flds) {
		let {name, description, cover, image, url} = flds;

		if (!name) this.errors.name = 'Product name can not be empty';
		// if(!description) this.errors.description = "Description can not be empty";
		// if(!url) this.errors.url = "URL can not be empty";

		return this.errors;
	}

	async addPost(flds, list = {}) {
		let {
			name, body, url, category, 
			tags, cover, image, primaryCategory,
			servingSize, preparationTime, cookingTime, requiredProducts
		} = flds;

		if (!name) this.errors.name = 'Product name can not be empty';
		// if(!body) this.errors.body = "Body can not be empty";
		// if(!url) this.errors.url = "URL can not be empty";
		// if(!serving) this.errors.serving = "Serving status can not be empty";
		// if(!prepTime) this.errors.prepTime = "Preparation time can not be empty";
		// if(!cookingTime) this.errors.cookingTime = "Cooking time can not be empty";
		

		if (!category) this.errors.category = 'Select Category';
		if (category && !Array.isArray(category)) this.errors.category = 'Invalid Category Type';
		if (category && Array.isArray(category) && !category[0]) this.errors.category = 'Select at least one category';
		if (category && Array.isArray(category)) {
			for (let x of category) {
				if (x && !list.category.includes(x)) this.errors.category = 'Invalid Category'
			}
		}

		// if (primaryCategory && !list.category.includes(primaryCategory))
		// 	this.errors.primaryCategory = "Invalid primary category";

		// if (!tags) this.errors.tags = 'Select tags';
		// if (tags && Array.isArray(tags) && !tags[0]) this.errors.tags = 'Select at least one tags';
		if (tags && !Array.isArray(tags)) this.errors.tags = 'Invalid tags Type';
		if (tags && Array.isArray(tags)) {
			for (let x of tags) {
				if (x && !list.tags.includes(x)) this.errors.tags = 'Invalid tags'
			}
		}

		// if (!requiredProducts) this.errors.requiredProducts = 'Select required Products';
		if (requiredProducts && !Array.isArray(requiredProducts)) this.errors.requiredProducts = 'Invalid required Products Type';
		// if (requiredProducts && Array.isArray(requiredProducts) && !requiredProducts[0]) this.errors.requiredProducts = 'Select at least one required product';
		if (requiredProducts && Array.isArray(requiredProducts)) {
			for (let x of requiredProducts) {
				if (x && !list.products.includes(x._id)) this.errors.requiredProducts = 'Invalid required Products'
			}
		} 


		return this.errors;

	}

	async addProduct(flds, list = {}) {
		let { name, model, unit, description,
			image, cover, url,
			brand, category, primaryCategory, tags,
			pricing, metaTitle, metaTags, metaDescription } = flds;

		if (!name) this.errors.name = 'Product name can not be empty';

		if (brand && !list.brand.includes(brand)) this.errors.brand = 'Invalid Brand';

		if (!category) this.errors.category = 'Select Category';
		if (category && !Array.isArray(category)) this.errors.category = 'Invalid Category Type';
		if (category && Array.isArray(category) && !category[0]) this.errors.category = 'Select at least one category';
		if (category && Array.isArray(category)) {
			for (let x of category) {
				if (x && !list.category.includes(x)) this.errors.category = 'Invalid Category'
			} 
		}

		if (primaryCategory && !list.category.includes(primaryCategory))
			this.errors.primaryCategory = "Invalid primary category";

		if (tags && !Array.isArray(tags)) this.errors.tags = 'Invalid tags Type';
		if (tags && Array.isArray(tags)) {
			for (let x of tags) {
				if ( x && !list.tags.includes(x)) this.errors.tags = 'Invalid Tag'
			}
		}


		if (!pricing || !pricing[0]) this.errors.pricing = 'Add product pricing';
		if (!Array.isArray(pricing)) this.errors.pricing = 'Invalid pricing type';

		if (pricing && Array.isArray(pricing)) {
			for (let x of pricing) {
				if (!x) this.errors.pricing = "Invalid pricing";
				if (x && !x.price || isFinite(+x.price)) this.errors.pricing = "Invalid Price";
			}
		}

		//TODO:ADD MORE VALIDATION 

		return this.errors;
	}
	async addCategory(flds, list = {}) {

		let { name, parent } = flds;
		if (!name) this.errors.name = 'Name can not be empty';
		if (parent && !list.category.includes(parent)) this.errors.parent = 'Select a valid parent category';
		return this.errors;
	}

	async addBrand(flds) {
		let { name } = flds;
		if (!name) this.errors.name = 'Empty Name';
		return this.errors;
	}

	async addAttributes(flds) {
		let { name } = flds;
		if (!name) this.errors.name = 'Empty Name';
		if (name && name.length < 3) this.errors.name = 'Name should have at least 3 characters'
		if (name && name.length > 30) this.errors.name = 'Name too long'
		return this.errors;
	}

	async addTag(flds) {
		let { name } = flds;
		if (!name) this.errors.name = 'Empty Name';
		if (name && name.length < 2) this.errors.name = 'Name should have at least 2 characters'
		if (name && name.length > 30) this.errors.name = 'Name too long'
		return this.errors;
	}


	async sliderUpload(flds) {

		let { target, caption, heading, text, position } = flds;

		if (!target) this.errors.target = 'Add Link to target Page'
		if (caption && !heading) this.errors.heading = 'Specify Caption Heading'
		if (position && !isFinite(position)) this.errors.position = 'Position Must be a valid number'
		return this.errors;
	}

	async siteSettings(flds) {

		let { title } = flds;

		if (!title) this.errors.title = 'Site title can not be empty';

		return this.errors;
	}
	async logoSettings(flds) {

		let { logo, icon } = flds;


		if (logo && !con.ObjectID.isValid(logo)) this.errors.logo = 'Invalid Site Logo, select from library';
		if (icon && !con.ObjectID.isValid(icon)) this.errors.icon = 'Invalid Site Icon, select from library';
		return this.errors;
	}
	async siteSettings(flds) {

		let { title, logo, icon } = flds;

		if (!title) this.errors.title = 'Site title can not be empty';
		if (logo && !con.ObjectID.isValid(logo)) this.errors.logo = 'Invalid Site Logo, select from library';
		if (icon && !con.ObjectID.isValid(icon)) this.errors.icon = 'Invalid Site Icon, select from library';
		return this.errors;
	}
	async addDeliveryRegion(flds) {

		let { name, pickUpLocation, country, time, charge } = flds;
		if (!name) this.errors.name = 'Add name';
		if (!country) this.errors.country = 'Select country';
		if (!pickUpLocation) this.errors.pickUpLocation = 'add location or address';
		if (!time) this.errors.time = 'add delivery time';
		if (!charge || _.isEmpty(charge)) this.errors.charge = 'add delivery charge';
		return this.errors;
	}
	async siteSettings(flds) {

		let { title, adminEmail, logo, icon } = flds;

		if (!title) this.errors.title = 'Site title can not be empty';
		if (!adminEmail) this.errors.adminEmail = 'admin email can not be empty';
		if (logo && !con.ObjectID.isValid(logo)) this.errors.logo = 'Invalid Site Logo, select from library';
		if (icon && !con.ObjectID.isValid(icon)) this.errors.icon = 'Invalid Site Icon, select from library';
		return this.errors;
	}
	async invoiceSettings(flds) {
		let { invoiceTitle } = flds;
		if (!invoiceTitle) this.errors.invoiceTitle = 'Add title in invoice';
		return this.errors;
	}

	async emailSettings(flds) {

		let { fromEmail, fromName, host, encryption, port, authentication, smtpUsername, smtpPassword } = flds;

		if (!fromEmail) this.errors.fromEmail = 'Add from email';
		if (!fromName) this.errors.fromName = 'Add from name';
		if (!host) this.errors.host = 'add SMTP host';
		if (!encryption) this.errors.encryption = 'select encryption type';
		if (encryption && !['SSL', 'TLS'].includes(encryption)) this.errors.encryption = 'invalid encryption type';
		if (!port) this.errors.port = 'add SMTP port';
		if (port && !isFinite(+port)) this.errors.port = 'invalid SMTP port';

		if (authentication) {
			if (!smtpUsername) this.errors.smtpUsername = 'add SMTP Username';
			if (!smtpPassword) this.errors.smtpPassword = 'add SMTP password';
		}

		return this.errors;
	}

	async sendEmail(flds) {
		let { recipient, subject, text, cc, bcc } = flds;
		if (!recipient) this.errors.receiver = 'Add email recipients';

		//split comma separated list for validation
		if (!Array.isArray(recipient)) recipient = recipient.split(',');

		cc = cc ? cc.split(',') : '';
		bcc = bcc ? bcc.split(',') : '';

		//validate emails
		for (let _mail of recipient) {
			if (!mail.validateEmail(_mail)) this.errors.recipient = 'Invalid email address';
		}
		for (let _mail of cc) {
			if (!mail.validateEmail(_mail)) this.errors.cc = 'Invalid email address';
		}
		for (let _mail of bcc) {
			if (!mail.validateEmail(_mail)) this.errors.bcc = 'Invalid email address';
		}

		return this.errors;
	}


	async emailTemplate(flds) {
		let { event, user, admin } = flds;

		let validEvents = ['newCustomer', 'order', 'orderStatus', 'newDealer'];

		if (!event || !validEvents.includes(event)) this.errors.event = 'Invalid event';

		// if(!customerSubject) this.errors.customerSubject = 'Customer Email Subject can not be empty';
		// if(!adminSubject) this.errors.adminSubject = 'Admin Email Subject can not be empty';

		// if ((new TextEncoder().encode(user.body)).length > 25 * 1024 * 1024) { //limit to 25MB
		// 	this.errors.userBody = 'message too long, maximum limit is 25 MB including image'
		// }
		// if ((new TextEncoder().encode(admin.body)).length > 25 * 1024 * 1024) { //limit to 25MB
		// 	this.errors.adminBody = 'message too long, maximum limit is 25 MB including image'
		// }

		return this.errors;
	}

	async SMSTemplate(flds) {
		let { event, user, admin } = flds;

		let validEvents = ['newCustomer', 'order', 'orderStatus', 'newDealer'];

		if (!event || !validEvents.includes(event)) this.errors.event = 'Invalid event';

		// if(!customerSubject) this.errors.customerSubject = 'Customer Email Subject can not be empty';
		// if(!adminSubject) this.errors.adminSubject = 'Admin Email Subject can not be empty';

		// if ((new TextEncoder().encode(user.body)).length > 25 * 1024 * 1024) { //limit to 25MB
		// 	this.errors.userBody = 'message too long, maximum limit is 25 MB including image'
		// }
		// if ((new TextEncoder().encode(admin.body)).length > 25 * 1024 * 1024) { //limit to 25MB
		// 	this.errors.adminBody = 'message too long, maximum limit is 25 MB including image'
		// }

		return this.errors;
	}

	async sendSms(flds) {
		let { recipient, text, } = flds;
		if (!recipient) this.errors.receiver = 'Add sms recipients';

		//split comma separated list for validation
		if (!Array.isArray(recipient)) recipient = recipient.split(',');

		//validate emails
		for (let number of recipient) {
			if (!isFinite(+(number.replace('+', '')))) this.errors.recipient = 'Invalid phone number';
		}
		return this.errors;
	}

	async smsSettings(flds) {

		let {
			parameters, authentication,
			protocol, method, url, contentType
		} = flds;

		if (!url) this.errors.url = "Url can not be empty"
		if (!protocol) this.errors.protocol = "select protocol"
		if (!["HTTP", "HTTPS"].includes(protocol)) this.errors.protocol = "Select valid protocol"
		if (!method) this.errors.method = "Select request method";
		if (!["GET", "POST"].includes(method)) this.errors.method = "Only GET & POST method is supported now"

		if (!parameters) this.errors.parameters = "Insert request parameters";
		if (parameters) {
			if (!parameters.recipient || !parameters.recipient.key) this.errors.recipient = "Set recipient parameter key"
			if (!parameters.text || !parameters.text.key) this.errors.text = "Set text parameter key"
			if (!parameters.mask || !parameters.mask.key) this.errors.mask = "Set mask parameter key"
			if (!parameters.mask || !parameters.mask.value) this.errors.mask = "Set mask parameter value"

			if (authentication && !parameters.authentication) this.errors.authentication = "Insert request parameters.authentication";
			if (authentication && parameters.authentication) {
				if (!parameters.authentication.username || !parameters.authentication.username.key) this.errors.username = "Set username parameter key"
				if (!parameters.authentication.username || !parameters.authentication.username.value) this.errors.username = "Set username parameter value"
				if (!parameters.authentication.password || !parameters.authentication.password.key) this.errors.password = "Set password parameter key"
				if (!parameters.authentication.password || !parameters.authentication.password.value) this.errors.password = "Set password parameter value"

			}

		}

		return this.errors;
	}
	async offer(flds) {
		let { name, category, brand, tag, product, excludedProducts,
			startDate, endDate, type, amount } = flds;

		if (!name) this.errors.name = "Name can not be empty";
		if (!type) this.errors.type = "Specify offer type";
		if (!amount) this.errors.amount = "Enter offer amount";
		if (!priority) this.errors.priority = "Set offer priority";

		if (category) {
			if (!Array.isArray(category)) this.errors.category = "Category type invalid";
			else {
				for (item of category) {
					if (!con.ObjectID.isValid(item)) this.errors.category = "Invalid Category id";
				}
			}
		}

		if (brand) {
			if (!Array.isArray(brand)) this.errors.brand = "brand type invalid";
			else {
				for (item of brand) {
					if (!con.ObjectID.isValid(item)) this.errors.brand = "Invalid brand id";
				}
			}
		}

		if (tag) {
			if (!Array.isArray(tag)) this.errors.tag = "tag type invalid";
			else {
				for (item of tag) {
					if (!con.ObjectID.isValid(item)) this.errors.tag = "Invalid tag id";
				}
			}
		}

		if (product) {
			if (!Array.isArray(product)) this.errors.product = "product type invalid";
			else {
				for (item of product) {
					if (!con.ObjectID.isValid(item)) this.errors.product = "Invalid product id";
				}
			}
		}

		if (excludedProducts) {
			if (!Array.isArray(product)) this.errors.excludedProducts = "excluded product type invalid";
			else {
				for (item of excludedProducts) {
					if (!con.ObjectID.isValid(item)) this.errors.excludedProducts = "Invalid excluded product id";
				}
			}
		}

		return this.errors;
	}

	async coupon(flds) {
		let { name, code,
			minimumOrder, maximumOrder, orderedProducts,
			startDate, endDate,
			amountType, amount, freeDelivery, freeProducts } = flds;

		//TODO: max use

		if (!name) this.errors.name = "Name can not be empty";
		if (!code) this.errors.name = "Code can not be empty";

		if (minimumOrder && maximumOrder && minimumOrder > maximumOrder)
			this.errors.minimumOrder = "Minimum can not be grater than maximum order";

		if (orderedProducts) {
			if (!Array.isArray(orderedProducts)) this.errors.orderedProducts = "Invalid formation";
			else {
				for (let item in orderedProducts) {
					if (!con.ObjectID.isValid(item._id)) this.errors.orderedProducts = "invalid product id";
					if (!isFinite(+item.quantity)) this.errors.orderedProducts = "invalid product quantity"
				}
			}
		}

		if (amount) {
			if (!amountType) this.errors.amountType = 'specify discount amount type';
			if (!['fixed', 'percentage'].includes(amountType)) this.errors.amountType = 'discount amount type can be fixed of percentage';
			if (!isFinite(+amount)) this.errors.amount = "invalid discount amount";
		}

		if (freeProducts) {
			if (!Array.isArray(freeProducts)) this.errors.freeProducts = "Invalid formation";
			else {
				for (item in freeProducts) {
					if (!con.ObjectID.isValid(item._id)) this.errors.freeProducts = "invalid product id";
					if (!con.ObjectID.isValid(item.variation)) this.errors.freeProducts = "invalid product variation";
					if (!isFinite(+item.quantity)) this.errors.freeProducts = "invalid product quantity"
				}
			}
		}



		return this.errors;
	}


	async bundle(flds) {
		let { name, products, category, brand, tag, price, startDate, endDate } = flds;

		if (!name) this.errors.name = "Name can not be empty";
		if (!price) this.errors.price = "Add bundle price";

		if (!products) this.errors.products = "select bundle products";

		if (products) {
			if (!Array.isArray(products)) this.errors.products = "Invalid formation";
			else {
				for (let item of products) {
					if (!con.ObjectID.isValid(item._id)) this.errors.products = "invalid product id";
					if (!con.ObjectID.isValid(item.variation)) this.errors.products = "invalid product variation";
					if (!isFinite(+item.quantity)) this.errors.products = "invalid product quantity";
				}
			}
		}

		if (category) {
			if (!Array.isArray(category)) this.errors.category = "Invalid formation";
			else {
				for (let item of category) {
					if (item && !con.ObjectID.isValid(item)) this.errors.category = "invalid category id";
				}
			}
		}
		if (brand) {
			if (brand && !con.ObjectID.isValid(brand)) this.errors.brand = "invalid brand id";	
		}
		if (tag) {
			if (!Array.isArray(tag)) this.errors.tag = "Invalid formation";
			else {
				for (let item of tag) {
					if (item && !con.ObjectID.isValid(item)) this.errors.tag = "invalid tag id";
				}
			}
		}

		return this.errors;
	}

	async staffRegister(flds) {
		let {
			firstName, lastName,
			address,
			salary, designation,
			fatherName, motherName,
			NID,
			phone, email, additionalInfo, password
		} = flds;

		if (!firstName && !lastName) this.errors.firstName = "Enter name";
		if (!phone) this.errors.phone = "Enter phone";

		if (!salary || !isFinite(+salary)) this.errors.salary = "Enter salary";
		if (!designation) this.errors.designation = "Enter designation";
		return this.errors;
	}

	async staffUpdate(flds) {
		let {
			firstName, lastName,
			address,
			salary, designation,
			fatherName, motherName,
			NID,
			phone, email, additionalInfo
		} = flds;

		if (!firstName && !lastName) this.errors.firstName = "Enter name";
		if (!phone) this.errors.phone = "Enter phone";

		if (!salary || !isFinite(+salary)) this.errors.salary = "Enter salary";
		if (!designation) this.errors.designation = "Enter designation";
		return this.errors;
	}

	async expenseAdd(flds) {
		let {
			topic, amount, date, staff
		} = flds;

		if (!topic ) this.errors.firstName = "Enter topic";
		if (!amount || !isFinite(+amount)) this.errors.amount = "Enter amount";

		return this.errors;
	}

	async dealerRegister(flds) {
		let { firstName, lastName, address, zipCode, phone, email, additionalInfo,  password,  code} = flds;
	
		if (!firstName && !lastName) this.errors.firstName = "Enter name";
		if (!phone) this.errors.phone = "Enter phone";
		
		if(!code) this.errors.code = "enter dealer code"
		

		if (!password) this.errors.password = "Enter password";
		return this.errors;
	  }

	async dealerUpdate(flds) {
		let { firstName, lastName, address, zipCode, phone, email, additionalInfo, code} = flds;
	
		if (!firstName && !lastName) this.errors.firstName = "Enter name";
		if (!phone) this.errors.phone = "Enter phone";

		if(!code) this.errors.code = "enter dealer code"

	
		return this.errors;
	  }
}

module.exports = Validator; 