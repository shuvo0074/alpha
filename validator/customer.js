const geo = require("./../lib/dbFunctions/public/geoFunctions.js");
const _ = require("lodash");
const con = require("./../lib/db.js");

class Validator {
  constructor() {
    this.errors = {};
  }
  //customer register form
  async customerRegister(flds) {
    let {
      firstName,
      lastName,
      country,
      city,
      address1,
      address2,
      zipCode,
      phone,
      email,
      additionalInfo,
      password,
      gender,
    } = flds;

    if (!firstName && !lastName) this.errors.firstName = "Enter name";
    if (!country) this.errors.country = "Enter country";
    if (!city) this.errors.city = "Enter city";
    if (!phone) this.errors.phone = "Enter phone";
    let countryData = await geo.getCountryByName(country);

    if (country && !countryData)
      this.errors.country = "Invalid country selection";
    if (country && countryData) {
      // let validCity = await geo.getCityByName(city);
      // if (!validCity) this.errors.city = "Invalid city";
    }
    if (!address1 && !address2) this.errors.address1 = "Enter address";
    if (!password) this.errors.password = "Enter password";
    return this.errors;
  }
  async customerUpdate(flds) {
    let {
      firstName,
      lastName,
      country,
      city,
      address1,
      address2,
      zipCode,
      phone,
      email,
      additionalInfo,
    } = flds;

    if (!firstName && !lastName) this.errors.firstName = "Enter name";
    if (!country) this.errors.country = "Enter country";
    if (!city) this.errors.city = "Enter city";
    if (!phone) this.errors.phone = "Enter phone";
    let countryData = await geo.getCountryByName(country);
    // if (country && !countryData)
    //   this.errors.country = "Invalid country selection";
    if (country && countryData) {
      // let validCity = await geo.getCityByName(city);
      // if (!validCity) this.errors.city = "Invalid city";
    }
    if (!address1 && !address2) this.errors.address1 = "Enter address";
    return this.errors;
  }

  async customerLogin(flds) {
    let { username, password } = flds;
    if (!username) this.errors.username = "Input your phone number or email";
    if (!password) this.errors.password = "Input you password";

    return this.errors;
  }

  async customerChangePassword(flds) {
    let { password, newPassword, newPassword2 } = flds;
    if (!password) this.errors.password = "current password required";
    if (!newPassword) this.errors.newPassword = "New password required";
    if (!newPassword2) this.errors.newPassword2 = "Confirm new password";
    if (newPassword && newPassword2 && newPassword != newPassword2)
      this.errors.newPassword2 = "Passwords do not match";
    return this.errors;
  }

  async addOrder(flds) {
    let { products, address } = flds;
    if (!products) this.errors.products = "Select Products";
    if (products && !Array.isArray(products))
      this.errors.products = "Invalid products type";
    if (products && !products[0]) this.errors.products = "Products Empty";
    if (products && Array.isArray(products)) {
      for (let x of products) {
        if (!x.id) this.errors.products = "Empty Product Id";
        if (x.id && (!x.quantity || x.quantity < 1))
          this.errors.products = "Select at least 1 as Quantity for" + x.id;
        if (x.id && x.quanity && !isFinite(+quantity))
          this.errors.products = "Select valid quantity" + x.id;
      }
    }
    if (!address) this.errors.address = "Input Shipping Address";
    //!add more adress validation
    if (address && address.length < 10)
      this.errors.address = "Address too short";
    if (address && address.length > 255)
      this.errors.address = " Address too long";

    return this.errors;
  }

  async checkoutBilling(flds) {
    if (!flds.billingAddress) {
      this.errors.billingAddress = "enter billing address";
    } else {
      let {
        firstName,
        lastName,
        country,
        city,
        address1,
        address2,
        zipCode,
        phone,
        email,
        additionalInfo,
      } = flds.billingAddress;

      if (!firstName) this.errors.firstName = "Enter your first name";
      if (!lastName) this.errors.lastName = "Enter your last name";
      if (!country) this.errors.country = "Enter your country";
      if (!city) this.errors.city = "Enter your city";
      if (!phone) this.errors.phone = "Enter your phone";
      let countryData = await geo.getCountryByName(country);
      // if (country && !countryData)
      //   this.errors.country = "Invalid country selection";
      if (country && countryData) {
        // let validCity = await geo.getCityByName(city);
        // if (!validCity) this.errors.city = "Invalid city";
      }
      if (!address1 && !address2) this.errors.address1 = "Enter address";
    }

    return this.errors;
  }

  async checkoutShipping(flds) {
    if (!flds.shippingAddress) {
      this.errors.shippingAddress = "enter shipping address";
    } else {
      let {
        firstName,
        lastName,
        country,
        city,
        address1,
        address2,
        zipCode,
        phone,
        email,
        additionalInfo,
      } = flds.shippingAddress;

      if (!firstName) this.errors.firstName = "Enter your first name";
      if (!lastName) this.errors.lastName = "Enter your last name";
      if (!country) this.errors.country = "Enter your country";
      if (!city) this.errors.city = "Enter your city";
      if (!phone) this.errors.phone = "Enter your phone";
      let countryData = await geo.getCountryByName(country);
      // if (country && !countryData)
      //   this.errors.country = "Invalid country selection";
      if (country && countryData) {
        // let validCity = await geo.getCityByName(city);
        // if (!validCity) this.errors.city = "Invalid city";
      }
      if (!address1 && !address2) this.errors.address1 = "Enter address";
    }

    return this.errors;
  }

  async customerAddress(flds) {
    if (_.isEmpty(flds)) {
      this.errors.address = "Enter address";
    } else {
      let {
        title,
        firstName,
        lastName,
        country,
        city,
        address1,
        address2,
        zipCode,
        phone,
        email,
      } = flds;

      // empty check
      if (!title) this.errors.title = "Enter address title";
      if (!firstName) this.errors.firstName = "Enter first name";
      if (!lastName) this.errors.lastName = "Enter last name";
      if (!country) this.errors.country = "Enter country name";
      if (!city) this.errors.city = "Enter city name";
      if (!address1) this.errors.address1 = "Enter first address";
      if (!address2) this.errors.address2 = "Enter second address";

      if (!zipCode) this.errors.zipCode = "Enter zip code";
      if (!phone) this.errors.phone = "Enter phone number";
      if (!email) this.errors.email = "Enter email";

      // validity check
      let isCountry = await geo.getCountryByName(country);
      if (country && !isCountry)
        this.errors.country = "Invalid country selection ";

      if (country && isCountry) {
        // let isCity = await geo.getCityByName(city);
        // if (!isCity) this.errors.city = "Invalid city selection";
      }

      // type check
      // ** country, city at this stage should have valid type
      if (title && !_.isString(title))
        this.errors.title = "Invalid address title type";
      if (firstName && !_.isString(firstName))
        this.errors.firstName = "Invalid first name type";
      if (lastName && !_.isString(lastName))
        this.errors.lastName = "Invalid last name type";
      if (address1 && !_.isString(address1))
        this.errors.address1 = "Invalid first address type";
      if (address2 && !_.isString(address2))
        this.errors.address2 = "Invalid  second address type";
      if (zipCode && !_.isNumber(zipCode))
        this.errors.zipCode = "Invalid zip code type";

      // phone number may have country code, + sign
      if (phone && !_.isString(phone)) this.errors.phone = "Invalid phone type";

      // need to add some more advanced custom regex
      let isEmail = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;

      if (email && !_.isString(email)) this.errors.email = "Invalid email type";
      else if (!isEmail.test(email)) this.errors.email = "Not an email";
    }

    return this.errors;
  }

  async addFeedback(flds) {
    let { title, message, product } = flds;

    // empty check
    if (!title) this.errors.title = "Enter feedback title";
    if (!message) this.errors.firstName = "Enter feedback message";
    if (product) {
      if (!con.ObjectID.isValid(product))
        this.errors.product = "Invalid product id";

      let db = await con.db();
      let productInfo = await db
        .collection("product")
        .findOne({ _id: new con.ObjectID(product) });
      if (!productInfo) this.errors.product = "Invalid product";
    }

    return this.errors;
  }
}

module.exports = Validator;
