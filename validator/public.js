const _ = require("lodash");
const con = require('./../lib/db.js');

class Validator {
  constructor() {
    this.errors = {};
  }
  //custom product type validation
  async validateCustomProductType(flds) {
      // color is optional
    
    let {type, size, color, frontImage, backImage } = flds;
    // validate type
    // valid types: "Man", "Woman", "Kids", "Full sleeve", "Sweater"
    if(!type) this.errors.type = "Enter product type";
    if(type && !_.isString(type)) this.errors.type = "Invalid product type";

    // validate front image
    if(!frontImage) this.errors.frontImage = "Please select a front image";
    if(frontImage && !con.ObjectID.isValid(frontImage)) this.errors.frontImage = "Invalid front image";

    // validate back image
    if(!backImage) this.errors.backImage = "Please select a back image";
    if(backImage && !con.ObjectID.isValid(backImage)) this.errors.backImage = "Invalid back image";

    // validate size
    // custom product must have at least one size
    if(!size) this.errors.size = "Enter at least one size";
    if(size && !_.isArray(size)) this.errors.size = "Invalid size format";

    return this.errors;
  }

}

module.exports = Validator;
