const http = require('http');
const formidable = require('formidable');
const url = require('url');

http.createServer((req, res) => {
    const form = new formidable.IncomingForm();
    let data = form.parse(req, (err, flds, files) => {
        console.log({
            error: err,
            fields: flds,
            files: files,
            queryString: url.parse(req.url).query
        })
    })
}).listen(8080)